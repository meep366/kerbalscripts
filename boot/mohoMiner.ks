@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

RCS OFF.
SAS OFF.
LOCK THROTTLE TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

showTerminal().

CLEARSCREEN.
CLEARGUIS().

generalView(handleMiningOptions@, updateMiningPopup@).

RCS OFF.
SAS OFF.
LOCK THROTTLE TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

DECLARE LOCAL FUNCTION handleMiningOptions {
    DECLARE PARAMETER mainGui, option, updateGui.

    DECLARE LOCAL launchToStationOption TO "Launch to station".
    DECLARE LOCAL mineOreOption TO "Mine Ore".
    DECLARE LOCAL returnToMiningOption TO "Land at mining site".
    DECLARE LOCAL transferFuelOption TO "Transfer fuel to station".

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveMinmusMiner").

    IF (option = launchToStationOption) {
        SET TARGET TO VESSEL("Moho Station").
        DECLARE LOCAL timeToOrbit TO 150.
        DECLARE LOCAL pitchOverRate TO -.3.
        DECLARE LOCAL timeToImpactBuffer TO 1.
        DECLARE LOCAL minimumAngleToCorrect TO 1.

        launchToTarget(TARGET, timeToOrbit, pitchOverRate, timeToImpactBuffer, minimumAngleToCorrect, updateGui).

        GEAR OFF.
        retractPanelsAndAntennas().
        WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
        KUNIVERSE:QUICKSAVETO("quicksaveDocking").

        DECLARE LOCAL dryMass TO SHIP:DRYMASS. //~43.181t
        DECLARE LOCAL isp TO getShipIsp().

        setDockingTarget(TARGET).
        dockWithTarget(updateGui, 7, 10).

        DECLARE LOCAL deorbitDeltaV TO 1000.
        transferFuel(deorbitDeltaV, 100, dryMass, isp, updateGui).
    } ELSE IF (option = mineOreOption) {
        mineOre(updateGui).
    } ELSE IF (option = returnToMiningOption) {
        DECLARE LOCAL dockingPort TO SHIP:PARTSTAGGED("shipDockingPort")[0].
        undock(dockingPort).

        RCS ON.
        landCraft(LATLNG(0.12, -70.73), updateGui).
        RCS OFF.
        deployPanelsAndAntennas().
    } ELSE {
        headsUpText("Invalid option").
    }
}

DECLARE LOCAL FUNCTION updateMiningPopup {
    DECLARE PARAMETER launchPopup.
    DECLARE LOCAL landedStatus TO "LANDED".
    DECLARE LOCAL orbitStatus TO "ORBITING".
    DECLARE LOCAL suborbitalStatus TO "SUB_ORBITAL".
    DECLARE LOCAL dockedStatus TO "DOCKED".

    DECLARE LOCAL launchToStationOption TO "Launch to station".
    DECLARE LOCAL mineOreOption TO "Mine Ore".
    DECLARE LOCAL returnToMiningOption TO "Land at mining site".

    launchPopup:CLEAR().
    IF (SHIP:STATUS = landedStatus) {
        launchPopup:ADDOPTION(launchToStationOption).
        launchPopup:ADDOPTION(mineOreOption).
    } ELSE IF (SHIP:STATUS = orbitStatus OR SHIP:STATUS = dockedStatus OR SHIP:STATUS = suborbitalStatus) {
        launchPopup:ADDOPTION(returnToMiningOption).
    } ELSE {
        launchPopup:ADDOPTION("No options").
        SET launchPopup:ENABLED TO FALSE.
    }
}