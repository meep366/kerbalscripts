@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
    RUNONCEPATH("library/utilityLibrary.ks").
    copyAndRunFiles().
}
showTerminal().

CLEARSCREEN.
CLEARGUIS().

DECLARE LOCAL preLaunchStatus TO "PRELAUNCH".
IF (SHIP:STATUS = preLaunchStatus) {
    generalView(handleRefuelingOptions@, updateRefuelingPopup@).
} ELSE {
    generalView().
}

RCS OFF.
SAS OFF.
LOCK THROTTLE TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

DECLARE LOCAL FUNCTION handleRefuelingOptions {
    DECLARE PARAMETER mainGui, option, updateGui.

    DECLARE LOCAL launchToKerbinStationOption TO "Launch to Kerbin Station".

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveKerbinRefueler").

    IF (option = launchToKerbinStationOption) {
        SET TARGET TO VESSEL("Kerbin Station").

        DECLARE LOCAL toOrbitTime TO 90.
        DECLARE LOCAL pitchRate TO -2/25.
        DECLARE LOCAL timeToImpactBuffer TO 1.
        DECLARE LOCAL minimumAngleToCorrect TO 1.
        DECLARE LOCAL dryMass TO SHIP:DRYMASS.
        DECLARE LOCAL isp TO getShipIsp().
        DECLARE LOCAL dockingPort TO getShipDockingPort().

        //Get rid of fairing when out of atmo
        WHEN (ALTITUDE > 65000) THEN {
            STAGE.
        }
        launchToTarget(TARGET, toOrbitTime, pitchRate, timeToImpactBuffer, minimumAngleToCorrect, updateGui).
        DECLARE LOCAL targetSet TO setDockingTarget(TARGET).
        IF (targetSet) {
            dockWithTarget(updateGui).

            DECLARE LOCAL deorbitDeltaV TO 65.
            transferFuel(deorbitDeltaV, 0, dryMass, isp, updateGui).
            undock(dockingPort).

            DECLARE LOCAL targetPeriapsis TO 30000.
            deorbitCraft(targetPeriapsis, updateGui).
        }
    } ELSE {
        headsUpText("Invalid option").
    }
}

DECLARE LOCAL FUNCTION updateRefuelingPopup {
    DECLARE PARAMETER launchPopup.
    DECLARE LOCAL launchToKerbinStationOption TO "Launch to Kerbin Station".

    launchPopup:CLEAR().
    IF (SHIP:STATUS = preLaunchStatus) {
        launchPopup:ADDOPTION(launchToKerbinStationOption).
    } ELSE {
        launchPopup:ADDOPTION("No options").
        SET launchPopup:ENABLED TO FALSE.
    }
}