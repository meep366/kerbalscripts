@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

showTerminal().
DECLARE LOCAL pitchOnTouchdown TO 1.
DECLARE LOCAL maxAngleOfAttack TO 20.
DECLARE LOCAL maxBankDegrees TO 40.
DECLARE LOCAL maxPitchDegrees TO 30.
DECLARE LOCAL minimumSpeed TO 80.
DECLARE LOCAL altitudeToCutThrottle TO 20.
DECLARE LOCAL isSpacefaring TO TRUE.
DECLARE LOCAL elevatorPID TO PIDLOOP(0.06, 0.0333, 0.045).
DECLARE LOCAL verticalSpeedPID TO PIDLOOP(0.06, 0.02, 0.045).
DECLARE LOCAL pitchAnglePID TO PIDLOOP(0.3, 0.075, 0.5).
DECLARE LOCAL aileronPID TO PIDLOOP(0.01, 0.05, 0.05).
DECLARE LOCAL yawDamperPID TO PIDLOOP(0.07, 0.0035, 0.084).
DECLARE LOCAL bankAnglePID TO PIDLOOP(3, 0.6, 3.75).
DECLARE LOCAL throttlePID TO PIDLOOP(0.6, 0.211, 0.428).

fly(pitchOnTouchdown, maxAngleOfAttack, maxBankDegrees, maxPitchDegrees, minimumSpeed,
        altitudeToCutThrottle, isSpacefaring, elevatorPID, verticalSpeedPID, pitchAnglePID,
        aileronPID, yawDamperPID, bankAnglePID, throttlePID).
