@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

showTerminal().

DECLARE LOCAL pitchOnTouchdown TO 1.
DECLARE LOCAL maxAngleOfAttack TO 20.
DECLARE LOCAL maxBankDegrees TO 40.
DECLARE LOCAL maxPitchDegrees TO 20.
DECLARE LOCAL minimumSpeed TO 80.
DECLARE LOCAL altitudeToCutThrottle TO 10.
DECLARE LOCAL isSpacefaring TO FALSE.
DECLARE LOCAL elevatorPID TO PIDLOOP(0.03, 0.0316, 0.00713).
DECLARE LOCAL verticalSpeedPID TO PIDLOOP(0.06, 0.0273, 0.033).
DECLARE LOCAL pitchAnglePID TO PIDLOOP(0.06, 0.00857, 0.175).
DECLARE LOCAL aileronPID TO PIDLOOP(0.009, 0.0036, 0.00563).
DECLARE LOCAL yawDamperPID TO PIDLOOP(2.1, 0.0933, 2.84).
DECLARE LOCAL bankAnglePID TO PIDLOOP(7, 0.933, 3.15).
DECLARE LOCAL throttlePID TO PIDLOOP(0.7, 0.0509, 0.576).

fly(pitchOnTouchdown, maxAngleOfAttack, maxBankDegrees, maxPitchDegrees, minimumSpeed,
        altitudeToCutThrottle, isSpacefaring, elevatorPID, verticalSpeedPID, pitchAnglePID,
        aileronPID, yawDamperPID, bankAnglePID, throttlePID).

