@LAZYGLOBAL OFF.
WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/boot/generalBoot.ks", "boot/generalBoot.ks").
    COPYPATH("0:/library/utilityLibrary.ks", "library/utilityLibrary.ks").
    RUNONCEPATH("library/utilityLibrary.ks").
    copyAndRunFiles().
}
RCS OFF.
SAS OFF.
LOCK THROTTLE TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

showTerminal().

CLEARSCREEN.
CLEARGUIS().

DECLARE LOCAL preLaunchStatus TO "PRELAUNCH".
DECLARE LOCAL launchRelayOption TO "Launch Relay Sat".
IF (SHIP:STATUS = preLaunchStatus) {
    generalView(handleRelayOptions@, updateRelayPopup@).
} ELSE {
    generalView().
}

DECLARE LOCAL FUNCTION handleRelayOptions {
    DECLARE PARAMETER mainGui, option, updateGui.

    IF (option = launchRelayOption) {
        WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
        KUNIVERSE:QUICKSAVETO("quicksaveRelayLaunch").

        DECLARE LOCAL relayDetails TO getRelayPlacement(updateGui).

        IF (relayDetails["relayAltitude"] < 10e9) {
            SET SHIP:NAME TO "Sol Inner Relay " + relayDetails["relayNumber"].
        } ELSE IF (relayDetails["relayAltitude"] < 30e9) {
            SET SHIP:NAME TO "Sol Central Relay " + relayDetails["relayNumber"].
        } ELSE {
            SET SHIP:NAME TO "Sol Outer Relay " + relayDetails["relayNumber"].
        }

        //Get rid of fairing when out of thick atmo
        WHEN (ALTITUDE > 65000) THEN {
            STAGE.
        }
        launchToOrbit(updateGui).
        deployPanelsAndAntennas().

        DECLARE LOCAL targetInclination TO 0.
        DECLARE LOCAL targetLongitude TO 0.
        DECLARE LOCAL targetArgumentPeriapsis TO 0.
        DECLARE LOCAL epoch TO 0.
        DECLARE LOCAL meanAnomalyAtEpoch TO (relayDetails["relayNumber"] - 1) * (360 / relayDetails["relayCount"]).
        DECLARE LOCAL searchInterval TO 0.
        DECLARE LOCAL searchDuration TO 0.
        DECLARE LOCAL targetBody TO BODY("SUN").
        DECLARE LOCAL eccentricity TO 0.
        DECLARE LOCAL semiMajorAxis TO targetBody:RADIUS + relayDetails["relayAltitude"].
        DECLARE LOCAL targetOrbit TO CREATEORBIT(targetInclination, eccentricity, semiMajorAxis, targetLongitude, targetArgumentPeriapsis,
                meanAnomalyAtEpoch, epoch, targetBody).

        DECLARE LOCAL options TO LEXICON(
                        "create_maneuver_nodes", "both",
                        "verbose", true).
        IF (relayDetails["relaySearchDuration"] <> 0) {
            options:ADD("search_duration", relayDetails["relaySearchDuration"]).
        }
        DECLARE LOCAL result TO getSuccessfulRSVP(targetOrbit, options, updateGui).

        IF (ALLNODES:LENGTH = 2) {
            WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
            KUNIVERSE:QUICKSAVETO("quicksaveTransferEject").

            CLEARSCREEN.
            PRINT "Executing first burn".
            PRINT "Target Altitude: " + relayDetails["relayAltitude"].
            PRINT "Target Epoch Mean Anomaly: " + meanAnomalyAtEpoch.
            executeNode(NEXTNODE, updateGui).

            WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
            KUNIVERSE:QUICKSAVETO("quicksaveTransferInsert").

            CLEARSCREEN.
            PRINT "Executing second burn".
            PRINT "Target Altitude: " + relayDetails["relayAltitude"].
            PRINT "Target Epoch Mean Anomaly: " + meanAnomalyAtEpoch.
            executeNode(NEXTNODE, updateGui).

            DECLARE LOCAL result TO getSuccessfulRSVP(targetOrbit, options, updateGui).

            WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
            KUNIVERSE:QUICKSAVETO("quicksaveAdjustmentBurn").
            executeNode(NEXTNODE, updateGui).

            WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
            KUNIVERSE:QUICKSAVETO("quicksaveAdjustmentBurn").
            executeNode(NEXTNODE, updateGui).

            DECLARE LOCAL targetAccuracy TO 0.0000004.
            DECLARE LOCAL isDone TO FALSE.
            UNTIL (isDone) {
                DECLARE LOCAL apoapsisDelta TO ABS(APOAPSIS - relayDetails["relayAltitude"]).
                DECLARE LOCAL periapsisDelta TO ABS(PERIAPSIS - relayDetails["relayAltitude"]).

                DECLARE LOCAL apoapsisAccuracy TO apoapsisDelta / relayDetails["relayAltitude"].
                DECLARE LOCAL periapsisAccuracy TO periapsisDelta / relayDetails["relayAltitude"].
                IF (apoapsisAccuracy < targetAccuracy AND periapsisAccuracy < targetAccuracy) {
                    WAIT 2.
                    SET isDone TO TRUE.
                    CLEARSCREEN.
                    PRINT "Relay Placed".
                    PRINT "Apoapsis Δ:  " + ROUND(ABS(APOAPSIS - relayDetails["relayAltitude"])) + "m".
                    PRINT "Periapsis Δ: " + ROUND(periapsisDelta) + "m".
                } ELSE {
                    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
                    KUNIVERSE:QUICKSAVETO("quicksaveAdjustApsis").
                    CLEARSCREEN.

                    IF (ETA:APOAPSIS < ETA:PERIAPSIS OR apoapsisAccuracy < targetAccuracy) {
                        PRINT "Adjusting Periapsis".
                        PRINT "Current error: " + ROUND(ABS(PERIAPSIS - relayDetails["relayAltitude"])) + "m     ".
                        setFineApsis(relayDetails["relayAltitude"], APOAPSIS, ETA:APOAPSIS, updateGui).
                    } ELSE {
                        PRINT "Adjusting Apoapsis".
                        PRINT "Current error: " + ROUND(ABS(APOAPSIS - relayDetails["relayAltitude"])) + "m     ".
                        setFineApsis(relayDetails["relayAltitude"], PERIAPSIS, ETA:PERIAPSIS, updateGui).
                    }
                }
            }
        } ELSE {
            PRINT "Unsuccessful RSVP".
        }
    } ELSE {
        headsUpText("Invalid option").
    }
}

DECLARE LOCAL FUNCTION updateRelayPopup {
    DECLARE PARAMETER launchPopup.

    launchPopup:CLEAR().
    IF (SHIP:STATUS = preLaunchStatus) {
        launchPopup:ADDOPTION(launchRelayOption).
    } ELSE {
        launchPopup:ADDOPTION("No options").
        SET launchPopup:ENABLED TO FALSE.
    }
}

DECLARE LOCAL FUNCTION getRelayPlacement {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL guiRelayDetails TO GUI(300).
    SET guiRelayDetails:X TO 1570.
    SET guiRelayDetails:Y TO 350.
    DECLARE LOCAL labelLandingCoordinates IS guiRelayDetails:ADDLABEL("<size=20><b>Relay Details</b></size>").
    SET labelLandingCoordinates:STYLE:ALIGN TO "CENTER".
    SET labelLandingCoordinates:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL relayNumber TO 1.
    DECLARE LOCAL relayCount TO 3.
    DECLARE LOCAL relayAltitude TO 25e9.
    DECLARE LOCAL relaySearchDuration TO 0.

    DECLARE LOCAL relayNumberText TO createGuiText(guiRelayDetails, "Relay Number", relayNumber:TOSTRING).
    DECLARE LOCAL relayCountText TO createGuiText(guiRelayDetails, "Relay Count", relayCount:TOSTRING).
    DECLARE LOCAL relayAltitudeText TO createGuiText(guiRelayDetails, "Relay Altitude", relayAltitude:TOSTRING).
    DECLARE LOCAL relayDurationText TO createGuiText(guiRelayDetails, "RSVP Search Duration", relaySearchDuration:TOSTRING).
    DECLARE LOCAL launchButton TO guiRelayDetails:ADDBUTTON("Launch Relay").
    DECLARE LOCAL relayDetails TO LEXICON().
    DECLARE LOCAL isDone TO FALSE.

    SET launchButton:ONCLICK TO {
        SET relayNumber TO relayNumberText:TEXT:TONUMBER(relayNumber).
        SET relayCount TO relayCountText:TEXT:TONUMBER(relayCount).
        SET relayAltitude TO relayAltitudeText:TEXT:TONUMBER(relayAltitude).
        SET relaySearchDuration TO relayDurationText:TEXT:TONUMBER(relaySearchDuration).
        relayDetails:ADD("relayNumber", relayNumber).
        relayDetails:ADD("relayCount", relayCount).
        relayDetails:ADD("relayAltitude", relayAltitude).
        relayDetails:ADD("relaySearchDuration", relaySearchDuration).

        guiRelayDetails:DISPOSE().
        SET isDone TO TRUE.
    }.

    guiRelayDetails:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    RETURN relayDetails.
}
