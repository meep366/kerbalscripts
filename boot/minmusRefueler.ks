@LAZYGLOBAL OFF.

SET CONFIG:IPU TO 2000.
WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

RCS OFF.
SAS OFF.
LOCK THROTTLE TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
SET STEERINGMANAGER:MAXSTOPPINGTIME TO 10.
SET STEERINGMANAGER:PITCHPID:KD TO 1.5.
SET STEERINGMANAGER:YAWPID:KD TO 1.5.

showTerminal().

CLEARSCREEN.
CLEARGUIS().

generalView(handleRefuelingOptions@, updateRefuelingPopup@).

RCS OFF.
SAS OFF.
LOCK THROTTLE TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

DECLARE LOCAL FUNCTION handleRefuelingOptions {
    DECLARE PARAMETER mainGui, option, updateGui.

    DECLARE LOCAL launchToKerbinStationOption TO "Launch to Kerbin Station".
    DECLARE LOCAL launchToMinmusStationOption TO "Launch to Minmus Station".

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveMinmusRefueler").

    DECLARE LOCAL dockingPort TO SHIP:PARTSTAGGED("shipDockingPort")[0].
    undock(dockingPort).
    DECLARE LOCAL timeToImpactBuffer TO 1.
    DECLARE LOCAL dryMass TO SHIP:DRYMASS. //~39.666t
    DECLARE LOCAL isp TO getShipIsp().

    IF (option = launchToKerbinStationOption) {
        SET TARGET TO VESSEL("Kerbin Station").
        DECLARE LOCAL minDeltaV TO 1400.
        DECLARE LOCAL minMonoprop TO 800.
        DECLARE LOCAL minAngleToCorrect TO 1.
        DECLARE LOCAL kerbinApoapsis TO 300000.
        DECLARE LOCAL closestApproachCuttoff TO 200.
        DECLARE LOCAL doCorrections TO TRUE.

        transferToPlanetVessel(TARGET, kerbinApoapsis, timeToImpactBuffer, minAngleToCorrect, closestApproachCuttoff, doCorrections, updateGui).

        transferFuel(minDeltaV, minMonoprop, dryMass, isp, updateGui).
    } ELSE IF (option = launchToMinmusStationOption) {
        SET TARGET TO VESSEL("Minmus Station").
        DECLARE LOCAL maxDeltaV TO 6000.
        DECLARE LOCAL maxMonoprop TO 2140.
        DECLARE LOCAL minAngleToCorrect TO 3.

        transferToMoonVessel(TARGET, timeToImpactBuffer, minAngleToCorrect, updateGui).
        transferFuel(maxDeltaV, maxMonoprop, dryMass, isp, updateGui).
    } ELSE {
        headsUpText("Invalid option").
    }
}

DECLARE LOCAL FUNCTION updateRefuelingPopup {
    DECLARE PARAMETER launchPopup.
    DECLARE LOCAL orbitStatus TO "ORBITING".
    DECLARE LOCAL dockedStatus TO "DOCKED".

    DECLARE LOCAL launchToKerbinStationOption TO "Launch to Kerbin Station".
    DECLARE LOCAL launchToMinmusStationOption TO "Launch to Minmus Station".

    launchPopup:CLEAR().
    IF (SHIP:BODY = KERBIN) {
        launchPopup:ADDOPTION(launchToMinmusStationOption).
    } ELSE IF (SHIP:BODY = MINMUS) {
        launchPopup:ADDOPTION(launchToKerbinStationOption).
    } ELSE {
        launchPopup:ADDOPTION("No options").
        SET launchPopup:ENABLED TO FALSE.
    }
}
