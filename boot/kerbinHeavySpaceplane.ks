@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

showTerminal().
DECLARE LOCAL pitchOnTouchdown TO 1.
DECLARE LOCAL maxAngleOfAttack TO 20.
DECLARE LOCAL maxBankDegrees TO 30.
DECLARE LOCAL maxPitchDegrees TO 15.
DECLARE LOCAL minimumSpeed TO 80.
DECLARE LOCAL altitudeToCutThrottle TO 20.
DECLARE LOCAL isSpacefaring TO TRUE.
DECLARE LOCAL elevatorPID TO PIDLOOP(0.07, 0.0014, 0.21).
DECLARE LOCAL verticalSpeedPID TO PIDLOOP(0.042, 0.014, 0.0315).
DECLARE LOCAL pitchAnglePID TO PIDLOOP(0.70, 0.0329, 0.893).
DECLARE LOCAL aileronPID TO PIDLOOP(0.018, 0.009, 0.009).
DECLARE LOCAL yawDamperPID TO PIDLOOP(0.6, 0.2, 0.45).
DECLARE LOCAL bankAnglePID TO PIDLOOP(1.2, 0.3, 1.2).
DECLARE LOCAL throttlePID TO PIDLOOP(6.0, 1.71, 5.25).
DECLARE LOCAL minGlideslopePitchDegrees TO -15.
DECLARE LOCAL highAltAileronPID TO PIDLOOP(0.06, 0.015, 0.06).
DECLARE LOCAL highAltElevatorPID TO PIDLOOP(0.7, 0.00933, 3.15).
DECLARE LOCAL highAltYawDamperPID TO PIDLOOP(2.8, 0.14, 3.36).

fly(pitchOnTouchdown, maxAngleOfAttack, maxBankDegrees, maxPitchDegrees, minimumSpeed,
        altitudeToCutThrottle, isSpacefaring, elevatorPID, verticalSpeedPID, pitchAnglePID,
        aileronPID, yawDamperPID, bankAnglePID, throttlePID, minGlideslopePitchDegrees,
        highAltAileronPID, highAltElevatorPID, highAltYawDamperPID).
