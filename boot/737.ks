@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

showTerminal().

DECLARE LOCAL pitchOnTouchdown TO 1.
DECLARE LOCAL maxAngleOfAttack TO 20.
DECLARE LOCAL maxBankDegrees TO 40.
DECLARE LOCAL maxPitchDegrees TO 30.
DECLARE LOCAL minimumSpeed TO 80.
DECLARE LOCAL altitudeToCutThrottle TO 10.
DECLARE LOCAL isSpacefaring TO FALSE.
DECLARE LOCAL elevatorPID TO PIDLOOP(0.1, 0.0666, 0.1).
DECLARE LOCAL verticalSpeedPID TO PIDLOOP(0.18, 0.1, 0.081).
DECLARE LOCAL pitchAnglePID TO PIDLOOP(0.35, 0.00933, 1.13).
DECLARE LOCAL aileronPID TO PIDLOOP(0.00446, 0.00343, 0.00387).
DECLARE LOCAL yawDamperPID TO PIDLOOP(2.1, 0.158, 1.67).
DECLARE LOCAL bankAnglePID TO PIDLOOP(7, 0.933, 3.15).
DECLARE LOCAL throttlePID TO PIDLOOP(1.4, 0.028, 4.2).

fly(pitchOnTouchdown, maxAngleOfAttack, maxBankDegrees, maxPitchDegrees, minimumSpeed,
        altitudeToCutThrottle, isSpacefaring, elevatorPID, verticalSpeedPID, pitchAnglePID,
        aileronPID, yawDamperPID, bankAnglePID, throttlePID).
