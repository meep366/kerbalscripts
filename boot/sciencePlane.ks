@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

showTerminal().

DECLARE LOCAL pitchOnTouchdown TO 1.
DECLARE LOCAL maxAngleOfAttack TO 25.
DECLARE LOCAL maxBankDegrees TO 45.
DECLARE LOCAL maxPitchDegrees TO 20.
DECLARE LOCAL minimumSpeed TO 40.
DECLARE LOCAL altitudeToCutThrottle TO 5.
DECLARE LOCAL isSpacefaring TO FALSE.
DECLARE LOCAL elevatorPID TO PIDLOOP(0.06, 0.0414, 0.0218).
DECLARE LOCAL verticalSpeedPID TO PIDLOOP(0.015, 0.01, 0.00563).
DECLARE LOCAL pitchAnglePID TO PIDLOOP(0.3, 0.105, 0.214).
DECLARE LOCAL aileronPID TO PIDLOOP(0.006, 0.004, 0.00225).
DECLARE LOCAL yawDamperPID TO PIDLOOP(0.07, 0.00509, 0.0578).
DECLARE LOCAL bankAnglePID TO PIDLOOP(2.1, 0.084, 3.15).
DECLARE LOCAL throttlePID TO PIDLOOP(0.7, 0.0509, 0.576).

fly(pitchOnTouchdown, maxAngleOfAttack, maxBankDegrees, maxPitchDegrees, minimumSpeed,
        altitudeToCutThrottle, isSpacefaring, elevatorPID, verticalSpeedPID, pitchAnglePID,
        aileronPID, yawDamperPID, bankAnglePID, throttlePID).

