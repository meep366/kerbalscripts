@LAZYGLOBAL OFF.

WAIT UNTIL (SHIP:UNPACKED).
IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
    COPYPATH("0:/library/utilityLibrary", "library/utilityLibrary.ks").
}
RUNONCEPATH("library/utilityLibrary.ks").
copyAndRunFiles().

showTerminal().

DECLARE LOCAL pitchOnTouchdown TO 1.
DECLARE LOCAL maxAngleOfAttack TO 25.
DECLARE LOCAL maxBankDegrees TO 60.
DECLARE LOCAL maxPitchDegrees TO 30.
DECLARE LOCAL minimumSpeed TO 45.
DECLARE LOCAL altitudeToCutThrottle TO 5.
DECLARE LOCAL isSpacefaring TO FALSE.
DECLARE LOCAL elevatorPID TO PIDLOOP(0.12, 0.141, 0.0255).
DECLARE LOCAL verticalSpeedPID TO PIDLOOP(0.042, 0.0494, 0.00893).
DECLARE LOCAL pitchAnglePID TO PIDLOOP(0.1, 0.02, 0.25).
DECLARE LOCAL aileronPID TO PIDLOOP(0.00744, 0.00244, 0.00567).
DECLARE LOCAL yawDamperPID TO PIDLOOP(0.255, 0.17, 0.0956).
DECLARE LOCAL bankAnglePID TO PIDLOOP(5.6, 0.386, 4.872).
DECLARE LOCAL throttlePID TO PIDLOOP(0.7, 0.0509, 0.576).

fly(pitchOnTouchdown, maxAngleOfAttack, maxBankDegrees, maxPitchDegrees, minimumSpeed,
        altitudeToCutThrottle, isSpacefaring, elevatorPID, verticalSpeedPID, pitchAnglePID,
        aileronPID, yawDamperPID, bankAnglePID, throttlePID).

