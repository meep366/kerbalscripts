@LAZYGLOBAL OFF.

DECLARE FUNCTION aStar {
    DECLARE PARAMETER settings, destinationType TO 1, destination TO 1, debug TO TRUE.

    DECLARE LOCAL route TO LIST().
    DECLARE LOCAL currentIPU TO CONFIG:IPU.

    DECLARE LOCAL goal TO "".
    DECLARE LOCAL wayPt TO "".

    DECLARE LOCAL charSize TO LIST(TERMINAL:CHARHEIGHT).

    DECLARE LOCAL start TO SHIP:GEOPOSITION.
    DECLARE LOCAL length TO 100.
    DECLARE LOCAL gridDist TO 0.
    DECLARE LOCAL gIndex TO 0. // Grid reference for the center of the graph which is the goal
    DECLARE LOCAL sIndex TO 0.
    DECLARE LOCAL ts TO length + 10.
    DECLARE LOCAL cs TO 8.

    DECLARE LOCAL latitudeDistance TO 0. // Calculate the distance between each graph segment
    DECLARE LOCAL lngDistance TO 0.

    IF (destinationType = "LATLNG") {
        SET goal TO LATLNG(destination:LAT, destination:LNG).
    } ELSE IF (destinationType = "WAYPOINT") {
        SET wayPt TO WAYPOINT(destination).
        SET goal TO wayPt:GEOPOSITION.
    } ELSE {
        SET goal TO LATLNG(start:LAT + destinationType, start:LNG + destination).  // Specify the physical lat/lan of the goal.
    }

    SET length TO MAX(100, MIN(300, CEILING((goal:DISTANCE / 100) * 3))).
    SET gridDist TO CEILING(goal:DISTANCE / (length / 3)).
    SET gIndex TO CEILING((length - 1) / 2).  // Grid reference for the center of the graph which is the goal
    SET sIndex TO gIndex - FLOOR(goal:DISTANCE / gridDist).

    SET latitudeDistance TO (goal:LAT - start:LAT) / (gIndex - sIndex).  // Calculate the distance between each graph segment
    SET lngDistance TO (goal:LNG - start:LNG) / (gIndex - sIndex).
    CLEARSCREEN.
    PRINT "Initializing".
    PRINT "Graph Size   : " + length.
    PRINT "Starting Ref : " + sIndex.

    SET ts TO length + 10.
    SET cs TO 8.
    IF (length > 160) {
        SET ts TO (length / 1.5) + 10.
        SET cs TO 6.
    }

    setTerminal(ts, ts, settings["IPU"], cs).

    CLEARSCREEN.
    CLEARVECDRAWS().

    placeMarker(debug, start, red, 5).
    placeMarker(debug, goal, green, 100, 1000).

    SET route TO aStarAlgorithm(debug, sIndex, gIndex, settings, start, goal, length, latitudeDistance, lngDistance).
    CLEARVECDRAWS().

    IF (route:LENGTH = 0) {
        PRINT "---{  Route can not be found  }---" AT (2, 1).
    }

    setTerminal(50, 40, currentIPU, charSize[0]).

    RETURN route.
}

//    /**
//    @sindex coordinates of the starting x cell in the graph
//    @gindex coordinates of the y call which is in the middle of the graph and location of the goal on both x and y.
//
//    @return LIST  returns a constructed list of the discovered route or empty if none is found.
//    **/
DECLARE LOCAL FUNCTION aStarAlgorithm {
    DECLARE PARAMETER debug, sIndex, gIndex, settings, start, goal, length, latitudeDistance, lngDistance.

    DECLARE LOCAL aStarRunMode TO 0.
    DECLARE LOCAL nodeList TO LEXICON().
    DECLARE LOCAL openSet TO LEXICON().
    DECLARE LOCAL closedSet TO LEXICON().
    DECLARE LOCAL fScoreList TO LEXICON().
    DECLARE LOCAL fScore TO LEXICON().
    DECLARE LOCAL gScore TO LEXICON().
    DECLARE LOCAL cameFrom TO LEXICON().
    DECLARE LOCAL neighborList TO LIST(LIST(1, 0), LIST(1, -1), LIST(0, -1), LIST(-1, 1), LIST(-1, 0), LIST(-1, -1), LIST(0, 1), LIST(1, 1)). // Neighbours of cells.
    DECLARE LOCAL vectors TO LIST().
    DECLARE LOCAL r TO "".
    DECLARE LOCAL m TO LIST().

    FROM {
        DECLARE LOCAL c TO 0.
    } UNTIL (c = length) STEP {
        SET c TO c + 1.
    } DO {
        SET r TO r + " ".
    }

    FROM {
        DECLARE LOCAL c TO 0.
    } UNTIL (c = length) STEP {
        SET c TO c + 1.
    } DO {
        m:ADD(r).
    }

    // Estimate the Heuristic cost of getting to the goal.
    DECLARE LOCAL estimatedHeuristic TO estimateHeuristicCost(LIST(sIndex, gIndex), gIndex).
    DECLARE LOCAL current TO LIST(sIndex, gIndex).

    // Add starting position to open list
    openSet:ADD(sIndex + "," + gIndex, TRUE).

    SET nodeList[sIndex + "," + gIndex] TO LEXICON("LAT", start:LAT, "LNG", start:LNG, "TERRAINHEIGHT", start:TERRAINHEIGHT, "POSITION", start:POSITION, "FSCORE", estimatedHeuristic).
    SET nodeList[gIndex + "," + gIndex] TO LEXICON("LAT", goal:LAT, "LNG", goal:LNG, "TERRAINHEIGHT", goal:TERRAINHEIGHT, "POSITION", goal:POSITION, "FSCORE", 0).

    SET gScore[sIndex + "," + gIndex] TO 0.
    SET fScoreList[estimatedHeuristic] TO LEXICON(sIndex + ","+  gIndex, LIST(sIndex, gIndex)).
    SET fScore[sIndex + "," + gIndex] TO estimatedHeuristic.

    UNTIL (openSet:LENGTH = 0 OR aStarRunMode = -1) {
        DECLARE LOCAL fScores TO fScoreList:KEYS.
        DECLARE LOCAL localFScore TO length * length.
        DECLARE LOCAL delScore TO "".

        FOR score in fScores {
            IF (fScoreList[score]:LENGTH = 0) {
                fScoreList:REMOVE(score).
            } ELSE IF (score < localFScore) {
                DECLARE LOCAL scorekey TO fScoreList[score]:KEYS.
                IF (NOT closedSet:HASKEY(scorekey[0])) {
                    SET current TO fScoreList[score][scorekey[0]].
                    SET delScore TO scorekey[0].
                    SET localFScore TO score.
                } ELSE {
                    fScoreList[score]:REMOVE(scorekey[0]).
                }
            }
        }
        IF (fScoreList:HASKEY(localFScore)) {
            fScoreList[localFScore]:REMOVE(delScore).
        }
        IF (NOT closedSet:HASKEY(current[0] + "," + current[1])) {
            PRINT "S" AT (gIndex, sIndex).
            PRINT "G" AT (gIndex, gIndex).
            PRINT "IPU        : " + CONFIG:IPU AT (5, 63).
            PRINT "Grid       : " + current[0] + ":" + current[1] AT (5, 64).
            PRINT "Open Set   : " + openSet:LENGTH AT (5, 65).
            PRINT "Closed Set : " + closedSet:LENGTH AT (5, 66).
            PRINT "fScore     : " + localFScore + "   " AT (5, 67).
            PRINT "Map size   : " + nodeList:LENGTH + "   " AT (5, 68).
            PRINT "                                              " AT (5, 70).

            IF (current[0] = gIndex AND current[1] = gIndex) {
                RETURN constructRoute(debug, gIndex, nodeList, cameFrom, m).
            }
            openSet:REMOVE(current[0] + "," + current[1]).
            closedSet:ADD(current[0] + "," + current[1], TRUE).
            getNeighbors(debug, current, gScore[current[0] + "," + current[1]], settings, nodeList, neighborList, closedSet, length,
                    latitudeDistance, lngDistance, m, gScore, openSet, cameFrom, gIndex, fScore, fScoreList).
        } ELSE {
            PRINT current[0] + "," + current[1] + " is in closed list" AT (5, 70).
            openSet:REMOVE(current[0] + "," + current[1]).
        }

        IF (TERMINAL:INPUT:HASCHAR) {
            DECLARE LOCAL char TO TERMINAL:INPUT:GETCHAR().
            IF (char = TERMINAL:INPUT:ENDCURSOR) {
                SET aStarRunMode TO -1.
                setTerminal().
            }
        }
    }
    RETURN LIST().
}

DECLARE LOCAL FUNCTION getNeighbors {
    DECLARE PARAMETER debug, current, currentGScore, settings, nodeList, neighborList, closedSet, length,
            latitudeDistance, lngDistance, m, gScore, openSet, cameFrom, gIndex, fScore, fScoreList.

    DECLARE LOCAL node TO nodeList[current[0] + "," + current[1]].
    DECLARE LOCAL sCount TO 0. // Counter for neighbors with bad slopes

    // Work through the neighbor list starting directly ahead and working around clockwise.
    FOR ne IN neighborList {
        DECLARE LOCAL gridY TO current[0] + ne[0].
        DECLARE LOCAL gridX TO current[1] + ne[1].
        DECLARE LOCAL neighbor TO gridY + "," + gridX.

        IF (closedSet:HASKEY(neighbor)) {
            // Continue and do nothing
        } ELSE {
            IF (gridY >= 0 AND gridY <= length - 1 AND gridX >= 0 AND gridX <= length - 1) {
                IF (testNeighbor(debug, current, ne, LIST(gridX, gridY), settings, neighborList, nodeList, latitudeDistance, lngDistance, m)) {
                    DECLARE LOCAL tentativeGScore TO gScore[current[0] + "," + current[1]] + 1 + nodeList[neighbor]["WEIGHT"].

                    //  We don't want to fall off the grid!
                    IF (openSet:HASKEY(neighbor) = FALSE) {
                        openSet:ADD(neighbor,TRUE).
                    } ELSE IF (tentativeGScore >= currentGScore) {
                        //  This is not a better path.  Do nothing.
                    }

                    SET cameFrom[neighbor] TO current.
                    SET gScore[neighbor] TO tentativeGScore.
                    SET fScore[neighbor] TO gScore[neighbor] + estimateHeuristicCost(LIST(gridY, gridX), gIndex).
                    SET nodeList[neighbor]["FSCORE"] TO fScore[neighbor].

                    IF (fScoreList:HASKEY(fScore[neighbor])) {
                        IF (NOT fScoreList[fScore[neighbor]]:HASKEY(neighbor)) {
                            fScoreList[fScore[neighbor]]:ADD(neighbor, LIST(gridY, gridX)).
                        }
                    } ELSE {
                        fScoreList:ADD(fScore[neighbor], LEXICON(neighbor, LIST(gridY, gridX))).
                    }
                }
            } ELSE {
                SET sCount TO sCount + 1.
            }
        }
    }
}

DECLARE LOCAL FUNCTION testNeighbor {
    DECLARE PARAMETER debug, current, ne, printAt, settings, neighborList, nodeList, latitudeDistance, lngDistance, m.

    // This bit is nasty!  It took me more than a few hours to balance it right and the ne[0] bit is a hack and a half but it works.
    DECLARE LOCAL offsetX TO 0.
    DECLARE LOCAL offsetY TO 0.
    DECLARE LOCAL node TO nodeList[current[0] + "," + current[1]].
    DECLARE LOCAL _grid TO LATLNG(node["LAT"], node["LNG"]).
    IF (ne[0] <> 0) {
        IF (ne[1] = 0) {
            SET offsetY TO (ne[0] * latitudeDistance) / 2.
            SET offsetX TO (ne[0] * lngDistance) / 2.
        } ELSE {
            SET offsetY TO (ne[0] * latitudeDistance).
            SET offsetX TO (ne[0] * lngDistance).
        }
    }
    SET _grid TO LATLNG(node["LAT"] + offsetY, node["LNG"] + offsetX).
    IF (ne[1] <> 0) {
        SET offsetY TO (ne[1] * lngDistance).
        SET offsetX TO -(ne[1] * latitudeDistance).
    }

    DECLARE LOCAL grid TO LATLNG(_grid:LAT + offsetY, _grid:LNG + offsetX).
    DECLARE LOCAL heightDiff TO grid:TERRAINHEIGHT - node["TERRAINHEIGHT"].
    DECLARE LOCAL maxAngle TO testAngle(grid, neighborList).

    // We want to avoid trying to drive up or down cliffs and especially taking a dip if we can help it
    DECLARE LOCAL setList TO 0.
    DECLARE LOCAL distance TO (grid:POSITION - node["POSITION"]):MAG.
    DECLARE LOCAL angle TO ARCSIN(heightDiff / distance).
    DECLARE LOCAL weight TO 0.
    DECLARE LOCAL c TO " ".
    IF (angle > settings["MinSlope"] AND angle < settings["MaxSlope"] AND ROUND(grid:TERRAINHEIGHT) >= 0 AND maxAngle < MAX(ABS(Settings["MinSlope"]), Settings["MaxSlope"])) {
        SET c TO ".".
        placeMarker(debug, grid, yellow, 5, 100, ROUND(angle), 0.05).
        SET setList TO 1.
    } ELSE IF (grid:TERRAINHEIGHT < 0) {
        SET c TO "!".
        placeMarker(debug, grid, red, 5, 100, ROUND(angle), 0.05).
        SET setList TO 2.
    } ELSE {
        IF (angle <= settings["MinSlope"]) {
            SET c TO "v".
            SET weight TO 1.
        } ELSE {
            SET c TO "^".
            SET weight TO 1.
        }
    }
    PRINT c AT (printAt[0], printAt[1]).

    // Update the graph with what we've discovered about this cell.
    SET nodeList[printAt[1] + "," + printAt[0]] TO LEXICON(
            "LAT", grid:LAT,
            "LNG", grid:LNG,
            "TERRAINHEIGHT", grid:TERRAINHEIGHT,
            "POSITION", grid:POSITION,
            "FSCORE", 0,
            "WEIGHT", weight).
    SET m[printAt[1]] TO m[printAt[1]]:REMOVE(printAt[0], 1).
    SET m[printAt[1]] TO m[printAt[1]]:INSERT(printAt[0], c).
    IF (setList = 1) {
        RETURN TRUE.
    } ELSE {
        RETURN FALSE.
    }
}

DECLARE LOCAL FUNCTION testAngle {
    DECLARE PARAMETER grid, neighborList.

    DECLARE LOCAL mpla TO 2 * CONSTANT:PI * BODY:RADIUS / 360.
    DECLARE LOCAL mplo TO mpla * COS(grid:LAT).
    DECLARE LOCAL tg TO LATLNG(grid:LAT, grid:LNG).
    DECLARE LOCAL ma TO 0.
    DECLARE LOCAL angle TO 0.
    FOR ne IN neighborList {
        SET tg TO LATLNG(grid:LAT + (ne[0] / mpla), grid:LNG + (ne[1] / mplo)).
        SET angle TO ARCSIN(ABS(grid:TERRAINHEIGHT - tg:TERRAINHEIGHT) / (grid:POSITION - tg:POSITION):MAG).
        IF (angle > ma) {
            SET ma TO angle.
        }
    }
    RETURN ma.
}

// /**
//    @st LIST  These contain the x,y coordinates of the cell in the graph
//    @fn Int   Or gindex of the goal cell which is directly center of the graph

//    This is a monotone heuristic I guess and calculates the distance to travel to the goal horizontally and vertically, not diagonally
//    but fear not because the algorythm will drive you diagonally if you so wish
//
//    @return INT   Heuristic cost of getting from graph point to the goal
//  **/
DECLARE LOCAL FUNCTION estimateHeuristicCost {
    DECLARE PARAMETER st, fn.
    DECLARE LOCAL gridY TO 0.
    DECLARE LOCAL gridX TO 0.
    IF (st[0] > fn) {
        SET gridY TO st[0] - fn.
    } ELSE {
        SET gridY TO fn - st[0].
    }
    IF (st[1] > fn) {
        SET gridX TO st[1] - fn.
    } ELSE {
        SET gridX TO fn - st[1].
    }
    return gridY + gridX.
}

//  /**
//    @grid LATLNG    the 3d world space SPOT
//    @color COLOR    I'm AMERICAN, deal with it!
//    @size INT       How big do we want this arrow
//    @height INT     How high do we want the arrow
//    @text VARCHAR   Any text to put down
//    @textSize FLOAT How big should the text be
//  **/
DECLARE LOCAL FUNCTION placeMarker {
    DECLARE PARAMETER debug, grid, color TO blue, size TO 5, height TO 100, text TO "", textSize TO 1.

    IF (debug) {
        vectors:ADD(VECDRAWARGS(
                grid:ALTITUDEPOSITION(grid:TERRAINHEIGHT + height),
                        grid:POSITION - grid:ALTITUDEPOSITION(grid:TERRAINHEIGHT + height),
                        color, text, textSize, true,size)).
    }
}

//  /**
//      Reverse engineer the route from goal to origin
//
//      @return path  LIST()  Graph coordinates of the discovered route
//  **/
DECLARE LOCAL FUNCTION constructRoute {
    DECLARE PARAMETER debug, gIndex, nodeList, cameFrom, m.

    DECLARE LOCAL current TO LIST(gIndex, gIndex).
    DECLARE LOCAL totalpath TO LIST(
            LATLNG(
                    nodeList[current[0] + "," + current[1]]["LAT"],
                            nodeList[current[0] + "," + current[1]]["LNG"]
                    )
            ).
    UNTIL (NOT cameFrom:HASKEY(current[0] + "," + current[1])) {
        SET current TO cameFrom[current[0] + "," + current[1]].
        PRINT "*" AT (current[1], current[0]).
        SET m[current[0]] TO m[current[0]]:REMOVE(current[1], 1).
        SET m[current[0]] TO m[current[0]]:INSERT(current[1], "*").
        placeMarker(debug, LATLNG(nodeList[current[0] + "," + current[1]]["LAT"], nodeList[current[0] + "," + current[1]]["LNG"]), yellow, 1, 100, "" ,30).
        totalpath:INSERT(0, LATLNG(nodeList[current[0] + "," + current[1]]["LAT"], nodeList[current[0] + "," + current[1]]["LNG"])).
    }
    CLEARSCREEN.
    WRITEJSON(m,"0:/astar-rover/backup/map.json").
    RETURN totalpath.
}

DECLARE LOCAL FUNCTION setTerminal {
    DECLARE PARAMETER w TO 50, h TO 40, i TO 500, c TO 12.

    SET TERMINAL:WIDTH TO w.
    SET TERMINAL:HEIGHT TO h.
    SET CONFIG:IPU TO i.
    SET TERMINAL:CHARHEIGHT TO c.
}
