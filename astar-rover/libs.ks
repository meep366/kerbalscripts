@LAZYGLOBAL OFF.

DECLARE LOCAL vol TO "0:".
IF (EXISTS("1:/astar-rover/config/soundpack.json")) {
    SET vol TO "1:".
}

DECLARE FUNCTION center {
    PARAMETER s, y.

    DECLARE LOCAL x TO ROUND(TERMINAL:WIDTH / 2) - FLOOR(s:LENGTH / 2).
    PRINT s AT (x,y).
}

DECLARE FUNCTION updateSetting {
    DECLARE PARAMETER key, value.
    DECLARE LOCAL settings TO READJSON("1:/config/settings.json").
    SET settings[key] TO value.
    WRITEJSON(settings, "1:/config/settings.json").
}

DECLARE FUNCTION playSounds {
    DECLARE PARAMETER soundIndex, settings.
    IF (settings["Sound"] = 1) {
        DECLARE LOCAL soundpack TO READJSON(vol + "/astar-rover/config/soundpack.json").

        DECLARE LOCAL q TO LIST().
        DECLARE LOCAL c TO 0.
        FOR i IN soundpack[soundIndex] {
            q:ADD(GETVOICE(c)).
            SET q[c]:WAVE TO i[0].
            SET q[c]:ATTACK TO i[1].
            SET q[c]:DECAY TO i[2].
            SET q[c]:SUSTAIN TO 1.
            q[c]:PLAY(i[3]).
            SET c TO c + 1.
        }
    }
}

DECLARE FUNCTION displayBattery {
    PARAMETER y, c.
    PRINT "################################" AT (6, y).
    PRINT "#" AT (6, y + 1).
    PRINT "###" AT (37, y + 1).
    PRINT "################################" AT (6, y + 2).
    DECLARE LOCAL p TO ROUND(c / 3.3).
    DECLARE LOCAL b TO "".
    DECLARE LOCAL spc TO "     ".
    FROM {
        DECLARE LOCAL x TO 0.
    } UNTIL (x = p) STEP {
        SET x TO x + 1.
    } DO {
        SET b TO b + "|".
    }
    PRINT b + spc AT (7, y + 1).
    WAIT 1.
}

DECLARE FUNCTION scienceMenu {
    CLEARSCREEN.
    DECLARE LOCAL scienceParts TO getScienceParts().
    PRINT "  ---{  Select a science part }---" AT (2, 2).
    DECLARE LOCAL y TO 1.
    FOR p IN scienceParts {
        PRINT "(" + y + ") " + p:TITLE AT (4, y + 4).
        SET y TO y + 1.
    }
}

DECLARE FUNCTION doScience {
    DECLARE PARAMETER index.

    DECLARE LOCAL scienceParts TO getScienceParts().
    DECLARE LOCAL part TO scienceParts[index - 1].
    DECLARE LOCAL modules TO part:MODULES.

    FOR module IN modules {
        IF (module = "ModuleScienceExperiment") {
            transmitScience(part:GETMODULE("ModuleScienceExperiment")).
        } ELSE IF (module = "DMModuleScienceAnimate") {
            transmitScience(part:GETMODULE("DMModuleScienceAnimate")).
        }
    }
}

DECLARE LOCAL FUNCTION getScienceParts {
    RETURN SHIP:PARTSTAGGED("Skience").
}

DECLARE LOCAL FUNCTION transmitScience {
    DECLARE PARAMETER module.

    PRINT "Preparing science part" AT (2, 10).
    module:DUMP.
    module:RESET.
    PRINT "Performing Science" AT (2, 11).
    WAIT 0.
    module:DEPLOY.
    IF (AG2) {
        PRINT "Transmitting Science" AT (2, 12).
        WAIT UNTIL module:HASDATA.
        module:TRANSMIT.
    }
    module:RESET.
}
