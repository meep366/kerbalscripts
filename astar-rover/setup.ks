@LAZYGLOBAL OFF.

DECLARE FUNCTION setupMain {
    DECLARE LOCAL version TO OPEN("0:/astar-rover/config/version"):READALL:STRING.

    DECLARE LOCAL settingMode TO 0.
    // Valid settingModes
    //  0 - Main Menu
    //  1 - Automated installation
    //  2 - Settings Menu
    // 10 - Change Min slope
    // 11 - Change Max slope
    // 12 - Change IPU
    // 13 - Change DefaultSpeed

    CLEARSCREEN.

    DECLARE LOCAL settings TO LEXICON().
    DECLARE LOCAL error TO "".
    DECLARE LOCAL value TO -1.
    DECLARE LOCAL defaultValues TO LEXICON(
            "MinSlope", LIST(1, -45, 0, "Min Slope"),
            "MaxSlope", LIST(1, 0, 45, "Max Slope"),
            "IPU", LIST(500, 500, 2000, "IPU"),
            "DefaultSpeed", LIST(1, 1, 50, "Default Speed"),
            "TurnLimit", LIST(0.1, 0.1, 3, "Turn Limit"),
            "Sound", LIST(1, 0, 1, "Enable Sound"),
            "Increments", LIST(0.1, 0.1, 1, "LAT/LNG Increments")).

    SET settings TO initSettings(settings).
    mainHUD().

    UNTIL (settingMode = -1) {
        PRINT "Runmode : " + settingMode AT (2, TERMINAL:HEIGHT - 2).
        IF (TERMINAL:INPUT:HASCHAR) {
            DECLARE LOCAL char TO TERMINAL:INPUT:GETCHAR().
            DECLARE LOCAL number TO char:TONUMBER(-99).
            IF (settingMode = 1) {
                SET settingMode TO 0.
                mainHUD().
            }
            IF (char = TERMINAL:INPUT:ENDCURSOR) {
                SET settingMode TO -1.
                CLEARSCREEN.
            } ELSE IF (char = TERMINAL:INPUT:HOMECURSOR) {
                SET settingMode TO 0.
                mainHUD().
            } ELSE IF (settingMode = 2) {
                SET settings TO handleSettings(char, number, settings, defaultValues).
            } ELSE IF (number <> -99) {
                IF (settingMode = 0) {
                    SET settingMode TO handleHUD(number, defaultValues, settings).
                }
            }
            PRINT "Key " + char + " pressed" AT (2, TERMINAL:HEIGHT - 1).
        }
        WAIT 0.01.
    }
    RETURN settings.
}

DECLARE LOCAL FUNCTION initSettings {
    DECLARE PARAMETER settings.

    DECLARE LOCAL defaultSettings TO LEXICON(
                    "MinSlope", -15,
                    "MaxSlope", 25,
                    "IPU", 2000,
                    "DefaultSpeed", 4,
                    "TurnLimit", 1,
                    "Sound", 1,
                    "Odometer", 0,
                    "Version", version,
                    "SendScience", TRUE,
                    "Increments", 0.1,
                    "fCE", LEXICON(BODY:NAME, 1)).

    IF (NOT EXISTS("1:/config/settings.json")) {
        IF (EXISTS("0:/astar-rover/backup/" + SHIP:NAME + ".json")) {
            SET settings TO READJSON("0:/astar-rover/backup/" + SHIP:NAME + ".json").
            WRITEJSON(settings, "1:/config/settings.json").
        } ELSE {
            SET settings TO defaultSettings.
        }
    } ELSE {
        SET settings TO READJSON("1:/config/settings.json").
        SET settings["Version"] TO version.
    }
    DECLARE LOCAL keys TO defaultSettings:KEYS.
    FOR key IN keys {
        IF (settings:HASKEY(key) = FALSE) {
            settings:ADD(key, defaultSettings[key]).
        }
    }
    WRITEJSON(settings, "1:/config/settings.json").
    RETURN settings.
}

DECLARE LOCAL FUNCTION mainHUD {
    CLEARSCREEN.
    center("---{ A* Rover }---", 2).
    center("Pick an option below to configure and set up the rover", 4).
    PRINT "(1) Initialize rover" AT (5, 7).
    PRINT "(2) Rover Settings" AT (5, 8).
    PRINT "(3) Reboot KOS Processor" AT (5, 9).
    PRINT "(4) Backup Settings to Archive" AT (5, 10).
    IF (EXISTS("0:/astar-rover/backup/" + SHIP:NAME + ".json")) {
        PRINT "(5) Restore Settings from Archive" AT (5, 11).
    }
    PRINT "(9) Reset to Factory Settings" AT (5, 15).
    PRINT "Press the Home key to return to this menu" AT (5, 20).
    PRINT "Press End key to exit" AT (5, 22).
}

DECLARE LOCAL FUNCTION handleHUD {
    DECLARE PARAMETER option, defaultValues, settings.

    DECLARE LOCAL settingMode TO option.
    IF (option = 1) {
        initiate().
    } ELSE IF (option = 2) {
        settingsHUD(defaultValues, settings).
    } ELSE IF (option = 3) {
        REBOOT.
    } ELSE IF (option = 4) {
        COPYPATH("1:/config/settings.json", "0:/astar-rover/backup/" + SHIP:NAME + ".json").
    } ELSE IF (option = 5) {
        COPYPATH("0:/astar-rover/backup/" + SHIP:NAME + ".json", "1:/config/settings.json").
        REBOOT.
    } ELSE IF (option = 9) {
        SET settingMode TO reset().
    } ELSE {
        SET settingMode TO 0.
    }
    RETURN settingMode.
}

DECLARE LOCAL FUNCTION settingsHUD {
    DECLARE PARAMETER defaultValues, settings.

    CLEARSCREEN.
    DECLARE LOCAL row TO 2.
    DECLARE LOCAL column TO 1.
    DECLARE LOCAL dashRow TO "----------------------------------------------".

    FOR v IN defaultValues:KEYS {
        PRINT dashRow AT (2, row).
        PRINT "| " + column + " |                       |                |" AT (2, row + 1).
        PRINT defaultValues[v][3] AT (8, row + 1).
        PRINT settings[v] AT (34, row + 1).
        SET row TO row + 2.
        SET column TO column + 1.
    }
    PRINT dashRow AT (2, row).

    PRINT "Press number of value you wish to edit." AT (2, row + 2).
    PRINT "Use the Up and Down cursor arrows to" AT (2, row + 3).
    PRINT "select values" AT (3, row + 4).
    PRINT "Settings will automatically save" AT (2, row + 5).
}

DECLARE LOCAL FUNCTION initiate {
    CLEARSCREEN.
    DECLARE LOCAL y TO 4.
    center("---{ Initializing your rover }---", 2).
    SET y TO report("Switching to rover's local Volume", 2, y).
    SWITCH TO 1.
    DECLARE LOCAL capacity TO CORE:CURRENTVOLUME:CAPACITY.
    SET y TO report("Volume " + CORE:CURRENTVOLUME:NAME +" has a capacity of " + ROUND(capacity / 1000) + " kbytes ", 2, y).

    SET y TO report(ROUND(CORE:CURRENTVOLUME:FREESPACE / 1000, 3) + " kbytes free", 5, y).
    SET y TO report("To customize how the rover operates, change them in the settings menu", 2, y + 2).
    SET y TO report("Press Any key to continue", 5, y + 4).
}

DECLARE LOCAL FUNCTION report {
    DECLARE PARAMETER string, x, y.
    PRINT string AT (x, y).
    WAIT 0.2.
    SET y TO y + 1.
    RETURN y.
}

DECLARE LOCAL FUNCTION handleSettings {
    DECLARE PARAMETER K, N, settings, defaultValues.

    DECLARE LOCAL settingLines TO LIST(3, 5, 7, 9, 11, 13, 15, 17).
    DECLARE LOCAL keys TO defaultValues:KEYS.
    IF (N <> -99) {
        FOR s IN settingLines {
            PRINT " " AT (32, s).
        }
        IF (N < settingLines:LENGTH AND N <> 0) {
            PRINT "*" AT (32, settingLines[N - 1]).
            SET value TO N.
        }
    } ELSE {
        IF (value <> -1) {
            IF (K = TERMINAL:INPUT:UPCURSORONE) {
                PRINT "Up  " + value AT (28, TERMINAL:HEIGHT - 1).
                IF (settings[keys[value - 1]] < defaultValues[keys[value - 1]][2]) {
                    updateSetting(keys[value - 1], settings[keys[value - 1]] + defaultValues[keys[value - 1]][0]).
                }
            } ELSE IF (K = TERMINAL:INPUT:DOWNCURSORONE) {
                PRINT "Down" + value AT (28, TERMINAL:HEIGHT - 1).
                IF (settings[keys[value - 1]] > defaultValues[keys[value - 1]][1]) {
                    updateSetting(keys[value - 1], settings[keys[value - 1]] - defaultValues[keys[value - 1]][0]).
                }
            }
            SET settings TO READJSON("1:/config/settings.json").
            PRINT settings[keys[value - 1]] + "    " AT (34, settingLines[value - 1]).
        }
    }
    RETURN settings.
}

DECLARE LOCAL FUNCTION reset {
    CLEARSCREEN.
    center("---{ Resetting rover to factory settings }---", 2).
    DELETEPATH("1:/boot").
    DELETEPATH("1:/astar-rover").
    DELETEPATH("1:/config").
    report("Rover has been reset", 2, 4).
    report("Press Home key to continue", 2, 5).
    RETURN 0.
}
