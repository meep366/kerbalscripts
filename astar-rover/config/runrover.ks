@LAZYGLOBAL OFF.

CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").

IF (ADDONS:RT:AVAILABLE) {
    CLEARSCREEN.
    PRINT "---{    WAITING FOR CONNECTION    }---" AT (2, 2).
    PRINT "Acquiring signal " AT (2, 5).
    DECLARE LOCAL columnNumber TO 19.
    UNTIL (ADDONS:RT:HASKSCCONNECTION(SHIP)) {
        PRINT "." AT (columnNumber, 5).
        SET columnNumber TO columnNumber + 1.
        WAIT 1.
        IF (columnNumber = 25) {
            PRINT "       " AT (19, 5).
            SET columnNumber TO 19.
        }
    }
    SET WARP TO 0.
}

DECLARE LOCAL vol TO "1:".
IF (NOT EXISTS("1:/astar-rover")) {
    SET vol TO "0:".
}

CLEARSCREEN.
RUNONCEPATH(vol + "/astar-rover/rover").
