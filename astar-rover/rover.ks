@LAZYGLOBAL OFF.

// rover.ks
// Original direction and speed routines written by KK4TEE
// Updated with waypoint management by aidygus
// License: GPLv3
//
// This program provides waypoint functionality
// using an A* based algorithm to calculate the
// safest possible route from start to goal
// and monitoring for fully automated rovers
DECLARE FUNCTION controlRover {
    DECLARE PARAMETER debug TO TRUE.

    DECLARE LOCAL settings TO LEXICON().
    IF (NOT EXISTS("1:/config/settings.json")) {
        SET settings TO setupMain().
    } ELSE {
        SET settings TO READJSON("1:/config/settings.json").
        COPYPATH("1:/config/settings.json", "0:/astar-rover/backup/" + SHIP:NAME + ".json").
    }

    DECLARE LOCAL wheelPID TO PIDLOOP(0.3, 0.375, 0.06).
    SET wheelPID:MINOUTPUT TO -1.
    SET wheelPID:MAXOUTPUT TO 1.
    SET TERMINAL:WIDTH TO 50.
    SET TERMINAL:HEIGHT TO 40.

    LOCK turnLimit TO MIN(1, settings["TurnLimit"] / SHIP:GROUNDSPEED). //Scale the turning radius based on __current speed
    LOCK targetHeading TO 90.

    DECLARE LOCAL loopTime TO 0.01.
    DECLARE LOCAL loopEndTime TO TIME:SECONDS.
    DECLARE LOCAL wheelThrottle TO 0.
    DECLARE LOCAL wheelSteer TO 0.
    DECLARE LOCAL targetSpeed TO 0.
    DECLARE LOCAL slopeSpeed TO settings["DefaultSpeed"].
    DECLARE LOCAL currentHeading TO 0.
    DECLARE LOCAL northPole TO LATLNG(90, 0).
    DECLARE LOCAL angle TO 0.
    DECLARE LOCAL angleDiff TO 0.
    DECLARE LOCAL dir TO 0.
    DECLARE LOCAL lockCounter TO 0.
    DECLARE LOCAL nextWaypointHeading TO 0.
    DECLARE LOCAL whitespace TO "     ".
    DECLARE LOCAL header TO "".
    DECLARE LOCAL waitCursor TO LIST("-", "\", "|", "/").
    DECLARE LOCAL waitCount TO 0.
    DECLARE LOCAL frictionCoefficientList TO LEXICON("Kerbin", 1.1108, "Mun", 0.60, "Minmus", 1.0872, "Duna", 0.4, "Eve", 0.3).  // Need an equation to work out what friction coefficient is for given gravity
    DECLARE LOCAL directionOT TO 1.
    DECLARE LOCAL page TO 0.
    SET CONFIG:IPU TO 500.

    DECLARE LOCAL overSpeedDownSlopeBrakeTime TO 0.3.
    DECLARE LOCAL extremeSlopeAngle TO 8.
    DECLARE LOCAL overSpeedCruise TO 8.
    DECLARE LOCAL extraBrakeTime TO 0.7.
    DECLARE LOCAL cruiseSpeedBrakeTime TO 1.
    DECLARE LOCAL currentSlopeAngle TO 0.
    DECLARE LOCAL brakesOn TO TRUE.
    DECLARE LOCAL lastBrake TO -1.
    DECLARE LOCAL lastEvent TO LIST(TIME:SECONDS, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0). // Last time something happened in each run mode
    DECLARE LOCAL lastBat TO -1.
    DECLARE LOCAL brakeUseCount TO 0.
    DECLARE LOCAL currentOverSpeed TO overSpeedCruise.
    DECLARE LOCAL currentBrakeTime TO cruiseSpeedBrakeTime.
    DECLARE LOCAL stopDistance TO 0.5.
    DECLARE LOCAL gradient TO 0.
    DECLARE LOCAL wpm TO LIST().
    DECLARE LOCAL navpoints TO LIST().
    DECLARE LOCAL contractWayPoints TO LEXICON().
    DECLARE LOCAL contractWayPointNumbers TO LEXICON().
    DECLARE LOCAL maxCharge TO STAGE:ELECTRICCHARGE.
    DECLARE LOCAL resourceList TO LIST().
    DECLARE LOCAL electricResource TO 0.

    LIST RESOURCES IN resourceList.
    FOR res IN resourceList {
        IF (res:NAME = "ElectricCharge") {
            SET electricResource TO res.
        }
    }
    LOCK chargeLevel TO ROUND(100 * electricResource:AMOUNT / electricResource:CAPACITY).
    DECLARE LOCAL lastCharge TO electricResource:AMOUNT.
    DECLARE LOCAL menu TO 0.
    DECLARE LOCAL nextWrite TO -1.
    //     0 Main Menu
    //     1 Select Waypoint
    //     2 Do Science
    //     3 No Connection
    //     4 Low Power

    CLEARSCREEN.
    CLEARVECDRAWS().
    RCS OFF.
    SAS OFF.
    LOCK throttle TO 0.

    DECLARE LOCAL runMode TO 0.
    //    Valid runmodes
    //    0  Normal Operation
    //    1  Approaching Waymarker
    //    2  Change of slope angle ahead
    //    3  Moving away from waypoint
    //    4  Negotiating slope exceeding MinSlope (Depricated)
    //    5  Has hit an obstacle.
    //    6  Attempting to move round obstacle.
    //    7  Manual Hold
    //    8  SAS Hold
    //    12 No Connection
    //    13 Save power

    DECLARE LOCAL goal TO SHIP:GEOPOSITION.
    DECLARE LOCAL grid TO SHIP:GEOPOSITION.
    DECLARE LOCAL route TO LIST().
    DECLARE LOCAL routeWaypoint TO -1.

    displayHUD(header, settings).
    playSounds("start", settings).
    UNTIL (runMode = -1) {
        IF (chargeLevel < 10 AND runMode <> 13) {
            LIGHTS OFF.
            CLEARSCREEN.
            SET targetSpeed TO holdPosition(13, lastEvent, targetSpeed).
            SET brakesOn TO TRUE.
            SET runMode TO 13.
            SET menu TO 4.
            savePower().
        } ELSE IF (chargeLevel > 30 AND runMode = 13) {
            DECLARE LOCAL returnValues TO restoreOperations(header, settings, route, lastEvent).
            IF (returnValues:LENGTH > 0) {
                SET targetSpeed TO returnValues["targetSpeed"].
                SET brakesOn TO returnValues["brakesOn"].
            }
            SET runMode TO 0.
            SET menu TO 0.
        } ELSE IF ((ADDONS:RT:AVAILABLE AND NOT ADDONS:RT:HASKSCCONNECTION(SHIP)) AND runMode <> 12) {
            CLEARSCREEN.
            SET menu TO 3.
            SET targetSpeed TO holdPosition(12, lastEvent, targetSpeed).
            SET brakesOn TO TRUE.
            SET runMode TO 12.
        } ELSE IF ((ADDONS:RT:AVAILABLE AND ADDONS:RT:HASKSCCONNECTION(SHIP)) AND runMode = 12) {
            DECLARE LOCAL returnValues TO restoreOperations(header, settings, route, lastEvent).
            IF (returnValues:LENGTH > 0) {
                SET targetSpeed TO returnValues["targetSpeed"].
                SET brakesOn TO returnValues["brakesOn"].
            }
            SET runMode TO 0.
            SET menu TO 0.
        }

        IF (runMode <> 13 AND runMode <> 12) {
            // Update the compass:
            // I want the heading to match the navball
            // and be out of 360' instead of +/-180'
            // I do this by judging the heading relative
            // TO a lAT/LNG SET TO the north pole
            IF (northPole:bearing <= 0) {
                SET currentHeading TO ABS(northPole:BEARING).
            } ELSE {
                SET currentHeading TO (180 - northPole:BEARING) + 180.
            }

            DECLARE LOCAL upVector TO UP:VECTOR.
            DECLARE LOCAL velocityVector TO SHIP:VELOCITY:SURFACE:NORMALIZED.
            DECLARE LOCAL dotProduct TO VDOT(velocityVector, upVector).
            SET dir TO VDOT(SHIP:FACING:VECTOR, SRFPROGRADE:VECTOR).
            DECLARE LOCAL currentSlopeAngle TO 90 - ARCCOS(dotProduct).

            DECLARE LOCAL facingVector TO SHIP:FACING.
            SET targetSpeed TO targetSpeed + 0.1 * SHIP:CONTROL:PILOTWHEELTHROTTLE.
            SET targetSpeed TO MAX(0, targetSpeed).

            DECLARE LOCAL predicted1 TO BODY:GEOPOSITIONOF(facingVector + V(0, 0, MAX(15, stopDistance + 5))).
            DECLARE LOCAL predicted2 TO BODY:GEOPOSITIONOF(facingVector + V(0, 0, MAX(15, stopDistance + 5) + 1)).
            DECLARE LOCAL heightDiff TO predicted2:TERRAINHEIGHT - predicted1:TERRAINHEIGHT.
            DECLARE LOCAL distance TO (predicted1:POSITION - predicted2:POSITION):MAG.
            DECLARE LOCAL soundToPlay TO "blip".
            SET angle TO ARCSIN(heightDiff / distance).
            SET gradient TO ROUND(TAN(currentSlopeAngle), 5).
            SET angleDiff TO MAX(currentSlopeAngle, angle) - MIN(currentSlopeAngle, angle).
            SET stopDistance TO getStopDistance(GROUNDSPEED + 0.5, gradient, frictionCoefficientList).

            IF (route:LENGTH <> 0 AND routeWaypoint <> - 1 AND routeWaypoint <= route:LENGTH - 1) {
                DECLARE LOCAL headingDifference TO route[routeWaypoint]:HEADING - currentHeading.
                IF (headingDifference > 180) {
                    SET headingDifference TO headingDifference - 360.
                } ELSE IF (headingDifference < -180) {
                    SET headingDifference TO headingDifference + 360.
                }

                IF (routeWaypoint + 1 <> route:LENGTH) {
                    SET nextWaypointHeading TO route[routeWaypoint + 1]:HEADING - currentHeading.
                    IF (nextWaypointHeading > 180) { //Make sure the headings make sense
                        SET nextWaypointHeading TO nextWaypointHeading - 360.
                    } ELSE IF (nextWaypointHeading < -180) {
                        SET nextWaypointHeading TO nextWaypointHeading + 360.
                    }
                }

                IF (routeWaypoint = (route:LENGTH-1) AND route[routeWaypoint]:DISTANCE < 50) {
                    SET header TO "---{   Rover has arrived at location  }---         " + whitespace.
                    PRINT header AT (2, 1).
                    SET targetSpeed TO holdPosition(7, lastEvent, targetSpeed).
                    SET brakesOn TO TRUE.
                    SET runMode TO 7.
                    SET lastEvent[0] TO TIME:SECONDS.
                    COPYPATH("1:/config/settings.json","0:/astar-rover/backup/" + SHIP:NAME + ".json").
                }

                IF (ABS(headingDifference) > 20 AND runMode <> 5 AND runMode <> 6 AND runMode <> 3 and runMode <> 7) {
                    playSounds("direction", settings).
                    SET targetSpeed TO setSpeed(MAX(1, settings["DefaultSpeed"] / 6), 3, lastEvent, targetSpeed).
                    SET runMode TO 3.
                } ELSE IF (runMode = 3 AND Time:SECONDS - lastEvent[3] > 10 AND ABS(headingDifference) < 3 AND grid:DISTANCE > MAX(30, stopDistance)){
                    SET targetSpeed TO restoreSpeed(lastEvent, settings).
                    SET runMode To 0.
                }
                IF (grid:DISTANCE < MAX(30, stopDistance)) {
                    DECLARE LOCAL returnValues TO nextWaypoint(routeWaypoint, route).
                    SET grid TO returnValues["grid"].
                    SET routeWaypoint TO returnValues["routeWaypoint"].
                    SET runMode TO 3.
                }
            }
            IF (runMode <> 7) {
                IF (navpoints:LENGTH <> 0 AND routeWaypoint = route:LENGTH - 1 AND route[routeWaypoint]:DISTANCE < 95 AND (TIME:SECONDS - lastEvent[0]) > 15) {
                    SET targetSpeed TO 0.
                    SET routeWaypoint TO -1.
                    SET runMode TO 0.
                    DECLARE LOCAL gl TO navpoints[0].
                    navpoints:REMOVE(0).
                    SET route TO aStar(settings, "LATLNG", gl, false).
                    DECLARE LOCAL returnValues TO startNavigation(header, settings, route, lastEvent).
                    IF (returnValues:LENGTH > 0) {
                        SET routeWaypoint TO returnValues["routeWaypoint"].
                        SET grid TO returnValues["grid"].
                        SET runMode TO returnValues["runMode"].
                        SET targetSpeed TO returnValues["targetSpeed"].
                    }
                }
                IF (ROUND(GROUNDSPEED, 1) = 0.0 AND ABS(targetSpeed) > 0 AND TIME:SECONDS - lastEvent[0] > 10) {
                    // Hit an obstacle
                    SET runMode TO 5.
                    SET lastEvent[5] TO TIME:SECONDS.
                } ELSE {
                    IF (grid:DISTANCE < 5 + stopDistance AND ABS(nextWaypointHeading) > 40 AND runMode <> 1) {
                        // Big turn as we go to next waypoint
                        SET targetSpeed TO setSpeed(MAX(1, settings["DefaultSpeed"] / 4), 1, lastEvent, targetSpeed).
                        SET runMode TO 1.
                    }
                    IF (ABS(angleDiff) > 5 AND runMode <> 2 AND GROUNDSPEED > 2) {
                        // Predicted slope change angle
                        SET targetSpeed TO setSpeed(MAX(1, settings["DefaultSpeed"] / 4), 2, lastEvent, targetSpeed).
                        SET runMode TO 2.
                        playSounds("slopealert", settings).
                    }
                }
                IF (runMode <> 0) {
                    IF (runMode = 2 AND ABS(angleDiff) < 5 AND TIME:SECONDS - lastEvent[2] > 2) {
                        // No more slope change
                        SET targetSpeed TO restoreSpeed(lastEvent, settings).
                        SET runMode To 0.
                    } ELSE IF (runMode = 5 AND ROUND(GROUNDSPEED, 1) = 0.0 AND (TIME:SECONDS - lastEvent[5]) > 10) {
                        // Hit obstacle, changing direction
                        playSounds("alert", settings).
                        SET targetSpeed TO MAX(1, settings["DefaultSpeed"] / 4).
                        SET directionOT TO -1.
                        LOCK targetHeading TO (grid:HEADING - 90).
                        SET lastEvent[5] TO TIME:SECONDS.
                    } ELSE IF (runMode = 5 AND ROUND(GROUNDSPEED, 1) > 0.0 AND (TIME:SECONDS - lastEvent[5]) > 10) {
                        // Hit obstacle, now going forward
                        SET targetSpeed TO setSpeed(0, 6, lastEvent, targetSpeed).
                        SET runMode TO 6.
                        SET directionOT TO 1.
                    } ELSE IF (runMode = 6 AND round(GROUNDSPEED, 1) = 0.0 AND (TIME:SECONDS - lastEvent[6]) > 10) {
                        // Moving around obstacle
                        SET targetSpeed TO setSpeed(MAX(1, settings["DefaultSpeed"] / 4), 6, lastEvent, targetSpeed).
                        SET runMode TO 6.
                    } ELSE IF (runMode = 6 AND ROUND(GROUNDSPEED,1) > 0.0 AND (TIME:SECONDS - lastEvent[6]) > 15) {
                        // Moved around obstacle
                        LOCK targetHeading TO grid:HEADING.
                        SET runMode TO 3.
                    }
                }

                IF (angle < -4 AND runMode = 0) {
                    SET targetSpeed TO getSlopeSpeed(settings, gradient, frictionCoefficientList).
                }
            }
            IF (targetSpeed = 0 AND ROUND(GROUNDSPEED, 1) = 0) {
                SET wheelThrottle TO 0.
                BRAKES ON.
            } ELSE {
                IF (NOT brakesOn) {
                    BRAKES OFF.
                }
                IF (targetSpeed > settings["DefaultSpeed"]) {
                    SET targetSpeed TO settings["DefaultSpeed"].
                }
                IF (runMode <> 7 OR GROUNDSPEED > 0) {
                    SET wheelPID:SETPOINT TO targetSpeed.
                    SET wheelThrottle TO wheelPID:UPDATE(TIME:SECONDS, GROUNDSPEED * directionOT).

                    // Stop rollback at slow speeds
                    IF (ABS(GROUNDSPEED) < 2 and runMode <> 0 AND wheelThrottle < 0.2) {
                        SET wheelThrottle to MIN(0.2, MAX(-0.2, wheelThrottle)).
                    }
                    // If rover is rolling back then invert wheelThrottle to make it go forwards
                    IF (targetSpeed > 0 AND dir < 0 AND wheelThrottle < 0 AND directionOT = 1) {
                        SET wheelThrottle TO -wheelThrottle.
                    }
                    // Slow down rover accelleration to stop it from wheelying in low gravity
                    IF (GROUNDSPEED < ABS(targetSpeed) * 0.5 OR GROUNDSPEED <= settings["DefaultSpeed"] / 3) {
                        SET wheelThrottle TO MIN(wheelThrottle, MAX(0.5, 0.5 * GROUNDSPEED)).
                    }
                    // Going up a real steep slope
                    IF (ABS(currentSlopeAngle) > 10 AND GROUNDSPEED < settings["DefaultSpeed"] / 3) {
                        SET wheelThrottle TO wheelThrottle + (0.02 * ABS(currentSlopeAngle)).
                    }
                } ELSE {
                    SET wheelThrottle TO 0.
                }
            }

            IF (runMode <> 7) {
                DECLARE LOCAL errorSteering TO targetHeading - currentHeading.
                // Make sure the headings make sense
                IF (errorSteering > 180) {
                    SET errorSteering TO errorSteering - 360.
                } ELSE IF (errorSteering < -180) {
                    SET errorSteering TO errorSteering + 360.
                }
                DECLARE LOCAL desiredSteering TO -errorSteering / 20.
                SET wheelSteer TO MIN(1, MAX(-1, desiredSteering)) * turnLimit.
            } ELSE {
                SET wheelSteer TO turnLimit * SHIP:CONTROL:PILOTWHEELSTEER.
            }

            // Borrowed from gaiiden / RoverDriver https://github.com/Gaiiden/RoverDriver/blob/master/begindrive.txt
            IF (ABS(GROUNDSPEED) > ABS(targetSpeed * 1.1)) {
                IF (NOT brakesOn) {
                    SET brakesOn TO TRUE.
                    BRAKES on.
                    SET brakeUseCount TO brakeUseCount + 1.
                    IF (ABS(currentSlopeAngle) > extremeSlopeAngle) {
                        SET currentBrakeTime TO overSpeedDownSlopeBrakeTime + extraBrakeTime.
                    }
                    SET lastBrake TO TIME:SECONDS.
                }
            }

            // Do we need to disable the brakes?
            IF (brakesOn AND (ABS(GROUNDSPEED) <= ABS(targetSpeed) OR TIME:SECONDS - lastBrake >= currentBrakeTime) AND targetSpeed <> 0) {
                BRAKES OFF.
                SET brakesOn TO FALSE.
            }

            IF (wpm:LENGTH <> 0) {
                navLines().
            }

            // Handle User Input using action groups
            IF (TERMINAL:INPUT:HASCHAR) {
                DECLARE LOCAL soundToPlay TO "blip".
                DECLARE LOCAL char TO TERMINAL:INPUT:GETCHAR().
                IF (menu = 5) {
                    SET TERMINAL:WIDTH TO 50.
                    SET TERMINAL:HEIGHT TO 40.
                    SET TERMINAL:CHARHEIGHT TO 12.
                    SET menu TO 0.
                    displayHUD(header, settings).
                }
                DECLARE LOCAL number TO char:TONUMBER(-99).
                IF (char = TERMINAL:INPUT:UPCURSORONE) {
                    SET goal TO LATLNG(goal:LAT + settings["Increments"], goal:LNG).
                    navMarker().
                } ELSE IF (char = TERMINAL:INPUT:DOWNCURSORONE) {
                    SET goal TO LATLNG(goal:LAT-settings["Increments"],goal:LNG).
                    navMarker().
                } ELSE IF (char = TERMINAL:INPUT:LEFTCURSORONE) {
                    IF (menu = 1) {
                        SET page TO page - 1.
                        CLEARSCREEN.
                    } ELSE {
                        SET goal TO LATLNG(goal:LAT, goal:LNG - settings["Increments"]).
                        navMarker().
                    }
                } ELSE IF (char = TERMINAL:INPUT:RIGHTCURSORONE) {
                    IF (menu = 1) {
                        SET page TO page + 1.
                        CLEARSCREEN.
                    } ELSE {
                        SET goal TO LATLNG(goal:LAT, goal:LNG + settings["Increments"]).
                        navMarker().
                    }
                } ELSE IF (char = TERMINAL:INPUT:RETURN) {
                    IF (navpoints:LENGTH = 0) {
                        SET route TO aStar(settings, "LATLNG", goal, FALSE).
                    } ELSE {
                        SET route TO aStar(settings, "LATLNG", navpoints[0], FALSE).
                        navpoints:REMOVE(0).
                    }
                    DECLARE LOCAL returnValues TO startNavigation(header, settings, route, lastEvent).
                    IF (returnValues:LENGTH > 0) {
                        SET routeWaypoint TO returnValues["routeWaypoint"].
                        SET grid TO returnValues["grid"].
                        SET runMode TO returnValues["runMode"].
                        SET targetSpeed TO returnValues["targetSpeed"].
                    }
                    SET header TO "---{   Navigating to LAT " + ROUND(goal:LAT, 1) +" : LNG " + ROUND(goal:LNG, 1) +  "   }---" + whitespace + whitespace.
                    PRINT header AT (2,1).
                } ELSE IF (char = TERMINAL:INPUT:PAGEUPCURSOR) {
                    SET settings["DefaultSpeed"] TO settings["DefaultSpeed"] + 0.5.
                    SET targetSpeed TO settings["DefaultSpeed"].
                    updateSetting("DefaultSpeed", targetSpeed).
                } ELSE IF (char = TERMINAL:INPUT:PAGEDOWNCURSOR) {
                    SET settings["DefaultSpeed"] TO settings["DefaultSpeed"] - 0.5.
                    SET targetSpeed TO settings["DefaultSpeed"].
                    updateSetting("DefaultSpeed", targetSpeed).
                } ELSE IF (char = TERMINAL:INPUT:HOMECURSOR) {
                    IF (menu <> 0) {
                        SET TERMINAL:CHARHEIGHT TO 12.
                        SET runMode TO 0.
                        displayHUD(header, settings).
                        SET menu TO 0.
                    } ELSE {
                        SET goal TO SHIP:GEOPOSITION.
                        navMarker().
                        SET targetSpeed TO 0.
                        SET route TO LIST().
                        SET routeWaypoint TO -1.
                        SET targetSpeed TO holdPosition(0, lastEvent, targetSpeed).
                        SET brakesOn TO TRUE.
                        SET runMode TO 0.
                    }
                } ELSE IF (char = TERMINAL:INPUT:ENDCURSOR) {
                    SET runMode TO -1.
                    CLEARVECDRAWS().
                    SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
                } ELSE IF (char:TOUPPER = "I") {
                    navpoints:ADD(goal).
                    waypointMarker().
                } ELSE IF (char:TOUPPER = "W") {
                    CLEARSCREEN.
                    SET menu TO 1.
                    SET page TO 0.
                    DECLARE LOCAL waypoints TO ALLWAYPOINTS().
                    SET contractWayPoints TO LEXICON().
                    SET contractWayPointNumbers TO LEXICON().
                    DECLARE LOCAL count TO 0.
                    FOR waypoint IN waypoints {
                        IF (waypoint:BODY:NAME = BODY:NAME) {
                            SET contractWayPointNumbers[count] TO waypoint:NAME.
                            SET contractWayPoints[waypoint:NAME] TO waypoint:GEOPOSITION.
                            SET count TO count + 1.
                        }
                    }
                    SET contractWayPoints["Custom"] TO SHIP:GEOPOSITION.
                    SET contractWayPointNumbers[contractWayPointNumbers:LENGTH] TO "Custom".
                } ELSE IF (char:TOUPPER = "C") {
                    setupMain().
                    SET settings TO READJSON("1:/config/settings.json").
                    COPYPATH("1:/config/settings.json","0:/astar-rover/backup/" + SHIP:NAME + ".json").
                    displayHUD(header, settings).
                } ELSE IF (char:TOUPPER = "S") {
                    SET menu TO 2.
                    scienceMenu().
                } ELSE IF (char:TOUPPER = "R") {
                    SET menu TO 0.
                    displayHUD(header, settings).
                } ELSE IF (char:TOUPPER = "H") {
                    IF (runMode = 7) {
                        SET runMode to 0.
                        SET targetSpeed TO restoreSpeed(lastEvent, settings).
                    } ELSE {
                        BRAKES ON.
                        SET targetSpeed TO 0.
                        SET brakesOn TO TRUE.
                        SET runMode TO 7.
                    }
                } ELSE IF (char:TOUPPER = "V") {
                    CLEARVECDRAWS().
                } ELSE IF (char:TOUPPER = "L") {
                    IF (wpm:LENGTH <> 0) {
                        CLEARVECDRAWS().
                        SET wpm TO LIST().
                    } ELSE {
                        navLines().
                    }
                } ELSE IF (char:TOUPPER = "M") {
                    loadMap().
                } ELSE IF (char:TOUPPER = "N") {
                    DECLARE LOCAL returnValues TO nextWaypoint(routeWaypoint, route).
                    SET grid TO returnValues["grid"].
                    SET routeWaypoint TO returnValues["routeWaypoint"].
                    SET runMode TO 2.
                } ELSE IF (char:TOUPPER = "P") {
                    DECLARE LOCAL returnValues TO nextWaypoint(routeWaypoint, route, TRUE).
                    SET grid TO returnValues["grid"].
                    SET routeWaypoint TO returnValues["routeWaypoint"].
                    SET runMode TO 2.
                } ELSE IF (char = ",") {
                    SET KUNIVERSE:TIMEWARP:MODE TO "PHYSICS".
                    SET WARP TO 0.
                } ELSE IF (char = ".") {
                    SET KUNIVERSE:TIMEWARP:MODE TO "PHYSICS".
                    SET WARP TO 1.
                } ELSE IF (number <> -99) {
                    IF (menu = 2) {
                        doScience(number).
                        displayHUD(header, settings).
                        SET menu TO 0.
                    } ELSE IF (menu = 1) {
                        IF (number = 0) {
                            SET number TO 10.
                        }
                        SET number TO number + (page * 10).
                        IF (number <= contractWayPoints:LENGTH) {
                            DECLARE LOCAL waypointName TO contractWayPointNumbers[number - 1].
                            DECLARE LOCAL waypointPosition TO contractWayPoints[waypointName].
                            IF (waypointName = "Custom") {
                                DECLARE LOCAL coordinateResult TO coordinateView({}).
                                IF (NOT coordinateResult["Cancelled"]) {
                                    SET waypointPosition TO coordinateResult["Coordinates"].
                                }
                            }

                            SET route TO aStar(settings, "LATLNG", waypointPosition, FALSE).
                            SET menu TO 0.
                            IF (route:LENGTH <> 0) {
                                SET goal TO LATLNG(route[route:LENGTH - 1]:LAT, route[route:LENGTH - 1]:LNG).
                                DECLARE LOCAL returnValues TO startNavigation(header, settings, route, lastEvent).
                                IF (returnValues:LENGTH > 0) {
                                    SET routeWaypoint TO returnValues["routeWaypoint"].
                                    SET grid TO returnValues["grid"].
                                    SET runMode TO returnValues["runMode"].
                                    SET targetSpeed TO returnValues["targetSpeed"].
                                }
                                SET header TO "  ---{   Navigating to " + waypointName + "   }---" + whitespace.
                                PRINT header AT (0, 1).
                                CLEARVECDRAWS().
                            } ELSE {
                                displayHUD(header, settings).
                            }
                        }
                    }
                    PRINT whitespace + whitespace + whitespace + whitespace AT (2, TERMINAL:HEIGHT - 2).
                } ELSE {
                    SET soundToPlay TO "error".
                    PRINT "# UNKNOWN COMMAND # " AT (2, TERMINAL:HEIGHT - 2).
                }
                playSounds(soundToPlay, settings).
            }

            IF (targetHeading > 360) {
                SET targetHeading TO targetHeading - 360.
            } ELSE IF (targetHeading < 0) {
                SET targetHeading TO targetHeading + 360.
            }
        } ELSE {
            SET wheelThrottle TO 0.
            SET wheelSteer TO 0.
            BRAKES ON.
        }

        SET SHIP:CONTROL:WHEELTHROTTLE TO wheelThrottle.
        SET SHIP:CONTROL:WHEELSTEER TO wheelSteer.

        IF (menu = 1) {
            PRINT "Press a number to select a waypoint" AT (2, 3).
            IF (page < 0) {
                SET page TO FLOOR(contractWayPoints:LENGTH / 10).
            }
            IF (page > FLOOR(contractWayPoints:LENGTH / 10)) {
                SET page TO 0.
            }
            PRINT "Page " + (page + 1) + " of " + (CEILING(contractWayPoints:LENGTH / 10)) AT (4, 20).
            DECLARE LOCAL row TO 1.
            UNTIL (row = 11 OR (row + (page * 10) - 1) >= contractWayPoints:LENGTH) {
                DECLARE LOCAL contractWaypointName TO contractWayPointNumbers[row + (page * 10) - 1].
                DECLARE LOCAL contractWaypointPosition TO contractWayPoints[contractWaypointName].
                PRINT "(" + row + ") " + contractWaypointName AT (4, row + 4).
                DECLARE LOCAL position TO contractWaypointPosition.
                PRINT " " + ROUND((position:DISTANCE / 1000), 2) + " km  " AT (30, row + 4).
                SET row TO row + 1.
                IF (row = 11) {
                    break.
                }
            }
        } ELSE IF (menu = 3) {
            CLEARSCREEN.
            center("---{    CONNECTION LOST    }---", 5).

            IF (TIME:SECONDS - lastEvent[0] > 0.3) {
                PRINT "Acquiring Signal " + (waitCursor[waitCount]) + whitespace AT (2, 10).
                SET waitCount TO waitCount + 1.
                if (waitCount = 4) {
                    SET waitCount TO 0.
                }
                SET lastEvent[0] TO TIME:SECONDS.
            }
        } ELSE IF (menu = 4) {
            CLEARSCREEN.
            center("---{    LOW POWER MODE    }---", 5).
            center(whitespace + "Remaining charge : " + ROUND( chargeLevel, 1) + "%" + whitespace, 8).
            displayBattery(11, chargeLevel).
        } ELSE IF (menu = 0) {
            PRINT ": " + ROUND(targetSpeed, 1) + " m/s" + whitespace AT (18, 6).
            PRINT ": " + ROUND(GROUNDSPEED, 1) + " m/s" + whitespace AT (18, 7).
            PRINT ": " + ROUND ((GROUNDSPEED * 18) / 5) + " km/h" + whitespace AT (30, 7).
            PRINT ": " + ROUND(goal:DISTANCE / 1000, 2) + " km" + whitespace AT (18, 8).

            PRINT ": " + ROUND(targetHeading, 2) + whitespace AT (18, 10).
            PRINT ": " + ROUND(currentHeading, 2) + whitespace AT (18, 11).

            IF (DEFINED route AND route:LENGTH <> 0 AND routeWaypoint <> route:LENGTH - 1 AND routeWaypoint <> -1) {
                PRINT ": " + ROUND(ABS(route[routeWaypoint + 1]:HEADING)) + whitespace AT (18, 12).
                PRINT ": " + ROUND(nextWaypointHeading) + whitespace AT (18, 13).
            }
            PRINT ": " +  ROUND(grid:DISTANCE, 2) + whitespace AT (18, 15).
            PRINT ": " +  route:LENGTH + whitespace AT (18, 16).
            PRINT ": " +  (routeWaypoint + 1) + whitespace AT (18, 17).

            PRINT ": " + ROUND(currentSlopeAngle, 2) + whitespace AT (18, 20).
            PRINT ": " + ROUND(angle, 2) + whitespace AT (18, 21).
            PRINT ": " + ROUND(angleDiff, 2) + whitespace AT (18, 22).
            PRINT ": " + ROUND(90 - VECTORANGLE(UP:VECTOR, SHIP:FACING:STARVECTOR)) + whitespace AT (37, 20).

            PRINT ": " + ROUND(stopDistance, 2) + whitespace AT (18, 24).
            PRINT ": " + ROUND(gradient, 4) + whitespace AT (18, 25).

            PRINT ": " + runMode + whitespace AT (18, 27).

            PRINT ": " + ROUND( settings["Odometer"] / 1000, 1) + " km" + whitespace AT (18, 30).
            IF (lastCharge <= electricResource:AMOUNT) {
                PRINT ": +" AT (18, 31).
            } ELSE {
                PRINT ": -" AT (18, 31).
            }
            PRINT ROUND( chargeLevel, 1) + "%" + whitespace AT (22, 31).
            IF (ABS(GROUNDSPEED) > 0 AND targetSpeed <> 0) {
                PRINT ": " +  ROUND(getSlopeSpeed(settings, gradient, frictionCoefficientList), 2) + whitespace AT (18, 34).
            }

            PRINT ": " + ROUND(wheelThrottle, 3) + whitespace AT (37, 10).
            PRINT ": " + ROUND(wheelSteer, 3) + whitespace AT (37, 11).
            PRINT ": " + ROUND(turnLimit, 3) + whitespace AT (37, 12).
            PRINT ": " + ROUND(dir, 2) + whitespace AT (37, 13).
        }

        SET loopTime TO TIME:SECONDS - loopEndTime.
        SET loopEndTime TO TIME:SECONDS.

        SET settings["Odometer"] TO settings["Odometer"] + (GROUNDSPEED * loopTime).
        IF (TIME:SECONDS > nextWrite) {
            WRITEJSON(settings,"1:/config/settings.json").
            SET nextWrite TO TIME:SECONDS + 10.
        }
        WAIT 0.1. //  We only need to run an iteration once per physics tick. Ensure that we pause until the next tick.
    }
}

DECLARE LOCAL FUNCTION displayHUD {
    DECLARE PARAMETER header, settings.

    CLEARSCREEN.
    PRINT header AT (0, 1).
    PRINT "Target Speed" AT (2, 6).
    PRINT "Surface Speed" AT (2, 7).
    PRINT "Distance to goal" AT (2, 8).

    PRINT "Target Heading" AT (2, 10).
    PRINT "CurrentHeading" AT (2, 11).
    PRINT "Next Heading" AT (4, 12).
    PRINT "Next Bearing" AT (4, 13).

    PRINT "Waypoint Dist"  AT (2, 15).
    PRINT "Waypoints" AT (2, 16).
    PRINT "Current WP" AT (2, 17).

    PRINT "Current Angle" AT (2, 20).
    PRINT "Predicted Angle" AT (2, 21).
    PRINT "Roll" AT (32, 20).

    PRINT "Difference" AT (2, 22).
    PRINT "Stopping Dist" AT (2, 24).
    PRINT "Gradient" AT (2, 25).

    PRINT "Runmode" AT (2, 27).

    PRINT "Odometer" AT (2, 30).
    PRINT "Electric" AT (2, 31).

    PRINT "Slope Speed" AT (2, 34).
    PRINT "Throttle" AT (32, 10).
    PRINT "Steer" AT (32, 11).
    PRINT "Limit" AT (32, 12).
    PRINT "Dir" AT (32, 13).

    PRINT "Roverware Version : " + settings["Version"] AT (2, TERMINAL:HEIGHT - 1).
}

DECLARE LOCAL FUNCTION navMarker {
    DECLARE LOCAL vg TO VECDRAWARGS(
            goal:ALTITUDEPOSITION(goal:TERRAINHEIGHT + 1000),
                    goal:POSITION - goal:ALTITUDEPOSITION(goal:TERRAINHEIGHT + 1000),
                    GREEN, "", 1, TRUE, 50).
    center("---{   LAT " + ROUND(goal:LAT, 2) + " LNG " + ROUND(goal:LNG, 2) + "  }---         " + whitespace,1).
}

DECLARE LOCAL FUNCTION waypointMarker {
    wpm:ADD(VECDRAWARGS(
            goal:ALTITUDEPOSITION(goal:TERRAINHEIGHT + 800),
                    goal:POSITION - goal:ALTITUDEPOSITION(goal:TERRAINHEIGHT + 800),
                    BLUE, "", 1, TRUE, 50)).
}

DECLARE LOCAL FUNCTION navLines {
    DECLARE LOCAL c TO 0.
    DECLARE LOCAL wl TO FALSE.
    IF (wpm:LENGTH <> 0) {
        SET wl TO TRUE.
    }
    FOR w IN route {
        IF (c < route:LENGTH - 1) {
            IF (wl) {
                SET wpm[c]:START TO w:ALTITUDEPOSITION(w:TERRAINHEIGHT + 5).
                SET wpm[c]:VEC TO route[c+1]:POSITION - route[c + 1]:ALTITUDEPOSITION(route[c + 1]:TERRAINHEIGHT + 5).
            } ELSE {
                wpm:ADD(VECDRAWARGS(w:ALTITUDEPOSITION(w:TERRAINHEIGHT + 5), route[c + 1]:POSITION - route[c + 1]:ALTITUDEPOSITION(route[c + 1]:TERRAINHEIGHT + 5), GREEN)).
            }
            SET c TO c + 1.
        }
    }
}

DECLARE LOCAL FUNCTION startNavigation {
    DECLARE PARAMETER header, settings, route, lastEvent.

    DECLARE LOCAL returnValues TO LEXICON().

    SET TERMINAL:WIDTH TO 50.
    SET TERMINAL:HEIGHT TO 40.
    CLEARSCREEN.
    WRITEJSON(settings, "1:/config/settings.json").
    displayHUD(header, settings).
    IF (route:LENGTH <> 0) {
        DECLARE LOCAL grid TO LATLNG(route[1]:LAT, route[1]:LNG).
        SET returnValues["routeWaypoint"] TO 1.
        SET returnValues["grid"] TO grid.
        SET returnValues["runMode"] TO 0.
        LOCK targetHeading TO grid:HEADING.
        SET returnValues["targetSpeed"] TO restoreSpeed(lastEvent, settings).
        CLEARVECDRAWS().
    }
    RETURN returnValues.
}

DECLARE LOCAL FUNCTION setSpeed {
    DECLARE PARAMETER spd, mode, lastEvent, targetSpeed.
    IF (spd < targetSpeed) {
        SET targetSpeed TO spd.
        SET lastEvent[8] TO TIME:SECONDS.
    }
    SET lastEvent[mode] TO TIME:SECONDS.
    RETURN targetSpeed.
}

DECLARE LOCAL FUNCTION restoreSpeed {
    DECLARE PARAMETER lastEvent, settings.

    SET lastEvent[0] TO TIME:SECONDS.
    RETURN settings["DefaultSpeed"].
}

DECLARE LOCAL FUNCTION getStopDistance {
    DECLARE PARAMETER speed, gradient, frictionCoefficientList.

    DECLARE LOCAL constGravity TO BODY:mu / BODY:RADIUS ^ 2.
    RETURN speed ^ 2 / (2 * constGravity * frictionCoefficientList[BODY:NAME] + gradient).
}

DECLARE LOCAL FUNCTION getSlopeSpeed {
    DECLARE PARAMETER settings, gradient, frictionCoefficientList.

    DECLARE LOCAL constGravity TO BODY:mu / BODY:RADIUS ^ 2.

    RETURN SQRT(MAX(settings["DefaultSpeed"] / 2,
            ABS(getStopDistance(settings["DefaultSpeed"], 0, frictionCoefficientList)) * (2 * constGravity * frictionCoefficientList[BODY:NAME] + gradient))).
}

DECLARE LOCAL FUNCTION nextWaypoint {
    DECLARE PARAMETER routeWaypoint, route, back TO FALSE.
    DECLARE LOCAL returnValues TO LEXICON().

    IF (NOT back) {
        SET routeWaypoint TO routeWaypoint + 1.
    }
    IF (routeWaypoint >= route:LENGTH OR (routeWaypoint > 0 AND back)) {
        SET routeWaypoint TO routeWaypoint - 1.
    }
    DECLARE LOCAL grid TO LATLNG(route[routeWaypoint]:LAT, route[routeWaypoint]:LNG).
    LOCK targetHeading TO grid:HEADING.
    SET returnValues["grid"] TO grid.
    SET returnValues["routeWaypoint"] TO routeWaypoint.
    RETURN returnValues.
}

DECLARE LOCAL FUNCTION holdPosition {
    DECLARE PARAMETER mode, lastEvent, targetSpeed.
    BRAKES ON.
    SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
    RETURN setSpeed(0, mode, lastEvent, targetSpeed).
}

DECLARE LOCAL FUNCTION restoreOperations {
    DECLARE PARAMETER header, settings, route, lastEvent.

    CLEARSCREEN.
    displayHUD(header, settings).
    savePower(TRUE).

    DECLARE LOCAL returnValues TO LEXICON().
    IF (route:LENGTH <> 0) {
        SET returnValues["targetSpeed"] TO restoreSpeed(lastEvent, settings).
        SET returnValues["brakesOn"] TO  FALSE.
        SET WARP TO 0.
    }
    SET lastEvent[0] TO TIME:SECONDS.

    RETURN returnValues.
}

DECLARE LOCAL FUNCTION loadMap {
    SET menu TO 5.
    DECLARE LOCAL map TO READJSON("0:/astar-rover/backup/map.json").
    DECLARE LOCAL mapLength TO map:LENGTH.
    IF (mapLength > 160) {
        SET TERMINAL:WIDTH TO (mapLength / 1.5) + 10.
        SET TERMINAL:HEIGHT TO (mapLength / 1.5) + 10.
        SET TERMINAL:CHARHEIGHT TO 6.
    } ELSE {
        SET TERMINAL:WIDTH TO (mapLength) + 10.
        SET TERMINAL:HEIGHT TO (mapLength) + 10.
    }
    DECLARE LOCAL y TO 0.
    FOR row IN map {
        PRINT row AT (0, y).
        SET y TO y + 1.
    }
}

DECLARE LOCAL FUNCTION savePower {
    DECLARE PARAMETER op TO FALSE.

    DECLARE LOCAL ev TO "DEACTIVATE".
    IF (OP) {
        SET ev TO "ACTIVATE".
    }
    SET P TO SHIP:PARTSDUBBEDPATTERN("Communotron").
    FOR A IN P {
        IF A:HASMODULE("ModuleRTAntenna") {
            A:GETMODULE("ModuleRTAntenna"):DOEVENT(ev).
        }
    }
}
