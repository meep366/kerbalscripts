DECLARE FUNCTION executeRSVP {
    DECLARE PARAMETER destination, options, updateGui.

    DECLARE LOCAL result TO getSuccessfulRSVP(destination, options, updateGui).

    DECLARE LOCAL doTransfer TO confirmTransfer(updateGui).
    IF (doTransfer) {
        WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
        KUNIVERSE:QUICKSAVETO("quicksaveTransferEject").

        IF (HASNODE) {
            executeNode(NEXTNODE, updateGui).
        }
        IF (HASNODE) {
            DECLARE LOCAL doTransfer TO confirmTransfer(updateGui).
            IF (doTransfer) {
                WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
                KUNIVERSE:QUICKSAVETO("quicksaveTransferInsert").
                executeNode(NEXTNODE, updateGui).
            } ELSE {
                FOR node IN ALLNODES {
                    REMOVE node.
                }
            }
        }
    } ELSE {
        FOR node IN ALLNODES {
            REMOVE node.
        }
    }
}

DECLARE FUNCTION getBodyTransferOptions {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL guiTransferOptions TO GUI(300).
    SET guiTransferOptions:X TO 1570.
    SET guiTransferOptions:Y TO 350.
    DECLARE LOCAL labelTransferOptions TO guiTransferOptions:ADDLABEL("<size=20><b>Transfer Options</b></size>").
    SET labelTransferOptions:STYLE:ALIGN TO "CENTER".
    SET labelTransferOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL targetPeriapsis TO 100000.
    DECLARE LOCAL orbitType TO "Elliptical".
    DECLARE LOCAL orbitTypes TO LIST("Elliptical", "Circular", "None").
    DECLARE LOCAL orbitOrientation TO "Prograde".
    DECLARE LOCAL orbitOrientations TO LIST("Prograde", "Polar", "Retrograde").
    DECLARE LOCAL createNodes TO "Both".
    DECLARE LOCAL createNodeOptions TO LIST("Both", "First", "None").
    DECLARE LOCAL verbose TO "True".
    DECLARE LOCAL verboseOptions TO LIST("True", "False").
    DECLARE LOCAL searchInterval TO 0.
    DECLARE LOCAL searchDuration TO 0.

    DECLARE LOCAL finalPeriapsisText TO createGuiText(guiTransferOptions, "Final Orbit Periapsis", targetPeriapsis:TOSTRING, 80).
    DECLARE LOCAL finalOrbitTypeDropdown TO createGuiDropdown(guiTransferOptions, "Final Orbit Type", orbitTypes, 80).
    DECLARE LOCAL finalOrbitOrientationDropdown TO createGuiDropdown(guiTransferOptions, "Final Orbit Orientation", orbitOrientations, 80).
    DECLARE LOCAL createNodesDropdown TO createGuiDropdown(guiTransferOptions, "Create Manuever Nodes", createNodeOptions, 80).
    DECLARE LOCAL verboseDropdown TO createGuiDropdown(guiTransferOptions, "Verbose", verboseOptions, 80).
    DECLARE LOCAL searchIntervalText TO createGuiText(guiTransferOptions, "Search Interval", searchInterval:TOSTRING, 80).
    DECLARE LOCAL searchDurationText TO createGuiText(guiTransferOptions, "Search Duration", searchDuration:TOSTRING, 80).

    DECLARE LOCAL transferButton TO guiTransferOptions:ADDBUTTON("Transfer").
    DECLARE LOCAL cancelButton TO guiTransferOptions:ADDBUTTON("Cancel").
    DECLARE LOCAL isDone TO FALSE.

    DECLARE LOCAL options TO LEXICON().
    DECLARE LOCAL FUNCTION getTransferOptions {
        SET targetPeriapsis TO finalPeriapsisText:TEXT:TONUMBER(targetPeriapsis).
        SET orbitType TO finalOrbitTypeDropdown:VALUE.
        SET orbitOrientation TO finalOrbitOrientationDropdown:VALUE.
        SET createNodes TO createNodesDropdown:VALUE.
        SET verbose TO verboseDropdown:VALUE.
        IF (verbose = "True" ) {
            SET verbose TO TRUE.
        } ELSE {
            SET verbose TO FALSE.
        }
        SET searchInterval TO searchIntervalText:TEXT:TONUMBER(searchInterval).
        SET searchDuration TO searchDurationText:TEXT:TONUMBER(searchDuration).

        options:ADD("Cancelled", FALSE).
        options:ADD("final_orbit_periapsis", targetPeriapsis).
        options:ADD("final_orbit_type", orbitType).
        options:ADD("final_orbit_orientation", orbitOrientation).
        options:ADD("create_maneuver_nodes", createNodes).
        options:ADD("verbose", verbose).
        if (searchInterval <> 0) {
            options:ADD("search_interval", searchInterval).
        }
        if (searchDuration <> 0) {
            options:ADD("search_duration", searchDuration).
        }

        SET isDone TO TRUE.
    }

    SET transferButton:ONCLICK TO getTransferOptions@.
    SET cancelButton:ONCLICK TO {
        SET isDone TO TRUE.
        options:ADD("Cancelled", TRUE).
    }.

    guiTransferOptions:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }
    guiTransferOptions:DISPOSE().
    RETURN options.
}

DECLARE FUNCTION getVesselTransferOptions {
    DECLARE PARAMETER toParentVessel, updateGui.

    DECLARE LOCAL guiTransferOptions TO GUI(300).
    SET guiTransferOptions:X TO 1570.
    SET guiTransferOptions:Y TO 350.
    DECLARE LOCAL labelTransferOptions TO guiTransferOptions:ADDLABEL("<size=20><b>Transfer Options</b></size>").
    SET labelTransferOptions:STYLE:ALIGN TO "CENTER".
    SET labelTransferOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL parentPeriapsis TO 100000.
    DECLARE LOCAL closestApproachCutoff TO 200.
    DECLARE LOCAL doCorrections TO "True".
    DECLARE LOCAL correctionOptions TO LIST("True", "False").
    DECLARE LOCAL angleToCorrect TO 1.
    DECLARE LOCAL timeToImpactBuffer TO 1.

    DECLARE LOCAL parentPeriapsisText TO 0.
    DECLARE LOCAL closestApproachCutoffText TO 0.
    DECLARE LOCAL correctionsDropdown TO 0.
    IF (toParentVessel) {
        SET parentPeriapsisText TO createGuiText(guiTransferOptions, "Parent Periapsis", parentPeriapsis:TOSTRING).
        SET closestApproachCutoffText TO createGuiText(guiTransferOptions, "Approach Cutoff", closestApproachCutoff:TOSTRING).
        SET correctionsDropdown TO createGuiDropdown(guiTransferOptions, "Do Corrections", correctionOptions).
    }
    DECLARE LOCAL angleToCorrectText TO createGuiText(guiTransferOptions, "Min Angle to Correct", angleToCorrect:TOSTRING).
    DECLARE LOCAL timeToImpactText TO createGuiText(guiTransferOptions, "Time to Impact Buffer", timeToImpactBuffer:TOSTRING).

    DECLARE LOCAL transferButton TO guiTransferOptions:ADDBUTTON("Transfer").
    DECLARE LOCAL cancelButton TO guiTransferOptions:ADDBUTTON("Cancel").
    DECLARE LOCAL isDone TO FALSE.

    DECLARE LOCAL options TO LEXICON().
    DECLARE LOCAL FUNCTION getTransferOptions {
        SET angleToCorrect TO angleToCorrectText:TEXT:TONUMBER(angleToCorrect).
        SET timeToImpactBuffer TO timeToImpactText:TEXT:TONUMBER(timeToImpactBuffer).
        IF (toParentVessel) {
            SET parentPeriapsis TO parentPeriapsisText:TEXT:TONUMBER(parentPeriapsis).
            SET closestApproachCutoff TO closestApproachCutoffText:TEXT:TONUMBER(closestApproachCutoff).
            SET doCorrections TO correctionsDropdown:VALUE.
            IF (doCorrections = "True" ) {
                SET doCorrections TO TRUE.
            } ELSE {
                SET doCorrections TO FALSE.
            }
            options:ADD("parentPeriapsis", parentPeriapsis).
            options:ADD("closestApproachCutoff", closestApproachCutoff).
            options:ADD("doCorrections", doCorrections).
        }
        options:ADD("Cancelled", FALSE).
        options:ADD("angleToCorrect", angleToCorrect).
        options:ADD("timeToImpactBuffer", timeToImpactBuffer).

        SET isDone TO TRUE.
    }

    SET transferButton:ONCLICK TO getTransferOptions@.
    SET cancelButton:ONCLICK TO {
        SET isDone TO TRUE.
        options:ADD("Cancelled", TRUE).
    }.

    guiTransferOptions:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }
    guiTransferOptions:DISPOSE().
    RETURN options.
}

DECLARE FUNCTION confirmTransfer {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL gui TO GUI(200).
    SET gui:X TO 1640.
    SET gui:Y TO 350.

    DECLARE LOCAL label IS gui:ADDLABEL("Confirm Transfer?").
    SET label:STYLE:ALIGN TO "CENTER".
    SET label:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL yesButton TO gui:ADDBUTTON("Yes").
    DECLARE LOCAL noButton TO gui:ADDBUTTON("No").

    DECLARE LOCAL doTransfer TO FALSE.
    DECLARE LOCAL isDone TO FALSE.
    SET yesButton:ONCLICK TO {
        gui:DISPOSE().
        SET doTransfer TO TRUE.
        SET isDone TO TRUE.
    }.
    SET noButton:ONCLICK TO {
        gui:DISPOSE().
        SET doTransfer TO FALSE.
        SET isDone TO TRUE.
    }.

    gui:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    RETURN doTransfer.
}

DECLARE FUNCTION getTargetOrbitParameters {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL guiOrbitParameters TO GUI(300).
    SET guiOrbitParameters:X TO 1570.
    SET guiOrbitParameters:Y TO 350.
    DECLARE LOCAL labelLaunchOptions TO guiOrbitParameters:ADDLABEL("<size=20><b>Orbit Options</b></size>").
    SET labelLaunchOptions:STYLE:ALIGN TO "CENTER".
    SET labelLaunchOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL targetApoapsis TO 100000.
    DECLARE LOCAL targetPeriapsis TO 100000.
    DECLARE LOCAL targetInclination TO 0.
    DECLARE LOCAL targetLongitude TO 0.
    DECLARE LOCAL targetArgumentPeriapsis TO 0.
    DECLARE LOCAL epoch TO 0.
    DECLARE LOCAL meanAnomalyAtEpoch TO 0.
    DECLARE LOCAL searchInterval TO 0.
    DECLARE LOCAL searchDuration TO 0.
    DECLARE LOCAL targetBody TO BODY.

    DECLARE LOCAL apoapsisText TO createGuiText(guiOrbitParameters, "Apoapsis", targetApoapsis:TOSTRING).
    DECLARE LOCAL periapsisText TO createGuiText(guiOrbitParameters, "Periapsis", targetPeriapsis:TOSTRING).
    DECLARE LOCAL orbitInclinationText TO createGuiText(guiOrbitParameters, "Orbit Inclination", targetInclination:TOSTRING).
    DECLARE LOCAL longitudeText TO createGuiText(guiOrbitParameters, "Longitude of Ascending Node", targetLongitude:TOSTRING).
    DECLARE LOCAL argumentPeriapsisText TO createGuiText(guiOrbitParameters, "Argument of Periapsis", targetArgumentPeriapsis:TOSTRING).
    DECLARE LOCAL epochText TO createGuiText(guiOrbitParameters, "Epoch", epoch:TOSTRING).
    DECLARE LOCAL meanAnomalyText TO createGuiText(guiOrbitParameters, "Mean Anomaly at Epoch", meanAnomalyAtEpoch:TOSTRING).
    DECLARE LOCAL bodyText TO createGuiText(guiOrbitParameters, "Body", targetBody:NAME).
    DECLARE LOCAL searchIntervalText TO createGuiText(guiOrbitParameters, "Search Interval", searchInterval:TOSTRING).
    DECLARE LOCAL searchDurationText TO createGuiText(guiOrbitParameters, "Search Duration", searchDuration:TOSTRING).

    DECLARE LOCAL isDone TO FALSE.
    DECLARE LOCAL targetOrbitParameters TO LEXICON().
    DECLARE LOCAL orbitButton TO guiOrbitParameters:ADDBUTTON("Orbit").
    DECLARE LOCAL cancelButton TO guiOrbitParameters:ADDBUTTON("Cancel").

    DECLARE LOCAL FUNCTION goToOrbit {
        SET targetApoapsis TO apoapsisText:TEXT:TONUMBER(targetApoapsis).
        SET targetPeriapsis TO periapsisText:TEXT:TONUMBER(targetPeriapsis).
        SET targetInclination TO orbitInclinationText:TEXT:TONUMBER(targetInclination).
        SET targetLongitude TO longitudeText:TEXT:TONUMBER(targetLongitude).
        SET targetArgumentPeriapsis TO argumentPeriapsisText:TEXT:TONUMBER(targetArgumentPeriapsis).
        SET epoch TO epochText:TEXT:TONUMBER(epoch).
        SET meanAnomalyAtEpoch TO meanAnomalyText:TEXT:TONUMBER(meanAnomalyAtEpoch).
        SET targetBody TO BODY(bodyText:TEXT).
        SET searchInterval TO searchIntervalText:TEXT:TONUMBER(searchInterval).
        SET searchDuration TO searchDurationText:TEXT:TONUMBER(searchDuration).

        DECLARE LOCAL eccentricity TO (targetApoapsis - targetPeriapsis) / (targetApoapsis + targetPeriapsis + 2 * BODY:RADIUS).
        DECLARE LOCAL semiMajorAxis TO targetBody:RADIUS + (targetApoapsis + targetPeriapsis) / 2.

        DECLARE LOCAL targetOrbit TO CREATEORBIT(targetInclination, eccentricity, semiMajorAxis, targetLongitude, targetArgumentPeriapsis,
                meanAnomalyAtEpoch, epoch, targetBody).

        targetOrbitParameters:ADD("Cancelled", FALSE).
        targetOrbitParameters:ADD("targetOrbit", targetOrbit).
        targetOrbitParameters:ADD("searchInterval", searchInterval).
        targetOrbitParameters:ADD("searchDuration", searchDuration).

        SET isDone TO TRUE.
    }

    SET orbitButton:ONCLICK TO goToOrbit@.
    SET cancelButton:ONCLICK TO {
        SET isDone TO TRUE.
        targetOrbitParameters:ADD("Cancelled", TRUE).
    }.

    guiOrbitParameters:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    guiOrbitParameters:DISPOSE().

    RETURN targetOrbitParameters.
}

DECLARE FUNCTION getDeorbitPeriapsis {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL guiDeorbitOptions TO GUI(300).
    SET guiDeorbitOptions:X TO 1570.
    SET guiDeorbitOptions:Y TO 350.
    DECLARE LOCAL labelDeorbitOptions IS guiDeorbitOptions:ADDLABEL("<size=20><b>Deorbit Options</b></size>").
    SET labelDeorbitOptions:STYLE:ALIGN TO "CENTER".
    SET labelDeorbitOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL targetPeriapsis TO 25000.
    DECLARE LOCAL deorbitPeriapsisText TO createGuiText(guiDeorbitOptions, "Periapsis Altitude", targetPeriapsis:TOSTRING).

    DECLARE LOCAL deorbitButton TO guiDeorbitOptions:ADDBUTTON("Deorbit").
    DECLARE LOCAL cancelButton TO guiDeorbitOptions:ADDBUTTON("Cancel").
    DECLARE LOCAL isDone TO FALSE.

    SET deorbitButton:ONCLICK TO {
        SET targetPeriapsis TO deorbitPeriapsisText:TEXT:TONUMBER(targetPeriapsis).
        IF (targetPeriapsis > BODY:ATM:HEIGHT OR targetPeriapsis < 0) {
            headsUpText("Enter a periapsis below " + BODY:ATM:HEIGHT + "m and above 0m").
        } ELSE {
            SET isDone TO TRUE.
        }
    }.
    SET cancelButton:ONCLICK TO {
        SET isDone TO TRUE.
        SET targetPeriapsis TO -1.
    }.

    guiDeorbitOptions:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    guiDeorbitOptions:DISPOSE().

    RETURN targetPeriapsis.
}

DECLARE FUNCTION getFuelOptions {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL guiFuelOptions TO GUI(300).
    SET guiFuelOptions:X TO 1570.
    SET guiFuelOptions:Y TO 350.
    DECLARE LOCAL labelDeorbitOptions IS guiFuelOptions:ADDLABEL("<size=20><b>Fueling Options</b></size>").
    SET labelDeorbitOptions:STYLE:ALIGN TO "CENTER".
    SET labelDeorbitOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL requestedDeltaV TO 100.
    DECLARE LOCAL requestedMonopropUnits TO 0.
    DECLARE LOCAL shipDryMass TO 0.
    DECLARE LOCAL vacuumIsp TO 315.

    DECLARE LOCAL deltaVText TO createGuiText(guiFuelOptions, "Desired Delta V", requestedDeltaV:TOSTRING).
    DECLARE LOCAL monopropText TO createGuiText(guiFuelOptions, "Desired Monoprop", requestedMonopropUnits:TOSTRING).
    DECLARE LOCAL dryMassText TO createGuiText(guiFuelOptions, "Ship Dry Mass", shipDryMass:TOSTRING).
    DECLARE LOCAL ispText TO createGuiText(guiFuelOptions, "Engine Isp", vacuumIsp:TOSTRING).

    DECLARE LOCAL transferFuelButton TO guiFuelOptions:ADDBUTTON("Transfer Fuel").
    DECLARE LOCAL isDone TO FALSE.

    DECLARE LOCAL fuelOptions TO LEXICON().
    SET transferFuelButton:ONCLICK TO {
        SET requestedDeltaV TO deltaVText:TEXT:TONUMBER(requestedDeltaV).
        SET requestedMonopropUnits TO monopropText:TEXT:TONUMBER(requestedMonopropUnits).
        SET shipDryMass TO dryMassText:TEXT:TONUMBER(shipDryMass).
        SET vacuumIsp TO ispText:TEXT:TONUMBER(vacuumIsp).

        fuelOptions:ADD("requestedDeltaV", requestedDeltaV).
        fuelOptions:ADD("requestedMonopropUnits", requestedMonopropUnits).
        fuelOptions:ADD("shipDryMass", shipDryMass).
        fuelOptions:ADD("vacuumIsp", vacuumIsp).

        guiFuelOptions:DISPOSE().
        SET isDone TO TRUE.
    }.

    guiFuelOptions:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    RETURN fuelOptions.
}
