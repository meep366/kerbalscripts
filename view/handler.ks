@LAZYGLOBAL OFF.

DECLARE FUNCTION handleOption {
    DECLARE PARAMETER mainGui, option, updateGui.

    DECLARE LOCAL basicLaunchOption TO "Launch to basic orbit".
    DECLARE LOCAL customLaunchOption TO "Launch to custom orbit".
    DECLARE LOCAL targetLaunchOption TO "Launch to target".
    DECLARE LOCAL coordinateLaunchOption TO "Hop to coordinates".
    DECLARE LOCAL deorbitOption TO "Deorbit Craft".
    DECLARE LOCAL landOption TO "Land at Coordinates".
    DECLARE LOCAL transferOption TO "Transfer to target".
    DECLARE LOCAL customOrbitOption TO "Go to orbit".
    DECLARE LOCAL executeNodeOption TO "Execute node".
    DECLARE LOCAL dockOption TO "Dock with target".
    DECLARE LOCAL fuelOption TO "Transfer Fuel".

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveAction").

    IF (option = basicLaunchOption) {
        DECLARE LOCAL launchSettings TO getLaunchSettings(TRUE, updateGui).
        IF (NOT launchSettings["Cancelled"]) {
            launchToOrbit(updateGui, launchSettings["targetAltitude"], launchSettings["targetInclination"], launchSettings["targetPitchRate"]).
            deployPanelsAndAntennas().
        }
    } ELSE IF (option = customLaunchOption) {
        DECLARE LOCAL launchSettings TO getLaunchSettings(FALSE, updateGui).
        IF (NOT launchSettings["Cancelled"]) {
            DECLARE LOCAL apoapsis TO launchSettings["targetApoapsis"].
            DECLARE LOCAL periapsis TO launchSettings["targetPeriapsis"].
            DECLARE LOCAL inclination TO launchSettings["targetInclination"].
            DECLARE LOCAL longitude TO launchSettings["targetLongitude"].
            DECLARE LOCAL argumentPeriapsis TO launchSettings["targetArgumentOfPeriapsis"].
            DECLARE LOCAL pitchOverRate TO launchSettings["targetPitchRate"].
            DECLARE LOCAL safeHoldingAltitude TO launchSettings["targetHoldingAltitude"].

            launchToSpecificOrbit(updateGui, apoapsis, periapsis, inclination, longitude, argumentPeriapsis, pitchOverRate, safeHoldingAltitude).
            deployPanelsAndAntennas().
        }
    } ELSE IF (option = targetLaunchOption) {
        IF (NOT HASTARGET) {
            headsUpText("Please set a target").
        } ELSE {
            DECLARE LOCAL destination TO TARGET.
            DECLARE LOCAL launchSettings TO getTargetLaunchSettings(updateGui).
            IF (NOT launchSettings["Cancelled"]) {
                DECLARE LOCAL timeToOrbit TO launchSettings["timeToOrbit"].
                DECLARE LOCAL pitchOverRate TO launchSettings["pitchOverRate"].
                DECLARE LOCAL timeToImpactBuffer TO launchSettings["timeToImpactBuffer"].
                DECLARE LOCAL minimumAngleToCorrect TO launchSettings["minimumAngleToCorrect"].
                DECLARE LOCAL altitudeOffsetPercent TO launchSettings["altitudeOffsetPercent"].

                launchToTarget(destination, timeToOrbit, pitchOverRate, timeToImpactBuffer, minimumAngleToCorrect, updateGui, altitudeOffsetPercent).
                DECLARE LOCAL targetSet TO setDockingTarget(TARGET).
                IF (targetSet) {
                    dockWithTarget(updateGui).
                }
            }
        }
    } ELSE IF (option = coordinateLaunchOption) {
        DECLARE LOCAL launchSettings TO coordinateView(updateGui, TRUE).
        IF (NOT launchSettings["Cancelled"]) {
            DECLARE LOCAL coordinates TO launchSettings["Coordinates"].
            DECLARE LOCAL pitchRate TO launchSettings["pitchRate"].
            hopToCoordinates(coordinates, pitchRate, updateGui).
        }
    } ELSE IF (option = deorbitOption) {
        DECLARE LOCAL targetPeriapsis TO BODY:ATM:HEIGHT.
        IF (SHIP:STATUS = "ORBITING") {
            SET targetPeriapsis TO getDeorbitPeriapsis(updateGui).
        }
        IF (targetPeriapsis <> -1) {
            retractPanelsAndAntennas().
            deorbitCraft(targetPeriapsis, updateGui).
            RETURN TRUE.
        }
    } ELSE IF (option = landOption) {
        DECLARE LOCAL landingDetails TO coordinateView(updateGui).
        IF (NOT landingDetails["Cancelled"]) {
            DECLARE LOCAL coordinates TO landingDetails["Coordinates"].
            landCraft(coordinates, updateGui).
        }
    } ELSE IF (option = transferOption) {
        IF (HASTARGET) {
            DECLARE LOCAL destination TO TARGET.
            IF (destination:ISTYPE("Body")) {
                DECLARE LOCAL options TO getBodyTransferOptions(updateGui).
                IF (NOT options["Cancelled"]) {
                    options:REMOVE("Cancelled").
                    executeRSVP(destination, options, updateGui).
                }
            } ELSE IF (destination:ISTYPE("Vessel")) {
                IF (destination:BODY = BODY) {
                    DECLARE LOCAL options TO getVesselTransferOptions(FALSE, updateGui).
                    IF (NOT options["Cancelled"]) {
                        transferToVessel(destination, options["timeToImpactBuffer"], options["angleToCorrect"], updateGui).
                    }
                } ELSE IF (destination:BODY = BODY:BODY) {
                    DECLARE LOCAL options TO getVesselTransferOptions(TRUE, updateGui).
                    IF (NOT options["Cancelled"]) {
                        transferToPlanetVessel(
                                        destination,
                                        options["parentPeriapsis"],
                                        options["timeToImpactBuffer"],
                                        options["angleToCorrect"],
                                        options["closestApproachCutoff"],
                                        options["doCorrections"],
                                        updateGui).
                    }
                } ELSE IF (BODY:ORBITINGCHILDREN:FIND(destination:BODY) > -1) {
                    DECLARE LOCAL options TO getVesselTransferOptions(FALSE, updateGui).
                    IF (NOT options["Cancelled"]) {
                        transferToMoonVessel(destination, options["timeToImpactBuffer"], options["angleToCorrect"], updateGui).
                    }
                } ELSE {
                    headsUpText("Please select a vessel in the same or adjacent system").
                }
            } ELSE {
                headsUpText("Please select either a body or a vessel").
            }
        } ELSE {
            headsUpText("Please select a target").
        }
    } ELSE IF (option = customOrbitOption) {
        DECLARE LOCAL targetOrbitParameters TO getTargetOrbitParameters(updateGui).

        IF (NOT targetOrbitParameters["Cancelled"]) {
            targetOrbitParameters:REMOVE("Cancelled").
            DECLARE LOCAL options TO LEXICON(
                            "create_maneuver_nodes", "both",
                            "verbose", true).
            DECLARE LOCAL searchInterval TO targetOrbitParameters["searchInterval"].
            DECLARE LOCAL searchDuration TO targetOrbitParameters["searchDuration"].
            IF (searchInterval <> 0) {
                options:ADD("search_interval", searchInterval).
            }
            IF (searchDuration <> 0) {
                options:ADD("search_duration", searchDuration).
            }

            executeRSVP(targetOrbitParameters["targetOrbit"], options, updateGui).
        }
    } ELSE IF (option = executeNodeOption) {
        WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
        KUNIVERSE:QUICKSAVETO("quicksaveBurn").

        IF (HASNODE) {
            executeNode(NEXTNODE, updateGui).
        }
    } ELSE IF (option = dockOption) {
        IF (HASTARGET) {
            DECLARE LOCAL targetSet TO setDockingTarget(TARGET).
            IF (targetSet) {
                dockWithTarget(updateGui).
            }
        } ELSE {
            headsUpText("Please select a target").
        }
    } ELSE IF (option = fuelOption) {
        DECLARE LOCAL fuelOptions TO getFuelOptions(updateGui).

        transferFuel(fuelOptions["requestedDeltaV"], fuelOptions["requestedMonopropUnits"],
                fuelOptions["shipDryMass"], fuelOptions["vacuumIsp"], updateGui).

    }
}
