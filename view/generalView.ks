DECLARE FUNCTION generalView {
    DECLARE PARAMETER handler TO handleOption@, updateOptions TO updateLaunchOptions@.

    DECLARE LOCAL preLaunchStatus TO "PRELAUNCH".
    DECLARE LOCAL landedStatus TO "LANDED".
    DECLARE LOCAL splashedStatus TO "SPLASHED".
    DECLARE LOCAL orbitStatus TO "ORBITING".
    DECLARE LOCAL suborbitalStatus TO "SUB_ORBITAL".
    DECLARE LOCAL flyingStatus TO "FLYING".
    DECLARE LOCAL escapingStatus TO "ESCAPING".
    DECLARE LOCAL dockedStatus TO "DOCKED".

    DECLARE LOCAL mainGui TO GUI(250).
    SET mainGui:X TO 500.
    SET mainGui:Y TO 60.

    DECLARE LOCAL labelName TO mainGui:ADDLABEL("").
    SET labelName:STYLE:ALIGN TO "CENTER".
    SET labelName:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL labelStatus TO mainGui:ADDLABEL("").
    SET labelStatus:STYLE:ALIGN TO "CENTER".
    SET labelStatus:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL operationStatusBox to mainGui:ADDVBOX().
    DECLARE LOCAL currentStatusBox TO operationStatusBox:ADDHLAYOUT().
    DECLARE LOCAL labelOperationStatus TO currentStatusBox:ADDLABEL("").
    DECLARE LOCAL labelStatusSetting TO currentStatusBox:ADDLABEL("").
    SET labelStatusSetting:STYLE:ALIGN TO "RIGHT".

    //Orbit settings
    DECLARE LOCAL orbitSettingsBox to mainGui:ADDVBOX().

    //Apoapsis Settings
    DECLARE LOCAL apoapsisSettings TO orbitSettingsBox:ADDHLAYOUT().
    DECLARE LOCAL labelApoapsis TO createSettingsLeftLabel(apoapsisSettings, "Apoapsis").
    DECLARE LOCAL labelApoapsisSetting TO createSettingsLabel(apoapsisSettings).

    //Periapsis Settings
    DECLARE LOCAL periapsisSettings TO orbitSettingsBox:ADDHLAYOUT().
    DECLARE LOCAL labelPeriapsis TO createSettingsLeftLabel(periapsisSettings, "Periapsis").
    DECLARE LOCAL labelPeriapsisSetting TO createSettingsLabel(periapsisSettings).

    //Inclination Settings
    DECLARE LOCAL inclinationSettings TO orbitSettingsBox:ADDHLAYOUT().
    DECLARE LOCAL labelInclination TO createSettingsLeftLabel(inclinationSettings, "Inclination").
    DECLARE LOCAL labelInclinationSetting TO createSettingsLabel(inclinationSettings).

    //LAN Settings
    DECLARE LOCAL lanSettings TO orbitSettingsBox:ADDHLAYOUT().
    DECLARE LOCAL labelLan TO createSettingsLeftLabel(lanSettings, "LAN").
    DECLARE LOCAL labelLanSetting TO createSettingsLabel(lanSettings).

    //ArgPeriapsis Settings
    DECLARE LOCAL argPeriapsisSettings TO orbitSettingsBox:ADDHLAYOUT().
    DECLARE LOCAL labelArgPeriapsis TO createSettingsLeftLabel(argPeriapsisSettings, "Arg. of Periapsis").
    DECLARE LOCAL labelArgPeriapsisSetting TO createSettingsLabel(argPeriapsisSettings).

    DECLARE LOCAL launchPopup is mainGui:ADDPOPUPMENU().
    DECLARE LOCAL doLaunch TO FALSE.
    DECLARE LOCAL launchButton TO mainGui:ADDBUTTON("Go!").
    SET launchButton:ONCLICK TO {
        SET doLaunch TO TRUE.
    }.
    updateOptions:CALL(launchPopup).

    DECLARE LOCAL miscButtons TO mainGui:ADDHBOX().
    DECLARE LOCAL buttonTerminal TO miscButtons:ADDBUTTON("Terminal").
    SET buttonTerminal:STYLE:HEIGHT TO 25.
    SET buttonTerminal:ONCLICK TO {
        showTerminal().
    }.

    DECLARE LOCAL buttonScience TO miscButtons:ADDBUTTON("Science").
    SET buttonScience:STYLE:HEIGHT TO 25.
    SET buttonScience:ONCLICK TO {
        gatherAllScience().
        resetAllScience().
    }.

    DECLARE LOCAL buttonMining TO miscButtons:ADDBUTTON("Mine").
    SET buttonMining:STYLE:HEIGHT TO 25.
    SET buttonMining:ONCLICK TO {
        mineOre(updateGui@, shipConsumesRcs()).
    }.

    DECLARE LOCAL labelAltitude TO createInfoLabel(mainGui).
    DECLARE LOCAL labelThrustToWeight TO createInfoLabel(mainGui).
    DECLARE LOCAL labelTargetDistance TO createInfoLabel(mainGui, PURPLE).
    DECLARE LOCAL labelLatitude TO createInfoLabel(mainGui, YELLOW).
    DECLARE LOCAL labelLongitude TO createInfoLabel(mainGui, YELLOW).
    DECLARE LOCAL labelVerticalSpeed TO createInfoLabel(mainGui, RED).
    DECLARE LOCAL labelRadarAltitude TO createInfoLabel(mainGui, RED).
    DECLARE LOCAL labelTimeToImpact TO createInfoLabel(mainGui, RED).

    mainGui:SHOW().

    DECLARE LOCAL isDone TO FALSE.
    DECLARE LOCAL prevStatus TO "START".
    UNTIL (isDone) {
        IF (doLaunch) {
            IF (SHIP:STATUS <> splashedStatus) {
                SET launchPopup:ENABLED TO FALSE.
                SET launchButton:ENABLED TO FALSE.
                handler:CALL(mainGui, launchPopup:VALUE, updateGui@).
                SET launchPopup:ENABLED TO TRUE.
                SET launchButton:ENABLED TO TRUE.
            } ELSE {
                SET isDone TO TRUE.
            }
            SET doLaunch TO FALSE.
        }

        updateGui().
        IF (SHIP:STATUS <> prevStatus) {
            updateOptions:CALL(launchPopup).
        }
        SET prevStatus TO SHIP:STATUS.
        WAIT .1.
    }

    DECLARE LOCAL FUNCTION updateGui {
        DECLARE PARAMETER operationStatuses TO LEXICON().

        SET labelName:TEXT TO "<b><size=17>" + SHIP:NAME + "</size></b>".

        IF (SHIP:STATUS = preLaunchStatus OR SHIP:STATUS = landedStatus) {
            SET labelStatus:TEXT TO "<size=14>Landed at " + BODY:NAME + "</size>".
        } ELSE IF (SHIP:STATUS = orbitStatus) {
            SET labelStatus:TEXT TO "<size=14>Orbiting " + BODY:NAME + "</size>".
        } ELSE IF (SHIP:STATUS = suborbitalStatus) {
            SET labelStatus:TEXT TO "<size=14>Suborbital flight at " + BODY:NAME + "</size>".
        } ELSE IF (SHIP:STATUS = escapingStatus) {
            SET labelStatus:TEXT TO "<size=14>Escaping " + BODY:NAME + "</size>".
        } ELSE IF (SHIP:STATUS = splashedStatus) {
            SET labelStatus:TEXT TO "<size=14>Splashed down at " + BODY:NAME + "</size>".
        } ELSE IF (SHIP:STATUS = dockedStatus) {
            SET labelStatus:TEXT TO "<size=14>Docked over " + BODY:NAME + "</size>".
        } ELSE IF (SHIP:STATUS = flyingStatus) {
            SET labelStatus:TEXT TO "<size=14>Flying over " + BODY:NAME + "</size>".
        }

        SET labelApoapsisSetting:TEXT TO "<b>" + ROUND(APOAPSIS) + "m</b>".
        SET labelPeriapsisSetting:TEXT TO "<b>" + ROUND(PERIAPSIS) + "m</b>".
        SET labelInclinationSetting:TEXT TO "<b>" + ROUND(ORBIT:INCLINATION, 1) + "°</b>".
        SET labelLanSetting:TEXT TO "<b>" + ROUND(ORBIT:LAN, 1) + "°</b>".
        SET labelArgPeriapsisSetting:TEXT TO "<b>" + ROUND(ORBIT:ARGUMENTOFPERIAPSIS, 1) + "°</b>".

        SET labelAltitude:TEXT TO "<b>Altitude: </b>" + ROUND(ALTITUDE) + "m".
        SET labelLatitude:TEXT TO "<b>LAT: </b>" + ROUND(SHIP:GEOPOSITION:LAT, 4).
        SET labelLongitude:TEXT TO "<b>LNG: </b>" + ROUND(SHIP:GEOPOSITION:LNG, 4).
        SET labelThrustToWeight:TEXT TO "<b>TWR: </b>" + ROUND(getCurrentTWR(), 2).
        SET labelVerticalSpeed:TEXT TO "<b>Vertical Spd: </b>" + ROUND(VERTICALSPEED, 1) + "m/s".
        SET labelRadarAltitude:TEXT TO "<b>Radar Alt: </b>" + ROUND(ALT:RADAR) + "m".
        SET labelTimeToImpact:TEXT TO "<b>Impact Time: </b>" + ROUND(getImpactTime()) + "s".

        DECLARE LOCAL newLines TO CHAR(10) + CHAR(10).
        DECLARE LOCAL statusText TO "".
        DECLARE LOCAL statusSettingText TO "".
        DECLARE LOCAL statusCount TO 0.
        FOR status IN operationStatuses:KEYS {
            SET statusCount TO statusCount + 1.
            SET statusText TO statusText + status.
            SET statusSettingText TO statusSettingText + operationStatuses[status].
            IF (statusCount <> operationStatuses:KEYS:LENGTH) {
                SET statusText TO statusText + newLines.
                SET statusSettingText TO statusSettingText + newLines.
            }
        }

        SET labelOperationStatus:TEXT TO statusText.
        SET labelStatusSetting:TEXT TO "<b>" + statusSettingText + "</b>".
        SET labelOperationStatus:STYLE:HEIGHT TO statusCount * 25 + 15.
        SET labelStatusSetting:STYLE:HEIGHT TO statusCount * 25 + 15.
        IF (operationStatuses:KEYS:LENGTH = 0) {
            IF (operationStatusBox:VISIBLE) {
                operationStatusBox:HIDE().
            }
        } ELSE {
            IF (NOT operationStatusBox:VISIBLE) {
                operationStatusBox:SHOW().
            }
        }

        IF (getCurrentTWR() = 0) {
            labelThrustToWeight:HIDE().
        } ELSE {
            labelThrustToWeight:SHOW().
        }
        IF (HASTARGET AND NOT TARGET:ISTYPE("Body")) {
            SET labelTargetDistance:TEXT TO "<b>Target Dist: </b>" + ROUND(TARGET:POSITION:MAG) + "m".
            labelTargetDistance:SHOW().
        } ELSE {
            labelTargetDistance:HIDE().
        }

        IF (((SHIP:STATUS = flyingStatus OR SHIP:STATUS = suborbitalStatus) AND VERTICALSPEED < 0)) {
            IF (NOT labelVerticalSpeed:VISIBLE) {
                labelVerticalSpeed:SHOW().
                labelRadarAltitude:SHOW().
                labelTimeToImpact:SHOW().
            }
        } ELSE {
            IF (labelVerticalSpeed:VISIBLE) {
                labelLatitude:HIDE().
                labelLongitude:HIDE().
                labelVerticalSpeed:HIDE().
                labelRadarAltitude:HIDE().
                labelTimeToImpact:HIDE().
            }
        }
        IF (SHIP:STATUS = landedStatus OR SHIP:STATUS = preLaunchStatus) {
            IF (NOT labelLatitude:VISIBLE) {
                labelLatitude:SHOW().
                labelLongitude:SHOW().
            }
        }
    }
}

DECLARE LOCAL FUNCTION createSettingsLeftLabel {
    DECLARE PARAMETER parentWidget, labelText.

    DECLARE LOCAL settingsLabel TO parentWidget:ADDLABEL(labelText).
    SET settingsLabel:STYLE:WIDTH TO 115.

    RETURN settingsLabel.
}

DECLARE LOCAL FUNCTION createSettingsLabel {
    DECLARE PARAMETER parentWidget.

    DECLARE LOCAL settingsLabel TO parentWidget:ADDLABEL("").
    SET settingsLabel:STYLE:ALIGN TO "RIGHT".

    RETURN settingsLabel.
}

DECLARE LOCAL FUNCTION createInfoLabel {
    DECLARE PARAMETER parentWidget, color TO RGBA(.7176471, .9960784, 0, 1).

    DECLARE LOCAL infoLabel TO parentWidget:ADDLABEL("").
    SET infoLabel:STYLE:ALIGN TO "LEFT".
    SET infoLabel:STYLE:HSTRETCH TO TRUE.
    SET infoLabel:STYLE:TEXTCOLOR TO color.

    RETURN infoLabel.
}

DECLARE LOCAL FUNCTION getCurrentTWR {
    DECLARE LOCAL engineList TO LIST().
    LIST ENGINES IN engineList.

    DECLARE LOCAL currentThrust TO 0.
    FOR engine IN engineList {
        SET currentThrust TO currentThrust + engine:THRUST.
    }
    RETURN currentThrust / SHIP:MASS / CONSTANT:G0.
}

DECLARE LOCAL FUNCTION getImpactTime {
    DECLARE LOCAL seaLevelGravity TO (CONSTANT():G * BODY:MASS) / BODY:RADIUS ^ 2.
    DECLARE LOCAL gravity TO seaLevelGravity / ((BODY:RADIUS + ALTITUDE) / BODY:RADIUS) ^ 2.
    DECLARE LOCAL shipLatLng TO SHIP:GEOPOSITION.
    DECLARE LOCAL surfaceElevation TO shipLatLng:TERRAINHEIGHT.
    DECLARE LOCAL betterAltRadar TO MAX(0.1, MIN(ALTITUDE, ALTITUDE - surfaceElevation)).
    RETURN (-VERTICALSPEED - SQRT(VERTICALSPEED ^ 2 + (2 * gravity * betterAltRadar))) / -gravity.
}

DECLARE LOCAL FUNCTION updateLaunchOptions {
    DECLARE PARAMETER launchPopup.
    DECLARE LOCAL preLaunchStatus TO "PRELAUNCH".
    DECLARE LOCAL landedStatus TO "LANDED".
    DECLARE LOCAL splashedStatus TO "SPLASHED".
    DECLARE LOCAL orbitStatus TO "ORBITING".
    DECLARE LOCAL suborbitalStatus TO "SUB_ORBITAL".
    DECLARE LOCAL flyingStatus TO "FLYING".
    DECLARE LOCAL escapingStatus TO "ESCAPING".
    DECLARE LOCAL dockedStatus TO "DOCKED".

    DECLARE LOCAL basicLaunchOption TO "Launch to basic orbit".
    DECLARE LOCAL customLaunchOption TO "Launch to custom orbit".
    DECLARE LOCAL targetLaunchOption TO "Launch to target".
    DECLARE LOCAL coordinateLaunchOption TO "Hop to coordinates".

    DECLARE LOCAL deorbitOption TO "Deorbit Craft".
    DECLARE LOCAL landOption TO "Land at Coordinates".
    DECLARE LOCAL transferOption TO "Transfer to target".
    DECLARE LOCAL customOrbitOption TO "Go to orbit".
    DECLARE LOCAL executeNodeOption TO "Execute node".
    DECLARE LOCAL dockOption TO "Dock with target".
    DECLARE LOCAL fuelOption TO "Transfer Fuel".

    launchPopup:CLEAR().
    IF (SHIP:STATUS = landedStatus OR SHIP:STATUS = preLaunchStatus) {
        launchPopup:ADDOPTION(basicLaunchOption).
        launchPopup:ADDOPTION(customLaunchOption).
        launchPopup:ADDOPTION(targetLaunchOption).
        launchPopup:ADDOPTION(coordinateLaunchOption).
    } ELSE IF (SHIP:STATUS = orbitStatus OR SHIP:STATUS = dockedStatus) {
        IF (BODY:ATM:EXISTS) {
            launchPopup:ADDOPTION(deorbitOption).
        }

        launchPopup:ADDOPTION(landOption).
        launchPopup:ADDOPTION(transferOption).
        launchPopup:ADDOPTION(customOrbitOption).
        launchPopup:ADDOPTION(executeNodeOption).
        launchPopup:ADDOPTION(dockOption).

        IF (isDocked()) {
            launchPopup:ADDOPTION(fuelOption).
        }
    } ELSE IF (SHIP:STATUS = suborbitalStatus) {
        IF (BODY:ATM:EXISTS) {
            launchPopup:ADDOPTION(deorbitOption).
        }
        launchPopup:ADDOPTION(landOption).
        launchPopup:ADDOPTION(executeNodeOption).
    } ELSE IF (SHIP:STATUS = escapingStatus) {
        launchPopup:ADDOPTION(executeNodeOption).
    } ELSE IF(SHIP:STATUS = flyingStatus) {
        launchPopup:ADDOPTION(deorbitOption).
    } ELSE IF (SHIP:STATUS = splashedStatus) {
        launchPopup:ADDOPTION("No Options").
        SET launchPopup:ENABLED TO FALSE.
    }
}
