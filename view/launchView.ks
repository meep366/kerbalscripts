@LAZYGLOBAL OFF.

DECLARE FUNCTION getLaunchSettings {
    DECLARE PARAMETER isBasicOrbit, updateGui.

    DECLARE LOCAL guiLaunchOptions TO GUI(300).
    SET guiLaunchOptions:X TO 1570.
    SET guiLaunchOptions:Y TO 350.
    DECLARE LOCAL labelLaunchOptions TO guiLaunchOptions:ADDLABEL("<size=20><b>Orbit Options</b></size>").
    SET labelLaunchOptions:STYLE:ALIGN TO "CENTER".
    SET labelLaunchOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL targetOrbitAltitude TO 100000.
    DECLARE LOCAL targetApoapsis TO 100000.
    DECLARE LOCAL targetPeriapsis TO 100000.
    DECLARE LOCAL targetInclination TO FLOOR(ABS(SHIP:GEOPOSITION:LAT)).
    DECLARE LOCAL targetLongitude TO 0.
    DECLARE LOCAL targetArgumentPeriapsis TO 0.
    DECLARE LOCAL targetHoldingAltitude TO 75000.
    DECLARE LOCAL targetPitchRate TO -2/25.

    DECLARE LOCAL orbitAltitudeText TO 0.
    DECLARE LOCAL orbitInclinationText TO 0.
    DECLARE LOCAL pitchRateText TO 0.
    DECLARE LOCAL apoapsisText TO 0.
    DECLARE LOCAL periapsisText TO 0.
    DECLARE LOCAL longitudeText TO 0.
    DECLARE LOCAL argumentOfPeriapsisText TO 0.
    DECLARE LOCAL holdingAltitudeText TO 0.

    IF (isBasicOrbit) {
        SET orbitAltitudeText TO createGuiText(guiLaunchOptions, "Orbit Altiude", targetOrbitAltitude:TOSTRING).
        SET orbitInclinationText TO createGuiText(guiLaunchOptions, "Orbit Inclination", targetInclination:TOSTRING).
        SET pitchRateText TO createGuiText(guiLaunchOptions, "Pitch Rate", targetPitchRate:TOSTRING).
    } ELSE {
        SET apoapsisText TO createGuiText(guiLaunchOptions, "Apoapsis", targetApoapsis:TOSTRING).
        SET periapsisText TO createGuiText(guiLaunchOptions, "Periapsis", targetPeriapsis:TOSTRING).
        SET orbitInclinationText TO createGuiText(guiLaunchOptions, "Orbit Inclination", targetInclination:TOSTRING).
        SET longitudeText TO createGuiText(guiLaunchOptions, "Longitude of Ascending Node", targetLongitude:TOSTRING).
        SET argumentOfPeriapsisText TO createGuiText(guiLaunchOptions, "Argument of Periapsis", targetArgumentPeriapsis:TOSTRING).
        SET pitchRateText TO createGuiText(guiLaunchOptions, "Pitch Rate", targetPitchRate:TOSTRING).
        SET holdingAltitudeText TO createGuiText(guiLaunchOptions, "Safe Holding Altitude", targetHoldingAltitude:TOSTRING).
    }

    DECLARE LOCAL launchSettingsDone TO FALSE.
    DECLARE LOCAL launchOptions TO LEXICON().
    DECLARE LOCAL autoLaunchButton TO guiLaunchOptions:ADDBUTTON("Orbit").
    DECLARE LOCAL cancelButton TO guiLaunchOptions:ADDBUTTON("Cancel").

    DECLARE LOCAL FUNCTION autoLaunch {
        launchOptions:ADD("Cancelled", FALSE).
        IF (isBasicOrbit) {
            SET targetOrbitAltitude TO orbitAltitudeText:TEXT:TONUMBER(targetOrbitAltitude).
            SET targetInclination TO orbitInclinationText:TEXT:TONUMBER(targetInclination).
            SET targetPitchRate TO pitchRateText:TEXT:TONUMBER(targetPitchRate).

            launchOptions:ADD("targetAltitude", targetOrbitAltitude).
            launchOptions:ADD("targetInclination", targetInclination).
            launchOptions:ADD("targetPitchRate", targetPitchRate).
        } ELSE {
            SET targetApoapsis TO apoapsisText:TEXT:TONUMBER(targetApoapsis).
            SET targetPeriapsis TO periapsisText:TEXT:TONUMBER(targetPeriapsis).
            SET targetInclination TO orbitInclinationText:TEXT:TONUMBER(targetInclination).
            SET targetLongitude TO longitudeText:TEXT:TONUMBER(targetLongitude).
            SET targetArgumentPeriapsis TO argumentOfPeriapsisText:TEXT:TONUMBER(targetArgumentPeriapsis).
            SET targetPitchRate TO pitchRateText:TEXT:TONUMBER(targetPitchRate).
            SET targetHoldingAltitude TO holdingAltitudeText:TEXT:TONUMBER(targetHoldingAltitude).

            launchOptions:ADD("targetApoapsis", targetApoapsis).
            launchOptions:ADD("targetPeriapsis", targetPeriapsis).
            launchOptions:ADD("targetInclination", targetInclination).
            launchOptions:ADD("targetLongitude", targetLongitude).
            launchOptions:ADD("targetArgumentOfPeriapsis", targetArgumentPeriapsis).
            launchOptions:ADD("targetPitchRate", targetPitchRate).
            launchOptions:ADD("targetHoldingAltitude", targetHoldingAltitude).
        }

        SET launchSettingsDone TO TRUE.
    }

    SET autoLaunchButton:ONCLICK TO autoLaunch@.
    SET cancelButton:ONCLICK TO {
        SET launchSettingsDone TO TRUE.
        launchOptions:ADD("Cancelled", TRUE).
    }.

    guiLaunchOptions:SHOW().

    UNTIL (launchSettingsDone) {
        updateGui:CALL().
    }

    guiLaunchOptions:DISPOSE().

    RETURN launchOptions.
}

DECLARE FUNCTION getTargetLaunchSettings {
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL guiLaunchOptions TO GUI(300).
    SET guiLaunchOptions:X TO 1570.
    SET guiLaunchOptions:Y TO 350.
    DECLARE LOCAL labelLaunchOptions TO guiLaunchOptions:ADDLABEL("<size=20><b>Launch Options</b></size>").
    SET labelLaunchOptions:STYLE:ALIGN TO "CENTER".
    SET labelLaunchOptions:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL timeToOrbit TO 90.
    DECLARE LOCAL pitchOverRate TO -2/25.
    DECLARE LOCAL timeToImpactBuffer TO 1.
    DECLARE LOCAL minimumAngleToCorrect TO 1.
    DECLARE LOCAL altitudeOffsetPercent TO 2.

    DECLARE LOCAL timeToOrbitText TO createGuiText(guiLaunchOptions, "Time to Orbit", timeToOrbit:TOSTRING).
    DECLARE LOCAL pitchRateText TO createGuiText(guiLaunchOptions, "Pitch Rate", pitchOverRate:TOSTRING).
    DECLARE LOCAL timeToImpactText TO createGuiText(guiLaunchOptions, "Time to Impact Buffer", timeToImpactBuffer:TOSTRING).
    DECLARE LOCAL angleToCorrectText TO createGuiText(guiLaunchOptions, "Angle to Correct", minimumAngleToCorrect:TOSTRING).
    DECLARE LOCAL altitudeOffsetText TO createGuiText(guiLaunchOptions, "Altitude Offset Percent", altitudeOffsetPercent:TOSTRING).

    DECLARE LOCAL isDone TO FALSE.
    DECLARE LOCAL launchOptions TO LEXICON().
    DECLARE LOCAL targetLaunchButton TO guiLaunchOptions:ADDBUTTON("Launch to Target").
    DECLARE LOCAL cancelButton TO guiLaunchOptions:ADDBUTTON("Cancel").

    DECLARE LOCAL FUNCTION launchToTarget {
        SET timeToOrbit TO timeToOrbitText:TEXT:TONUMBER(timeToOrbit).
        SET pitchOverRate TO pitchRateText:TEXT:TONUMBER(pitchOverRate).
        SET timeToImpactBuffer TO timeToImpactText:TEXT:TONUMBER(timeToImpactBuffer).
        SET minimumAngleToCorrect TO angleToCorrectText:TEXT:TONUMBER(minimumAngleToCorrect).
        SET altitudeOffsetPercent TO altitudeOffsetText:TEXT:TONUMBER(altitudeOffsetPercent) / 100.

        launchOptions:ADD("Cancelled", FALSE).
        launchOptions:ADD("timeToOrbit", timeToOrbit).
        launchOptions:ADD("pitchOverRate", pitchOverRate).
        launchOptions:ADD("timeToImpactBuffer", timeToImpactBuffer).
        launchOptions:ADD("minimumAngleToCorrect", minimumAngleToCorrect).
        launchOptions:ADD("altitudeOffsetPercent", altitudeOffsetPercent).

        SET isDone TO TRUE.
    }

    SET cancelButton:ONCLICK TO {
        SET isDone TO TRUE.
        launchOptions:ADD("Cancelled", FALSE).
    }.

    SET targetLaunchButton:ONCLICK TO launchToTarget@.
    guiLaunchOptions:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    guiLaunchOptions:DISPOSE().
    RETURN launchOptions.
}