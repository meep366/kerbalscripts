@LAZYGLOBAL OFF.

DECLARE FUNCTION coordinateView {
    DECLARE PARAMETER updateGui, isAscent TO FALSE.

    DECLARE LOCAL guiLandingCoordinates TO GUI(300).
    SET guiLandingCoordinates:X TO 1570.
    SET guiLandingCoordinates:Y TO 350.
    DECLARE LOCAL labelLandingCoordinates IS guiLandingCoordinates:ADDLABEL("<size=20><b>Landing Details</b></size>").
    SET labelLandingCoordinates:STYLE:ALIGN TO "CENTER".
    SET labelLandingCoordinates:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL landingLatitude TO 0.
    DECLARE LOCAL landingLongitude TO 0.
    DECLARE LOCAL pitchRate TO -1.

    DECLARE LOCAL landingLatitudeText TO createGuiText(guiLandingCoordinates, "Landing Latitude", landingLatitude:TOSTRING).
    DECLARE LOCAL landingLongitudeText TO createGuiText(guiLandingCoordinates, "Landing Longitude", landingLongitude:TOSTRING).
    DECLARE LOCAL pitchRateText TO 0.
    IF (isAscent) {
        SET pitchRateText TO createGuiText(guiLandingCoordinates, "Pitch Rate", pitchRate:TOSTRING).
    }

    DECLARE LOCAL landButton TO guiLandingCoordinates:ADDBUTTON("Go to Coordinates").
    DECLARE LOCAL cancelButton TO guiLandingCoordinates:ADDBUTTON("Cancel").
    DECLARE LOCAL landingDetails TO LEXICON().
    DECLARE LOCAL isDone TO FALSE.

    SET landButton:ONCLICK TO {
        SET landingLatitude TO landingLatitudeText:TEXT:TONUMBER(landingLatitude).
        SET landingLongitude TO landingLongitudeText:TEXT:TONUMBER(landingLongitude).
        landingDetails:ADD("Coordinates", LATLNG(landingLatitude, landingLongitude)).
        landingDetails:ADD("Cancelled", FALSE).

        IF (isAscent) {
            SET pitchRate TO pitchRateText:TEXT:TONUMBER(pitchRate).
            landingDetails:ADD("PitchRate", pitchRate).
        }

        SET isDone TO TRUE.
    }.
    SET cancelButton:ONCLICK TO {
        SET isDone TO TRUE.
        landingDetails:ADD("Cancelled", TRUE).
    }.

    guiLandingCoordinates:SHOW().

    UNTIL (isDone) {
        updateGui:CALL().
    }

    guiLandingCoordinates:DISPOSE().

    RETURN landingDetails.
}