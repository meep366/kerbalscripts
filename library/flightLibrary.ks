@LAZYGLOBAL OFF.

DECLARE FUNCTION fly {
    DECLARE PARAMETER pitchOnTouchdown TO 1.
    DECLARE PARAMETER maxAngleOfAttack TO 20.
    DECLARE PARAMETER maxBankDegrees TO 40.
    DECLARE PARAMETER maxPitchDegrees TO 30.
    DECLARE PARAMETER minimumSpeed TO 80.
    DECLARE PARAMETER altitudeToCutThrottle TO 10.
    DECLARE PARAMETER isSpacefaring TO FALSE.
    DECLARE PARAMETER elevatorPID TO PIDLOOP(0.1, 0.0666, 0.1).
    DECLARE PARAMETER verticalSpeedPID TO PIDLOOP(0.0495, 0.0211, 0.0776).
    DECLARE PARAMETER pitchAnglePID TO PIDLOOP(0.1, 0.01333, 0.5).
    DECLARE PARAMETER aileronPID TO PIDLOOP(0.00446, 0.00343, 0.00387).
    DECLARE PARAMETER yawDamperPID TO PIDLOOP(.08, .029, .147).
    DECLARE PARAMETER bankAnglePID TO PIDLOOP(1.65, .541, 1.26).
    DECLARE PARAMETER throttlePID TO PIDLOOP(0.10, 0.01, 0.666).
    DECLARE PARAMETER minGlideslopePitchDegrees TO -20.
    DECLARE PARAMETER highAltAileronPID TO PIDLOOP(0.3, 0.075, 0.3).
    DECLARE PARAMETER highAltElevatorPID TO PIDLOOP(0.3, 0.075, 0.3).
    DECLARE PARAMETER highAltYawDamperPID TO PIDLOOP(0.6, 0.12, 0.75).

    CLEARSCREEN.
    CLEARVECDRAWS().
    CLEARGUIS().

    DECLARE LOCAL oldIPU to CONFIG:IPU.
    IF (oldIPU < 1000) {
        SET CONFIG:IPU to 1000.
    }

    // ****************
    // Initialize variables
    // ****************

    // Runway coordinates
    DECLARE LOCAL runwayKSCEndOne TO LATLNG(-.049849, -74.7281).
    DECLARE LOCAL runwayKSCEndTwo TO LATLNG(-.049849, -74.4889).
    DECLARE LOCAL anomalyIslandEndOne TO LATLNG(4.15, -173).
    DECLARE LOCAL anomalyIslandEndTwo TO LATLNG(4.60, -170.86).

    // Control mode settings
    DECLARE LOCAL preLaunchStatus TO "PRELAUNCH".
    DECLARE LOCAL landedStatus TO "LANDED".
    DECLARE LOCAL splashedStatus TO "SPLASHED".
    DECLARE LOCAL orbitingStatus TO "ORBITING".
    DECLARE LOCAL subOrbitalStatus TO "SUB_ORBITAL".
    DECLARE LOCAL flyingStatus TO "FLYING".
    DECLARE LOCAL bankMode TO "BNK".
    DECLARE LOCAL headingMode TO "HDG".
    DECLARE LOCAL targetMode TO "TGT".
    DECLARE LOCAL pitchMode TO "PIT".
    DECLARE LOCAL altMode TO "ALT".
    DECLARE LOCAL verticalSpeedMode TO "VSP".
    DECLARE LOCAL navMode TO "NAV".
    DECLARE LOCAL takeoffMode TO "TKF".
    DECLARE LOCAL toOrbitMode TO "OBT".
    DECLARE LOCAL circularizeMode TO "CIR".
    DECLARE LOCAL deorbitMode TO "DBT".
    DECLARE LOCAL ilsMode TO "ILS".
    DECLARE LOCAL flareMode TO "FLR".
    DECLARE LOCAL recoveryMode TO "RCY".
    DECLARE LOCAL maxThrustMode TO "MCT".
    DECLARE LOCAL speedMode TO "SPD".
    DECLARE LOCAL offMode TO "Off".
    DECLARE LOCAL stall TO "STL".
    DECLARE LOCAL avoidTerrain TO "TER".
    DECLARE LOCAL avoidHeights TO "HEI".

    // Control variables
    DECLARE LOCAL elevator TO 0.
    DECLARE LOCAL aileron TO 0.
    DECLARE LOCAL rudder TO 0.

    // Control modes
    DECLARE LOCAL autoPilotAndThrottleEnabled TO TRUE.
    DECLARE LOCAL autoPilotShutdown TO FALSE.
    DECLARE LOCAL lateralNavMode TO bankMode.
    DECLARE LOCAL verticalNavMode TO altMode.
    DECLARE LOCAL autoPilotMode TO navMode.
    DECLARE LOCAL usingAtmoPIDs TO TRUE.
    DECLARE LOCAL autoThrottleMode TO speedMode.
    DECLARE LOCAL recoveryEnabled TO TRUE.
    DECLARE LOCAL recoveryType TO stall.
    DECLARE LOCAL altitudeToAvoid TO 0.
    DECLARE LOCAL maxAltitudeToAvoid TO 0.

    // Control targets
    DECLARE LOCAL targetSpeed TO SHIP:AIRSPEED.
    DECLARE LOCAL targetPitch TO 5.
    DECLARE LOCAL targetBank TO 0.
    DECLARE LOCAL targetHeading TO headingMagnitude().
    DECLARE LOCAL targetAltitude TO SHIP:ALTITUDE.
    DECLARE LOCAL targetVerticalSpeed TO 0.
    DECLARE LOCAL targetName TO "KSC Runway".
    DECLARE LOCAL targetRunwayEndOne TO runwayKSCEndOne.
    DECLARE LOCAL targetRunwayEndTwo TO runwayKSCEndTwo.
    DECLARE LOCAL targetCoordinates TO targetRunwayEndOne.
    IF (BODY = LAYTHE) {
        SET targetName TO "Anomaly Island".
        SET targetRunwayEndOne TO anomalyIslandEndOne.
        SET targetRunwayEndTwo TO anomalyIslandEndTwo.
        SET targetCoordinates TO anomalyIslandEndOne.
    }

    // Previous settings for recovery
    DECLARE LOCAL previousLatNavMode TO "".
    DECLARE LOCAL previousVertNavMode TO "".
    DECLARE LOCAL previousAutoThrottle TO "".
    DECLARE LOCAL previousAutoPilot TO "".
    DECLARE LOCAL previousBank TO 0.
    DECLARE LOCAL previousSpeed TO 0.

    // Flight variables
    DECLARE LOCAL targetOrbitAltitude TO 75000.
    DECLARE LOCAL targetOrbitMargin TO 2000.
    DECLARE LOCAL targetInclination TO 0.
    DECLARE LOCAL takeoffAltitude TO 2000.
    DECLARE LOCAL takeoffPitch TO 15.
    DECLARE LOCAL glideSlopeAngle TO 4.
    DECLARE LOCAL glideslopeHeight TO 5.
    DECLARE LOCAL minApproachSpeed TO 90.
    DECLARE LOCAL maxApproachSpeed TO 250.
    DECLARE LOCAL flareAltitude TO 85.
    DECLARE LOCAL brakeOnSpoolUp TO FALSE.

    // Shuttle variables
    DECLARE LOCAL pressureToSwitchRapiers TO 0.03.
    DECLARE LOCAL toOrbit TO FALSE.
    DECLARE LOCAL maxControlAltitude TO 45000.
    DECLARE LOCAL glideslopeStartAltitude TO 25000.
    DECLARE LOCAL glideslopeStartDistance TO 150000.
    DECLARE LOCAL ilsStartAltitude TO 3500.
    DECLARE LOCAL usingHighAltPIDs TO FALSE.
    DECLARE LOCAL lowAltYawDamperPID TO yawDamperPID.
    DECLARE LOCAL lowAltElevatorPID TO elevatorPID.
    DECLARE LOCAL lowAltAileronPID TO aileronPID.

    // Flight stats
    DECLARE LOCAL flightStartTime TO TIME:SECONDS.
    DECLARE LOCAL flightSurfaceDistance TO 0.
    DECLARE LOCAL prevCoordinates TO SHIP:GEOPOSITION.
    DECLARE LOCAL maxSpeed TO 0.
    DECLARE LOCAL maxAltitude TO ALTITUDE.
    DECLARE LOCAL maxGees TO 0.

    // ****************
    // Set up PID loops
    // ****************

    //PID Elevator
    SET elevatorPID:MAXOUTPUT TO 1.
    SET elevatorPID:MINOUTPUT TO -1.
    SET elevatorPID:SETPOINT TO 0.

    //PID Vertical Speed
    SET verticalSpeedPID:MAXOUTPUT TO 1.
    SET verticalSpeedPID:MINOUTPUT TO -1.
    SET verticalSpeedPID:SETPOINT TO targetVerticalSpeed.

    // PID Pitch Angle
    SET pitchAnglePID:MAXOUTPUT TO maxPitchDegrees.
    SET pitchAnglePID:MINOUTPUT TO -maxPitchDegrees.
    SET pitchAnglePID:SETPOINT TO 2000.

    //PID Aileron
    SET aileronPID:MAXOUTPUT TO 1.
    SET aileronPID:MINOUTPUT TO -1.
    SET aileronPID:SETPOINT TO 0.

    //PID Yaw Damper
    SET yawDamperPID:MAXOUTPUT TO 1.
    SET yawDamperPID:MINOUTPUT TO -1.
    SET yawDamperPID:SETPOINT TO 0.

    // PID BankAngle
    SET bankAnglePID:MAXOUTPUT TO maxBankDegrees.
    SET bankAnglePID:MINOUTPUT TO -maxBankDegrees.
    SET bankAnglePID:SETPOINT TO 0.

    //PID Throttle
    SET throttlePID:MAXOUTPUT TO 1.
    SET throttlePID:MINOUTPUT TO 0.
    SET throttlePID:SETPOINT TO targetSpeed.

    //PID Wheel
    DECLARE LOCAL wheelPID TO PIDLOOP(.07, 0, 0).
    SET wheelPID:MAXOUTPUT TO 1.
    SET wheelPID:MINOUTPUT TO -1.
    SET wheelPID:SETPOINT TO 0.

    //PID High Alt Yaw
    SET highAltYawDamperPID:MAXOUTPUT TO 1.
    SET highAltYawDamperPID:MINOUTPUT TO -1.
    SET highAltYawDamperPID:SETPOINT TO 0.

    //PID High Alt Elevator
    SET highAltElevatorPID:MAXOUTPUT TO 1.
    SET highAltElevatorPID:MINOUTPUT TO -1.
    SET highAltElevatorPID:SETPOINT TO 0.

    //PID High Alt Aileron
    SET highAltAileronPID:MAXOUTPUT TO 1.
    SET highAltAileronPID:MINOUTPUT TO -1.
    SET highAltAileronPID:SETPOINT TO 0.

    // ****************
    // Takeoff logic
    // ****************

    DECLARE LOCAL startingHeading TO ROUND(headingMagnitude(), 0).
    IF (SHIP:STATUS = preLaunchStatus OR SHIP:STATUS = landedStatus) {
        SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
        BRAKES ON.
        WAIT UNTIL KUNIVERSE:CANQUICKSAVE.
        KUNIVERSE:QUICKSAVETO("quicksaveTakeoff").

        DECLARE LOCAL guiTakeOff TO GUI(300).
        SET guiTakeOff:X TO 1570.
        SET guiTakeOff:Y TO 350.
        DECLARE LOCAL labelAutoTakeoff IS guiTakeOff:ADDLABEL("<size=20><b>Takeoff Options</b></size>").
        SET labelAutoTakeoff:STYLE:ALIGN TO "CENTER".
        SET labelAutoTakeoff:STYLE:HSTRETCH TO TRUE.

        DECLARE LOCAL autoTakeOffBox TO guiTakeOff:ADDHBOX().
        DECLARE LOCAL autoTakeOffAltitudeLabel TO autoTakeOffBox:ADDLABEL("Atmo Level Off Altitude").
        SET autoTakeOffAltitudeLabel:STYLE:WIDTH TO 215.
        DECLARE LOCAL autoTakeOffAltitudeText TO autoTakeOffBox:ADDTEXTFIELD(takeoffAltitude:TOSTRING).
        SET autoTakeOffAltitudeText:STYLE:WIDTH TO 60.

        DECLARE LOCAL autoTakeOffOrbitDistanceText TO 0.
        DECLARE LOCAL autoTakeOffInclinationText TO 0.
        IF (isSpacefaring) {
            DECLARE LOCAL autoTakeOffOrbitBox TO guiTakeOff:ADDHBOX().
            DECLARE LOCAL autoTakeOffOrbitDistanceLabel TO autoTakeOffOrbitBox:ADDLABEL("Orbit Altiude").
            SET autoTakeOffAltitudeLabel:STYLE:WIDTH TO 215.
            SET autoTakeOffOrbitDistanceText TO autoTakeOffOrbitBox:ADDTEXTFIELD(targetOrbitAltitude:TOSTRING).
            SET autoTakeOffOrbitDistanceText:STYLE:WIDTH TO 60.

            DECLARE LOCAL autoTakeOffInlincationBox TO guiTakeOff:ADDHBOX().
            DECLARE LOCAL autoTakeOffInlincationLabel TO autoTakeOffInlincationBox:ADDLABEL("Orbit Inclination").
            SET autoTakeOffInlincationLabel:STYLE:WIDTH TO 215.
            SET autoTakeOffInclinationText TO autoTakeOffInlincationBox:ADDTEXTFIELD(targetInclination:TOSTRING).
            SET autoTakeOffInclinationText:STYLE:WIDTH TO 60.
        }

        DECLARE LOCAL autoTakeOffHeadingBox TO guiTakeOff:ADDHBOX().
        DECLARE LOCAL autoTakeOffHeadingLabel TO autoTakeOffHeadingBox:ADDLABEL("Runway Heading").
        SET autoTakeOffHeadingLabel:STYLE:WIDTH TO 215.
        DECLARE LOCAL autoTakeOffHeadingText TO autoTakeOffHeadingBox:ADDTEXTFIELD(startingHeading:TOSTRING).
        SET autoTakeOffHeadingText:STYLE:WIDTH TO 60.

        DECLARE LOCAL autoTakeOffDone TO FALSE.
        DECLARE LOCAL autoTakeOffYes TO guiTakeOff:ADDBUTTON("Atmo Flight").
        IF (isSpacefaring) {
            DECLARE LOCAL autoTakeOffOrbit TO guiTakeOff:ADDBUTTON("Orbit").
            SET autoTakeOffOrbit:ONCLICK TO {
                SET startingHeading TO autoTakeOffHeadingText:TEXT:TONUMBER(startingHeading).
                SET takeoffAltitude TO flareAltitude * 3.
                SET targetOrbitAltitude TO autoTakeOffOrbitDistanceText:TEXT:TONUMBER(targetOrbitAltitude).
                SET targetInclination TO autoTakeOffInclinationText:TEXT:TONUMBER(targetInclination).
                guiTakeOff:HIDE().
                SET autoPilotMode TO takeoffMode.
                SET toOrbit TO TRUE.
                headsUpText("Takeoff!").
                BRAKES OFF.
                LIGHTS ON.
                IF (MAXTHRUST = 0) {
                    STAGE.
                }
                SET autoTakeOffDone TO TRUE.
            }.
        }
        DECLARE LOCAL autoTakeOffManual TO guiTakeOff:ADDBUTTON("Manual").

        guiTakeOff:SHOW().

        SET autoTakeOffYes:ONCLICK TO {
            SET startingHeading TO autoTakeOffHeadingText:TEXT:TONUMBER(startingHeading).
            SET takeoffAltitude TO autoTakeOffAltitudeText:TEXT:TONUMBER(takeoffAltitude).
            guiTakeOff:HIDE().
            SET autoPilotMode TO takeoffMode.
            headsUpText("Takeoff!").
            BRAKES OFF.
            LIGHTS ON.
            SET brakeOnSpoolUp TO brakeForSpoolUp().
            IF (MAXTHRUST = 0) {
                STAGE.
            }
            SET autoTakeOffDone TO TRUE.
        }.

        SET autoTakeOffManual:ONCLICK TO {
            SET takeoffAltitude TO autoTakeOffAltitudeText:TEXT:TONUMBER(takeoffAltitude).
            guiTakeOff:HIDE().
            BRAKES OFF.
            WAIT UNTIL (ALT:RADAR > takeoffAltitude).
            SET autoTakeOffDone TO TRUE.
            SET targetSpeed TO SHIP:AIRSPEED.
            SET targetAltitude TO SHIP:ALTITUDE.
        }.
        WAIT UNTIL autoTakeOffDone.
    }

    // ****************
    // GUI setup
    // ****************

    DECLARE LOCAL FUNCTION setWaypoint {
        DECLARE PARAMETER guiWaypoint, runwayEndOne, runwayEndTwo, text.

        IF (SHIP:STATUS = orbitingStatus) {
            SET autoPilotMode TO deorbitMode.
        } ELSE {
            SET lateralNavMode TO targetMode.
        }

        SET targetRunwayEndOne to runwayEndOne.
        SET targetRunwayEndTwo TO runwayEndTwo.
        SET targetCoordinates TO runwayEndOne.
        SET targetName TO text.

        guiWaypoint:HIDE().
    }

    DECLARE LOCAL FUNCTION setAutopilotNav {
        SET autoPilotMode TO navMode.
        SET verticalNavMode TO altMode.
        SET lateralNavMode TO bankMode.
        SET autoPilotAndThrottleEnabled TO TRUE.
        SET targetAltitude TO ROUND(SHIP:ALTITUDE).
        SET targetHeading TO ROUND(headingMagnitude()).
        SET targetBank TO bankAngle().
    }

    DECLARE LOCAL FUNCTION setAutopilotILS {
        IF (SHIP:STATUS = orbitingStatus) {
            SET autoPilotMode TO deorbitMode.
        } ELSE {
            SET autoPilotMode TO ilsMode.
        }
        SET autoPilotAndThrottleEnabled TO TRUE.
    }

    DECLARE LOCAL FUNCTION setAutopilotOff {
        SET autoPilotMode TO offMode.
    }

    DECLARE LOCAL FUNCTION setHeadingMode {
        SET lateralNavMode TO headingMode.
    }

    DECLARE LOCAL FUNCTION setHeadingWest {
        SET targetHeading TO ((ROUND(targetHeading / 5) * 5) - 5).
        IF (targetHeading < 0) {
            SET targetHeading TO targetHeading + 360.
        }
    }

    DECLARE LOCAL FUNCTION setHeadingEast {
        SET targetHeading TO ((ROUND(targetHeading / 5) * 5) + 5).
        IF (targetHeading > 360) {
            SET targetHeading TO targetHeading - 360.
        }
    }

    DECLARE LOCAL FUNCTION setBankMode {
        SET lateralNavMode TO bankMode.
    }

    DECLARE LOCAL FUNCTION setBankLeft {
        SET targetBank TO ROUND(targetBank) - 1.
        IF (targetBank < -maxBankDegrees) {
            SET targetBank TO -maxBankDegrees.
        }
    }

    DECLARE LOCAL FUNCTION setBankRight {
        SET targetBank TO ROUND(targetBank) + 1.
        IF (targetBank > maxBankDegrees) {
            SET targetBank TO maxBankDegrees.
        }
    }

    DECLARE LOCAL FUNCTION setAltitudeMode {
        SET verticalNavMode TO altMode.
    }

    DECLARE LOCAL FUNCTION setAltitudeDown {
        SET targetAltitude TO (ROUND(targetAltitude / 100) * 100) - 100.
    }

    DECLARE LOCAL FUNCTION setAltitudeUp {
        SET targetAltitude TO (ROUND(targetAltitude / 100) * 100) + 100.
    }

    DECLARE LOCAL FUNCTION setPitchMode {
        SET verticalNavMode TO pitchMode.
    }

    DECLARE LOCAL FUNCTION setPitchDown {
        SET targetPitch TO ROUND(targetPitch) - 1.
        IF (targetPitch < -maxPitchDegrees) {
            SET targetPitch TO -maxPitchDegrees.
        }
    }

    DECLARE LOCAL FUNCTION setPitchUp {
        SET targetPitch TO ROUND(targetPitch) + 1.
        IF (targetPitch > maxPitchDegrees) {
            SET targetPitch TO maxPitchDegrees.
        }
    }

    DECLARE LOCAL FUNCTION setAutothrottleSpeed {
        SET autoThrottleMode TO speedMode.
        SET autoPilotAndThrottleEnabled TO TRUE.
    }

    DECLARE LOCAL FUNCTION setAutothrottleMax {
        SET autoThrottleMode TO maxThrustMode.
        SET autoPilotAndThrottleEnabled TO TRUE.
    }

    DECLARE LOCAL FUNCTION setAutothrottleOff {
        SET autoThrottleMode TO offMode.
    }

    DECLARE LOCAL FUNCTION setSpeedDown {
        SET targetSpeed TO (ROUND(targetSpeed / 5) * 5) - 5.
        IF (targetSpeed < 0) {
            SET targetSpeed TO 0.
        }
    }

    DECLARE LOCAL FUNCTION setSpeedUp {
        SET targetSpeed TO (ROUND(targetSpeed / 5) * 5) + 5.
    }

    DECLARE LOCAL mainGUI TO createAutopilotGUI(setWaypoint@, setAutopilotNav@, setAutopilotILS@,
            setAutopilotOff@, setHeadingMode@, setHeadingWest@, setHeadingEast@, setBankMode@,
            setBankLeft@, setBankRight@, setAltitudeMode@, setAltitudeDown@, setAltitudeUp@,
            setPitchMode@, setPitchDown@, setPitchUp@, setAutothrottleSpeed@, setAutothrottleMax@,
            setAutothrottleOff@, setSpeedDown@, setSpeedUp@).

    DECLARE LOCAL checkboxILSVectors TO mainGUI:WIDGETS[3]:WIDGETS[0].
    DECLARE LOCAL labelMode TO mainGUI:WIDGETS[0].
    DECLARE LOCAL autopilotSettings TO mainGUI:WIDGETS[2].
    DECLARE LOCAL labelWaypoint TO autopilotSettings:WIDGETS[5]:WIDGETS[0].
    DECLARE LOCAL labelWaypointDist TO autopilotSettings:WIDGETS[5]:WIDGETS[1].
    DECLARE LOCAL labelHDG TO autopilotSettings:WIDGETS[0]:WIDGETS[2].
    DECLARE LOCAL labelBNK TO autopilotSettings:WIDGETS[1]:WIDGETS[2].
    DECLARE LOCAL labelALT TO autopilotSettings:WIDGETS[2]:WIDGETS[2].
    DECLARE LOCAL labelPIT TO autopilotSettings:WIDGETS[3]:WIDGETS[2].
    DECLARE LOCAL labelSPD TO mainGUI:WIDGETS[5]:WIDGETS[1].
    DECLARE LOCAL labelAirspeed TO mainGUI:WIDGETS[6].
    DECLARE LOCAL labelVerticalSpeed TO mainGUI:WIDGETS[7].
    DECLARE LOCAL labelLatitude TO mainGUI:WIDGETS[8].
    DECLARE LOCAL labelLongitude TO mainGUI:WIDGETS[9].

    mainGui:SHOW().

    // ****************
    // Abort logic
    // ****************

    ON ABORT {
        SET autoPilotAndThrottleEnabled TO FALSE.
        headsUpText("Your controls!!!").
        PRESERVE.
    }

    IF (isSpacefaring) {
        IF (ORBIT:APOAPSIS > 20000 AND ORBIT:APOAPSIS < BODY:ATM:HEIGHT) {
            IF (VERTICALSPEED < 0) {
                SET autoThrottleMode TO offMode.
                SET autoPilotMode TO deorbitMode.
            } ELSE {
                SET autoPilotMode TO toOrbitMode.
                SET autoThrottleMode TO maxThrustMode.
            }
        } ELSE IF (ORBIT:APOAPSIS > BODY:ATM:HEIGHT) {
            IF (ORBIT:PERIAPSIS < BODY:ATM:HEIGHT) {
                IF (ETA:APOAPSIS < ETA:PERIAPSIS) {
                    SET autoPilotMode TO circularizeMode.
                } ELSE {
                    SET autoPilotMode TO deorbitMode.
                }
            } ELSE {
                SET autoPilotMode TO offMode.
            }
            SET autoThrottleMode TO offMode.
        }
    }

    // ****************
    // Main Loop
    // ****************

    DECLARE LOCAL runwayUnitVector TO V(0, 1, 0).
    DECLARE LOCAL previousSpeed TO 0.
    DECLARE LOCAL azimuthAchieved TO FALSE.
    DECLARE LOCAL ilsVectorDraw TO VECDRAW().

    SAS OFF.
    RCS OFF.

    DECLARE LOCAL mainLoopRunning TO TRUE.
    UNTIL (NOT mainLoopRunning) {
        WAIT 0. // Skip a physics tick

        DECLARE LOCAL angleToRunway TO deltaHeading(MOD(ARCTAN2(runwayUnitVector:Y, runwayUnitVector:X) + 360, 360)).
        DECLARE LOCAL grndDistance TO groundDistance(targetCoordinates).
        DECLARE LOCAL centerLineDist TO centerLineDistance(targetCoordinates, runwayUnitVector).

        IF (autoPilotAndThrottleEnabled) {
            SET autoPilotShutdown TO FALSE.

            IF (autoPilotMode = takeoffMode) {
                SET usingAtmoPIDs TO TRUE.
                SET recoveryEnabled TO FALSE.
                SET autoThrottleMode TO maxThrustMode.
                SET lateralNavMode TO bankMode.
                SET verticalNavMode TO pitchMode.
                SET targetHeading TO startingHeading.
                SET targetBank TO 0.

                IF (brakeOnSpoolUp) {
                    IF (getCurrentThrust() < getPossibleThrust() * .8) {
                        BRAKES ON.
                    } ELSE {
                        BRAKES OFF.
                    }
                }

                //Stay at starting pitch until we're ready to rotate
                IF (AIRSPEED < 1.25 * minimumSpeed) {
                    SET SHIP:CONTROL:WHEELSTEER TO wheelPID:UPDATE(TIME:SECONDS, deltaHeading(targetHeading)).
                    SET targetPitch TO pitchAngle().
                } ELSE {
                    SET targetPitch TO takeoffPitch.
                }

                IF (ALT:RADAR > takeoffAltitude) {
                    IF (NOT toOrbit) {
                        headsUpText("Takeoff complete, leveling out").
                        SET autoPilotMode TO navMode.
                        SET autoThrottleMode TO speedMode.
                        SET lateralNavMode TO bankMode.
                        SET verticalNavMode TO altMode.

                        SET targetBank TO 0.
                        SET targetAltitude TO takeoffAltitude.
                        SET targetSpeed TO AIRSPEED.
                    } ELSE {
                        headsUpText("Takeoff complete, going to orbit").
                        SET autoPilotMode TO toOrbitMode.
                    }
                    IF (KUNIVERSE:CANQUICKSAVE) {
                        KUNIVERSE:QUICKSAVETO("quicksaveStartingFlight").
                    }
                }
            } ELSE IF (autoPilotMode = ilsMode) {
                SET usingAtmoPIDs TO TRUE.

                IF (ABS(ALTITUDE - glideslopeAltitude(targetCoordinates, glideSlopeAngle, glideSlopeHeight)) < 20) {
                    SET recoveryEnabled TO FALSE.
                } ELSE {
                    SET recoveryEnabled TO TRUE.
                }

                // Determine which way we will land on the runway and create runway vector
                DECLARE LOCAL distanceToEndOne TO groundDistance(targetRunwayEndOne).
                DECLARE LOCAL distanceToEndTwo TO groundDistance(targetRunwayEndTwo).
                IF (distanceToEndOne < distanceToEndTwo) {
                    SET targetCoordinates TO targetRunwayEndOne.
                    DECLARE LOCAL latitudeScalar TO targetRunwayEndTwo:LAT - targetRunwayEndOne:LAT.
                    DECLARE LOCAL longitudeScalar TO targetRunwayEndTwo:LNG - targetRunwayEndOne:LNG.
                    SET runwayUnitVector TO V(latitudeScalar, longitudeScalar, 0):NORMALIZED.
                } ELSE {
                    SET targetCoordinates TO targetRunwayEndTwo.
                    DECLARE LOCAL latitudeScalar TO targetRunwayEndOne:LAT - targetRunwayEndTwo:LAT.
                    DECLARE LOCAL longitudeScalar TO targetRunwayEndOne:LNG - targetRunwayEndTwo:LNG.
                    SET runwayUnitVector TO V(latitudeScalar, longitudeScalar, 0):NORMALIZED.
                }

                // Determine if we're on a valid approach
                DECLARE LOCAL coordinateVector TO V(targetCoordinates:LAT - GEOPOSITION:LAT, targetCoordinates:LNG - GEOPOSITION:LNG, 0).

                DECLARE LOCAL glideslopeAlt TO glideslopeAltitude(targetCoordinates, glideSlopeAngle, glideSlopeHeight).
                DECLARE LOCAL validVerticalDiff TO ALTITUDE - targetRunwayEndOne:TERRAINHEIGHT < 2.5 * glideslopeAlt.
                DECLARE LOCAL validHorizontalDiff TO ABS(centerLineDist) < grndDistance.

                // Muliply by the length remaining until runway to see if we're at a valid approach angle
                // 63 = ~30km @ 180°
                DECLARE LOCAL distanceToTurnAroundMultipler TO 63.
                DECLARE LOCAL validAngle TO ABS(angleToRunway) < VDOT(runwayUnitVector, coordinateVector) * distanceToTurnAroundMultipler.

                IF (validVerticalDiff AND validHorizontalDiff AND validAngle) {
                    // Set target altitude for final approach
                    SET verticalNavMode TO altMode.
                    IF (ALT:RADAR + GEOPOSITION:TERRAINHEIGHT + 10 < glideslopeAlt AND AIRSPEED > 350) {
                        SET targetAltitude TO GEOPOSITION:TERRAINHEIGHT + ALT:RADAR + 10.
                    } ELSE {
                        SET targetAltitude TO glideslopeAlt.
                    }

                    DECLARE LOCAL offsetLatitude TO targetCoordinates:LAT + VDOT(runwayUnitVector, coordinateVector) * 2/3 * -runwayUnitVector:X.
                    DECLARE LOCAL offsetLongitude TO targetCoordinates:LNG + VDOT(runwayUnitVector, coordinateVector) * 2/3 * -runwayUnitVector:Y.
                    DECLARE LOCAL targetPoint TO LATLNG(offsetLatitude, offsetLongitude).

                    IF (ABS(centerLineDist) < 2) {
                        SET targetHeading TO targetCoordinates:HEADING.
                    } ELSE {
                        SET targetHeading TO targetPoint:HEADING.
                    }

                    SET lateralNavMode TO headingMode.

                    // Set target airspeed on final approach
                    DECLARE LOCAL altitudeSpeedMultiplier TO 5.
                    SET targetSpeed TO MIN(maxApproachSpeed, MAX(SQRT(glideslopeAlt) * altitudeSpeedMultiplier, minApproachSpeed)).
                    IF (autoThrottleMode <> offMode) {
                        SET autoThrottleMode TO speedMode.
                    }

                    // Brake if we're more than 20% overspeed
                    DECLARE LOCAL brakeSpeedLimit TO 1.2.
                    IF (SHIP:AIRSPEED > targetSpeed * brakeSpeedLimit AND grndDistance < 7000) {
                        BRAKES ON.
                    } ELSE {
                        BRAKES OFF.
                    }
                } ELSE {
                    DECLARE LOCAL safeApproachAltitude TO 3500.
                    DECLARE LOCAL safeApproachDistanceDegrees TO 5.

                    //Start at 3500m up
                    SET targetAltitude TO safeApproachAltitude.
                    SET verticalNavMode TO altMode.

                    //Aim 5° away from runway
                    DECLARE LOCAL startApproachVector TO V(safeApproachDistanceDegrees, safeApproachDistanceDegrees, 0).
                    DECLARE LOCAL offsetLatitude TO targetCoordinates:LAT + startApproachVector:X * -runwayUnitVector:X.
                    DECLARE LOCAL offsetLongitude TO targetCoordinates:LNG + startApproachVector:Y * -runwayUnitVector:Y.
                    DECLARE LOCAL targetPoint TO LATLNG(offsetLatitude, offsetLongitude).

                    SET targetHeading TO targetPoint:HEADING.
                    SET lateralNavMode TO headingMode.
                    SET autoThrottleMode TO speedMode.
                    SET targetSpeed TO 350.
                }
            } ELSE IF (autoPilotMode = flareMode) {
                SET usingAtmoPIDs TO TRUE.
                SET recoveryEnabled TO FALSE.
                // Configure Flare mode
                DECLARE LOCAL altitudeToLevelOut TO 30.

                IF (verticalNavMode <> verticalSpeedMode) {
                    SET verticalNavMode TO verticalSpeedMode.
                    SET targetHeading TO MOD(ARCTAN2(runwayUnitVector:Y, runwayUnitVector:X) + 360, 360).
                    SET lateralNavMode TO headingMode.
                    SET targetSpeed TO minimumSpeed.
                }

                // Adjust craft flight
                IF (ALT:RADAR > altitudeToCutThrottle) {
                    SET targetVerticalSpeed TO -3.
                    IF (ALTITUDE < targetCoordinates:TERRAINHEIGHT + altitudeToLevelOut) {
                        SET lateralNavMode TO bankMode.
                        SET targetBank TO 0.
                    }
                } ELSE IF (SHIP:STATUS <> landedStatus) {
                    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
                    SET autoThrottleMode TO offMode.
                    SET targetVerticalSpeed TO -1.
                } ELSE {
                    SET verticalNavMode TO pitchMode.
                    SET targetPitch TO pitchOnTouchdown.
                    BRAKES ON.
                }
            } ELSE IF (autoPilotMode = recoveryMode) {
                SET usingAtmoPIDs TO TRUE.
                SET recoveryEnabled TO TRUE.
                IF ((recoveryType = stall AND ABS(angleOfAttack()) < maxAngleOfAttack)
                        OR (recoveryType = avoidTerrain AND ALT:RADAR > 100 AND VERTICALSPEED > 0)
                        OR (recoveryType = avoidHeights AND ALTITUDE > maxAltitudeToAvoid)) {
                    SET verticalNavMode TO previousVertNavMode.
                    SET lateralNavMode TO previousLatNavMode.
                    SET autoThrottleMode TO previousAutoThrottle.
                    SET autoPilotMode TO previousAutoPilot.
                    SET targetBank TO previousBank.
                    SET targetSpeed TO previousSpeed.
                    SET aileronPID:MAXOUTPUT TO 1.
                    SET aileronPID:MINOUTPUT TO -1.
                    SET elevatorPID:MAXOUTPUT TO 1.
                    SET elevatorPID:MINOUTPUT TO -1.
                    SET maxAltitudeToAvoid TO 0.
                    headsUpText("Recovery completed").
                } ELSE {
                    IF (sideslipAngle() < 2 AND ABS(rudder) < .5) {
                        SET aileronPID:MAXOUTPUT TO 1.
                        SET aileronPID:MINOUTPUT TO -1.
                        SET lateralNavMode TO bankMode.
                        SET targetBank TO 0.
                        IF (bankAngle() < 20 AND ABS(aileron) < .5) {
                            SET elevatorPID:MAXOUTPUT TO 1.
                            SET elevatorPID:MINOUTPUT TO -1.
                            SET verticalNavMode TO pitchMode.
                            IF (recoveryType = stall) {
                                SET autoThrottleMode TO maxThrustMode.
                                SET targetPitch TO progradePitchAngle().
                            } ELSE IF (recoveryType = avoidTerrain OR recoveryType = avoidHeights) {
                                SET targetPitch TO maxPitchDegrees.
                            }
                        } ELSE {
                            SET verticalNavMode TO recoveryMode.
                            SET elevatorPID:MAXOUTPUT TO 0.
                            SET elevatorPID:MINOUTPUT TO 0.
                        }
                    } ELSE {
                        SET lateralNavMode TO recoveryMode.
                        SET verticalNavMode TO recoveryMode.
                        SET elevatorPID:MAXOUTPUT TO 0.
                        SET elevatorPID:MINOUTPUT TO 0.
                        SET aileronPID:MAXOUTPUT TO 0.
                        SET aileronPID:MINOUTPUT TO 0.
                    }
                    IF (recoveryType = stall OR recoveryType = avoidHeights) {
                        SET autoThrottleMode TO maxThrustMode.
                    } ELSE IF (recoveryType = avoidTerrain) {
                        SET autoThrottleMode TO speedMode.
                        SET targetSpeed TO 0.
                    }
                }
            } ELSE IF (autoPilotMode = toOrbitMode) {
                SET usingAtmoPIDs TO TRUE.
                SET recoveryEnabled TO FALSE.

                SET lateralNavMode TO headingMode.
                SET targetHeading TO azimuth(targetInclination, targetAltitude).

                IF (ABS(deltaHeading(targetHeading)) < 2 AND ABS(bankAngle()) < 10) {
                    SET azimuthAchieved TO TRUE.
                }

                IF (azimuthAchieved) {
                    SET autoThrottleMode TO maxThrustMode.
                    SET verticalNavMode TO pitchMode.
                    SET targetPitch TO 9.
                } ELSE {
                    SET autoThrottleMode TO speedMode.
                    SET targetSpeed TO 2 * minimumSpeed.
                    SET verticalNavMode TO altMode.
                    SET targetAltitude TO GEOPOSITION:TERRAINHEIGHT + takeoffAltitude * 2.
                }

                DECLARE LOCAL currentSpeed TO VELOCITY:SURFACE:MAG.
                IF (BODY:ATM:ALTITUDEPRESSURE(SHIP:ALTITUDE) < pressureToSwitchRapiers AND currentSpeed < previousSpeed) {
                    headsUpText("Switching to closed cycle").
                    setRapierAirBreathing(FALSE).
                }
                IF (ORBIT:APOAPSIS > targetOrbitAltitude + targetOrbitMargin) {
                    SET targetPitch TO progradePitchAngle().
                    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
                    SET autoThrottleMode TO offMode.
                    SET autoPilotMode TO circularizeMode.
                    UNLOCK STEERING.
                    SET SHIP:CONTROL:NEUTRALIZE TO TRUE.

                    headsUpText("Coasting to target apoapsis").
                }
                SET previousSpeed TO currentSpeed.
            } ELSE IF (autoPilotMode = offMode) { // Manual mode
                SET usingAtmoPIDs TO FALSE.
                SET recoveryEnabled TO FALSE.
                SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
            } ELSE IF (autoPilotMode = navMode) {
                SET usingAtmoPIDs TO TRUE.
                SET recoveryEnabled TO TRUE.
            } ELSE IF (autoPilotMode = circularizeMode) {
                SET usingAtmoPIDs TO FALSE.
                SET recoveryEnabled TO FALSE.
                IF (SHIP:STATUS <> orbitingStatus) {
                    RCS ON.
                    SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
                    LOCK STEERING TO SRFPROGRADE.

                    // Create orbital burn if we're above the atmosphere
                    IF (ALTITUDE > BODY:ATM:HEIGHT) {
                        DECLARE LOCAL deltaV TO calculateSemiMajorAxisDeltaV(targetOrbitAltitude).
                        DECLARE LOCAL apoapsisNode TO NODE(ORBIT:ETA:APOAPSIS + TIME:SECONDS, 0, 0, deltaV).
                        ADD apoapsisNode.
                        headsUpText("Executing node to circularize").
                        executeNode(apoapsisNode, updateFlightGui@).
                        UNLOCK STEERING.
                        SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
                        RCS OFF.
                    }
                } ELSE {
                    SET autoPilotMode TO offMode.
                }
            } ELSE IF (autoPilotMode = deorbitMode) {
                SET recoveryEnabled TO FALSE.

                DECLARE LOCAL latitudeScalar TO targetRunwayEndTwo:LAT - targetRunwayEndOne:LAT.
                DECLARE LOCAL longitudeScalar TO targetRunwayEndTwo:LNG - targetRunwayEndOne:LNG.
                SET runwayUnitVector TO V(latitudeScalar, longitudeScalar, 0):NORMALIZED.
                DECLARE LOCAL runwayHeading TO MOD(ARCTAN2(runwayUnitVector:Y, runwayUnitVector:X) + 360, 360).
                DECLARE LOCAL inclinationAngleToRunway TO (90 - ORBIT:INCLINATION) - runwayHeading.

                IF (ABS(inclinationAngleToRunway) > 90) {
                    SET runwayUnitVector TO V(-latitudeScalar, -longitudeScalar, 0):NORMALIZED.
                    SET targetCoordinates TO targetRunwayEndOne.
                } ELSE {
                    SET targetCoordinates TO targetRunwayEndTwo.
                }

                DECLARE LOCAL safeApproachDistanceDegrees TO 2.25.
                DECLARE LOCAL startApproachVector TO V(safeApproachDistanceDegrees, safeApproachDistanceDegrees, 0).
                DECLARE LOCAL offsetLatitude TO targetCoordinates:LAT + startApproachVector:X * -runwayUnitVector:X.
                DECLARE LOCAL offsetLongitude TO targetCoordinates:LNG + startApproachVector:Y * -runwayUnitVector:Y.
                SET targetCoordinates TO LATLNG(offsetLatitude, offsetLongitude).

                IF (SHIP:STATUS = orbitingStatus) {
                    IF (ABS(targetCoordinates:LAT) > ORBIT:INCLINATION + .5) {
                        headsUpText("Target latitude too high for current inclination").
                        SET autoPilotMode TO offMode.
                    } ELSE {
                        DECLARE LOCAL distanceToTouchdown TO 1850000 + 250000 * COS(ORBIT:INCLINATION).
                        IF (BODY = LAYTHE) {
                            SET distanceToTouchdown TO 750000 + 65000 * COS(ORBIT:INCLINATION).
                        }
                        DECLARE LOCAL bestApproachTime TO 0.
                        DECLARE LOCAL closestApproach TO 360.
                        DECLARE LOCAL maxOrbitCount TO 1000.
                        DECLARE LOCAL maxApproachDiff TO .5.
                        DECLARE LOCAL orbitCount TO 0.
                        DECLARE LOCAL closeApproach TO FALSE.

                        UNTIL (closeApproach) {
                            CLEARSCREEN.
                            DECLARE LOCAL timeOffset TO ORBIT:PERIOD * orbitCount.
                            DECLARE LOCAL currentOrbit TO ORBITAT(SHIP, TIME:SECONDS + timeOffset).
                            DECLARE LOCAL inclineSine TO SIN(currentOrbit:INCLINATION).

                            DECLARE LOCAL targetLat TO targetCoordinates:LAT.
                            DECLARE LOCAL targetLong TO targetCoordinates:LNG.

                            DECLARE LOCAL trueAnomalyAtDestination TO 0.
                            IF (ORBIT:INCLINATION > 1 AND ORBIT:INCLINATION < 179) {
                                DECLARE LOCAL sineRatio TO SIN(targetLat) / inclineSine.
                                SET trueAnomalyAtDestination TO MOD(ARCSIN(sineRatio) - currentOrbit:ARGUMENTOFPERIAPSIS + 360, 360).
                            }

                            DECLARE LOCAL meanAnomalyAtDestination TO calculateMeanFromTrueAnomaly(trueAnomalyAtDestination, currentOrbit).
                            DECLARE LOCAL currentMeanAnomaly TO calculateMeanAnomalyFromTime().
                            DECLARE LOCAL meanAnomalyToDestination TO MOD(meanAnomalyAtDestination - currentMeanAnomaly + 360, 360).
                            DECLARE LOCAL timeToDestination TO calculateTimeFromMeanAnomaly(meanAnomalyToDestination).

                            DECLARE LOCAL rotationFraction TO currentOrbit:PERIOD / BODY:ROTATIONPERIOD.
                            DECLARE LOCAL deltaLng TO -360 * rotationFraction.
                            DECLARE LOCAL longitudeToDestination TO deltaLng * timeToDestination / ORBIT:PERIOD.

                            DECLARE LOCAL postionAtDestination TO POSITIONAT(SHIP, TIME:SECONDS + timeToDestination).
                            DECLARE LOCAL coordinatesAtDestination TO currentOrbit:BODY:GEOPOSITIONOF(postionAtDestination).
                            DECLARE LOCAL currentLongitude TO coordinatesAtDestination:LNG + longitudeToDestination + MOD(deltaLng * orbitCount, 360).
                            SET coordinatesAtDestination TO LATLNG(coordinatesAtDestination:LAT, currentLongitude).

                            DECLARE LOCAL anomalyTraveled TO distanceToTouchdown * 360 / (2 * CONSTANT:PI * BODY:RADIUS).
                            DECLARE LOCAL meanAnomalyAtBurn TO MOD(meanAnomalyAtDestination - anomalyTraveled + 360, 360).
                            DECLARE LOCAL meanAnomalyToBurn TO meanAnomalyAtBurn - currentMeanAnomaly.

                            DECLARE LOCAL timeToBurn TO calculateTimeFromMeanAnomaly(meanAnomalyToBurn) + timeOffset.

                            IF (meanAnomalyToBurn < 0) {
                                SET timeToBurn TO calculateTimeFromMeanAnomaly(meanAnomalyToBurn + 360) - ORBIT:PERIOD + timeOffset.
                            } ELSE {
                                SET timeToBurn TO calculateTimeFromMeanAnomaly(meanAnomalyToBurn) + timeOffset.
                            }

                            DECLARE LOCAL currentApproach TO ABS(coordinatesAtDestination:LNG - targetLong).
                            IF (currentApproach < closestApproach) {
                                SET closestApproach TO currentApproach.
                                SET bestApproachTime TO timeToBurn.
                            }

                            IF ((closestApproach < maxApproachDiff OR orbitCount > maxOrbitCount) AND timeToBurn > 0) {
                                SET closeApproach TO TRUE.
                            } ELSE {
                                SET orbitCount TO orbitCount + 1.
                            }
                        }
                        DECLARE LOCAL deltaV TO calculateSemiMajorAxisDeltaV(0).
                        DECLARE LOCAL reentryNode TO NODE(TIME:SECONDS + bestApproachTime, 0, 0, deltaV).
                        ADD reentryNode.
                        headsUpText("Executing node to circularize").
                        executeNode(reentryNode, updateFlightGui@).
                    }
                } ELSE IF (SHIP:STATUS = subOrbitalStatus) {
                    SET usingAtmoPIDs TO FALSE.
                    LOCK STEERING TO HEADING(VANG(SRFPROGRADE:VECTOR, NORTH:VECTOR), 10).
                } ELSE IF (SHIP:STATUS = flyingStatus) {
                    DECLARE LOCAL reentryStartPitch TO 10.

                    IF (ALTITUDE < maxControlAltitude) {
                        DECLARE LOCAL settlingAltitude TO 1000.
                        DECLARE LOCAL underGlideslopePitch TO 5.
                        DECLARE LOCAL maxHighAltBankDegrees TO 15.
                        DECLARE LOCAL maxGlideslopeBankDegrees TO 10.
                        DECLARE LOCAL maxGlideslopePitchDegrees TO 5.
                        DECLARE LOCAL maxILSPitchDegrees TO 7.
                        DECLARE LOCAL cubicFactor TO -.0000000000114151853931434.
                        DECLARE LOCAL quadraticFactor TO 0.000001733314203282.
                        DECLARE LOCAL linearFactor TO 0.173883952103085.
                        DECLARE LOCAL constantFactor TO -2401.55228022838.
                        DECLARE LOCAL glideslopeOffset TO 10000 + 5000 * COS(angleToRunway).

                        DECLARE LOCAL groundDistanceToApproach TO groundDistance(targetCoordinates) + glideslopeOffset.
                        SET usingAtmoPIDs TO TRUE.

                        IF (ALTITUDE < maxControlAltitude - settlingAltitude) {
                            SET lateralNavMode TO targetMode.
                        } ELSE {
                            SET lateralNavMode TO bankMode.
                            SET targetBank TO 0.
                        }

                        IF (ALTITUDE > glideslopeStartAltitude) {
                            IF (NOT usingHighAltPIDs) {
                                SET aileronPID TO highAltAileronPID.
                                SET yawDamperPID TO highAltYawDamperPID.
                                SET elevatorPID TO highAltElevatorPID.

                                aileronPID:RESET.
                                yawDamperPID:RESET.
                                elevatorPID:RESET.
                                SET usingHighAltPIDs TO TRUE.
                            }
                            DECLARE LOCAL circleDistance TO circleDistance(targetCoordinates).
                            SET verticalNavMode TO altMode.
                            SET targetAltitude TO (circleDistance + 1185000) / 53.
                            SET pitchAnglePID:MAXOUTPUT TO maxGlideslopePitchDegrees.
                            SET pitchAnglePID:MINOUTPUT TO -maxGlideslopePitchDegrees.
                            SET bankAnglePID:MAXOUTPUT TO maxHighAltBankDegrees.
                            SET bankAnglePID:MINOUTPUT TO -maxHighAltBankDegrees.
                        } ELSE IF (ALTITUDE - targetRunwayEndOne:TERRAINHEIGHT > ilsStartAltitude) {
                            IF (usingHighAltPIDs) {
                                SET bankAnglePID:MAXOUTPUT TO maxGlideslopeBankDegrees.
                                SET bankAnglePID:MINOUTPUT TO -maxGlideslopeBankDegrees.
                                SET aileronPID TO lowAltAileronPID.
                                SET yawDamperPID TO lowAltYawDamperPID.
                                SET elevatorPID TO lowAltElevatorPID.

                                aileronPID:RESET.
                                yawDamperPID:RESET.
                                elevatorPID:RESET.

                                SET usingHighAltPIDs TO FALSE.
                                setRapierAirBreathing(TRUE).
                            }

                            UNLOCK STEERING.
                            SET verticalNavMode TO altMode.
                            SET pitchAnglePID:MAXOUTPUT TO maxGlideslopePitchDegrees.
                            SET pitchAnglePID:MINOUTPUT TO minGlideslopePitchDegrees.

                            IF (groundDistanceToApproach > glideslopeStartDistance) {
                                SET targetAltitude TO glideslopeStartAltitude.
                            } ELSE {
                                SET targetAltitude TO groundDistanceToApproach ^ 3 * cubicFactor +
                                        groundDistanceToApproach ^ 2 * quadraticFactor +
                                        groundDistanceToApproach * linearFactor +
                                        constantFactor.
                            }
                        } ELSE {
                            headsUpText("Starting runway approach").
                            SET autopilotMode TO ilsMode.
                            SET autoThrottleMode TO speedMode.
                            IF (BODY = KERBIN AND AIRSPEED > 350) {
                                SET pitchAnglePID:MAXOUTPUT TO maxILSPitchDegrees.
                                SET pitchAnglePID:MINOUTPUT TO -maxILSPitchDegrees.
                            } ELSE {
                                SET pitchAnglePID:MAXOUTPUT TO maxPitchDegrees.
                                SET pitchAnglePID:MINOUTPUT TO -maxPitchDegrees.
                            }
                            SET bankAnglePID:MAXOUTPUT TO maxBankDegrees.
                            SET bankAnglePID:MINOUTPUT TO -maxBankDegrees.
                        }
                    } ELSE {
                        SET usingAtmoPIDs TO FALSE.
                        LOCK STEERING TO HEADING(circleBearing(targetCoordinates), reentryStartPitch).
                    }
                }
            }

            // Common autopilot code
            IF (usingAtmoPIDs) {
                SAS OFF.
                RCS OFF.

                // Deal with lateral navigation
                IF (lateralNavMode = targetMode) {
                    IF (circleDistance(targetCoordinates) < 100) {
                        SET lateralNavMode TO bankMode.
                        SET targetBank TO 0.
                        SET aileronPID:SETPOINT TO MIN(maxBankDegrees, MAX(-maxBankDegrees, targetBank)).
                        headsUpText("Overflying target, switching to bank mode.").
                    } ELSE {
                        SET targetHeading TO circleBearing(targetCoordinates).
                        SET aileronPID:SETPOINT TO bankAnglePID:UPDATE(TIME:SECONDS, -deltaHeading(targetHeading)).
                        SET targetBank TO aileronPID:SETPOINT.
                    }
                } ELSE IF (lateralNavMode = headingMode) {
                    SET aileronPID:SETPOINT TO bankAnglePID:UPDATE(TIME:SECONDS, -deltaHeading(targetHeading)).
                    SET targetBank TO aileronPID:SETPOINT.
                } ELSE IF (lateralNavMode = bankMode) {
                    SET aileronPID:SETPOINT TO MIN(maxBankDegrees, MAX(-maxBankDegrees, targetBank)).
                    SET targetBank TO aileronPID:SETPOINT.
                }

                SET aileron TO aileronPID:UPDATE(TIME:SECONDS, bankAngle()).

                // Deal with vertical navigation
                IF (verticalNavMode = altMode) {
                    SET pitchAnglePID:SETPOINT TO targetAltitude.
                    SET elevatorPID:SETPOINT TO pitchAnglePID:UPDATE(TIME:SECONDS, ALTITUDE).
                    SET targetPitch TO elevatorPID:SETPOINT.
                } ELSE IF (verticalNavMode = pitchMode) {
                    SET elevatorPID:SETPOINT TO targetPitch.
                } ELSE IF (verticalNavMode = verticalSpeedMode) {
                    SET verticalSpeedPID:SETPOINT TO targetVerticalSpeed.
                }

                IF (verticalNavMode = verticalSpeedMode) {
                    SET elevator TO verticalSpeedPID:UPDATE(TIME:SECONDS, VERTICALSPEED).
                } ELSE {
                    SET elevator TO elevatorPID:UPDATE(TIME:SECONDS, pitchAngle()).
                }

                // Reset trim
                SET SHIP:CONTROL:ROLLTRIM TO 0.
                SET SHIP:CONTROL:PITCHTRIM TO 0.

                // Set yaw to reduce sideslip
                SET rudder TO yawDamperPID:UPDATE(TIME:SECONDS, sideslipAngle()).
                SET SHIP:CONTROL:YAW TO rudder.

                // Apply Controls
                SET SHIP:CONTROL:ROLL TO aileron.
                SET SHIP:CONTROL:PITCH TO elevator.
            }

            // Stall and Terrain Protections
            // Check for stall before terrain
            IF (recoveryEnabled AND autoPilotMode <> recoveryMode) {
                DECLARE LOCAL recoveryNeeded TO FALSE.
                DECLARE LOCAL altitudeToRecover TO ABS(VERTICALSPEED * 8).
                DECLARE LOCAL timeToGainAltitude TO 8.
                DECLARE LOCAL lookAheadDistance TO timeToGainAltitude * AIRSPEED.
                DECLARE LOCAL terrainPoint TO circleDestination(headingMagnitude(), lookAheadDistance).
                SET altitudeToAvoid TO terrainPoint:TERRAINHEIGHT + 15.
                SET maxAltitudeToAvoid TO MAX(maxAltitudeToAvoid, altitudeToAvoid).

                IF (ABS(angleOfAttack()) > maxAngleOfAttack) {
                    SET recoveryNeeded TO TRUE.
                    SET recoveryType TO stall.
                    playStallWarning().
                    headsUpText("Stick pusher!").
                } ELSE IF (VERTICALSPEED < 0 AND ALT:RADAR < altitudeToRecover) {
                    SET recoveryNeeded TO TRUE.
                    SET recoveryType TO avoidTerrain.
                    playTerrainWarning().
                    headsUpText("Terrain! Pulling up!").
                } ELSE IF (altitudeToAvoid > ALTITUDE + VERTICALSPEED * timeToGainAltitude) {
                    IF (verticalNavMode = altMode) {
                        SET targetAltitude TO altitudeToAvoid.
                    } ELSE IF (verticalNavMode = pitchMode) {
                        SET targetPitch TO 3.
                    }
                    SET recoveryNeeded TO TRUE.
                    SET recoveryType TO avoidHeights.
                    playTerrainWarning().
                    headsUpText("Terrain ahead! Pulling up!").
                }

                IF (recoveryNeeded) {
                    SET previousVertNavMode TO verticalNavMode.
                    SET previousLatNavMode TO lateralNavMode.
                    SET previousAutoThrottle TO autoThrottleMode.
                    SET previousAutoPilot TO autoPilotMode.
                    SET previousBank TO targetBank.
                    SET previousSpeed TO targetSpeed.

                    SET autoPilotMode TO recoveryMode.
                    SET verticalNavMode TO recoveryMode.
                    SET autoThrottleMode TO recoveryMode.
                    SET lateralNavMode TO recoveryMode.
                }
            }

            // Autothrottle
            IF (autoThrottleMode = speedMode) {
                SET throttlePID:SETPOINT TO targetSpeed.
                DECLARE LOCAL throttleValue TO throttlePID:UPDATE(TIME:SECONDS, AIRSPEED).
                SET SHIP:CONTROL:PILOTMAINTHROTTLE TO throttleValue.
            } ELSE IF (autoThrottleMode = maxThrustMode) {
                SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 1.
            } ELSE IF (autoThrottleMode = offMode) {
                UNLOCK THROTTLE.
            }

            // Auto raise/low gear and detect time to flare when landing
            IF (ALTITUDE < flareAltitude * 2 + targetCoordinates:TERRAINHEIGHT) {
                GEAR ON.
                LIGHTS ON.
                // Change to flare mode
                IF (autoPilotMode = ilsMode AND ALTITUDE < flareAltitude + targetCoordinates:TERRAINHEIGHT) {
                    headsUpText("Flaring").
                    IF (KUNIVERSE:CANQUICKSAVE) {
                        KUNIVERSE:QUICKSAVETO("quicksaveFlare").
                    }
                    SET autoPilotMode TO flareMode.
                }
            } ELSE {
                LIGHTS OFF.
                GEAR OFF.
            }
        } ELSE {
            // Total Autopilot shutdown. Show info only
            IF (NOT autoPilotShutdown) {
                SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
                UNLOCK THROTTLE.
                SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
                IF (NOT SAS) {
                    SAS ON.
                }
                SET autoPilotShutdown TO TRUE.
            }
        }

        // ILS vectors
        IF (checkboxILSVectors:PRESSED) {
            DECLARE LOCAL lengthOfGlideslopeDegrees TO 5.
            DECLARE LOCAL bodyRadius TO SHIP:BODY:RADIUS.
            DECLARE LOCAL ilsStart TO targetCoordinates:POSITION + V(0, 0, glideslopeHeight).
            DECLARE LOCAL ilsEndPosition TO LATLNG(targetCoordinates:LAT - runwayUnitVector:X * lengthOfGlideslopeDegrees, targetCoordinates:LNG - runwayUnitVector:Y * lengthOfGlideslopeDegrees).
            DECLARE LOCAL ilsEndPositionAltitude TO targetCoordinates:TERRAINHEIGHT + glideslopeHeight + TAN(glideSlopeAngle) * lengthOfGlideslopeDegrees * (2 * CONSTANT:PI * bodyRadius / 360).
            DECLARE LOCAL curveOfKerbinAdjust TO SQRT(bodyRadius ^ 2 + (TAN(lengthOfGlideslopeDegrees) * bodyRadius) ^ 2) - bodyRadius.

            SET ilsVectorDraw TO VECDRAW(ilsStart, ilsEndPosition:ALTITUDEPOSITION(ilsEndPositionAltitude + curveOfKerbinAdjust), MAGENTA, "", 1.0, TRUE, 1, FALSE, TRUE).
        } ELSE {
            CLEARVECDRAWS().
        }

        // GUI element update
        IF (autoPilotShutdown) {
            SET labelMode:TEXT TO "<b><size=17>INP | INP | INP | INP</size></b>".
            SET labelWaypointDist:TEXT TO "".
            SET labelHDG:TEXT TO "".
            SET labelALT:TEXT TO "".
            SET labelBNK:TEXT TO "".
            SET labelPIT:TEXT TO "".
            SET labelSPD:TEXT TO "".
        } ELSE {
            SET labelMode:TEXT TO "<b><size=17>" + autoPilotMode +" | " + verticalNavMode + " | " + lateralNavMode + " | " + autoThrottleMode +"</size></b>".
            SET labelWaypoint:TEXT TO targetName.
            SET labelWaypointDist:TEXT TO ROUND(circleDistance(targetCoordinates) / 1000, 1) + " km".
            SET labelHDG:TEXT TO "<b>" + ROUND(targetHeading, 2):TOSTRING + "º</b>".
            SET labelALT:TEXT TO "<b>" + ROUND(targetAltitude, 2):TOSTRING + " m</b>".
            SET labelBNK:TEXT TO "<b>" + ROUND(targetBank, 2) + "º</b>".
            SET labelPIT:TEXT TO "<b>" + ROUND(targetPitch, 2) + "º</b>".
            SET labelSPD:TEXT TO "<b>" + ROUND(targetSpeed) + " m/s | " + ROUND(metersPerSecondToKPH(targetSpeed), 2) + " km/h</b>".
        }
        SET labelAirspeed:TEXT TO "<b>Airspeed:</b> " + ROUND(SHIP:AIRSPEED) + " m/s" +
                " | Mach " + ROUND(getMachNumber(SHIP:AIRSPEED), 3).
        SET labelVerticalSpeed:TEXT TO "<b>Vertical speed:</b> " + ROUND(SHIP:VERTICALSPEED) + " m/s".
        SET labelLatitude:text TO "<b>LAT:</b> " + ROUND(SHIP:GEOPOSITION:LAT, 4) + "º".
        SET labelLongitude:text TO "<b>LNG:</b> " + ROUND(SHIP:GEOPOSITION:LNG, 4) + "º".

        // Update flight stats
        DECLARE LOCAL latitiudeChange TO ABS(SHIP:GEOPOSITION:LAT - prevCoordinates:LAT).
        DECLARE LOCAL longitudeChange TO ABS(SHIP:GEOPOSITION:LNG - prevCoordinates:LNG).
        SET prevCoordinates TO SHIP:GEOPOSITION.
        SET flightSurfaceDistance TO flightSurfaceDistance + latitiudeChange + longitudeChange.
        SET maxSpeed TO MAX(maxSpeed, AIRSPEED).
        SET maxAltitude TO MAX(maxAltitude, ALTITUDE).
        IF (hasAccelerometer()) {
            SET maxGees TO MAX(maxGees, SHIP:SENSORS:ACC:MAG).
        }

        // Print console info
        DECLARE LOCAL mode TO autoThrottleMode.
        IF (autoPilotAndThrottleEnabled) {
            SET mode TO mode + " " + autoPilotMode.
        } ELSE {
            SET mode TO mode + " MANUAL".
        }
        DECLARE LOCAL gsAltitude TO glideslopeAltitude(targetCoordinates, glideSlopeAngle, glideSlopeHeight).
        printPlaneStats(mode, autoPilotMode, elevator, aileron, rudder, wheelPID:OUTPUT,
                targetCoordinates:TERRAINHEIGHT, gsAltitude, grndDistance, centerLineDist, angleToRunway,
                targetVerticalSpeed).

        IF ((SHIP:STATUS = landedStatus OR SHIP:STATUS = splashedStatus) AND AIRSPEED < 5 AND autoPilotMode <> takeoffMode) {
            SET mainLoopRunning TO FALSE.
        }
    }

    // ****************
    // Landing
    // ****************

    BRAKES ON.

    WAIT UNTIL AIRSPEED < 1.

    SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
    SAS OFF.
    RCS OFF.

    CLEARSCREEN.
    DECLARE LOCAL flightSurfaceMeters TO flightSurfaceDistance / 360 * 2 * CONSTANT:PI * BODY:RADIUS.

    PRINT "Landed at                (" + ROUND(SHIP:GEOPOSITION:LAT, 2) + ", " + ROUND(SHIP:GEOPOSITION:LNG, 2) + ")".
    PRINT "Flight time:             " + formatTime(TIME:SECONDS - flightStartTime).
    PRINT "Flight Distance:         " + ROUND(flightSurfaceMeters / 1000, 1) + "km".
    PRINT "Top Speed:               " + ROUND(maxSpeed, 2) + "m/s".
    PRINT "Average Ground Speed:    " + ROUND(flightSurfaceMeters / (TIME:SECONDS - flightStartTime), 1) + "m/s".
    PRINT "Highest Altitude:        " + ROUND(maxAltitude) + "m".
    IF (maxGees <> 0) {
        PRINT "Max Gees:                " + ROUND(maxGees / 9.8, 2) + "g".
    }

    IF (KUNIVERSE:CANQUICKSAVE) {
        KUNIVERSE:QUICKSAVETO("quicksaveLanded").
    }
    CLEARVECDRAWS().
    CLEARGUIS().

    SET CONFIG:IPU TO oldIPU.
}

DECLARE LOCAL FUNCTION brakeForSpoolUp {
    DECLARE LOCAL engineList TO LIST().
    LIST engines IN engineList.
    FOR engine in engineList {
        IF (engine:NAME = "TurboFanEngine") {
            RETURN TRUE.
        }
    }
    RETURN FALSE.
}

DECLARE LOCAL FUNCTION getPossibleThrust {
    DECLARE LOCAL engineList TO LIST().
    LIST engines IN engineList.
    FOR engine in engineList {
        IF (engine:NAME = "TurboFanEngine") {
            RETURN engine:POSSIBLETHRUST.
        }
    }
    RETURN 0.
}

DECLARE LOCAL FUNCTION getCurrentThrust {
    DECLARE LOCAL engineList TO LIST().
    LIST engines IN engineList.
    FOR engine in engineList {
        IF (engine:NAME = "TurboFanEngine") {
            RETURN engine:THRUST.
        }
    }
    RETURN 0.
}

DECLARE LOCAL FUNCTION updateFlightGui {
    DECLARE PARAMETER operationStatuses TO LEXICON().
}