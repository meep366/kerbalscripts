@LAZYGLOBAL OFF.

// The function predictlandcoord() returns the geocoordinates where the craft will hit the ground.
// Returns the geoposition of where your craft will be when it is at targetheight above the terrain
DECLARE FUNCTION predictLandingCoordinates {
    DECLARE PARAMETER method TO "precise".
    DECLARE PARAMETER cdTimesA TO 0.
    DECLARE PARAMETER targetHeight TO 0.
    DECLARE PARAMETER shipPosition TO SHIP:POSITION - SHIP:BODY:POSITION.
    DECLARE PARAMETER velocity TO SHIP:VELOCITY:ORBIT.
    DECLARE PARAMETER refBody TO SHIP:BODY.

    DECLARE LOCAL currentLandCoords TO LATLNG(0, 0).
    DECLARE LOCAL landCoordsEvalTime TO 0.
    DECLARE LOCAL prevLandCoordsEvalTime TO landCoordsEvalTime.
    SET landCoordsEvalTime TO TIME:SECONDS.
    DECLARE LOCAL newGeoCoords TO LATLNG(0, 0).

    // Does a stepwise simulation until the craft hits the targetheight
    // Takes about half a second to compute
    // The only method here that takes drag into account
    IF (method = "precise") {
        DECLARE LOCAL simTime TO 0.
        UNTIL (shipPosition:MAG < refBody:RADIUS + MAX(0, newGeoCoords:TERRAINHEIGHT) + targetHeight) {
            DECLARE LOCAL dragAcceleration TO dragForce(cdTimesA, shipPosition:MAG - refBody:RADIUS,
                    velocity - VCRS(refBody:ANGULARVEL, shipPosition), refBody) / SHIP:MASS.
            // Use kepler outside of the atmosphere
            IF (dragAcceleration:mag = 0) {
                DECLARE LOCAL newPositionVelocityTime is positionVelocityTimeAtAltitude(49999).
                SET shipPosition TO newPositionVelocityTime[0].
                SET velocity TO newPositionVelocityTime[1].
                SET simTime TO simTime + newPositionVelocityTime[2].
            // Simulate stepwise inside of the atmosphere
            } ELSE {
                DECLARE LOCAL timestep TO 8.
                SET simTime TO simTime + timestep.
                DECLARE LOCAL accRes TO gravityAcceleration(shipPosition, refBody) + dragAcceleration.
                SET shipPosition TO accRes / 2 * timestep * timestep + velocity * timestep + shipPosition.
                SET velocity TO accRes * timestep + velocity.
                // For the geocoordinates, take the rotation of the planet into account
                SET newGeoCoords to convertPositionVectorToGeoCoords(R(0, refBody:ANGULARVEL:MAG * simTime * CONSTANT:RADTODEG, 0) * shipPosition).
            }
        }
    // Uses the orbit's ellipse to calculate where the ship will hit the planet
    } ELSE IF (method = "kepler") {
        // Assume the terrain is always as high as it is now
        SET newGeoCoords TO convertPositionVectorToGeoCoords(shipPosition).
        DECLARE LOCAL geoHeight TO MAX(0, newGeoCoords:TERRAINHEIGHT).
        DECLARE LOCAL newPositionVelocityTime TO positionVelocityTimeAtAltitude(geoHeight + targetHeight).
        DECLARE LOCAL newPosition TO newPositionVelocityTime[0].
        DECLARE LOCAL deltaT TO newPositionVelocityTime[2].

        // Let the planet rotate and see how high the terrain actually is
        SET newGeoCoords TO convertPositionVectorToGeoCoords(R(0, refBody:ANGULARVEL:MAG * deltaT * CONSTANT:RADTODEG, 0) * newPosition).
        SET geoHeight TO MAX(0, newGeoCoords:TERRAINHEIGHT).
        SET newPositionVelocityTime TO positionVelocityTimeAtAltitude(geoHeight + targetHeight).
        SET newPosition TO newPositionVelocityTime[0].
        SET deltaT TO newPositionVelocityTime[2].
        SET newGeoCoords TO convertPositionVectorToGeoCoords(R(0, refBody:ANGULARVEL:MAG * deltaT * CONSTANT:RADTODEG, 0) * newPosition).
    // Assumes the planet is flat, gravity is constant and drag does not exist
    } ELSE IF (method = "flat") {
        // Assume the terrain is always as high as it is now
        SET newGeoCoords TO convertPositionVectorToGeoCoords(shipPosition).
        DECLARE LOCAL geoHeight TO MAX(0, newGeoCoords:TERRAINHEIGHT).

        // Assume the average gravity is the same as the gravity at avgAltFactor * heightToFall
        DECLARE LOCAL avgAltFactor TO 0.
        DECLARE LOCAL avgPosition TO (avgAltFactor * shipPosition:MAG + (1 - avgAltFactor) * (refBody:RADIUS + geoHeight + targetHeight)) * shipPosition:NORMALIZED.
        DECLARE LOCAL avgGravity TO gravityAcceleration(avgPosition, refBody).

        // The time it would take in the vacuum
        DECLARE LOCAL verticalVelocity TO -avgGravity:NORMALIZED * velocity.
        DECLARE LOCAL deltaT TO (verticalVelocity + SQRT(verticalVelocity * verticalVelocity + 2 * avgGravity:MAG * (shipPosition:MAG - refBody:RADIUS - geoHeight - targetHeight))) / avgGravity:MAG.

        // Update the height of the terrain based on a new estimation of the landing spot
        DECLARE LOCAL newPosition TO avgGravity / 2 * deltaT * deltaT + velocity * deltaT + shipPosition.
        SET newGeoCoords TO convertPositionVectorToGeoCoords(R(0, refBody:ANGULARVEL:MAG * deltaT * CONSTANT:RADTODEG, 0) * newPosition).
        SET geoHeight TO MAX(0, newGeoCoords:TERRAINHEIGHT).

        // Update the avgGravity based on this new estimation
        SET avgPosition TO (avgAltFactor * shipPosition:MAG + (1 - avgAltFactor) * (refBody:RADIUS + geoHeight + targetHeight)) * shipPosition:NORMALIZED.
        SET avgGravity TO gravityAcceleration(avgPosition, refBody).

        // The time to get to the altitude of avgPosition
        SET verticalVelocity TO -avgGravity:NORMALIZED * velocity.
        SET deltaT TO (verticalVelocity + SQRT(verticalVelocity * verticalVelocity + 2 * avgGravity:MAG * (shipPosition - avgPosition):MAG)) / avgGravity:MAG.

        // Correct the directions of the vectors
        SET avgPosition TO avgGravity / 2 * deltaT * deltaT + velocity * deltaT + shipPosition.
        SET avgGravity TO gravityAcceleration(avgPosition, refBody).

        // Correct the vertical velocity
        SET verticalVelocity TO -avgGravity:NORMALIZED * velocity.

        //The time it takes to get to the ground
        SET deltaT TO (verticalVelocity + AQRT(verticalVelocity * verticalVelocity + 2 * avgGravity:MAG * (shipPosition:MAG - refBody:RADIUS - geoHeight - targetHeight))) / avgGravity:MAG.
        SET newPosition TO avgGravity / 2 * deltaT*deltaT + velocity*deltaT + shipPosition.
        SET newGeoCoords TO convertPositionVectorToGeoCoords(R(0, refBody:ANGULARVEL:MAG * deltaT * CONSTANT:RADTODEG, 0) * newPosition).
    } ELSE {
        print "ERROR: No method '" + method + "' for predictLandingCoordinates".
        return -1.
    }
    DECLARE LOCAL prevLandCoords TO currentLandCoords.
    SET currentLandCoords to newGeoCoords.
    RETURN currentLandCoords.
}

// Function uses kepler to calculate the posvec, velocityVector at a certain altitude (past apoapsis) and the time it takes to get there
DECLARE LOCAL FUNCTION positionVelocityTimeAtAltitude {
    DECLARE PARAMETER targetAlt.
    DECLARE PARAMETER startPosition TO SHIP:POSITION - SHIP:BODY:POSITION.
    DECLARE PARAMETER startVelocity TO SHIP:VELOCITY:ORBIT.
    DECLARE PARAMETER refBody TO SHIP:BODY.

    // This is all based on this equation for the radius of an ellipse:
    // r(phi) = r0 / (1 + ecc * cos(phi))
    // where r0 is the radius the orbit with the same angular momentum would have it it were circular
    // and ecc is the eccentricity of the ellipse

    // (angular momentum) / mass is a constant vector
    DECLARE LOCAL rCrossV TO VCRS(startPosition, startVelocity).

    // Get r0 by the angular momentum
    DECLARE LOCAL r0 TO rCrossV:SQRMAGNITUDE / refBody:MU.

    // ecc = sqrt(1 + E / E0) with E="Energy of the orbit" and E0 = GMm / (2r0)
    DECLARE LOCAL ecc TO SQRT(1 + r0 * (startVelocity:SQRMAGNITUDE / refBody:MU - 2 / startPosition:MAG)).

    // The calculation of the startAngle depends on whether we're past apoapsis or not
    DECLARE LOCAL pastApoapsis TO FALSE.
    IF (startPosition * startVelocity < 0) {
        SET pastApoapsis TO TRUE.
    }

    // The angle between periapsis and startPosition
    // (inverse of the ellipsis function)
    DECLARE LOCAL startPhi TO (arccos(1 / ecc * (r0 / startPosition:MAG - 1))).
    IF (pastApoapsis) {
        SET startPhi TO 360 - startPhi.
    }

    // The angle between periapsis and targetPosition where altitude = targetalt
    DECLARE LOCAL targetPhi TO 360 - ARCCOS(1 / ecc * (r0 / (refBody:radius + targetAlt) - 1)).
    DECLARE LOCAL deltaPhi TO targetPhi - startPhi.

    // Rotate the current position by deltaPhi to get the direction of targetPosition
    DECLARE LOCAL targetPosition TO (ANGLEAXIS(deltaPhi, rCrossV) * startPosition):NORMALIZED * (refBody:RADIUS + targetAlt).

    // Calculate the magnitude of the targetVelocity by conservation of energy
    // E = 0.5 * m * v^2 - GMm / r = const.
    DECLARE LOCAL targetVelocityMagnitude TO SQRT(startVelocity:SQRMAGNITUDE + 2 * refBody:MU * (1 / targetPosition:MAG - 1 / startPosition:MAG)).

    // Calculate the angle between targetPosition and targetVelocity by conservation of angular momentum
    // L = m * (rxv) <=> |L| = m * |r| * |v| * sin(phi) = const
    DECLARE LOCAL targetPositionVelocityAngle TO 180 - ARCSIN(rCrossV:MAG / targetPosition:MAG / targetVelocityMagnitude).

    // Rotate the targetPosition by this angle to get the direction of targetVelocity
    DECLARE LOCAL targetVelocity TO (ANGLEAXIS(targetPositionVelocityAngle, rCrossV) * targetPosition):NORMALIZED * targetVelocityMagnitude.

    // Calculate the relevant part of the area of the ellipse
    DECLARE LOCAL partArea TO ellipseArea(targetPhi, ecc, r0) - ellipseArea(startPhi, ecc, r0).

    // Calculate the semi-major axis a
    DECLARE LOCAL a TO r0 / (1 - ecc * ecc).

    // Use Kepler3 to calculate the time it takes to get to targetPosition
    // time / partArea = orbitPeriod / fullArea
    DECLARE LOCAL time TO 2 * partArea / SQRT((1 - ecc * ecc) * a * refBody:MU).

    RETURN LIST(targetPosition, targetVelocity, time).
}

// Calculates the area from periapsis to phi
DECLARE LOCAL FUNCTION ellipseArea {
    DECLARE PARAMETER phi, ecc, r0.

    DECLARE LOCAL k1 TO 1 - ecc * ecc.
    DECLARE LOCAL k2 TO ecc - 1.
    DECLARE LOCAL ta TO TAN(phi / 2).

    // The area integral of an ellipsis
    // with rvec = r(phi) * (cos(phi), sin(phi), 0)^T
    // calculated with geogebra, because wolframalpha was giving me a non continuous function
    RETURN r0 * r0 / k1 * ((CONSTANT:PI * FLOOR(phi / 360 + 0.5)
            - ARCTAN(ta * k2 / SQRT(k1)) * CONSTANT:DEGTORAD) / SQRT(k1) + ecc * ta / (k2 * (ta * ta - 1) - 2)).
}

DECLARE LOCAL FUNCTION gravityAcceleration {
    DECLARE PARAMETER shipPosition TO SHIP:POSITION - SHIP:BODY:POSITION.
    DECLARE PARAMETER refBody TO SHIP:BODY.
    RETURN -refBody:MU * shipPosition:NORMALIZED / shipPosition:SQRMAGNITUDE.
}

DECLARE LOCAL FUNCTION dragForce {
    DECLARE PARAMETER cdTimesA.
    DECLARE PARAMETER height TO SHIP:ALTITUDE.
    DECLARE PARAMETER velocity TO SHIP:VELOCITY:SURFACE.
    DECLARE PARAMETER refBody TO SHIP:BODY.

    RETURN -density(height, refBody) * velocity:NORMALIZED * velocity:SQRMAGNITUDE * cdTimesA / 2000.
}

DECLARE LOCAL FUNCTION density {
    DECLARE PARAMETER height TO SHIP:ALTITUDE.
    DECLARE PARAMETER refBody TO SHIP:BODY.
    DECLARE PARAMETER method TO "quick".

    IF (method = "quick") {
        SET height to height / 1000. //altitude in km
        IF (refBody:name = "Kerbin") {
            IF (height > 50) {
                RETURN 0.
            } ELSE IF (height > 40) {
                RETURN -0.0001 * height + 0.005.
            } ELSE IF (height > 30) {
                RETURN -0.0005 * height + 0.021.
            } ELSE IF (height > 25) {
                RETURN -0.0018 * height + 0.06.
            } ELSE IF (height > 20) {
                RETURN -0.005 * height + 0.14.
            } ELSE IF (height > 15) {
                RETURN -0.0136 * height + 0.312.
            } ELSE IF (height > 10) {
                RETURN -0.036 * height + 0.648.
            } ELSE IF (height > 7.5) {
                RETURN -0.0632 * height + 0.92.
            } ELSE IF (height > 5) {
                RETURN -0.0784 * height + 1.034.
            } ELSE IF (height > 2.5) {
                RETURN -0.1024 * height + 1.154.
            } ELSE {
                RETURN -0.1308 * height + 1.225.
            }
        } ELSE {
            PRINT "Error. Body is not Kerbin.".
            RETURN "Error. Body is not Kerbin.".
        }
    } ELSE IF (method = "accurate") {
        IF (refBody:name = "Kerbin") {
            IF (height > 68798) {
                RETURN 0.
            }

            // Convert the altitude to values for Earth
            // Formula: https://wiki.kerbalspaceprogram.com/wiki/Kerbin#Atmosphere
            DECLARE LOCAL h TO 7.96375 * height / (6371 + 0.00125 * height). //km

            // density [kg/m^3] = k * pressure [Pa]
            DECLARE LOCAL k TO 0.

            // k = 1 / (R * T). R is the specific gas constant
            DECLARE LOCAL R TO 287.053. //J / kg - K

            // Temperature T dependends on altitude:
            // Equations: http://www.braeunig.us/space/atmmodel.htm#table4
            IF (h > 84.852) {
                SET k TO 0.
            } ELSE IF (h > 71) {
                SET k TO 1 / (R * (356.65 - 2 * h)).
            } ELSE IF (h > 51) {
                SET k TO 1 / (R * (413.45 - 2.8 * h)).
            } ELSE IF (h > 47) {
                SET k TO 1 / (R * 270.65).
            } ELSE IF (h > 32) {
                SET k TO 1 / (R * (139.05 + 2.8 * h)).
            } ELSE IF (h > 20) {
                SET k TO 1 / (R * (196.65 + h)).
            } ELSE IF (h > 11) {
                SET k TO 1 / (R * 216).
            } ELSE {
                SET k TO 1 / (R * (288.15 - 6.5 * h)).
            }
            RETURN k * refBody:ARM:ALTITUDEPRESSURE(height) * constant:ATMTOKPA * 1000.
        } ELSE {
            PRINT "Error. Body is not Kerbin.".
            RETURN "Error. Body is not Kerbin.".
        }
    }
}

DECLARE LOCAL FUNCTION convertPositionVectorToGeoCoords {
    DECLARE PARAMETER positionVector.

    // Sphere coordinates relative to xyz-coordinates
    DECLARE LOCAL latitude TO 90 - VANG(V(0, 1, 0), positionVector).

    // Circle coordinates relative to xz-coordinates
    DECLARE LOCAL equatorVector TO V(positionVector:X, 0, positionVector:Z).
    DECLARE LOCAL phi TO VANG(V(1, 0, 0), equatorVector).

    IF (equatorVector:Z < 0) {
        SET phi to 360 - phi.
    }

    // Angle between x-axis and geoCoordinates
    DECLARE LOCAL alpha TO VANG(V(1, 0, 0), LATLNG(0, 0):POSITION - SHIP:BODY:POSITION).
    IF (LATLNG(0, 0):POSITION - SHIP:BODY:POSITION):Z >= 0 {
        SET alpha TO 360 - alpha.
    }
    RETURN LATLNG(latitude, phi + alpha).
}
