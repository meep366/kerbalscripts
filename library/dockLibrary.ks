DECLARE FUNCTION dockWithTarget {
    DECLARE PARAMETER updateGui, proportion TO 7, integralFactor TO 3.

    LIGHTS ON.
    SAS OFF.
    RCS OFF.

    DECLARE LOCAL initialStoppingTime TO STEERINGMANAGER:MAXSTOPPINGTIME.
    DECLARE LOCAL initialPitchKd TO STEERINGMANAGER:PITCHPID:KD.
    DECLARE LOCAL initialYawKd TO STEERINGMANAGER:YAWPID:KD.
    SET STEERINGMANAGER:MAXSTOPPINGTIME TO 1.
    SET STEERINGMANAGER:PITCHPID:KD TO 0.
    SET STEERINGMANAGER:YAWPID:KD TO 0.

    DECLARE LOCAL currentSteering TO TARGET:PORTFACING:VECTOR:NORMALIZED * -1.
    LOCK STEERING TO currentSteering.

    WAIT 4.

    RCS ON.

    DECLARE LOCAL relativeVelocityOrbits TO TARGET:SHIP:VELOCITY:ORBIT - SHIP:VELOCITY:ORBIT.
    DECLARE LOCAL upUnitVector TO (FACING * R(-90, 0, 0)):VECTOR:NORMALIZED.
    DECLARE LOCAL forwardUnitVector TO FACING:VECTOR:NORMALIZED.
    DECLARE LOCAL starboardUnitVector TO (FACING * R(0, 90, 0)):VECTOR:NORMALIZED.

    DECLARE LOCAL upError TO TARGET:POSITION * upUnitVector.
    DECLARE LOCAL forwardError TO TARGET:POSITION * forwardUnitVector.
    DECLARE LOCAL starboardError TO TARGET:POSITION * starboardUnitVector.

    DECLARE LOCAL upSpeed TO relativeVelocityOrbits * upUnitVector.
    DECLARE LOCAL forwardSpeed TO relativeVelocityOrbits * forwardUnitVector.
    DECLARE LOCAL starboardSpeed TO relativeVelocityOrbits * starboardUnitVector.

    DECLARE LOCAL integral TO proportion / integralFactor.
    DECLARE LOCAL upIntegral TO 0.
    DECLARE LOCAL starboardIntegral TO 0.
    DECLARE LOCAL forwardIntegral TO 0.
    DECLARE LOCAL maximumDesiredSpeed TO 1.5.
    DECLARE LOCAL maximumIntegral TO 5.

    DECLARE LOCAL standoff TO TARGET:POSITION:MAG.
    DECLARE LOCAL horizontalStandoff TO 25.
    DECLARE LOCAL minimumDeviationForApproach TO .25.

    IF (standoff < 15) {
        SET standoff TO 15.
    } ELSE IF (standoff > 25) {
        SET standoff TO 25.
    }

    DECLARE LOCAL done TO FALSE.
    UNTIL (done) {
        CLEARSCREEN.
        DECLARE LOCAL forwardDesired TO (standoff - forwardError) / 10.

        IF (ABS(upError) < minimumDeviationForApproach AND ABS(starboardError) < minimumDeviationForApproach) {
            SET forwardDesired TO forwardError / 20 * -1.
            SET standoff TO forwardError.
        }
        IF (forwardDesired > maximumDesiredSpeed) {
            SET forwardDesired TO maximumDesiredSpeed.
        }
        IF (forwardDesired < -1 * maximumDesiredSpeed) {
            SET forwardDesired TO -1 * maximumDesiredSpeed.
        }

        DECLARE LOCAL upDesired TO upError / 12 * -1.
        DECLARE LOCAL starboardDesired TO starboardError / 12 * -1.

        IF (forwardError < 0) {
            IF (upError < 0 AND upError > -horizontalStandoff) {
                SET upDesired TO (upError + horizontalStandoff)  / 12 * -1.
            } ELSE IF (upError > 0 AND upError < horizontalStandoff) {
                SET upDesired TO (upError - horizontalStandoff)  / 12 * -1.
            }
            IF (starboardError < 0 AND starboardError > -horizontalStandoff) {
                SET starboardDesired TO (starboardError + horizontalStandoff)  / 12 * -1.
            } ELSE IF (starboardError > 0 AND starboardError < horizontalStandoff) {
                SET starboardDesired TO (starboardError - horizontalStandoff)  / 12 * -1.
            }
        }

        IF (upDesired > maximumDesiredSpeed) {
            SET upDesired TO maximumDesiredSpeed.
        }
        IF (upDesired < -1 * maximumDesiredSpeed) {
            SET upDesired TO -1 * maximumDesiredSpeed.
        }
        IF (starboardDesired > maximumDesiredSpeed) {
            SET starboardDesired TO maximumDesiredSpeed.
        }
        IF (starboardDesired < -1 * maximumDesiredSpeed) {
            SET starboardDesired TO -1 * maximumDesiredSpeed.
        }

        DECLARE LOCAL forwardPotential TO forwardSpeed - forwardDesired.
        DECLARE LOCAL upPotential TO upSpeed - upDesired.
        DECLARE LOCAL starboardPotential TO starboardSpeed - starboardDesired.
        SET forwardIntegral TO forwardIntegral + forwardPotential * .1.
        SET starboardIntegral TO starboardIntegral + starboardPotential * .1.
        SET upIntegral TO upIntegral + upPotential * .1.

        IF (forwardIntegral > maximumIntegral) {
            SET forwardIntegral TO maximumIntegral.
        }
        IF (forwardIntegral < -1 * maximumIntegral) {
            SET forwardIntegral TO -1 * maximumIntegral.
        }
        IF (starboardIntegral > maximumIntegral) {
            SET starboardIntegral TO maximumIntegral.
        }
        IF (starboardIntegral < -1 * maximumIntegral) {
            SET starboardIntegral TO -1 * maximumIntegral.
        }
        IF (upIntegral > maximumIntegral) {
            SET upIntegral TO maximumIntegral.
        }
        IF (upIntegral < -1 * maximumIntegral) {
            SET upIntegral TO -1 * maximumIntegral.
        }

        DECLARE LOCAL forwardControl TO forwardPotential * proportion + forwardIntegral * integral.
        SET SHIP:CONTROL:FORE TO forwardControl.

        DECLARE LOCAL upControl TO upPotential * proportion + upIntegral * integral.
        SET SHIP:CONTROL:TOP TO upControl.

        DECLARE LOCAL starboardControl TO starboardPotential * proportion + starboardIntegral * integral.
        SET SHIP:CONTROL:STARBOARD TO starboardControl.

        CLEARSCREEN.
        DECLARE LOCAL dockStatusLexicon TO LEXICON(
                "Up", ROUND(upError, 2) + "m, " + ROUND(upSpeed, 2) + "m/s",
                "Forward", ROUND(forwardError, 2) + "m, " + ROUND(forwardSpeed, 2) + "m/s",
                "Starboard", ROUND(starboardError, 2) + "m, " + ROUND(starboardSpeed, 2) + "m/s").
        PRINT "Up: " + ROUND(upError, 2) + "m, " + ROUND(upSpeed, 2) + "m/s".
        PRINT "Forward: " + ROUND(forwardError, 2) + "m, " + ROUND(forwardSpeed, 2) + "m/s".
        PRINT "Starboard: " + ROUND(starboardError, 2) + "m, " + ROUND(starboardSpeed, 2) + "m/s".

        IF (ABS(upError) < minimumDeviationForApproach AND ABS(starboardError) < minimumDeviationForApproach) {
            PRINT "Approaching".
            dockStatusLexicon:ADD("Approaching", "").
        }
        IF (ABS(upError) > minimumDeviationForApproach OR ABS(starboardError) > minimumDeviationForApproach) {
            PRINT "Holding At: " + ROUND(standoff) + "m+".
            dockStatusLexicon:ADD("Holding At: " + ROUND(standoff) + "m+", "").
        }

        updateGui:CALL(dockStatusLexicon).

        IF (HASTARGET) {
            SET currentSteering TO TARGET:PORTFACING:VECTOR:NORMALIZED * -1.
            SET relativeVelocityOrbits TO TARGET:SHIP:VELOCITY:ORBIT - SHIP:VELOCITY:ORBIT.
            SET upUnitVector TO (FACING * R(-90, 0, 0)):VECTOR:NORMALIZED.
            SET forwardUnitVector TO FACING:VECTOR:NORMALIZED.
            SET starboardUnitVector TO (FACING * R(0, 90, 0)):VECTOR:NORMALIZED.

            SET upError TO TARGET:POSITION * upUnitVector.
            SET forwardError TO TARGET:POSITION * forwardUnitVector.
            SET starboardError TO TARGET:POSITION * starboardUnitVector.

            SET upSpeed TO relativeVelocityOrbits * upUnitVector.
            SET forwardSpeed TO relativeVelocityOrbits * forwardUnitVector.
            SET starboardSpeed TO relativeVelocityOrbits * starboardUnitVector.
        } ELSE {
            SET done TO TRUE.
        }

        WAIT .1.
    }

    SET STEERINGMANAGER:MAXSTOPPINGTIME TO initialStoppingTime.
    SET STEERINGMANAGER:PITCHPID:KD TO initialPitchKd.
    SET STEERINGMANAGER:YAWPID:KD TO initialYawKd.

    RCS OFF.
    SAS OFF.
    LOCK THROTTLE TO 0.
    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
}
