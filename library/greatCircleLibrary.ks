@LAZYGLOBAL OFF.

// Use to find the initial bearing for the shortest path around a sphere to...
DECLARE FUNCTION circleBearing {
    DECLARE PARAMETER
            destination,                //...this point...
            origin TO SHIP:GEOPOSITION. //...from this point.

    RETURN MOD(360 + ARCTAN2(SIN(destination:LNG - origin:LNG) * COS(destination:LAT),
            COS(origin:LAT) * SIN(destination:LAT) - SIN(origin:LAT) * COS(destination:LAT) * COS(destination:LNG - origin:LNG)), 360).
}

// Use to find where you will end up if you travel...
DECLARE FUNCTION circleDestination {
    DECLARE PARAMETER
            bearing,                    // ...with this as your initial bearing...
            distance,                   // ...for this distance...
            origin TO SHIP:GEOPOSITION, //...from this point...
            radius TO BODY:RADIUS.      // ...around a sphere of this radius.

    DECLARE LOCAL lat TO ARCSIN(SIN(origin:LAT) * COS((distance * 180) / (radius * CONSTANT():PI)) +
            COS(origin:LAT) * SIN((distance * 180) / (radius * CONSTANT():PI)) * COS(bearing)).
    DECLARE LOCAL lng TO 0.
    IF (ABS(lat) <> 90) {
        SET lng TO origin:LNG + ARCTAN2(SIN(bearing) * SIN((distance * 180) / (radius * CONSTANT():PI)) * COS(origin:LAT),
                COS((distance * 180) / (radius * CONSTANT():PI)) - SIN(origin:LAT) * SIN(lat)).
    }

    RETURN LATLNG(lat, lng).
}

// Use to find the distance...
DECLARE FUNCTION circleDistance {
    DECLARE PARAMETER
            destination,                        //...to this point...
            origin TO SHIP:GEOPOSITION,         //...from this point...
            radius TO BODY:RADIUS + ALTITUDE.   //...around a body of this radius (note: if you are flying you may want to use ship:body:radius + altitude).

    DECLARE LOCAL a TO SIN((origin:LAT - destination:LAT) / 2) ^ 2 +
            COS(origin:LAT) * COS(destination:LAT) * SIN((origin:LNG - destination:LNG) / 2) ^ 2.

    RETURN radius * CONSTANT():PI * ARCTAN2(SQRT(a), SQRT(1 - a)) / 90.
}

// Use to find the mid point on the outside of a sphere between...
DECLARE FUNCTION circleMidpoint {
    DECLARE PARAMETER
            destination,                //...this point...
            origin TO SHIP:GEOPOSITION. //...and this point.

    DECLARE LOCAL resultA TO COS(destination:LAT) * COS(destination:LNG - origin:LNG).
    DECLARE LOCAL resultB TO COS(destination:lat) * SIN(destination:LNG - origin:LNG).

    RETURN LATLNG(ARCTAN2(SIN(origin:LAT) + SIN(destination:LAT), SQRT((COS(origin:LAT) + resultA) ^ 2 + resultB ^ 2)),
            origin:LNG + ARCTAN2(resultB, COS(origin:LAT) + resultA)).
}

DECLARE FUNCTION deltaHeading {
    DECLARE PARAMETER targetHeading.

    DECLARE LOCAL delta TO targetHeading - headingMagnitude().
    IF (delta > 180) {
        SET delta TO delta - 360.
    } ELSE IF (delta < -180) {
        SET delta TO delta + 360.
    }
    RETURN delta.
}

// Returns distance to a point in ground from the ship's ground position (ignores altitude)
DECLARE FUNCTION groundDistance {
    DECLARE PARAMETER targetPosition.

    RETURN VXCL(UP:VECTOR, targetPosition:POSITION):MAG.
}

//Returns the altitude of the glideslope
DECLARE FUNCTION glideslopeAltitude {
    DECLARE PARAMETER threshold.
    DECLARE PARAMETER glideSlopeAngle TO 5.
    DECLARE PARAMETER glideSlopeHeight TO 25.

    DECLARE LOCAL bodyAngle TO ABS(SHIP:GEOPOSITION:LNG) - ABS(threshold:LNG).
    // Why this correction? https://imgur.com/a/CPHnD
    DECLARE LOCAL correction TO SQRT((BODY:RADIUS ^ 2) + (TAN(bodyAngle) * BODY:RADIUS) ^ 2 ) - BODY:RADIUS.
    RETURN (TAN(glideSlopeAngle) * groundDistance(threshold)) + threshold:TERRAINHEIGHT + correction + glideSlopeHeight.
}

//Returns the ground distance of the centerline
DECLARE FUNCTION centerLineDistance {
    DECLARE PARAMETER threshold, runwayUnitVector.

    DECLARE LOCAL coordinateVector TO V(threshold:LAT - SHIP:GEOPOSITION:LAT, threshold:LNG - SHIP:GEOPOSITION:LNG, 0).
    DECLARE LOCAL offsetLatitude TO threshold:LAT + VDOT(runwayUnitVector, coordinateVector) * -runwayUnitVector:X.
    DECLARE LOCAL offsetLongitude TO threshold:LNG + VDOT(runwayUnitVector, coordinateVector) * -runwayUnitVector:Y.

    DECLARE LOCAL marker TO LATLNG(offsetLatitude, offsetLongitude).
    RETURN groundDistance(marker).
}