@LAZYGLOBAL OFF.

// Runs all science experiments on the ship or from a list of parts.
DECLARE FUNCTION runAllScience {
    DECLARE PARAMETER sensorList TO LIST().  // Optional parameter - list of science parts

    // If no list of science parts sent to the function, get a list of all science parts on the ship.
    IF (sensorList:length = 0) {
        SET sensorList TO getSensorList().
    }

    DECLARE LOCAL counter to 0.

    PRINT "Running science experiments...".
    FOR sensor IN sensorList {
        // Only proceed is the experiment doesn't already have data
        IF (NOT sensor:GETMODULE("ModuleScienceExperiment"):HASDATA) {
            PRINT "Running experiment on " + sensor:NAME.
            sensor:GETMODULE("ModuleScienceExperiment"):DEPLOY.
            SET counter TO counter + 1.
        }
    }
    PRINT "Completed " + counter + " available science experiments".

    RETURN sensorList.
}

DECLARE FUNCTION gatherAllScience {
    DECLARE LOCAL partList TO SHIP:PARTSNAMED("ScienceBox").

    FOR part IN partList {
        part:GETMODULE("ModuleScienceContainer"):DOACTION("Collect all", TRUE).
    }
}

DECLARE FUNCTION resetAllScience {
    DECLARE PARAMETER sensorList TO LIST().  //O ptional parameter - list of science parts

    // If no list of science parts sent to the function, get a list of all science parts on the ship.
    IF (sensorList:length = 0) {
        SET sensorList TO getSensorList().
    }

    DECLARE LOCAL counter TO 0.
    For sensor IN sensorList {
        // Only proceed is the experiment has data
        IF (sensor:GETMODULE("ModuleScienceExperiment"):HASDATA) {
            IF (sensor:NAME = ("GooExperiment")) {
                // If it the part is a goo canister, we need to reset it.
                // Deleting the data will not allow the experiement to be run again
                IF (sensor:GETMODULE("ModuleScienceExperiment"):HASEVENT("Reset goo canister")) {
                    sensor:GETMODULE("ModuleScienceExperiment"):DOEVENT("Reset goo canister").
                    SET counter TO counter + 1.
                }
            } ELSE IF (sensor:NAME = ("science.module")) {
                // If it the part is a science bay, we need to reset it.
                // Deleting the data will not allow the experiement to be run again
                IF (sensor:GETMODULE("ModuleScienceExperiment"):HASEVENT("Reset materials bay")) {
                    sensor:GETMODULE("ModuleScienceExperiment"):DOEVENT("Reset materials bay").
                    SET counter TO counter + 1.
                }
            } ELSE IF (sensor:GETMODULE("ModuleScienceExperiment"):hasaction("Delete data")) {
                sensor:GETMODULE("ModuleScienceExperiment"):doaction("Delete data", true).
                SET counter TO counter + 1.
            } ELSE IF (sensor:GETMODULE("ModuleScienceExperiment"):HASACTION("Discard crew report")) {
                sensor:GETMODULE("ModuleScienceExperiment"):DOACTION("Discard crew report", true).
                SET counter TO counter + 1.
            } ELSE IF (sensor:GETMODULE("ModuleScienceExperiment"):HASACTION("Discard data")) {
                // Discard data is needed for the Atmospheric Fluid Spectro-Variometer
                sensor:GETMODULE("ModuleScienceExperiment"):DOACTION("Discard data", true).
                SET counter TO counter + 1.
            }
        }
    }
    RETURN counter.
}

// Returns a list of all science parts
DECLARE LOCAL FUNCTION getSensorList {
    DECLARE LOCAL sensorList TO LIST().
    DECLARE LOCAL partlist TO SHIP:PARTS.

    FOR part IN partlist {
        IF (part:HASMODULE("ModuleScienceExperiment")) {
            sensorList:ADD(part).
        }
    }
    RETURN sensorList.
}
