@LAZYGLOBAL OFF.

DECLARE FUNCTION printAscentDetails {
    DECLARE PARAMETER status, targetInclination, targetPitch, targetHeading, updateGui.

    PRINT "Status:          " + status + "              " AT (0, 0).
    PRINT "Apoapsis:        " + ROUND(SHIP:APOAPSIS, 0) + "m" AT (0, 1).
    PRINT "Pitch:           " + ROUND(pitchAngle(), 1) + "°  " AT (0, 2).
    PRINT "Target Pitch:    " + ROUND(targetPitch, 1) + "°  " AT (0, 3).
    PRINT "Heading:         " + ROUND(headingMagnitude(), 1) + "°       " AT (0, 4).
    PRINT "Target Heading:  " + ROUND(targetHeading, 1) + "°       " AT (0, 5).
    PRINT "Inclination:     " + ROUND(ORBIT:INCLINATION, 1) + "°  " AT (0, 6).
    PRINT "Tgt Inclination: " + ROUND(targetInclination, 1) + "°  " AT (0, 7).
    PRINT "LAN:             " + ROUND(ORBIT:LAN, 1) + "°  " AT (0, 8).

    DECLARE LOCAL statusLexicon TO LEXICON(
            "Status", status,
                    "Pitch", ROUND(pitchAngle(), 1) + "°  ",
                    "Target Pitch", ROUND(targetPitch, 1) + "°  ",
                    "Heading", ROUND(headingMagnitude(), 1) + "°",
                    "Target Heading", ROUND(targetHeading, 1) + "°",
                    "Tgt Inclination", ROUND(targetInclination, 1) + "°").
    updateGui:CALL(statusLexicon).
}

DECLARE FUNCTION printTargetApproachDetails {
    DECLARE PARAMETER status, angleToRetrograde, thrustAngle, distance, impactTime, burnTime, closestApproach, relativeSpeed, updateGui.

    CLEARSCREEN.
    PRINT status.
    PRINT "Altitude:           " + ROUND(ALTITUDE) + "m".
    PRINT "Angle:              " + ROUND(angleToRetrograde, 3) + "°".
    PRINT "Thrust Angle:       " + ROUND(thrustAngle, 3) + "°".
    PRINT "Distance to target: " + ROUND(distance) + "m".
    PRINT "Rendezvous Time:    " + ROUND(impactTime) + "s".
    PRINT "Burn Time:          " + ROUND(burnTime) + "s".
    PRINT "Closest Approach:   " + ROUND(closestApproach) + "m".
    PRINT "Relative Speed:     " + ROUND(relativeSpeed) + "m/s".

    DECLARE LOCAL statusLexicon TO LEXICON(
            status, "",
                    "Angle", ROUND(angleToRetrograde, 3) + "°",
                    "Thrust Angle", ROUND(thrustAngle, 3) + "°",
                    "Rendezvous Time", ROUND(impactTime) + "s",
                    "Burn Time", ROUND(burnTime) + "s",
                    "Closest Approach", ROUND(closestApproach) + "m",
                    "Relative Speed", ROUND(relativeSpeed) + "m/s").
    updateGui:CALL(statusLexicon).
}

DECLARE FUNCTION printBurnDetails {
    DECLARE PARAMETER status, timeToNode, burnTime, angleToBurn, updateGui.

    IF (timeToNode < 0) {
        SET timeToNode TO 0.
    }

    DECLARE LOCAL statusLexicon TO LEXICON(
            "Status", status,
                    "Time to node", formatTime(timeToNode),
                    "Burn time", ROUND(burnTime) + "s",
                    "Angle to burn", ROUND(angleToBurn, 1) + "°").
    updateGui:CALL(statusLexicon).
}

DECLARE FUNCTION printPlaneStats {
    DECLARE PARAMETER mode.
    DECLARE PARAMETER autoPilotMode TO "---".
    DECLARE PARAMETER elevator TO STEERINGMANAGER:PITCHPID:OUTPUT.
    DECLARE PARAMETER aileron TO STEERINGMANAGER:ROLLPID:OUTPUT.
    DECLARE PARAMETER rudder TO STEERINGMANAGER:YAWPID:OUTPUT.
    DECLARE PARAMETER wheel TO 0.
    DECLARE PARAMETER runwayAlt TO 0.
    DECLARE PARAMETER gsAltitude TO 0.
    DECLARE PARAMETER runwayDistance TO 0.
    DECLARE PARAMETER centerLineDist TO 0.
    DECLARE PARAMETER angleToRunway TO 0.
    DECLARE PARAMETER targetVerticalSpeed TO 0.

    PRINT "MODE: " + mode + "            " AT (0, 0).

    PRINT "Pitch angle           " + ROUND(pitchAngle(), 2) + "°       " AT (0, 2).
    PRINT "Bank angle            " + ROUND(bankAngle(), 2) + "°     " AT (0, 3).
    PRINT "Sideslip angle:       " + ROUND(sideslipAngle(), 2) + "°    " AT (0, 4).

    PRINT "Heading               " + ROUND(headingMagnitude(), 2) + "°       " AT (0, 6).
    PRINT "Angle of attack:      " + ROUND(angleOfAttack(), 2) + "°       " AT (0, 7).

    PRINT "Throttle:             " + ROUND(THROTTLE, 3) + "            " AT (0, 10).
    PRINT "Elevator:             " + ROUND(elevator, 3) + "            " AT (0, 11).
    PRINT "Aileron:              " + ROUND(aileron, 3) + "            " AT (0, 12).
    PRINT "Rudder:               " + ROUND(rudder, 3) + "            " AT (0, 13).

    IF (autoPilotMode = "TKF" AND ALT:RADAR < 5) {
        PRINT "Wheel:                " + ROUND(wheel, 3) + "            " AT (0, 14).
    } ELSE IF (autoPilotMode = "ILS" OR autoPilotMode = "FLR") {
        PRINT "Angle to runway:  " + ROUND(angleToRunway, 2) + "°    " AT (0, 15).
        PRINT "Radar Altitude:   " + ROUND(ALT:RADAR, 2) + "m     " AT (0, 16).
        PRINT "Runway Alt Δ:     " + ROUND(ALT:RADAR - runwayAlt, 1) + "m     " AT (0, 17).
        PRINT "GS Altitude Δ:    " + ROUND(ALTITUDE - gsAltitude, 2) + "m     " AT (0, 18).
        PRINT "Runway distance:  " + ROUND(runwayDistance, 2) + "m     " AT (0, 19).
        PRINT "Center line dist: " + ROUND(centerLineDist, 2) + "m     " AT (0, 20).
        IF (autoPilotMode = "FLR") {
            PRINT "Target vert speed: " + ROUND(targetVerticalSpeed, 2) + "m/s     " AT (0, 21).
        }
    } ELSE {
        PRINT "                                                 " AT (0, 14).
        PRINT "                                                 " AT (0, 15).
        PRINT "                                                 " AT (0, 16).
        PRINT "                                                 " AT (0, 17).
        PRINT "                                                 " AT (0, 18).
        PRINT "                                                 " AT (0, 19).
        PRINT "                                                 " AT (0, 20).
        PRINT "                                                 " AT (0, 21).
    }
}