@LAZYGLOBAL OFF.

DECLARE FUNCTION transferToMoonVessel {
    DECLARE PARAMETER targetVessel, timeToImpactBuffer, minmumAngleToCorrect, updateGui.

    DECLARE LOCAL targetPeriapsis TO targetVessel:ORBIT:APOAPSIS.
    DECLARE LOCAL options TO LEXICON(
            "create_maneuver_nodes", "first",
            "verbose", true,
            "search_interval", 151200,
            "final_orbit_periapsis", targetPeriapsis).
    DECLARE LOCAL result TO getSuccessfulRSVP(targetVessel:BODY, options, updateGui).
    DECLARE LOCAL ejectionNode TO NEXTNODE.
    executeNode(ejectionNode, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveAdjustmentBurn").
    adjustDestinationPeriapsis(targetVessel, 8, TRUE, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksavePeriapsisBurn").
    DECLARE LOCAL periapsisOrbit TO ORBIT:NEXTPATCH.
    IF (BODY = targetVessel:BODY) {
        SET periapsisOrbit TO ORBIT.
    }
    performPeriapsisCaptureBurn(targetVessel, targetVessel:BODY:SOIRADIUS * .5, periapsisOrbit, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveAdjustmentBurn").
    //Run RSVP until we have nodes to intercept with target
    DECLARE LOCAL closestApproachList TO list().
    DECLARE LOCAL finalApproach TO FALSE.
    UNTIL (finalApproach) {
        DECLARE LOCAL options TO LEXICON(
                "create_maneuver_nodes", "first",
                "verbose", true,
                "search_interval", 21600,
                "search_duration", ORBIT:PERIOD).
        DECLARE LOCAL result TO getSuccessfulRSVP(targetVessel, options, updateGui).
        DECLARE LOCAL interceptNode TO NEXTNODE.
        executeNode(interceptNode, updateGui).
        SET closestApproachList TO getClosestApproach(targetVessel).
        IF (closestApproachList[0] < 7500) {
            SET finalApproach TO TRUE.
        }
    }

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveZeroTargetBurn").
    zeroTargetApproach(targetVessel, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveTargetApproach").
    approachTargetVessel(targetVessel, minmumAngleToCorrect, updateGui).
    steerToTargetVessel(targetVessel, timeToImpactBuffer, minmumAngleToCorrect, updateGui).
    suicideBurnToTarget(targetVessel, timeToImpactBuffer, updateGui).
    killRemainingTargetVelocity(targetVessel, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveDocking").
    setDockingTarget(TARGET).
    dockWithTarget(updateGui).
}

DECLARE FUNCTION transferToPlanetVessel {
    DECLARE PARAMETER targetVessel, planetPeriapsis, timeToImpactBuffer, minmumAngleToCorrect, closestApproachCuttoff, doCorrections, updateGui.

    DECLARE LOCAL targetPeriapsis TO targetVessel:ORBIT:APOAPSIS.
    DECLARE LOCAL options TO LEXICON(
            "create_maneuver_nodes", "first",
            "verbose", true,
            "search_interval", 151200).

    DECLARE LOCAL result TO getSuccessfulRSVP(targetVessel, options, updateGui).
    DECLARE LOCAL ejectionNode TO NEXTNODE.
    executeNode(ejectionNode, updateGui).

    DECLARE LOCAL statusLexicon TO LEXICON("Status", "Warping to next orbit").
    updateGui:CALL(statusLexicon).

    DECLARE LOCAL warpTime TO TIME:SECONDS + ORBIT:NEXTPATCHETA.
    KUNIVERSE:TIMEWARP:WARPTO(warpTime).
    WAIT UNTIL (TIME:SECONDS > warpTime).
    WAIT UNTIL (KUNIVERSE:TIMEWARP:ISSETTLED).

    IF (doCorrections) {
        WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
        KUNIVERSE:QUICKSAVETO("quicksaveAdjustmentBurn").
        adjustDestinationPeriapsis(targetVessel, .5, FALSE, updateGui).

        WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
        KUNIVERSE:QUICKSAVETO("quicksavePeriapsisBurn").
        performPeriapsisCaptureBurn(targetVessel, planetPeriapsis, ORBIT, updateGui).

        KUNIVERSE:QUICKSAVETO("quicksaveZeroTargetBurn").
        DECLARE LOCAL closestApproachList TO list().
        DECLARE LOCAL finalApproach TO FALSE.
        UNTIL (finalApproach) {
            DECLARE LOCAL options TO LEXICON(
                    "create_maneuver_nodes", "first",
                            "verbose", true).
            DECLARE LOCAL result TO getSuccessfulRSVP(targetVessel, options, updateGui).
            DECLARE LOCAL interceptNode TO NEXTNODE.
            executeNode(interceptNode, updateGui).
            SET closestApproachList TO getClosestApproach(targetVessel).
            IF (closestApproachList[0] < 7500) {
                SET finalApproach TO TRUE.
            }
        }
    }

    zeroTargetApproach(targetVessel, updateGui, closestApproachCuttoff).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveTargetApproach").
    approachTargetVessel(targetVessel, minmumAngleToCorrect, updateGui).
    steerToTargetVessel(targetVessel, timeToImpactBuffer, minmumAngleToCorrect, updateGui).
    suicideBurnToTarget(targetVessel, timeToImpactBuffer, updateGui).
    killRemainingTargetVelocity(targetVessel, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveDocking").
    setDockingTarget(TARGET).
    dockWithTarget(updateGui).
}

DECLARE FUNCTION transferToVessel {
    DECLARE PARAMETER targetVessel, timeToImpactBuffer, minmumAngleToCorrect, updateGui.

    KUNIVERSE:QUICKSAVETO("quicksaveZeroTargetBurn").
    DECLARE LOCAL closestApproachList TO list().
    DECLARE LOCAL finalApproach TO FALSE.
    UNTIL (finalApproach) {
        DECLARE LOCAL options TO LEXICON(
                        "create_maneuver_nodes", "first",
                        "verbose", true).
        DECLARE LOCAL result TO getSuccessfulRSVP(targetVessel, options, updateGui).
        DECLARE LOCAL interceptNode TO NEXTNODE.
        executeNode(interceptNode, updateGui).
        SET closestApproachList TO getClosestApproach(targetVessel).
        IF (closestApproachList[0] < 7500) {
            SET finalApproach TO TRUE.
        }
    }
    zeroTargetApproach(targetVessel, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveTargetApproach").
    approachTargetVessel(targetVessel, minmumAngleToCorrect, updateGui).
    steerToTargetVessel(targetVessel, timeToImpactBuffer, minmumAngleToCorrect, updateGui).
    suicideBurnToTarget(targetVessel, timeToImpactBuffer, updateGui).
    killRemainingTargetVelocity(targetVessel, updateGui).

    WAIT UNTIL(KUNIVERSE:CANQUICKSAVE).
    KUNIVERSE:QUICKSAVETO("quicksaveDocking").
    setDockingTarget(TARGET).
    dockWithTarget(updateGui).
}

DECLARE FUNCTION approachTargetVessel {
    DECLARE PARAMETER targetVessel, minimumAngleToCorrect, updateGui.

    DECLARE LOCAL currentHeading TO V(0, 0, 0).
    LOCK STEERING TO currentHeading.

    DECLARE LOCAL approachBurnTime TO 0.
    DECLARE LOCAL relativeVelocityOrbit TO 0.
    DECLARE LOCAL targetRetrogradeVector TO 0.
    DECLARE LOCAL deltaV TO 0.

    DECLARE LOCAL timeToImpact TO 0.

    DECLARE LOCAL closingToTarget TO FALSE.
    DECLARE LOCAL currentAngle TO 0.
    DECLARE LOCAL previousAngle TO 0.

    DECLARE LOCAL differenceCount TO 1000.
    DECLARE LOCAL differenceList TO list().
    DECLARE LOCAL totalDifference TO 0.
    DECLARE LOCAL iteration TO 0.

    DECLARE LOCAL statusLexicon TO LEXICON("Status", "Warping to target").
    updateGui:CALL(statusLexicon).

    DECLARE LOCAL warpTime TO getClosestApproach(targetVessel)[1] - 210.
    KUNIVERSE:TIMEWARP:WARPTO(warpTime).
    WAIT UNTIL (TIME:SECONDS > warpTime).
    WAIT UNTIL (KUNIVERSE:TIMEWARP:ISSETTLED).

    UNTIL (closingToTarget) {
        IF (iteration > differenceCount AND totalDifference > 0) {
            SET closingToTarget TO TRUE.
        }
        DECLARE LOCAL difference TO currentAngle - previousAngle.

        SET totalDifference TO totalDifference + difference.
        IF (iteration >= differenceCount) {
            SET differenceList[MOD(iteration, differenceCount)] TO difference.
            SET totalDifference TO totalDifference - differenceList[MOD(iteration + 1, differenceCount)].
        } ELSE {
            differenceList:ADD(difference).
        }

        SET relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
        SET targetRetrogradeVector TO -1 * targetVessel:DIRECTION:VECTOR.
        SET targetRetrogradeVector TO targetRetrogradeVector:NORMALIZED.
        SET deltaV to relativeVelocityOrbit:MAG.
        SET timeToImpact TO targetVessel:DISTANCE / relativeVelocityOrbit:MAG.
        IF (MAXTHRUST > 0) {
            SET approachBurnTime TO (MASS * deltaV) / (MAXTHRUST * 1.9).
        }

        DECLARE LOCAL multiplier TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) * 6 / minimumAngleToCorrect.
        IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) > 5) {
            SET multiplier TO (90 / VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        }
        SET currentHeading to multiplier * (relativeVelocityOrbit - targetRetrogradeVector) + relativeVelocityOrbit.

        SET iteration TO iteration + 1.
        SET previousAngle TO currentAngle.
        SET currentAngle TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector).


        DECLARE LOCAL status TO "Waiting to ajdust angle".
        DECLARE LOCAL thrustAngle TO VECTORANGLE(relativeVelocityOrbit, currentHeading).
        DECLARE LOCAL closestApproach TO targetVessel:DISTANCE * SIN(VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        printTargetApproachDetails(status, currentAngle, thrustAngle, targetVessel:DISTANCE,
                timeToImpact, approachBurnTime, closestApproach, deltaV, updateGui).
    }
}

DECLARE FUNCTION steerToTargetVessel {
    DECLARE PARAMETER targetVessel, timeToImpactBuffer, minimumAngleToCorrect, updateGui.

    DECLARE LOCAL currentThrottle TO 0.
    LOCK THROTTLE to currentThrottle.
    DECLARE LOCAL currentHeading TO V(0, 0, 0).
    LOCK STEERING TO currentHeading.

    DECLARE LOCAL relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
    DECLARE LOCAL deltaV TO relativeVelocityOrbit:MAG.
    DECLARE LOCAL approachBurnTime TO 0.
    IF (MAXTHRUST > 0) {
        SET approachBurnTime TO (MASS * deltaV) / (MAXTHRUST * 1.9).
    }
    DECLARE LOCAL targetRetrogradeVector TO 0.
    DECLARE LOCAL timeToImpact TO targetVessel:DISTANCE / relativeVelocityOrbit:MAG.

    UNTIL (timeToImpact < approachBurnTime + timeToImpactBuffer) {
        SET relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
        SET targetRetrogradeVector TO -1 * targetVessel:DIRECTION:VECTOR.
        SET deltaV to relativeVelocityOrbit:MAG.
        IF (MAXTHRUST > 0) {
            SET approachBurnTime TO (MASS * deltaV) / (MAXTHRUST * 1.9).
        }
        SET timeToImpact TO targetVessel:DISTANCE / relativeVelocityOrbit:MAG.
        SET relativeVelocityOrbit TO relativeVelocityOrbit:NORMALIZED.
        SET targetRetrogradeVector TO targetRetrogradeVector:NORMALIZED.
        DECLARE LOCAL closestApproach TO targetVessel:DISTANCE * SIN(VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).

        DECLARE LOCAL multiplier TO VECTORANGLE (relativeVelocityOrbit, targetRetrogradeVector) * 6 / minimumAngleToCorrect.
        IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) > 5) {
            SET multiplier TO (90 / VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        }
        SET currentHeading to multiplier * (relativeVelocityOrbit - targetRetrogradeVector) + relativeVelocityOrbit.

        DECLARE LOCAL minimumAngleToFullBurn TO 45.
        DECLARE LOCAL maximumBadAngleThrottle TO .0.
        IF (closestApproach < 50) {
            SET currentThrottle TO 0.
            SET multiplier TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) * 5.
            IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) > 9.2) {
                SET multiplier TO (90 / VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
            }
            SET currentHeading TO multiplier * (relativeVelocityOrbit - relativeVelocityOrbit) + relativeVelocityOrbit.
        } ELSE IF (VECTORANGLE(currentHeading, SHIP:FACING:VECTOR) > minimumAngleToFullBurn) {
            SET currentThrottle TO MIN((VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) - minimumAngleToCorrect) / 6, maximumBadAngleThrottle).
        } ELSE {
            SET currentThrottle TO (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) - minimumAngleToCorrect) / 6.
        }

        DECLARE LOCAL status TO "Maintaining angle at maximum speed".
        DECLARE LOCAL angle TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector).
        DECLARE LOCAL thrustAngle TO VECTORANGLE(relativeVelocityOrbit, currentHeading).
        DECLARE LOCAL closestApproach TO targetVessel:DISTANCE * SIN(VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        printTargetApproachDetails(status, angle, thrustAngle, targetVessel:DISTANCE,
                timeToImpact, approachBurnTime, closestApproach, deltaV, updateGui).

        WAIT .05.
    }
}

DECLARE FUNCTION suicideBurnToTarget {
    DECLARE PARAMETER targetVessel, timeToImpactBuffer, updateGui.

    DECLARE LOCAL currentThrottle TO 0.
    LOCK THROTTLE TO currentThrottle.

    DECLARE LOCAL relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
    DECLARE LOCAL targetRetrogradeVector TO -1 * targetVessel:DIRECTION:VECTOR.
    SET targetRetrogradeVector TO targetRetrogradeVector:NORMALIZED.
    DECLARE LOCAL multiplier TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) * 5.
    IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) > 9.2) {
        SET multiplier TO (90 / VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
    }
    DECLARE LOCAL currentHeading TO multiplier * (relativeVelocityOrbit - relativeVelocityOrbit) + relativeVelocityOrbit.
    LOCK STEERING TO currentHeading.

    DECLARE LOCAL minimumApproachDistance TO 70.
    DECLARE LOCAL minimumApproachVelocity TO 10.
    DECLARE LOCAL error TO 0.
    DECLARE LOCAL integral TO 0.
    DECLARE LOCAL absoluteMaxIntegral TO 50.
    DECLARE LOCAL errorProportion TO .3.
    DECLARE LOCAL integralTerm TO .01.
    DECLARE LOCAL timeToImpact TO (targetVessel:DISTANCE - minimumApproachDistance) / relativeVelocityOrbit:MAG.
    DECLARE LOCAL deltaV TO relativeVelocityOrbit:MAG.
    DECLARE LOCAL targetSpeed TO ((timeToImpact - timeToImpactBuffer) * (MAXTHRUST / MASS)) * 1.9.
    DECLARE LOCAL approachBurnTime TO (MASS * deltaV) / (MAXTHRUST * 1.9).

    UNTIL (TARGET:DISTANCE < minimumApproachDistance OR relativeVelocityOrbit:MAG < minimumApproachVelocity) {
        SET relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
        SET targetRetrogradeVector TO -1 * targetVessel:DIRECTION:VECTOR.
        SET timeToImpact TO (targetVessel:DISTANCE - minimumApproachDistance) / relativeVelocityOrbit:MAG.
        SET deltaV TO relativeVelocityOrbit:MAG.

        IF (MAXTHRUST > 0) {
            SET targetSpeed TO ((timeToImpact - timeToImpactBuffer) * (MAXTHRUST / MASS)) * 1.9.
            SET approachBurnTime TO (MASS * deltaV) / (MAXTHRUST * 1.9).
        }

        SET error TO relativeVelocityOrbit:MAG - targetSpeed.
        SET integral TO integral + error.
        IF (integral > absoluteMaxIntegral) {
            SET integral to absoluteMaxIntegral.
        }
        IF (integral < -1 * absoluteMaxIntegral) {
            SET integral to -1 * absoluteMaxIntegral.
        }

        SET relativeVelocityOrbit TO relativeVelocityOrbit:NORMALIZED.
        SET targetRetrogradeVector TO targetRetrogradeVector:NORMALIZED.
        IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) / 3 - 1 > error * errorProportion + integral * integralTerm) {
            SET currentThrottle TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) / 4.
        }
        IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) / 3 - 1 < error * errorProportion + integral * integralTerm) {
            SET currentThrottle TO error * errorProportion + integral * integralTerm.
        }
        DECLARE LOCAL multiplier TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) * 5.
        IF (VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector) > 9.2) {
            SET multiplier TO (90 / VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        }
        SET currentHeading TO multiplier * (relativeVelocityOrbit - relativeVelocityOrbit) + relativeVelocityOrbit.


        DECLARE LOCAL status TO "Closing speed".
        DECLARE LOCAL angle TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector).
        DECLARE LOCAL thrustAngle TO VECTORANGLE(relativeVelocityOrbit, currentHeading).
        DECLARE LOCAL closestApproach TO targetVessel:DISTANCE * SIN(VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        printTargetApproachDetails(status, angle, thrustAngle, targetVessel:DISTANCE,
                timeToImpact, approachBurnTime, closestApproach, deltaV, updateGui).

        WAIT .25.

        SET relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
    }
}

DECLARE FUNCTION killRemainingTargetVelocity {
    DECLARE PARAMETER targetVessel, updateGui.

    DECLARE LOCAL relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.

    CLEARSCREEN.
    PRINT "Distance: " + ROUND(targetVessel:DISTANCE, 2) + "m".
    PRINT "Closing speed: " + ROUND(relativeVelocityOrbit:MAG, 2) + "m/s".

    LOCK STEERING TO relativeVelocityOrbit.
    DECLARE LOCAL currentThrottle TO relativeVelocityOrbit:MAG / 20.
    LOCK THROTTLE TO currentThrottle.

    PRINT "Killing final velocity".

    DECLARE LOCAL maximumFinalVelocity TO .25.
    UNTIL (relativeVelocityOrbit:MAG < maximumFinalVelocity) {
        SET relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
        IF (VECTORANGLE(FACING:VECTOR, relativeVelocityOrbit) > 9.2) {
            SET currentThrottle TO 0.
        } ELSE {
            SET currentThrottle TO relativeVelocityOrbit:MAG / 30.
        }
        DECLARE LOCAL statusLexicon TO LEXICON(
                        "Status", "Killing final velocity",
                        "Distance", ROUND(targetVessel:DISTANCE, 2) + "m",
                        "Speed", ROUND(relativeVelocityOrbit:MAG, 2) + "m/s").
        updateGui:CALL(statusLexicon).
    }

    PRINT "Final Distance to target: " + ROUND(targetVessel:DISTANCE, 2) + "m".
    PRINT "Final Speed: " + ROUND(relativeVelocityOrbit:MAG, 2) + "m/s".
    DECLARE LOCAL statusLexicon TO LEXICON(
                    "Status", "Approach Complete",
                    "Distance", ROUND(targetVessel:DISTANCE, 2) + "m",
                    "Speed", ROUND(relativeVelocityOrbit:MAG, 2) + "m/s").
    updateGui:CALL(statusLexicon).

    RCS OFF.
    SAS OFF.
    LOCK THROTTLE TO 0.
    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
}

DECLARE FUNCTION setDockingTarget {
    DECLARE PARAMETER targetVessel.

    DECLARE LOCAL shipDockingPort TO getShipDockingPort().
    DECLARE LOCAL targetDockingPorts TO targetVessel:PARTSTITLED(shipDockingPort:TITLE).

    DECLARE LOCAL closestDockingPort TO 0.
    DECLARE LOCAL closestDockingPortDistance TO 3000.
    FOR dockingPort IN targetDockingPorts {
        IF (dockingPort:STATE = "Ready" AND dockingPort:TARGETABLE) {
            SET TARGET TO dockingPort.
            IF (TARGET:POSITION:MAG < closestDockingPortDistance) {
                SET closestDockingPort TO dockingPort.
                SET closestDockingPortDistance TO TARGET:POSITION:MAG.
            } ELSE {
                SET TARGET TO closestDockingPort.
            }
        }
    }
    IF (closestDockingPort = 0) {
        headsUpText("Target docking ports in use or out of range").
        RETURN FALSE.
    }

    RETURN TRUE.
}


DECLARE FUNCTION adjustDestinationPeriapsis {
    DECLARE PARAMETER targetVessel, targetInclinationError, adjustingNextPatch, updateGui.

    DECLARE LOCAL maximumPeriapsisError TO 5000.
    DECLARE LOCAL targetBody TO targetVessel:BODY.
    DECLARE LOCAL targetPeriapsis TO targetVessel:ORBIT:APOAPSIS.

    //Until we've got our periapsis/inclination within a margin of error, we keep adjusting our entry
    DECLARE LOCAL validInsert TO FALSE.
    UNTIL (validInsert) {
        IF (NOT adjustingNextPatch OR (ORBIT:HASNEXTPATCH AND targetBody = ORBIT:NEXTPATCH:BODY)) {
            DECLARE LOCAL currentOrbit TO ORBIT.
            IF (adjustingNextPatch) {
                SET currentOrbit TO ORBIT:NEXTPATCH.
            }
            DECLARE LOCAL periapsisError TO ABS(targetPeriapsis - currentOrbit:PERIAPSIS).
            DECLARE LOCAL inclinationError TO ABS(targetVessel:ORBIT:INCLINATION - currentOrbit:INCLINATION).
            IF (periapsisError <= maximumPeriapsisError AND inclinationError < targetInclinationError) {
                SET validInsert TO TRUE.
                BREAK.
            }
        }

        //If we're around the target body and are leaving the body, just complete periapsis burn
        IF (BODY = targetVessel:BODY AND ORBIT:HASNEXTPATCH AND ORBIT:ETA:PERIAPSIS < 3600) {
            SET validInsert TO TRUE.
            BREAK.
        }

        //Run function to create maneuver to improve periapsis/inclination
        refineTransferToBody(targetBody, targetPeriapsis, TRUE, updateGui).

        //If we don't generate a node, we can't improve on current path, so we finish
        IF (HASNODE) {
            DECLARE LOCAL adjustmentNode TO NEXTNODE.
            executeNode(adjustmentNode, updateGui).
        } ELSE {
            SET validInsert TO TRUE.
        }
    }
}

DECLARE FUNCTION performPeriapsisCaptureBurn {
    DECLARE PARAMETER targetVessel, targetApoapsis, currentOrbit, updateGui.

    DECLARE LOCAL targetBody TO targetVessel:BODY.

    //Calculate parameters for capturing orbit
    DECLARE LOCAL deltaV TO calculateSemiMajorAxisDeltaV(currentOrbit:PERIAPSIS, targetApoapsis, currentOrbit:PERIAPSIS, currentOrbit).
    DECLARE LOCAL timeToPeriapsis TO currentOrbit:ETA:PERIAPSIS.
    DECLARE LOCAL periapsisNode TO NODE(TIME:SECONDS + timeToPeriapsis, 0, 0, deltaV).
    ADD periapsisNode.

    executeNode(periapsisNode, updateGui).
}

//Calculates the relative speed between this vessel and a target at a given time
DECLARE FUNCTION getRelativeSpeedAtTime {
    DECLARE PARAMETER targetVessel, timeAtSeparation.

    DECLARE LOCAL shipVelocity TO VELOCITYAT(SHIP, timeAtSeparation):ORBIT.
    DECLARE LOCAL targetVelocity TO VELOCITYAT(targetVessel, timeAtSeparation):ORBIT.
    DECLARE LOCAL relativeVelocity TO targetVelocity - shipVelocity.

    RETURN relativeVelocity:MAG.
}

