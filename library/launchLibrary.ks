@LAZYGLOBAL OFF.

DECLARE FUNCTION launchToSpecificOrbit {
    DECLARE PARAMETER updateGui.
    DECLARE PARAMETER apoapsis TO 100000.
    DECLARE PARAMETER periapsis TO 100000.
    DECLARE PARAMETER inclination TO 0.
    DECLARE PARAMETER longitude TO 0.
    DECLARE PARAMETER argumentPeriapsis TO 0.
    DECLARE PARAMETER pitchOverRate TO -2/25.
    DECLARE PARAMETER safeHoldingAltitude TO 75000.

    warpToWindowForLAN(longitude, updateGui).
    launchToOrbit(updateGui, safeHoldingAltitude, inclination).
    burnToFinalPeriapsisArgument(periapsis, apoapsis, argumentPeriapsis, updateGui).
}

DECLARE FUNCTION launchToOrbit {
    DECLARE PARAMETER updateGui, orbitAltitude TO 100000, inclination TO 0, pitchOverRate TO -2/25.

    CLEARSCREEN.
    PRINT "Status:          Pre-Launch" AT (0, 0).

    SAS OFF.
    RCS OFF.

    // Set throttle to 100%
    LOCK THROTTLE TO 1.

    // Stage when previous stage is out of fuel
    WHEN (MAXTHRUST = 0) THEN {
        headsUpText("Staging").
        STAGE.
        PRESERVE.
    }
    retractPanelsAndAntennas().

    // Gravity turn
    executeGravityTurn(orbitAltitude, inclination, pitchOverRate, updateGui).

    // Point prograde until we're out of atmosphere
    LOCK STEERING TO SRFPROGRADE.
    DECLARE LOCAL status TO "Coasting to apoapsis".
    UNTIL (BODY:ATM:ALTITUDEPRESSURE(ALTITUDE) = 0) {
        printAscentDetails(status, inclination, pitchOfVector(SRFPROGRADE:VECTOR), headingOfVector(SRFPROGRADE:VECTOR), updateGui).
    }

    // Create node at apoapsis to circularize orbit
    DECLARE LOCAL deltaV TO calculateSemiMajorAxisDeltaV(orbitAltitude, orbitAltitude).
    DECLARE LOCAL timeAtApoapsis TO TIME:SECONDS + ETA:APOAPSIS.
    DECLARE LOCAL apoapsisNode TO NODE(timeAtApoapsis, 0, 0, deltaV).
    ADD apoapsisNode.

    // Circularize orbit
    PRINT "Status:          Executing circularization burn"  AT (0, 0).
    executeNode(apoapsisNode, updateGui).

    CLEARSCREEN.

    // Set the throttle to 0 and verify the reset doesn't increase the throttle
    LOCK THROTTLE TO 0.
    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
}

DECLARE FUNCTION launchToTarget {
    DECLARE PARAMETER targetVessel, timeToOrbit, pitchOverRate, timeToImpactBuffer, minimumAngleToCorrect, updateGui.
    DECLARE PARAMETER altitudeOffset TO .02.

    waitForLaunchWindow(targetVessel, timeToOrbit, updateGui).
    retractPanelsAndAntennas().

    DECLARE LOCAL targetAltitudeOffset TO targetVessel:ALTITUDE * altitudeOffset.
    DECLARE LOCAL targetAltitude TO targetVessel:ALTITUDE + targetAltitudeOffset.
    DECLARE LOCAL targetInclination TO ORBITAT(targetVessel, TIME + timeToOrbit):INCLINATION.

    DECLARE LOCAL launchDetails TO "Launching to: " + ROUND(targetAltitude) + "m at " + ROUND(targetInclination, 2) + "°".
    DECLARE LOCAL statusLexicon TO LEXICON(
                    launchDetails, "",
                    "Launching in: 3", "").
    updateGui:CALL(statusLexicon).

    PRINT launchDetails.
    PRINT "Launching in: 3".
    WAIT 1.
    PRINT "2" AT (14, 2).
    DECLARE LOCAL statusLexicon TO LEXICON(
                    launchDetails, "",
                    "Launching in: 2", "").
    updateGui:CALL(statusLexicon).
    WAIT 1.
    PRINT "1"  AT (14, 2).
    DECLARE LOCAL statusLexicon TO LEXICON(
                    launchDetails, "",
                    "Launching in: 1", "").
    updateGui:CALL(statusLexicon).
    WAIT 1.
    PRINT "0"  AT (14, 2).
    DECLARE LOCAL statusLexicon TO LEXICON(
                    launchDetails, "",
                    "Launching in: 0", "").
    updateGui:CALL(statusLexicon).

    DECLARE LOCAL currentThrottle TO 1.
    LOCK THROTTLE TO currentThrottle.

    WHEN MAXTHRUST = 0 THEN {
        PRINT "Staging".
        STAGE.
    }.

    //========================LAUNCH==============================
    STAGE.
    PRINT "Launch!".

    CLEARSCREEN.

    executeGravityTurn(targetAltitude, targetInclination, pitchOverRate, updateGui).
    coastToApoapsis(targetVessel, targetAltitude, targetAltitudeOffset, updateGui).
    steerToTargetVessel(targetVessel, timeToImpactBuffer, minimumAngleToCorrect, updateGui).
    suicideBurnToTarget(targetVessel, timeToImpactBuffer, updateGui).
    killRemainingTargetVelocity(targetVessel, updateGui).
}

DECLARE FUNCTION calculateSemiMajorAxisDeltaV {
    DECLARE PARAMETER targetPeriapsis, targetApoapsis TO ORBIT:APOAPSIS, targetRadius TO ORBIT:APOAPSIS, currentOrbit TO ORBIT.

    DECLARE LOCAL mu TO currentOrbit:BODY:MU.
    DECLARE LOCAL radiusOfOrbit TO targetRadius + currentOrbit:BODY:RADIUS.
    DECLARE LOCAL startingSemiMajorAxis TO (currentOrbit:APOAPSIS + currentOrbit:PERIAPSIS) / 2 + currentOrbit:BODY:RADIUS.
    DECLARE LOCAL startingVelocity TO SQRT(mu * (2 / radiusOfOrbit - 1 / startingSemiMajorAxis)).
    DECLARE LOCAL endingSemiMajorAxis TO (targetApoapsis + targetPeriapsis) / 2 + currentOrbit:BODY:RADIUS.
    DECLARE LOCAL endingVelocity TO SQRT(mu * (2 / radiusOfOrbit - 1 / endingSemiMajorAxis)).

    RETURN endingVelocity - startingVelocity.
}

DECLARE FUNCTION hopToCoordinates {
    DECLARE PARAMETER targetPosition, pitchOverRate, updateGui.

    retractPanelsAndAntennas().

    DECLARE LOCAL targetBearing TO circleBearing(targetPosition).
    DECLARE LOCAL targetDistance TO circleDistance(targetPosition).
    DECLARE LOCAL overshootPosition TO circleDestination(targetBearing, targetDistance * 1.1).

    DECLARE LOCAL currentSteering TO HEADING(90, 90).
    LOCK STEERING TO currentSteering.
    LOCK THROTTLE TO 1.

    // Need some velocity to calculate future landing position
    WAIT .25.
    GEAR OFF.

    DECLARE LOCAL isDone TO FALSE.
    DECLARE LOCAL distanceToTarget TO 2 * CONSTANT:PI * BODY:RADIUS.
    DECLARE LOCAL prevDistanceToTarget TO 0.

    UNTIL (isDone) {
        DECLARE LOCAL heading TO targetBearing.
        DECLARE LOCAL currentPitch TO MAX(10, pitchOverRate * SHIP:VELOCITY:SURFACE:MAG + 90).
        SET currentSteering TO HEADING(heading, currentPitch).
        DECLARE LOCAL currentLandingCoordinates TO predictLandingCoordinates("Kepler").

        SET prevDistanceToTarget TO distanceToTarget.
        SET distanceToTarget TO circleDistance(targetPosition, currentLandingCoordinates).

        PRINT "Current Pitch    " + ROUND(currentPitch, 1) + "°  " AT (0, 0).
        PRINT "Current Heading  " + ROUND(heading, 1) + "°       " AT (0, 1).
        PRINT "Target position  " + ROUND(overshootPosition:LAT, 2) + ", " + ROUND(overshootPosition:LNG, 2) AT (0, 2).
        PRINT "Current Landing  " + ROUND(currentLandingCoordinates:LAT, 2) + ", " + ROUND(currentLandingCoordinates:LNG, 2) AT (0, 3).
        PRINT "Current Position " + ROUND(SHIP:GEOPOSITION:LAT, 2) + ", " + ROUND(SHIP:GEOPOSITION:LNG, 2) AT (0, 4).
        PRINT "Distance to Tgt  " + distanceToTarget AT (0, 5).

        DECLARE LOCAL statusLexicon TO LEXICON(
                        "Status", "Hopping to coordinates",
                        "Current Position", ROUND(SHIP:GEOPOSITION:LAT, 2) + "°, " + ROUND(SHIP:GEOPOSITION:LNG, 2) + "°",
                        "Target position", ROUND(overshootPosition:LAT, 2) + "°, " + ROUND(overshootPosition:LNG, 2) + "°",
                        "Current Landing", ROUND(currentLandingCoordinates:LAT, 2) + "°, " + ROUND(currentLandingCoordinates:LNG, 2) + "°",
                        "Distance to Tgt", ROUND(distanceToTarget) + "m",
                        "Current Pitch", ROUND(currentPitch, 1) + "°",
                        "Current Heading", ROUND(heading, 1) + "°").
        updateGui:CALL(statusLexicon).

        IF (prevDistanceToTarget * 1.05 < distanceToTarget) {
            SET isDone TO TRUE.
        }
    }
    LOCK THROTTLE TO 0.
    CLEARSCREEN.
    landingBurn(updateGui).
}

// Instantaneous azimuth
DECLARE FUNCTION azimuth {
    DECLARE PARAMETER inclination, orbitAltitude.
    DECLARE PARAMETER autoSwitch TO FALSE.

    DECLARE LOCAL shipLat TO SHIP:LATITUDE.
    IF (ABS(inclination) < ABS(shipLat)) {
        SET inclination TO shipLat.
    }

    DECLARE LOCAL sineRatio TO COS(inclination) / COS(shipLat).
    IF (sineRatio < -1) {
        SET sineRatio TO -1.
    } ELSE IF (sineRatio > 1) {
        SET sineRatio TO 1.
    }

    DECLARE LOCAL heading TO ARCSIN(sineRatio).
    IF (autoSwitch) {
        IF (angleToBodyDescendingNode(SHIP) < angleToBodyAscendingNode(SHIP)) {
            SET heading TO 180 - heading.
        }
    } ELSE IF (inclination < 0) {
        SET heading TO 180 - heading.
    }

    DECLARE LOCAL periapsisRadius TO ALTITUDE + BODY:RADIUS.
    DECLARE LOCAL apoapsisRadius TO orbitAltitude + BODY:RADIUS.
    DECLARE LOCAL orbitSpeed TO SQRT((2 * BODY:MU / periapsisRadius) - (2 * BODY:MU / (apoapsisRadius + periapsisRadius))).
    DECLARE LOCAL eastSpeed TO orbitSpeed * SIN(heading) - VDOT(SHIP:VELOCITY:ORBIT, HEADING(90, 0):VECTOR).
    DECLARE LOCAL northSpeed TO orbitSpeed * COS(heading) - VDOT(SHIP:VELOCITY:ORBIT, HEADING(0, 0):VECTOR).

    SET heading TO 90 - ARCTAN2(northSpeed, eastSpeed).

    RETURN MOD(heading + 360, 360).
}

DECLARE LOCAL FUNCTION timeToAscendingNode {
    DECLARE LOCAL shipAngularMomentum TO VCRS(SHIP:POSITION - BODY:POSITION, SHIP:VELOCITY:ORBIT * SHIP:MASS).
    DECLARE LOCAL positionVector TO SHIP:POSITION - BODY:POSITION.

    DECLARE LOCAL ascendingNodeVector TO VCRS(shipAngularMomentum, SHIP:BODY:ANGULARVEL). //kOS uses LHR instead of RHR
    DECLARE LOCAL eccentricityVector TO (SHIP:VELOCITY:ORBIT:MAG ^ 2 / SHIP:BODY:MU - 1 / positionVector:MAG)
            * positionVector - (VDOT(positionVector, SHIP:VELOCITY:ORBIT) / SHIP:BODY:MU) * SHIP:VELOCITY:ORBIT.

    DECLARE LOCAL positionEccentricityAngle TO ARCCOS(VDOT(eccentricityVector, positionVector) / (eccentricityVector:MAG * positionVector:MAG)).
    IF (VANG(VCRS(positionVector, eccentricityVector), shipAngularMomentum) < 90) { //Is ahead
        SET positionEccentricityAngle TO 360 - positionEccentricityAngle.
    }

    DECLARE LOCAL shipEccentricAnomaly TO 2 * ARCTAN2(SIN(positionEccentricityAngle / 2) * SQRT((1 - eccentricityVector:MAG) / (1 + eccentricityVector:MAG)), COS(positionEccentricityAngle / 2)).
    DECLARE LOCAL meanAnomaly TO ((CONSTANT:DEGTORAD * shipEccentricAnomaly) - (ORBIT:ECCENTRICITY * SIN(shipEccentricAnomaly))) * CONSTANT:RADTODEG.

    DECLARE LOCAL ascendingNodeAngle TO ARCCOS(VDOT(eccentricityVector, ascendingNodeVector) / (eccentricityVector:MAG * ascendingNodeVector:MAG)).
    IF (VANG(VCRS(ascendingNodeVector, eccentricityVector), shipAngularMomentum) < 90) { //Is ahead
        SET ascendingNodeAngle TO 360 - ascendingNodeAngle.
    }

    DECLARE LOCAL ascendingNodeEccentricAnomaly TO 2 * ARCTAN2(SIN(ascendingNodeAngle / 2) * SQRT((1 - eccentricityVector:MAG) / (1 + eccentricityVector:MAG)),
            COS(ascendingNodeAngle / 2)).
    DECLARE LOCAL ascendingNodeMeanAnomaly TO ascendingNodeEccentricAnomaly - SHIP:ORBIT:ECCENTRICITY * SIN(ascendingNodeEccentricAnomaly).

    DECLARE LOCAL timeToAscending TO 0.
    IF (meanAnomaly >= ascendingNodeMeanAnomaly) {
        SET timeToAscending TO ((360 - meanAnomaly) + ascendingNodeMeanAnomaly) / (360 / SHIP:ORBIT:PERIOD).
    } ELSE {
        SET timeToAscending TO (ascendingNodeMeanAnomaly - meanAnomaly) / (360 / SHIP:ORBIT:PERIOD).
    }

    RETURN timeToAscending.
}

// Angle to descending node with respect to ves' body's equator
DECLARE LOCAL FUNCTION angleToBodyDescendingNode {
    DECLARE PARAMETER vessel TO SHIP.

    DECLARE LOCAL joinVector TO -orbitLAN(vessel).
    DECLARE LOCAL angle TO VANG((vessel:POSITION - vessel:BODY:POSITION):NORMALIZED, joinVector).
    IF (vessel:STATUS = "LANDED") {
        SET angle TO angle - 90.
    } ELSE {
        DECLARE LOCAL signVector TO VCRS(-BODY:POSITION, joinVector).
        DECLARE LOCAL sign TO VDOT(orbitBinormal(vessel), signVector).
        IF (sign < 0) {
            SET angle TO angle * -1.
        }
    }
    RETURN angle.
}

// Angle to ascending node with respect to ves' body's equator
DECLARE LOCAL FUNCTION angleToBodyAscendingNode {
    DECLARE PARAMETER vessel TO SHIP.

    DECLARE LOCAL joinVector TO orbitLAN(vessel).
    DECLARE LOCAL angle TO VANG((vessel:POSITION - vessel:BODY:POSITION):NORMALIZED, joinVector).
    IF (vessel:status = "LANDED") {
        SET angle TO angle - 90.
    } ELSE {
        DECLARE LOCAL signVector TO VCRS(-BODY:POSITION, joinVector).
        DECLARE LOCAL sign TO VDOT(orbitBinormal(vessel), signVector).
        IF (sign < 0) {
            SET angle TO angle * -1.
        }
    }
    RETURN angle.
}

// Vector pointing in the direction of longitude of ascending node
DECLARE LOCAL FUNCTION orbitLAN {
    DECLARE PARAMETER vessel TO SHIP.

    RETURN ANGLEAXIS(vessel:ORBIT:LAN, vessel:BODY:ANGULARVEL:NORMALIZED) * SOLARPRIMEVECTOR.
}

// In the direction of orbital angular momentum of ves
// Typically same as Normal
DECLARE LOCAL FUNCTION orbitBinormal {
    DECLARE PARAMETER vessel TO SHIP.

    RETURN VCRS((vessel:POSITION - vessel:BODY:POSITION):NORMALIZED, orbitTangent(vessel)):NORMALIZED.
}

// Same as orbital prograde vector for ves
DECLARE LOCAL FUNCTION orbitTangent {
    DECLARE PARAMETER vessel TO SHIP.

    RETURN vessel:VELOCITY:ORBIT:NORMALIZED.
}

DECLARE LOCAL FUNCTION vectorAtLongitude {
    DECLARE PARAMETER longitude.

    DECLARE LOCAL orbitPosition TO ORBIT:POSITION - BODY:POSITION.
    DECLARE LOCAL orbitVelocity TO ORBIT:VELOCITY:ORBIT.
    DECLARE LOCAL orbitNormal TO VCRS(orbitPosition, orbitVelocity).

    DECLARE LOCAL deltaLongitude TO longitude - ORBIT:LAN.
    DECLARE LOCAL targetNormal TO ANGLEAXIS(deltaLongitude, V(0, -1, 0)) * orbitNormal.

    RETURN VCRS(V(0, 1, 0), targetNormal).
}

DECLARE LOCAL FUNCTION warpToWindowForLAN {
    DECLARE PARAMETER targetLongitude, updateGui.

    DECLARE LOCAL angleToNode TO 180.

    UNTIL (angleToNode < 1.5) {
        DECLARE LOCAL ascendingNode TO vectorAtLongitude(targetLongitude).
        DECLARE LOCAL descendingNode TO vectorAtLongitude(targetLongitude + 180).
        DECLARE LOCAL targetNode TO V(0, 0, 0).

        IF (VDOT(ascendingNode, SHIP:VELOCITY:ORBIT) > 0) {
            SET targetNode to ascendingNode.
        } ELSE {
            SET targetNode to descendingNode.
        }

        SET angleToNode to VANG(SHIP:POSITION - BODY:POSITION, targetNode).
        IF (angleToNode > 10) {
            SET WARP TO 5.
        } ELSE IF (angleToNode > 2) {
            SET WARP TO 3.
        } ELSE {
            SET WARP TO 1.
        }

        DECLARE LOCAL launchWindowStatus TO LEXICON(
                        "Status", "Waiting for Launch Window",
                        "Angle to target", ROUND(VANG(SHIP:POSITION - BODY:POSITION, targetNode), 2) + "°").
        updateGui:CALL(launchWindowStatus).

        WAIT .1.
    }

    SET WARP TO 0.
    WAIT UNTIL (KUNIVERSE:TIMEWARP:ISSETTLED).
    WAIT 1.
}

DECLARE LOCAL FUNCTION waitForLaunchWindow {
    DECLARE PARAMETER targetVessel, timeToOrbit, updateGui.

    DECLARE LOCAL targetPosition TO POSITIONAT(targetVessel, TIME + timeToOrbit).
    DECLARE LOCAL upVector TO UP:VECTOR.

    DECLARE LOCAL minimumLaunchAngle TO 0.
    DECLARE LOCAL maximumLaunchAngle TO 15.

    UNTIL (VECTORANGLE(targetPosition, upVector) < maximumLaunchAngle AND VECTORANGLE(targetPosition, upVector) > minimumLaunchAngle) {
        SET targetPosition TO POSITIONAT(TARGET, TIME + timeToOrbit).
        SET upVector TO UP:VECTOR.
        CLEARSCREEN.

        PRINT "Angle to target: " + ROUND(VECTORANGLE(targetPosition, upVector), 2) + "°".

        DECLARE LOCAL maximumWarpAngle TO 40.
        DECLARE LOCAL minimumWarpAngle TO 25.
        DECLARE LOCAL warpSpeed TO 4.

        IF (VECTORANGLE(targetPosition, upVector) > maximumWarpAngle) {
            SET WARP TO warpSpeed.
        }
        IF (VECTORANGLE(targetPosition, upVector) < minimumWarpAngle) {
            SET WARP TO 0.
        }
        DECLARE LOCAL launchWindowStatus TO LEXICON(
                "Status", "Waiting for Launch Window",
                "Angle to target", ROUND(VECTORANGLE(targetPosition, upVector), 2) + "°").
        updateGui:CALL(launchWindowStatus).
    }
}

DECLARE LOCAL FUNCTION coastToApoapsis {
    DECLARE PARAMETER targetVessel, targetAltitude, targetAltitudeOffset, updateGui.

    DECLARE LOCAL relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
    DECLARE LOCAL targetRetrogradeVector TO -1 * targetVessel:DIRECTION:VECTOR.
    DECLARE LOCAL angleToRetrograde TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector).
    SET relativeVelocityOrbit TO relativeVelocityOrbit:NORMALIZED.
    SET targetRetrogradeVector TO targetRetrogradeVector:NORMALIZED.

    DECLARE LOCAL currentHeading TO 3 * (relativeVelocityOrbit - targetRetrogradeVector) + relativeVelocityOrbit.
    WAIT 1.

    DECLARE LOCAL burnTime TO 0.
    DECLARE LOCAL beginCruiseAltitude TO targetAltitude - targetAltitudeOffset.
    DECLARE LOCAL maxWarpAltitude TO targetAltitude - 2 * targetAltitudeOffset.

    UNTIL (ALTITUDE > beginCruiseAltitude) {
        SET relativeVelocityOrbit TO targetVessel:VELOCITY:ORBIT - VELOCITY:ORBIT.
        SET targetRetrogradeVector TO -1 * targetVessel:DIRECTION:VECTOR.
        IF (MAXTHRUST > 0) {
            SET burnTime to relativeVelocityOrbit:MAG * MASS / (MAXTHRUST * 2).
        }
        IF (BODY:ATM:ALTITUDEPRESSURE(ALTITUDE) > .0002) {
            LOCK STEERING TO SRFPROGRADE.
        } ELSE {
            LOCK STEERING TO currentHeading.
        }

        DECLARE LOCAL impactTime TO targetVessel:DISTANCE / relativeVelocityOrbit:MAG.
        DECLARE LOCAL status TO "Waiting until near apoapsis to adjust orbit".
        DECLARE LOCAL thrustAngle TO VECTORANGLE(relativeVelocityOrbit, currentHeading).
        DECLARE LOCAL closestApproach TO targetVessel:DISTANCE * SIN(VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector)).
        printTargetApproachDetails(status, angleToRetrograde, thrustAngle, targetVessel:DISTANCE,
                impactTime, burnTime, closestApproach, relativeVelocityOrbit:MAG, updateGui).

        SET relativeVelocityOrbit TO relativeVelocityOrbit:NORMALIZED.
        SET targetRetrogradeVector TO targetRetrogradeVector:NORMALIZED.
        SET currentHeading TO 3 * (relativeVelocityOrbit - targetRetrogradeVector) + relativeVelocityOrbit.

        WAIT .5.

        SET angleToRetrograde TO VECTORANGLE(relativeVelocityOrbit, targetRetrogradeVector).

        IF (KUNIVERSE:TIMEWARP:MODE = "RAILS" AND ALTITUDE < targetAltitude - 4000) {
            SET WARP TO 3.
        }
        IF (ALTITUDE > maxWarpAltitude) {
            SET WARP TO 0.
        }
    }
}

DECLARE LOCAL FUNCTION executeGravityTurn {
    DECLARE PARAMETER targetAltitude, targetInclination, pitchOverRate, updateGui.

    // Launch pointing straight up
    DECLARE LOCAL currentSteering TO HEADING(90, 90).
    LOCK STEERING TO currentSteering.

    // Burn until apoapsis is at target altitude
    UNTIL (APOAPSIS > targetAltitude) {
        DECLARE LOCAL heading TO azimuth(targetInclination, targetAltitude).

        // Will pitch the craft over from 0° to 10° based on pitchOverRate and current velocity
        DECLARE LOCAL currentPitch TO MAX(10, pitchOverRate * SHIP:VELOCITY:SURFACE:MAG + 90).
        SET currentSteering TO HEADING(heading, currentPitch).

        printAscentDetails("Ascent", targetInclination, currentPitch, heading, updateGui).
    }

    // Shut down engine after apoapsis is at targetAltitude
    LOCK THROTTLE TO 0.
}

DECLARE LOCAL FUNCTION burnToFinalPeriapsisArgument {
    DECLARE PARAMETER targetPeriapsis, targetApoapsis, targetArgument, updateGui.

    DECLARE LOCAL timeToAscendingNode TO timeToAscendingNode().
    DECLARE LOCAL timeToArgument TO calculateTimeFromMeanAnomaly(targetArgument).
    DECLARE LOCAL timeToBurn TO timeToAscendingNode + timeToArgument.
    IF (timeToBurn < 180) {
        SET timeToBurn TO timeToBurn + ORBIT:PERIOD.
    }

    DECLARE LOCAL apoapsisDeltaV TO calculateSemiMajorAxisDeltaV(ORBIT:PERIAPSIS, targetApoapsis).
    DECLARE LOCAL apoapsisNode TO NODE(TIME:SECONDS + timeToBurn, 0, 0, apoapsisDeltaV).
    ADD apoapsisNode.
    executeNode(apoapsisNode, updateGui).

    DECLARE LOCAL periapsisDeltaV TO calculateSemiMajorAxisDeltaV(targetPeriapsis).
    DECLARE LOCAL periapsisNode TO NODE(TIME:SECONDS + ETA:APOAPSIS, 0, 0, periapsisDeltaV).
    ADD periapsisNode.
    executeNode(periapsisNode, updateGui).
}

// Calculates the time it takes to pass a given amount of mean anomaly given the orbital period
DECLARE FUNCTION calculateTimeFromMeanAnomaly {
    // meanAnomonaly: The amount of mean anomaly
    // orbitPeriod: The orbital period, defaults to current period
    DECLARE PARAMETER meanAnomonaly, orbitPeriod TO OBT:PERIOD.

    RETURN (orbitPeriod * meanAnomonaly / 360).
}