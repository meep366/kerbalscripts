@LAZYGLOBAL OFF.

DECLARE FUNCTION createGuiText {
    DECLARE PARAMETER gui, labelText, defaultValue, textWidth TO 60.

    DECLARE LOCAL box TO gui:ADDHBOX().
    DECLARE LOCAL label TO box:ADDLABEL(labelText).
    SET label:STYLE:WIDTH TO 215.
    DECLARE LOCAL text TO box:ADDTEXTFIELD(defaultValue).
    SET text:STYLE:WIDTH TO textWidth.

    RETURN text.
}

DECLARE FUNCTION createGuiDropdown {
    DECLARE PARAMETER gui, labelText, options, dropdownWidth TO 60.

    DECLARE LOCAL box TO gui:ADDHBOX().
    DECLARE LOCAL label TO box:ADDLABEL(labelText).
    SET label:STYLE:WIDTH TO 215.

    DECLARE LOCAL dropdown TO box:ADDPOPUPMENU().
    SET dropdown:STYLE:WIDTH TO dropdownWidth.
    FOR option IN options {
        dropdown:ADDOPTION(option).
    }

    RETURN dropdown.
}

DECLARE FUNCTION createAutopilotGUI {
    DECLARE PARAMETER waypointButtonClick, setAutopilotNav, setAutopilotILS,
            setAutopilotOff, setHeadingMode, setHeadingWest, setHeadingEast,
            setBankMode, setBankLeft, setBankRight, setAltitudeMode, setAltitudeDown,
            setAltitudeUp, setPitchMode, setPitchDown, setPitchUp, setAutothrottleSpeed,
            setAutothrottleMax, setAutothrottleOff, setSpeedDown, setSpeedUp.

    DECLARE LOCAL runwayKSCEndOne TO LATLNG(-.049849, -74.7281).
    DECLARE LOCAL runwayKSCEndTwo TO LATLNG(-.049849, -74.4889).
    DECLARE LOCAL runwayOldAirfieldEndOne TO LATLNG(-1.51795834829155, -71.972).
    DECLARE LOCAL runwayOldAirfieldEndTwo TO LATLNG(-1.51795834829155, -71.853).
    DECLARE LOCAL baikerbanurEndOne TO LATLNG(20.1219, -148.2085).
    DECLARE LOCAL baikerbanurEndTwo TO LATLNG(19.8093, -148.466).
    DECLARE LOCAL glacierLakeEndOne TO LATLNG(73.503744, 83.681286).
    DECLARE LOCAL glacierLakeEndTwo TO LATLNG(73.404164, 82.647442).
    DECLARE LOCAL craterEndOne TO LATLNG(9.081153, -163.073119).
    DECLARE LOCAL craterEndTwo TO LATLNG(7.246336, -160.534489).
    DECLARE LOCAL mahiMahiEndOne TO LATLNG(-50.186481, -124.330678).
    DECLARE LOCAL mahiMahiEndTwo TO LATLNG(-50.705692, -124.206856).
    DECLARE LOCAL coveEndOne TO LATLNG(3.710028, -72.380808).
    DECLARE LOCAL coveEndTwo TO LATLNG(3.783303, -72.243614).
    DECLARE LOCAL southPoleEndOne TO LATLNG(-74.5, -129).
    DECLARE LOCAL southPoleEndTwo TO LATLNG(-74, -129).
    DECLARE LOCAL northPoleEndOne TO LATLNG(80, -54.5).
    DECLARE LOCAL northPoleEndTwo TO LATLNG(80.5, -54).
    DECLARE LOCAL anomalyIslandEndOne TO LATLNG(4.15, -173).
    DECLARE LOCAL anomalyIslandEndTwo TO LATLNG(4.60, -170.86).

    // Create a GUI window
    DECLARE LOCAL mainGui TO GUI(300).
    SET mainGui:X TO 500.
    SET mainGui:Y TO 60.

    //Waypoint Selection screen
    DECLARE LOCAL guiWayPoint TO GUI(200).
    SET guiWayPoint:X TO mainGui:X + 300.
    SET guiWayPoint:Y TO mainGui:Y + 200.
    DECLARE LOCAL labelSelectWaypoint TO guiWayPoint:ADDLABEL("<size=20><b>Select waypoint:</b></size>").
    SET labelSelectWaypoint:STYLE:ALIGN TO "CENTER".
    SET labelSelectWaypoint:STYLE:HSTRETCH TO TRUE.

    IF (BODY = KERBIN) {
        DECLARE LOCAL buttonKSCRunway TO guiWayPoint:ADDBUTTON("KSC Runway").
        SET buttonKSCRunway:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, runwayKSCEndOne, runwayKSCEndTwo, "KSC Runway").

        DECLARE LOCAL buttonOldAirfield TO guiWayPoint:ADDBUTTON("Old Airfield").
        SET buttonOldAirfield:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, runwayOldAirfieldEndOne, runwayOldAirfieldEndTwo, "Old Airfield").

        DECLARE LOCAL buttonBaikerbanur TO guiWayPoint:ADDBUTTON("Baikerbanur").
        SET buttonBaikerbanur:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, baikerbanurEndOne, baikerbanurEndTwo, "Baikerbanur").

        DECLARE LOCAL buttonGlacierLake TO guiWayPoint:ADDBUTTON("Glacier Lake").
        SET buttonGlacierLake:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, glacierLakeEndOne, glacierLakeEndTwo, "Glacier Lake").

        DECLARE LOCAL buttonCrater TO guiWayPoint:ADDBUTTON("Crater Rim").
        SET buttonCrater:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, craterEndOne, craterEndTwo, "Crater Rim").

        DECLARE LOCAL buttonMahiMahi TO guiWayPoint:ADDBUTTON("Mahi Mahi").
        SET buttonMahiMahi:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, mahiMahiEndOne, mahiMahiEndTwo, "Mahi Mahi").

        DECLARE LOCAL buttonCove TO guiWayPoint:ADDBUTTON("Cove").
        SET buttonCove:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, coveEndOne, coveEndTwo, "Cove").

        DECLARE LOCAL buttonSouthPole TO guiWayPoint:ADDBUTTON("South Pole").
        SET buttonSouthPole:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, southPoleEndOne, southPoleEndTwo, "South Pole").

        DECLARE LOCAL buttonNorthPole TO guiWayPoint:ADDBUTTON("North Pole").
        SET buttonNorthPole:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, northPoleEndOne, northPoleEndTwo, "North Pole").
    } ELSE IF (BODY = LAYTHE) {
        DECLARE LOCAL buttonAnomalyIsland TO guiWayPoint:ADDBUTTON("Anomaly Island").
        SET buttonAnomalyIsland:ONCLICK TO waypointButtonClick@:bind(guiWayPoint, anomalyIslandEndOne, anomalyIslandEndTwo, "Anomaly Island").
    }

    // Add widgets to the GUI
    DECLARE LOCAL labelMode TO mainGui:ADDLABEL("<b>AP Mode</b>").
    SET labelMode:STYLE:ALIGN TO "CENTER".
    SET labelMode:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL autoPilotButtons TO mainGui:ADDHBOX().
    DECLARE LOCAL buttonNAV TO autoPilotButtons:ADDBUTTON("HLD").
    DECLARE LOCAL buttonILS TO autoPilotButtons:ADDBUTTON("ILS").
    DECLARE LOCAL buttonAutoPilotOff TO autoPilotButtons:ADDBUTTON("OFF").

    SET buttonNAV:ONCLICK TO setAutopilotNav@.
    SET buttonILS:ONCLICK TO setAutopilotILS@.
    SET buttonAutoPilotOff:ONCLICK TO setAutopilotOff@.

    //Autopilot settings
    DECLARE LOCAL autoPilotSettings to mainGui:ADDVBOX().

    //Heading Settings
    DECLARE LOCAL headingSettings TO autoPilotSettings:ADDHLAYOUT().
    DECLARE LOCAL buttonHDG TO headingSettings:ADDBUTTON("HDG").
    SET buttonHDG:STYLE:WIDTH TO 40.
    SET buttonHDG:STYLE:HEIGHT TO 25.

    DECLARE LOCAL buttonHDGLeft TO headingSettings:ADDBUTTON("◀").
    SET buttonHDGLeft:STYLE:WIDTH TO 40.
    SET buttonHDGLeft:STYLE:HEIGHT TO 25.

    DECLARE LOCAL labelHDG TO headingSettings:ADDLABEL("").
    SET labelHDG:STYLE:HEIGHT TO 25.
    SET labelHDG:STYLE:ALIGN TO "CENTER".

    DECLARE LOCAL buttonHDGRight TO headingSettings:ADDBUTTON("▶").
    SET buttonHDGRight:STYLE:WIDTH TO 40.
    SET buttonHDGRight:STYLE:HEIGHT TO 25.

    SET buttonHDG:ONCLICK TO setHeadingMode@.
    SET buttonHDGLeft:ONCLICK TO setHeadingWest@.
    SET buttonHDGRight:ONCLICK TO setHeadingEast@.

    //Bank Settings
    DECLARE LOCAL bankSettings TO autoPilotSettings:ADDHLAYOUT().
    DECLARE LOCAL buttonBNK TO bankSettings:ADDBUTTON("BNK").
    SET buttonBNK:STYLE:WIDTH TO 40.
    SET buttonBNK:STYLE:HEIGHT TO 25.

    DECLARE LOCAL buttonBNKLeft TO bankSettings:ADDBUTTON("◀").
    SET buttonBNKLeft:STYLE:WIDTH TO 40.
    SET buttonBNKLeft:STYLE:HEIGHT TO 25.

    DECLARE LOCAL labelBNK TO bankSettings:ADDLABEL("").
    SET labelBNK:STYLE:HEIGHT TO 25.
    SET labelBNK:STYLE:ALIGN TO "CENTER".

    DECLARE LOCAL buttonBNKRight TO bankSettings:ADDBUTTON("▶").
    SET buttonBNKRight:STYLE:WIDTH TO 40.
    SET buttonBNKRight:STYLE:HEIGHT TO 25.

    SET buttonBNK:ONCLICK TO setBankMode@.
    SET buttonBNKLeft:ONCLICK TO setBankLeft@.
    SET buttonBNKRight:ONCLICK TO setBankRight@.

    //Altitude Settings
    DECLARE LOCAL altitudeSettings to autoPilotSettings:ADDHLAYOUT().
    DECLARE LOCAL buttonALT TO altitudeSettings:ADDBUTTON("ALT").
    SET buttonALT:STYLE:WIDTH TO 40.
    SET buttonALT:STYLE:HEIGHT TO 25.

    DECLARE LOCAL buttonALTDown TO altitudeSettings:ADDBUTTON("▼").
    SET buttonALTDown:STYLE:WIDTH TO 40.
    SET buttonALTDown:STYLE:HEIGHT TO 25.

    DECLARE LOCAL labelALT TO altitudeSettings:ADDLABEL("").
    SET labelALT:STYLE:HEIGHT TO 25.
    SET labelALT:STYLE:ALIGN TO "CENTER".

    DECLARE LOCAL buttonALTUp TO altitudeSettings:ADDBUTTON("▲").
    SET buttonALTUp:STYLE:WIDTH TO 40.
    SET buttonALTUp:STYLE:HEIGHT TO 25.

    SET buttonALT:ONCLICK TO setAltitudeMode@.
    SET buttonALTDown:ONCLICK TO setAltitudeDown@.
    SET buttonALTUp:ONCLICK TO setAltitudeUp@.

    //Pitch Settings
    DECLARE LOCAL pitchSettings to autoPilotSettings:ADDHLAYOUT().
    DECLARE LOCAL buttonPIT TO pitchSettings:ADDBUTTON("PIT").
    SET buttonPIT:STYLE:WIDTH TO 40.
    SET buttonPIT:STYLE:HEIGHT TO 25.

    DECLARE LOCAL buttonPITDown TO pitchSettings:ADDBUTTON("▼").
    SET buttonPITDown:STYLE:WIDTH TO 40.
    SET buttonPITDown:STYLE:HEIGHT TO 25.

    DECLARE LOCAL labelPIT TO pitchSettings:ADDLABEL("").
    SET labelPIT:STYLE:HEIGHT TO 25.
    SET labelPIT:STYLE:ALIGN TO "CENTER".

    DECLARE LOCAL buttonPITUp TO pitchSettings:ADDBUTTON("▲").
    SET buttonPITUp:STYLE:WIDTH TO 40.
    SET buttonPITUp:STYLE:HEIGHT TO 25.

    SET buttonPIT:ONCLICK TO setPitchMode@.
    SET buttonPITDown:ONCLICK TO setPitchDown@.
    SET buttonPITUp:ONCLICK TO setPitchUp@.

    // Waypoints selection
    DECLARE LOCAL buttonWaypoints TO autoPilotSettings:ADDBUTTON("Select waypoint").
    DECLARE LOCAL waypointSettings TO autoPilotSettings:ADDHLAYOUT().
    DECLARE LOCAL labelWaypoint TO waypointSettings:ADDLABEL("No waypoint selected").

    IF (BODY = KERBIN) {
        SET labelWaypoint:TEXT TO "KSC Runway".
    } ELSE IF (BODY = LAYTHE) {
        SET labelWaypoint:TEXT TO "Anomaly Island".
    }

    DECLARE LOCAL labelWaypointDist TO waypointSettings:ADDLABEL("").
    SET labelWaypointDist:STYLE:ALIGN TO "RIGHT".
    SET buttonWaypoints:ONCLICK TO {
        guiWayPoint:SHOW().
    }.

    DECLARE LOCAL baseSelectButtons TO mainGui:ADDHBOX().
    DECLARE LOCAL checkboxILSVectors TO baseSelectButtons:ADDBUTTON("HoloILS™").
    SET checkboxILSVectors:TOGGLE TO TRUE.

    // Autothrottle
    DECLARE LOCAL autoThrottleButtons TO mainGui:ADDHBOX().
    DECLARE LOCAL buttonSPD TO autoThrottleButtons:ADDBUTTON("SPD").
    DECLARE LOCAL buttonMCT TO autoThrottleButtons:ADDBUTTON("MCT").
    DECLARE LOCAL buttonAutoThrottleOff TO autoThrottleButtons:ADDBUTTON("OFF").

    SET buttonSPD:ONCLICK TO setAutothrottleSpeed@.
    SET buttonMCT:ONCLICK TO setAutothrottleMax@.
    SET buttonAutoThrottleOff:ONCLICK TO setAutothrottleOff@.

    DECLARE LOCAL speedControlBox TO mainGui:ADDHBOX().
    LOCAL buttonSPDDown TO speedControlBox:ADDBUTTON("▼").
    SET buttonSPDDown:STYLE:WIDTH TO 45.
    SET buttonSPDDown:STYLE:HEIGHT TO 25.

    DECLARE LOCAL labelSPD TO speedControlBox:ADDLABEL("").
    SET labelSPD:STYLE:HEIGHT TO 25.
    SET labelSPD:STYLE:ALIGN TO "CENTER".

    DECLARE LOCAL buttonSPDUp TO speedControlBox:ADDBUTTON("▲").
    SET buttonSPDUp:STYLE:WIDTH TO 45.
    SET buttonSPDUp:STYLE:HEIGHT TO 25.

    SET buttonSPDDown:ONCLICK TO setSpeedDown@.
    SET buttonSPDUp:ONCLICK TO setSpeedUp@.

    DECLARE LOCAL labelAirspeed TO mainGui:ADDLABEL("<b>Airspeed</b>").
    SET labelAirspeed:STYLE:ALIGN TO "LEFT".
    SET labelAirspeed:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL labelVerticalSpeed TO mainGui:ADDLABEL("<b>Vertical speed</b>").
    SET labelVerticalSpeed:STYLE:ALIGN TO "LEFT".
    SET labelVerticalSpeed:STYLE:HSTRETCH TO TRUE.

    DECLARE LOCAL labelLatitude IS mainGui:ADDLABEL("<b>LAT</b>").
    SET labelLatitude:STYLE:ALIGN TO "LEFT".
    SET labelLatitude:STYLE:HSTRETCH TO TRUE.
    SET labelLatitude:STYLE:TEXTCOLOR TO YELLOW.

    DECLARE LOCAL labelLongitude IS mainGui:ADDLABEL("<b>LNG</b>").
    SET labelLongitude:STYLE:ALIGN TO "LEFT".
    SET labelLongitude:STYLE:HSTRETCH TO TRUE.
    SET labelLongitude:STYLE:TEXTCOLOR TO YELLOW.

    LOCAL buttonReboot TO mainGui:ADDBUTTON("Reboot").

    DECLARE FUNCTION rebootAutoPilot {
        mainGui:HIDE().
        SET SHIP:CONTROL:NEUTRALIZE TO TRUE.
        SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
        REBOOT.
    }
    SET buttonReboot:ONCLICK TO rebootAutoPilot@.

    RETURN mainGui.
}
