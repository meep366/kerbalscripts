@LAZYGLOBAL OFF.

DECLARE FUNCTION headingMagnitude {
    DECLARE LOCAL northPole TO LATLNG(90,0).
    RETURN MOD(360 - northPole:BEARING, 360).
}

DECLARE FUNCTION pitchAngle {
    RETURN -(VANG(SHIP:UP:VECTOR, SHIP:FACING:FOREVECTOR) - 90).
}

DECLARE FUNCTION progradePitchAngle {
    RETURN -(VANG(SHIP:UP:VECTOR, VXCL(SHIP:FACING:STARVECTOR, SHIP:VELOCITY:SURFACE)) - 90).
}

DECLARE FUNCTION sideslipAngle {
    DECLARE LOCAL yawErrorVector TO VXCL(FACING:TOPVECTOR, SHIP:SRFPROGRADE:VECTOR).
    DECLARE LOCAL yawErrorAngle TO VANG(FACING:VECTOR, yawErrorVector).

    IF (VDOT(SHIP:FACING:STARVECTOR, SHIP:SRFPROGRADE:VECTOR) < 0) {
        RETURN yawErrorAngle.
    } ELSE {
        RETURN -yawErrorAngle.
    }
}

DECLARE FUNCTION angleOfAttack {
    DECLARE LOCAL pitchErrorVector TO VXCL(FACING:STARVECTOR, SHIP:SRFPROGRADE:VECTOR).
    DECLARE LOCAL pitchErrorAngle TO VANG(FACING:VECTOR, pitchErrorVector).

    IF (VDOT(SHIP:FACING:TOPVECTOR, SHIP:srfprograde:VECTOR) < 0) {
        RETURN pitchErrorAngle.
    } ELSE {
        RETURN -pitchErrorAngle.
    }
}

DECLARE FUNCTION bankAngle {
    DECLARE LOCAL starBoardRotation TO SHIP:FACING * R(0, 90, 0).
    DECLARE LOCAL starBoardVector TO starBoardRotation:VECTOR.
    DECLARE LOCAL horizonVector TO VCRS(SHIP:UP:VECTOR, SHIP:FACING:VECTOR).

    IF (VDOT(SHIP:UP:VECTOR, starBoardVector) < 0) {
        RETURN VANG(starBoardVector, horizonVector).
    } ELSE {
        RETURN -VANG(starBoardVector, horizonVector).
    }
}

DECLARE FUNCTION deployPanelsAndAntennas {
    PANELS ON.
    DECLARE LOCAL antennaList TO SHIP:PARTSDUBBEDPATTERN("Communotron").
    FOR part IN antennaList {
        IF (part:HASMODULE("ModuleDeployableAntenna")) {
            IF (part:GETMODULE("ModuleDeployableAntenna"):HASEVENT("Extend Antenna")) {
                part:GETMODULE("ModuleDeployableAntenna"):DOEVENT("Extend Antenna").
            }
        }
    }
}

DECLARE FUNCTION retractPanelsAndAntennas {
    PANELS OFF.
    DECLARE LOCAL antennaList TO SHIP:PARTSDUBBEDPATTERN("Communotron").
    FOR part IN antennaList {
        IF (part:HASMODULE("ModuleDeployableAntenna")) {
            IF (part:GETMODULE("ModuleDeployableAntenna"):HASEVENT("Retract Antenna")) {
                part:GETMODULE("ModuleDeployableAntenna"):DOEVENT("Retract Antenna").
            }
        }
    }
}

DECLARE FUNCTION getShipDockingPort {
    DECLARE LOCAL shipDockingPorts TO SHIP:PARTSDUBBEDPATTERN("Clamp-O-Tron Docking Port").
    DECLARE LOCAL shipDockingPort TO 0.
    IF (shipDockingPorts:LENGTH = 0) {
        headsUpText("No docking port found on ship").
        RETURN FALSE.
    } ELSE {
        FOR dockingPort IN shipDockingPorts {
            IF (dockingPort:STATE = "Ready") {
                SET shipDockingPort TO dockingPort.
                BREAK.
            }
        }
    }
    IF (shipDockingPort = 0) {
        headsUpText("Ship docking ports in use").
        RETURN FALSE.
    }
    RETURN shipDockingPort.
}

DECLARE FUNCTION getShipIsru {
    DECLARE LOCAL shipIsruList TO SHIP:PARTSDUBBEDPATTERN("Convert-O-Tron").
    IF (shipIsruList:LENGTH = 0) {
        headsUpText("No ISRU found on ship").
        RETURN FALSE.
    } ELSE {
        RETURN shipIsruList[0].
    }
}

DECLARE FUNCTION isDocked {
    DECLARE LOCAL shipDockingPorts TO SHIP:PARTSDUBBEDPATTERN("Clamp-O-Tron Docking Port").
    FOR dockingPort IN shipDockingPorts {
        IF (dockingPort:STATE <> "Ready" AND dockingPort:STATE <> "Disbled") {
            RETURN TRUE.
        }
    }
    RETURN FALSE.
}

DECLARE FUNCTION undock {
    DECLARE PARAMETER dockingPort.

    IF (dockingPort:STATE <> "Ready") {
        dockingPort:UNDOCK.
        WAIT 5.

        PRINT "Warping away".

        SET WARP TO 4.
        WAIT 15.
        SET WARP TO 0.
    }
}

DECLARE FUNCTION changeIsruState {
    DECLARE PARAMETER isru, state, fuelType.

    DECLARE LOCAL moduleIterator TO isru:MODULES:ITERATOR.
    UNTIL (moduleIterator:NEXT = FALSE) {
        IF (moduleIterator:VALUE = "ModuleResourceConverter") {
            DECLARE LOCAL resourceModule TO isru:GETMODULEBYINDEX(moduleIterator:INDEX).
            IF (resourceModule:HASEVENT(state + " isru [" + fuelType + "]")) {
                resourceModule:DOEVENT(state + " isru ["+ fuelType +"]").
            }
        }
    }
}

DECLARE FUNCTION getResourceCapacity {
    DECLARE PARAMETER fuelType.

    DECLARE LOCAL maxResource TO 0.
    DECLARE LOCAL resources TO SHIP:RESOURCES.
    DECLARE LOCAL resourcesIterator TO SHIP:RESOURCES:ITERATOR.
    UNTIL (resourcesIterator:NEXT = FALSE) {
        IF (resourcesIterator:VALUE:NAME = fuelType) {
            SET maxResource TO maxResource + resourcesIterator:VALUE:CAPACITY.
        }
    }
    RETURN maxResource.
}

DECLARE FUNCTION getShipIsp {
    DECLARE PARAMETER pressure TO 0.

    DECLARE LOCAL engineList TO LIST().
    LIST ENGINES IN engineList.

    DECLARE LOCAL totalThrust TO 0.
    DECLARE LOCAL weightedThrust TO 0.
    FOR eng IN engineList {
        SET totalThrust TO totalThrust + eng:POSSIBLETHRUSTAT(pressure).
        SET weightedThrust TO weightedThrust + eng:POSSIBLETHRUSTAT(pressure) / eng:ISPAT(pressure).
    }
    RETURN totalThrust / weightedThrust.
}

DECLARE FUNCTION shipConsumesRcs {
    DECLARE LOCAL rcsConsumers TO LIST(
                    "Place-Anywhere 7 Linear RCS Port",
                    "Place Anywhere 1 Linear RCS Port",
                    "RV-105 RCS Thruster Block",
                    "RV-1X Variable Thruster Block",
                    "O-10 " + CHAR(34) + "Puff" + CHAR(34) + " MonoPropellant Fuel Engine").
    FOR consumer IN rcsConsumers {
        DECLARE LOCAL consumers TO SHIP:PARTSTITLED(consumer).
        IF (consumers:LENGTH > 0) {
            RETURN TRUE.
        }
    }
    RETURN FALSE.
}

DECLARE FUNCTION setRapierAirBreathing {
    DECLARE PARAMETER airBreathing.

    DECLARE LOCAL rapiers TO list().
    DECLARE LOCAL engineLIST TO LIST().

    LIST engines IN engineList.
    FOR engine in engineList {
        IF (engine:NAME = "RAPIER") {
            rapiers:ADD(engine).
        }
    }

    FOR rapier IN rapiers {
        IF (NOT airBreathing = rapier:PrimaryMode) {
            rapier:togglemode().
        }
    }
}

DECLARE FUNCTION hasThermometer {
    DECLARE LOCAL sensorList TO LIST().
    LIST SENSORS IN sensorList.
    FOR sensor IN sensorList {
        IF (sensor:TYPE = "TEMP") {
            RETURN TRUE.
        }
    }
    RETURN FALSE.
}

DECLARE FUNCTION hasAccelerometer {
    DECLARE LOCAL sensorList TO LIST().
    LIST SENSORS IN sensorList.
    FOR sensor IN sensorList {
        IF (sensor:TYPE = "ACC") {
            RETURN TRUE.
        }
    }
    RETURN FALSE.
}

DECLARE FUNCTION getMachNumber {
    DECLARE PARAMETER speedMetersPerSecond.
    DECLARE LOCAL airTemp TO 288.15.
    IF (hasThermometer()) {
        SET airTemp TO SHIP:SENSORS:TEMP.
    }
    RETURN speedMetersPerSecond / SQRT(1.4 * 286 * airTemp).
}

DECLARE FUNCTION setWheelFrictionAuto {
    DECLARE LOCAL gearList TO SHIP:PARTSDUBBEDPATTERN("Gear").
    DECLARE LOCAL startingFriction TO LEXICON().

    // Need to gather all starting frictions before turning off override
    // Otherwise will have incomplete gear list due to adjusting mirrored parts at the same time
    FOR gear IN gearList {
        DECLARE LOCAL gearModule TO gear:GETMODULE("ModuleWheelBase").
        IF (gearModule:HASFIELD("Friction Control")) {
            startingFriction:ADD(gear, gearModule:GETFIELD("Friction Control")).
        }
    }
    FOR gear IN gearList {
        DECLARE LOCAL gearModule TO gear:GETMODULE("ModuleWheelBase").
        IF (gearModule:HASEVENT("Friction Control: override")) {
            gearModule:DOEVENT("Friction Control: override").
        }
    }
    RETURN startingFriction.
}

DECLARE FUNCTION setWheelFrictionManual {
    DECLARE PARAMETER frictionValues TO LEXICON().

    DECLARE LOCAL gearList TO SHIP:PARTSDUBBEDPATTERN("GearBay").
    FOR gear IN gearList {
        DECLARE LOCAL gearModule TO gear:GETMODULE("ModuleWheelBase").
        IF (gearModule:HASEVENT("Friction Control: auto")) {
            gearModule:DOEVENT("Friction Control: auto").
        }
        IF (gearModule:HASFIELD("Friction Control") AND frictionValues:HASKEY(gear)) {
            gearModule:SETFIELD("Friction Control", frictionValues[gear]).
        }
    }
}