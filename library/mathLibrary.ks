@LAZYGLOBAL OFF.

// headingOfVector returns the heading of the vector (number ranging 0 to 360)
DECLARE FUNCTION headingOfVector {
    DECLARE PARAMETER vector.

    DECLARE LOCAL east TO VCRS(SHIP:UP:VECTOR, SHIP:NORTH:VECTOR).
    DECLARE LOCAL trigX TO VDOT(SHIP:NORTH:VECTOR, vector).
    DECLARE LOCAL trigY TO VDOT(east, vector).

    DECLARE LOCAL result TO ARCTAN2(trigY, trigX).

    IF (result < 0) {
        RETURN 360 + result.
    } ELSE {
        RETURN result.
    }
}

// pitchOfVector returns the pitch of the vector (number ranging -90 to  90)
DECLARE FUNCTION pitchOfVector {
    DECLARE PARAMETER vector.

    RETURN 90 - VANG(SHIP:UP:VECTOR, vector).
}