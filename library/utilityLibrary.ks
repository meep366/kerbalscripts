@LAZYGLOBAL OFF.

DECLARE FUNCTION showTerminal {
    CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").
}

DECLARE FUNCTION headsUpText {
    DECLARE PARAMETER text.
    HUDTEXT(text, 8, 2, 35, YELLOW, FALSE).
}

DECLARE FUNCTION copyAndRunFiles {
    IF (HOMECONNECTION:ISCONNECTED AND PATH() = "1:/") {
        DELETEPATH("1:/library/").
        DELETEPATH("1:/view/").
        DELETEPATH("1:/rsvp/").
        DELETEPATH("1:/precisionLanding/").
        DELETEPATH("1:/img/").
        DELETEPATH("1:/astar-rover/").

        COPYPATH("0:/library/", "1:/library/").
        COPYPATH("0:/view/", "1:/view/").
        COPYPATH("0:/rsvp/", "1:/rsvp/").
        COPYPATH("0:/precisionLanding/", "1:/precisionLanding/").
        COPYPATH("0:/img/", "1:/img/").
        COPYPATH("0:/astar-rover/", "1:/astar-rover/").
    }
    CD("/library/").
    runFiles().
    CD("/view/").
    runFiles().
    CD("/astar-rover/").
    runFiles().
    CD("/").
    RUNONCEPATH("rsvp/main.ks").
    RUNONCEPATH("precisionLanding/Land_at.ks").
    RUNONCEPATH("precisionLanding/Landing_vac.ks").
}

DECLARE LOCAL FUNCTION runFiles {
    DECLARE LOCAL listOfFiles TO LIST().
    LIST FILES IN listOfFiles.
    FOR fileItem IN listOfFiles {
        IF (fileItem:ISFILE) {
            RUNONCEPATH(fileItem).
        }
    }
}

//Convert a time number in to Xd Yh Zm αs
DECLARE FUNCTION formatTime {
    DECLARE PARAMETER time.

    DECLARE LOCAL timeRemainder TO time.
    DECLARE LOCAL days TO FLOOR(time / 21600).
    SET timeRemainder TO timeRemainder - days * 21600.

    DECLARE LOCAL hours TO FLOOR(timeRemainder / 3600).
    SET timeRemainder TO timeRemainder - hours * 3600.

    DECLARE LOCAL minutes TO FLOOR(timeRemainder / 60).
    SET timeRemainder TO timeRemainder - minutes * 60.

    RETURN days + "d " + hours + "h " + minutes + "m " + ROUND(timeRemainder) + "s".
}

DECLARE FUNCTION metersPerSecondToKPH {
    DECLARE PARAMETER metersPerSecond.

    RETURN metersPerSecond * 3.6.
}