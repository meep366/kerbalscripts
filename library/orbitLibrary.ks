@LAZYGLOBAL OFF.

//Executes a given maneuver-node
DECLARE FUNCTION executeNode {
    //The maneuver node to execute
    DECLARE PARAMETER maneuverNode, updateGui.

    DECLARE LOCAL deltaVOffset TO 1.
    DECLARE LOCAL burnTimeMargin TO 10.
    DECLARE LOCAL burnAngleMargin TO 2.5.

    LOCAL nodeBackup TO LIST().
    FOR node IN allNodes {
        IF (node <> maneuverNode ) {
            nodeBackup:ADD(node).
        }
        REMOVE node.
    }
    ADD maneuverNode.

    DECLARE LOCAL LOCK acceleration TO AVAILABLETHRUST / MASS.
    DECLARE LOCAL LOCK safeAcceleration TO MAX(acceleration, 0.001).
    SAS OFF.

    DECLARE LOCAL burnMean TO calculateBurnMean(maneuverNode:DELTAV:MAG)[0].
    DECLARE LOCAL scale TO (maneuverNode:DELTAV:MAG + deltaVOffset) / maneuverNode:DELTAV:MAG.

    SET maneuverNode:PROGRADE TO maneuverNode:PROGRADE * scale.
    SET maneuverNode:RADIALOUT TO maneuverNode:RADIALOUT * scale.
    SET maneuverNode:NORMAL TO maneuverNode:NORMAL * scale.

    DECLARE LOCAL status TO "Steering to maneuver node".

    LOCK STEERING TO maneuverNode:DELTAV.

    UNTIL (VANG(FACING:VECTOR, maneuverNode:DELTAV) < burnAngleMargin) OR (maneuverNode:ETA < burnMean + burnTimeMargin) {
        printBurnDetails(status, maneuverNode:ETA, burnMean * 2, VANG(FACING:VECTOR, maneuverNode:DELTAV), updateGui).
    }

    SET status TO "Warping to maneuver node".
    printBurnDetails(status, maneuverNode:ETA, burnMean * 2, VANG(FACING:VECTOR, maneuverNode:DELTAV), updateGui).

    DECLARE LOCAL timeToReorient TO 120.

    //Warp to node with TIME for turning and some tolerance
    IF (TIME:SECONDS < maneuverNode:TIME - burnMean - burnTimeMargin - timeToReorient) {
        KUNIVERSE:TIMEWARP:WARPTO(maneuverNode:ETA - burnMean - burnTimeMargin - timeToReorient + TIME:SECONDS).
        WAIT 1.
        WAIT UNTIL (KUNIVERSE:TIMEWARP:WARP = 0).
        WAIT 1.
        WAIT UNTIL (KUNIVERSE:TIMEWARP:ISSETTLED).
        WAIT 1.
    }

    SET status TO "Steering to maneuver node".
    UNTIL (VANG(FACING:VECTOR, maneuverNode:DELTAV) < burnAngleMargin) OR (maneuverNode:ETA < burnMean + burnTimeMargin) {
        printBurnDetails(status, maneuverNode:ETA, burnMean * 2, VANG(FACING:VECTOR, maneuverNode:DELTAV), updateGui).
    }

    SET status TO "Warping to maneuver node".
    printBurnDetails(status, maneuverNode:ETA, burnMean * 2, VANG(FACING:VECTOR, maneuverNode:DELTAV), updateGui).

    KUNIVERSE:TIMEWARP:WARPTO(maneuverNode:ETA - burnMean - burnTimeMargin + TIME:SECONDS).

    SET status TO "Waiting to burn".

    UNTIL (maneuverNode:ETA < burnMean) {
        printBurnDetails(status, maneuverNode:ETA, burnMean * 2, VANG(FACING:VECTOR, maneuverNode:DELTAV), updateGui).
    }

    SET status TO "Executing burn".

    WHEN (MAXTHRUST = 0) THEN {
        headsUpText("Staging").
        STAGE.
        PRESERVE.
    }

    //Do the burn, THROTTLE down when below 30 m/s.
    LOCK STEERING TO maneuverNode:deltav.
    LOCK THROTTLE TO ((maneuverNode:DELTAV:MAG / 20) ^ 2 + 0.01) * 20 / safeAcceleration.

    UNTIL (maneuverNode:DELTAV:MAG < deltaVOffset) {
        printBurnDetails(status, maneuverNode:ETA, burnMean, VANG(FACING:VECTOR, maneuverNode:DELTAV), updateGui).
    }

    UNLOCK STEERING.
    LOCK THROTTLE TO 0.
    REMOVE maneuverNode.

    FOR node IN nodeBackup {
        ADD node.
    }
    
    LOCK THROTTLE TO 0.
    SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
}

DECLARE FUNCTION getSuccessfulRSVP {
    DECLARE PARAMETER destination, options, updateGui.

    DECLARE LOCAL statusLexicon TO LEXICON("Status", "Running RSVP").
    updateGui:CALL(statusLexicon).
    DECLARE LOCAL result TO RSVP:GOTO(destination, options).

    DECLARE LOCAL isDone TO FALSE.
    UNTIL (isDone) {
        IF (result["success"]) {
            SET isDone TO TRUE.
            RETURN result.
        } ELSE {
            PRINT result.
            PRINT "Unsuccesful RSVP, retrying".
            DECLARE LOCAL statusLexicon TO LEXICON("Status", "RSVP Failed, retrying").
            updateGui:CALL(statusLexicon).
            WAIT 5.
            FOR node IN ALLNODES {
                REMOVE node.
            }
            SET result TO RSVP:GOTO(destination, options).
        }
    }
}

DECLARE FUNCTION setFineApsis {
    DECLARE PARAMETER apsisAltitude, desiredAltitude, apsisEta, updateGui.

    DECLARE LOCAL engineList TO LIST().
    LIST ENGINES IN engineList.
    DECLARE LOCAL firstEngine TO TRUE.

    FOR eng IN engineList {
        IF (firstEngine) {
            SET eng:THRUSTLIMIT TO .5.
            SET firstEngine TO FALSE.
        } ELSE {
            SET eng:THRUSTLIMIT TO 0.
        }
    }

    DECLARE LOCAL apsisDeltaV TO calculateSemiMajorAxisDeltaV(apsisAltitude, desiredAltitude).
    DECLARE LOCAL apsisNode TO NODE(TIME:SECONDS + apsisEta, 0, 0, apsisDeltaV).
    ADD apsisNode.
    WAIT 10.
    executeNode(apsisNode, updateGui).
}

//Calculates when in time the mean of a given burn is done
//For example a 10s burn with constant acceleration will have a burn mean of 5s
//With constantly rising acceleration it would be closer to 7s
//If the burn means lines up with the maneuver node you will hit the orbital change pretty much perfectly every time #itjustworks
//(Full throttle is assumed for the burn duration)
DECLARE LOCAL FUNCTION calculateBurnMean {
    //burnDeltaV: DeltaV needed for the burn
    //pressure: At what pressure level the burn is performed (0 is vacuum, 1 is the Kerbal Space Center)
    //stageTime: Time alotted to each staging process
    DECLARE PARAMETER burnDeltaV, pressure TO 0, stageTime TO 0.

    //Sum of the full deltaV of all stages accounted for in the burn
    DECLARE LOCAL wholeDV TO 0.

    //Time of the whole burn
    DECLARE LOCAL burnTime TO 0.

    //Mean time of the whole burn
    DECLARE LOCAL burnMean TO 0.

    //Starts at the current stage and will contain the last stage participating in the burn at the end.
    DECLARE LOCAL finalStage TO stage:number.

    //deltaV in the stage we are currently calculating
    DECLARE LOCAL stageDeltaV TO 0.

    //Updates the deltaV dictionary for the whole ship
    DECLARE LOCAL deltaVLexicon TO parseDeltaV(pressure).
    DECLARE LOCAL deltaVDict TO deltaVLexicon["deltaVDict"].
    DECLARE LOCAL rawDict TO deltaVLexicon["rawDict"].

    //Now we calculate the deltaV and burn time for stage after stage, until we have enough for the burn
    UNTIL (wholeDV >= burnDeltaV) {
        IF (deltaVDict:HASKEY(finalStage)) {
            SET stageDeltaV TO deltaVDict[finalStage][0].
        } ELSE {
            SET stageDeltaV TO 0.
        }
        //If the current stage has more than enough deltaV, you only calculate for the part you need
        IF (stageDeltaV >= burnDeltaV - wholeDV) {
            SET stageDeltaV TO burnDeltaV - wholeDV.
        }
        //Add the burn time and burn mean time in the right way
        IF (stageDeltaV > 0) {
            SET wholeDV TO wholeDV + stageDeltaV.

            DECLARE LOCAL F TO rawDict[finalStage][3].
            DECLARE LOCAL m_d TO rawDict[finalStage][4].
            DECLARE LOCAL m_0 TO deltaVDict[finalStage][3].

            //I used the rocket equation, Wolfram Alpha, Maple and some evil Integrals to get to these formulas.
            //Don't mess with them.

            //Stage burn time
            DECLARE LOCAL t_1 TO - (CONSTANT:E ^ (LN(m_0) - (stageDeltaV * m_d) / F) - m_0) / m_d.

            //Stage mean burn time
            DECLARE LOCAL t_m TO (m_0 * LN((m_d * t_1 - m_0) / -m_0) + m_d * t_1) / (m_d * LN((m_0 - m_d * t_1) / m_0)).

            //Burn mean actually holds the sum of the (stage mean time) * (stage dV)
            SET burnMean TO burnMean + (burnTime + t_m) * stageDeltaV.
            //This makes it easier to get the correct mean afterwards
            SET burnTime TO burnTime + stageTime + t_1.
        }
        //If you get to the root stage of your ship you don't have the juice to do the burn and won't go to space today
        // (or at least not do the full burn)
        SET finalStage TO finalStage - 1.
        IF (finalStage < 0) {
            BREAK.
        }
    }
    IF (wholeDV > 0) {
        SET burnMean TO burnMean / wholeDV.
        RETURN list(burnMean, burnTime).
    } ELSE {
        RETURN list(0, 0).
    }
}

//Parses the whole craft for engines, fuel, mass, etc. and calculates deltaV per stage
//Does not work if you mix different fuel types being burned in one stage
DECLARE LOCAL FUNCTION parseDeltaV {
    // pressure used for the ISP, thrust, etc. Default is vacuum(0).
    DECLARE PARAMETER pressure TO 0.

    DECLARE LOCAL stagePartDict TO LEXICON().      // lists parts per stage
    DECLARE LOCAL stageEngineDict TO LEXICON().    // lists engines per stage
    DECLARE LOCAL rawDict TO LEXICON().            // lists stages with mass[0], drymass[1], ISP[2], F[3], M°[4] and engines[5] (list)
    DECLARE LOCAL deltaVDict TO LEXICON().         // lists stages with deltaV[0], TWR at start[1], TWR at the end[2] and whole mass[3]

    DECLARE LOCAL highStage TO 0.                   // the highest stage in the ship

    DECLARE LOCAL shipParts TO LIST().
    LIST PARTS IN shipParts.

    //Builds stagePartDict, stageEngineDict and rawDict
    FOR part IN shipParts {
        //Ignores Separatrons
        //TODO Ignore or manually work with solid rocket boosters
        IF (part:TYPENAME = "ENGINE" AND part:NAME <> "sepMotor1") {
            LOCAL stageNum TO part:STAGE.
            IF (NOT stageEngineDict:HASKEY(stageNum)) {
                SET stageEngineDict[stageNum] TO LIST().
                SET highStage to MAX(highStage, stageNum).
            }
            stageEngineDict[stageNum]:ADD(part).
        } ELSE {
            LOCAL stageNum TO part:STAGE + 1.
            IF (NOT stagePartDict:HASKEY(stageNum)) {
                SET stagePartDict[stageNum] TO LIST().
                SET rawDict[stageNum] TO LIST(0, 0, 0, 0, 0, LIST()).
                SET highStage to max(highStage, stageNum).
            }
            stagePartDict[stageNum]:ADD(part).
            SET rawDict[stageNum][0] TO rawDict[stageNum][0] + part:MASS.
            SET rawDict[stageNum][1] TO rawDict[stageNum][1] + part:DRYMASS.
        }
    }

    //To get the engine thrust kOS has to shortly activate every engine
    //Stops all engines while measuring deltaV
    LOCK THROTTLE TO 0.

    FOR stageNum IN stageEngineDict:KEYS {
        //This part uses how the stage number for each part is listed in KSP to get all active engines (and their thrust/isp/etc) for each stage
        //This also accounts for asparagus staging
        FOR eng IN stageEngineDict[stageNum] {
            //The engine stage always is the last stage the engine is active in
            DECLARE LOCAL maxStage TO eng:STAGE.

            //The parent stage is the first stage the engine is active in (somehow)
            DECLARE LOCAL minStage TO eng:PARENT:STAGE + 1.

            //Adds up the mass
            SET rawDict[minStage][0] TO rawDict[minStage][0] + eng:MASS.

            //And the dry mass
            SET rawDict[minStage][1] TO rawDict[minStage][1] + eng:DRYMASS.

            FOR relevStage IN RANGE(minStage, maxStage + 1) {
                IF (NOT rawDict:HASKEY(relevStage)) {
                    SET stagePartDict[relevStage] TO LIST().
                    SET rawDict[relevStage] TO LIST(0, 0, 0, 0, 0, LIST()).
                    SET highStage to MAX(highStage, relevStage).
                }
                //Sums up the thrust per ISP
                SET rawDict[relevStage][2] TO rawDict[relevStage][2] + eng:POSSIBLETHRUSTAT(pressure) / eng:ISPAT(pressure).

                //Sums up the thrust
                SET rawDict[relevStage][3] TO rawDict[relevStage][3] + eng:POSSIBLETHRUSTAT(pressure).

                //Sums up the massflow
                SET rawDict[relevStage][4] TO rawDict[relevStage][4] + eng:POSSIBLETHRUSTAT(pressure) / (eng:ISPAT(pressure) * 9.81).

                //Adds all relevant engines
                rawDict[relevStage][5]:ADD(eng).
            }
        }
    }

    // Gets the mean isp per stage
    FOR stageNum IN rawDict:KEYS {
        IF (rawDict[stageNum][2] <> 0) {
            //By dividing combined thrust by the thrust per ISP you get the effective ISP
            SET rawDict[stageNum][2] TO rawDict[stageNum][3] / rawDict[stageNum][2].
        }
    }

    LOCAL accelerationMass TO 0.
    //Calculates the actual deltaV and TWR per Stage
    FOR stageNum IN RANGE(0, highStage + 1) {
        IF (NOT rawDict:HASKEY(stageNum)) {
            rawDict:ADD(stageNum, LIST(0, 0, 0, 0, 0, LIST())).
        }
        SET deltaVDict[stageNum] TO LIST(0, 0, 0, 0).
        SET deltaVDict[stageNum][0] TO 9.81 * rawDict[stageNum][2] * LN((accelerationMass + rawDict[stageNum][0]) / (accelerationMass + rawDict[stageNum][1])).
        SET deltaVDict[stageNum][1] TO rawDict[stageNum][3] / (accelerationMass + rawDict[stageNum][0]) / 9.81.
        SET deltaVDict[stageNum][2] TO rawDict[stageNum][3] / (accelerationMass + rawDict[stageNum][1]) / 9.81.
        SET accelerationMass TO accelerationMass + rawDict[stageNum][0].
        SET deltaVDict[stageNum][3] TO accelerationMass.
    }

    DECLARE LOCAL listLexicon TO LEXICON().
    listLexicon:ADD("deltaVDict", deltaVDict).
    listLexicon:ADD("rawDict", rawDict).
    RETURN listLexicon.
}
