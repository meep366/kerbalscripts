@LAZYGLOBAL OFF.

DECLARE FUNCTION playTerrainWarning {
    DECLARE LOCAL song TO LIST().
    DECLARE LOCAL v0 TO GETVOICE(0).

    song:ADD(SLIDENOTE("c5", "f5", .5)).
    song:ADD(SLIDENOTE("c5", "f5", .5)).
    song:ADD(SLIDENOTE("c5", "f5", .5)).
    song:ADD(SLIDENOTE("c5", "f5", .5)).

    SET v0 TO GETVOICE(0).
    SET v0:ATTACK TO 0.0333.
    SET v0:DECAY TO 0.02.
    SET v0:SUSTAIN TO 1.0.
    SET v0:RELEASE TO 0.1.
    SET v0:TEMPO TO .5.
    SET v0:WAVE TO "square".

    v0:PLAY(song).
}

DECLARE FUNCTION playStallWarning {
    DECLARE LOCAL song TO LIST().
    DECLARE LOCAL v0 TO GETVOICE(0).

    song:ADD(NOTE("b5", .15)).
    song:ADD(NOTE("g5", .15)).

    song:ADD(NOTE("b5", .15)).
    song:ADD(NOTE("g5", .15)).

    song:ADD(NOTE("b5", .15)).
    song:ADD(NOTE("g5", .15)).

    SET v0 TO GETVOICE(0).
    SET v0:ATTACK TO 0.0333.
    SET v0:DECAY TO 0.02.
    SET v0:SUSTAIN TO 1.0.
    SET v0:RELEASE TO 0.1.
    SET v0:TEMPO TO 1.
    SET v0:WAVE TO "square".

    v0:PLAY(song).
}

DECLARE FUNCTION odeToJoy {
    SET song TO list().

    song:add(note(0, 1)).
    song:add(note("e4", .25)).
    song:add(note("e4", .25)).
    song:add(note("f4", .25)).
    song:add(note("g4", .25)).

    song:add(note("g4", .25)).
    song:add(note("f4", .25)).
    song:add(note("e4", .25)).
    song:add(note("d4", .25)).

    song:add(note("c4", .25)).
    song:add(note("c4", .25)).
    song:add(note("d4", .25)).
    song:add(note("e4", .25)).

    song:add(note("e4", .25)).
    song:add(note("d4", .125)).
    song:add(note("d4", .5)).

    song:add(note("e4", .25)).
    song:add(note("e4", .25)).
    song:add(note("f4", .25)).
    song:add(note("g4", .25)).

    song:add(note("g4", .25)).
    song:add(note("f4", .25)).
    song:add(note("e4", .25)).
    song:add(note("d4", .25)).

    song:add(note("c4", .25)).
    song:add(note("c4", .25)).
    song:add(note("d4", .25)).
    song:add(note("e4", .25)).

    song:add(note("d4", .25)).
    song:add(note("c4", .125)).
    song:add(note("c4", .5)).

    song:add(note("d4", .25)).
    song:add(note("d4", .25)).
    song:add(note("e4", .25)).
    song:add(note("c4", .25)).

    song:add(note("d4", .25)).
    song:add(note("e4", .125)).
    song:add(note("f4", .125)).
    song:add(note("e4", .25)).
    song:add(note("c4", .25)).

    song:add(note("d4", .25)).
    song:add(note("e4", .125)).
    song:add(note("f4", .125)).
    song:add(note("e4", .25)).
    song:add(note("d4", .25)).

    song:add(note("c4", .25)).
    song:add(note("d4", .25)).
    song:add(note("g3", .25)).
    song:add(note("e4", .25)).

    song:add(note("e4", .25)).
    song:add(note("e4", .25)).
    song:add(note("f4", .25)).
    song:add(note("g4", .25)).

    song:add(note("g4", .25)).
    song:add(note("f4", .25)).
    song:add(note("e4", .25)).
    song:add(note("d4", .25)).

    song:add(note("c4", .25)).
    song:add(note("c4", .25)).
    song:add(note("d4", .25)).
    song:add(note("e4", .25)).

    song:add(note("d4", .375)).
    song:add(note("c4", .125)).
    song:add(note("c4", .5)).

    set v0 to getvoice(0).

    set v0:attack to 0.0333. // take 1/30 th of a second to max volume.
    set v0:decay to 0.02.  // take 1/50th second to drop back down to sustain.
    set v0:sustain to 0.80. // sustain at 80% of max vol.
    set v0:release to 0.2. // takes 1/20th of a second to fall to zero volume at the end.
    set v0:tempo to 1.5.
    set v0:wave to "square".

    v0:play(song).
}

DECLARE FUNCTION chopsticks {
    SET song TO list().
    song:add(note(0, 1)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).

    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).

    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("b4", .25)).

    song:add(note("c5", .5)).
    song:add(note("c5", .25)).
    song:add(note("c5", .25)).
    song:add(note("b4", .25)).
    song:add(note("a4", .25)).

    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).

    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).
    song:add(note("g4", .25)).

    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("b4", .25)).
    song:add(note("a4", .25)).
    song:add(note("b4", .25)).

    song:add(note("c5", .5)).
    song:add(note("c5", .25)).
    song:add(note("c5", .5)).

    set v0 to getvoice(0).

    set v0:attack to 0.0333. // take 1/30 th of a second to max volume.
    set v0:decay to 0.02.  // take 1/50th second to drop back down to sustain.
    set v0:sustain to 0.80. // sustain at 80% of max vol.
    set v0:release to 0.2. // takes 1/20th of a second to fall to zero volume at the end.
    set v0:wave to "square".

    SET songTwo TO list().

    songTwo:add(note(0, 1)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).

    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).

    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).

    songTwo:add(note("c4", .5)).
    songTwo:add(note("c4", .25)).
    songTwo:add(note("c4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("e4", .25)).

    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).
    songTwo:add(note("f4", .25)).

    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("e4", .25)).

    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("d4", .25)).
    songTwo:add(note("e4", .25)).
    songTwo:add(note("d4", .25)).

    songTwo:add(note("c4", .5)).
    songTwo:add(note("c4", .25)).
    songTwo:add(note("c4", .5)).

    set v1 to getvoice(1).

    set v1:attack to 0.0333. // take 1/30 th of a second to max volume.
    set v1:decay to 0.02.  // take 1/50th second to drop back down to sustain.
    set v1:sustain to 0.80. // sustain at 80% of max vol.
    set v1:release to 0.2. // takes 1/20th of a second to fall to zero volume at the end.
    set v1:wave to "square".

    v0:play(song).
    v1:play(songTwo).
}