@LAZYGLOBAL OFF.

DECLARE FUNCTION zeroTargetApproach {
    DECLARE PARAMETER targetVessel, updateGui, closestApproachCuttoff TO 200.

    //Run refineTransferToBody until closest approaach to target is <100m
    DECLARE LOCAL closestApproachList TO list().
    DECLARE LOCAL finalApproach TO FALSE.
    UNTIL (finalApproach) {
        SET closestApproachList TO getClosestApproach(targetVessel).
        IF (closestApproachList[0] < closestApproachCuttoff) {
            SET finalApproach TO TRUE.
            BREAK.
        }
        refineTransferToBody(targetVessel, targetVessel:ORBIT:PERIAPSIS, FALSE, updateGui).

        //If we don't generate a node, we can't improve on current path, so we finish
        IF (HASNODE) {
            DECLARE LOCAL zeroNode TO NEXTNODE.
            executeNode(zeroNode, updateGui).
        } ELSE {
            SET finalApproach TO TRUE.
        }
    }
}

DECLARE FUNCTION refineTransferToBody {
    DECLARE PARAMETER targetDestination, targetAltitude, optimizingPeriapsis, updateGui.
    CLEARSCREEN.

    //Setting up current orbit variables
    DECLARE LOCAL destinationBody TO targetDestination.
    IF (NOT optimizingPeriapsis) {
        SET destinationBody TO targetDestination:BODY.
    }
    DECLARE LOCAL timeToPeriapsis TO 720.
    DECLARE LOCAL currentOrbit TO 0.
    DECLARE LOCAL currentInclination TO 0.
    DECLARE LOCAL currentPeriapsis TO 0.
    DECLARE LOCAL closestApproachList TO list().
    DECLARE LOCAL relativeSpeed TO 0.
    DECLARE LOCAL targetError TO .005.

    //Setting up targets
    DECLARE LOCAL targetInclination TO 0.
    DECLARE LOCAL targetPeriapsis TO targetAltitude.

    //Setting up node variables
    DECLARE LOCAL radial TO 0.
    DECLARE LOCAL normal TO 0.
    DECLARE LOCAL prograde TO 0.
    DECLARE LOCAL waitTimeToBurn TO timeToPeriapsis * .5.
    DECLARE LOCAL optimalNode TO NODE(TIME + waitTimeToBurn, radial, normal, prograde).
    ADD optimalNode.

    //Setting up loop variables
    DECLARE LOCAL currentError TO 0.
    DECLARE LOCAL currentPass TO 0.
    DECLARE LOCAL passes TO 5.
    DECLARE LOCAL isFindingBody TO FALSE.
    DECLARE LOCAL deltaDirection TO 1.
    DECLARE LOCAL radialDelta TO .5.
    DECLARE LOCAL normalDelta TO .5.
    DECLARE LOCAL progradeDelta TO .5.
    DECLARE LOCAL timeDelta TO 3600.
    DECLARE LOCAL flippedDelta TO FALSE.
    DECLARE LOCAL previousError TO 9999.
    DECLARE LOCAL variableBestError TO previousError.
    DECLARE LOCAL variableBestValue TO 0.
    DECLARE LOCAL isOptimized TO FALSE.
    DECLARE LOCAL optimizedCount TO 0.

    UNTIL (isOptimized) {
        //Error will either be distance to periapsis/inclination or distance to target
        SET currentOrbit TO ORBITAT(SHIP, TIME:SECONDS + waitTimeToBurn).
        IF (currentOrbit:HASNEXTPATCH AND currentOrbit:NEXTPATCH:BODY = destinationBody) {
            SET timeToPeriapsis TO currentOrbit:NEXTPATCH:ETA:PERIAPSIS.
            SET currentOrbit TO ORBITAT(SHIP, TIME:SECONDS + timeToPeriapsis).
            SET currentInclination TO currentOrbit:INCLINATION.
            SET currentPeriapsis TO currentOrbit:PERIAPSIS.

            IF (optimizingPeriapsis) {
                SET currentError TO calculatePeriapsisError(currentInclination, currentPeriapsis,
                        targetInclination, targetPeriapsis, destinationBody).
            } ELSE {
                SET closestApproachList TO getClosestApproach(targetDestination, currentOrbit).
                SET relativeSpeed TO getRelativeSpeedAtTime(targetDestination, closestApproachList[1]).
                SET currentError TO calculateInterceptError(targetDestination, closestApproachList, currentInclination, currentPeriapsis).
            }
            IF (isFindingBody) {
                SET previousError TO currentError + .5.
                SET variableBestError TO previousError.
                IF (optimizedCount = 0) {
                    SET variableBestValue TO prograde.
                } ELSE IF (optimizedCount = 1) {
                    SET variableBestValue TO normal.
                } ELSE IF (optimizedCount = 2) {
                    SET variableBestValue TO radial.
                } ELSE IF (optimizedCount = 3) {
                    SET variableBestValue TO waitTimeToBurn.
                }
                SET isFindingBody TO FALSE.
            }
        } ELSE IF (currentOrbit:BODY = destinationBody) {
            SET timeToPeriapsis TO currentOrbit:ETA:PERIAPSIS.
            SET currentInclination TO currentOrbit:INCLINATION.
            SET currentPeriapsis TO currentOrbit:PERIAPSIS.

            IF (NOT optimizingPeriapsis) {
                SET closestApproachList TO getClosestApproach(targetDestination, currentOrbit).
                SET relativeSpeed TO getRelativeSpeedAtTime(targetDestination, closestApproachList[1]).
                SET currentError TO calculateInterceptError(targetDestination, closestApproachList, currentInclination, currentPeriapsis).
            } ELSE {
                SET currentError TO calculatePeriapsisError(currentInclination, currentPeriapsis,
                        targetInclination, targetPeriapsis, destinationBody).
            }
        } ELSE {
            SET closestApproachList TO getClosestApproach(targetDestination, currentOrbit).
            SET relativeSpeed TO getRelativeSpeedAtTime(targetDestination, closestApproachList[1]).
            SET currentError TO calculateInterceptError(targetDestination, closestApproachList, currentInclination, currentPeriapsis).
            SET isFindingBody TO TRUE.
        }

        //Loop end condition when we're close enough to target
        IF (currentError < targetError) {
            SET isOptimized TO TRUE.
            BREAK.
        }

        //Keep track of the minimum error on the current variable
        IF (currentError < variableBestError) {
            SET variableBestError TO currentError.
            IF (optimizedCount = 0) {
                SET variableBestValue TO prograde.
            } ELSE IF (optimizedCount = 1) {
                SET variableBestValue TO normal.
            } ELSE IF (optimizedCount = 2) {
                SET variableBestValue TO radial.
            } ELSE IF (optimizedCount = 3) {
                SET variableBestValue TO waitTimeToBurn.
            }
        }

        //If we're moving in the wrong direction, try the other direction. If both directions are wrong, try the next variable
        IF (currentError >= previousError AND NOT flippedDelta) {
            SET deltaDirection TO -1 * deltaDirection.
            SET flippedDelta TO TRUE.
        } ELSE IF (currentError >= previousError AND flippedDelta) {
            // Set the variable to the best value that we found during the local search
            IF (optimizedCount = 0) {
                SET prograde TO variableBestValue.
                SET variableBestValue TO normal.
            } ELSE IF (optimizedCount = 1) {
                SET normal TO variableBestValue.
                SET variableBestValue TO radial.
            } ELSE IF (optimizedCount = 2) {
                SET radial TO variableBestValue.
                SET variableBestValue TO waitTimeToBurn.
            } ELSE IF (optimizedCount = 3) {
                SET waitTimeToBurn TO variableBestValue.
                SET variableBestValue TO prograde.
            }
            SET variableBestError TO currentError.
            SET optimizedCount TO optimizedCount + 1.
            SET flippedDelta TO FALSE.
        }

        CLEARSCREEN.
        //Different variables to attempt to reach the optimal soluition, all parts of the manuever node
        IF (optimizedCount = 0) {
            PRINT "Adjusting Prograde by " + progradeDelta + "m/s".
            SET prograde TO prograde + progradeDelta * deltaDirection.
        } ELSE IF (optimizedCount = 1) {
            PRINT "Adjusting Normal by " + normalDelta + "m/s".
            SET normal TO normal + normalDelta * deltaDirection.
        } ELSE IF (optimizedCount = 2) {
            PRINT "Adjusting Radial by " + radialDelta + "m/s".
            SET radial TO radial + radialDelta * deltaDirection.
        } ELSE IF (optimizedCount = 3) {
            PRINT "Adjusting Time by " + timeDelta + "s".
            IF (waitTimeToBurn + timeDelta * deltaDirection < 180) {
                SET optimizedCount TO optimizedCount + 1.
                SET variableBestValue TO prograde.
            } ELSE IF (waitTimeToBurn + timeDelta * deltaDirection > timeToPeriapsis * .8) {
                SET optimizedCount TO optimizedCount + 1.
                SET variableBestValue TO prograde.
            } ELSE {
                SET waitTimeToBurn TO waitTimeToBurn + timeDelta * deltaDirection.
            }
        } ELSE IF (optimizedCount > 3) {
            //Once we've optimized every variable, we run the loop again, either for a certain number of passes, or until the error is minimized
            IF (currentPass < passes) {
                SET currentPass TO currentPass + 1.

                SET radialDelta TO radialDelta / 2.
                SET normalDelta TO normalDelta / 2.
                SET progradeDelta TO progradeDelta / 2.
                SET timeDelta TO timeDelta / 2.

                SET optimizedCount TO 0.
                SET flippedDelta TO FALSE.
            }  ELSE {
                SET isOptimized TO TRUE.
            }
        }

        REMOVE optimalNode.
        SET optimalNode TO NODE(TIME + waitTimeToBurn, radial, normal, prograde).
        ADD optimalNode.

        SET previousError TO currentError.

        IF (NOT optimizingPeriapsis) {
            PRINT "Current Distance to target: " + ROUND(closestApproachList[0], 3).
            PRINT "Current Relative Speed: " + ROUND(relativeSpeed, 3).
        }
        PRINT "Current Time to Periapsis: " + formatTime(timeToPeriapsis).
        PRINT "Current Inclination: " + ROUND(currentInclination, 3).
        PRINT "Current Periapsis: " + ROUND(currentPeriapsis, 3).
        PRINT "Current Prograde: " + ROUND(prograde, 3).
        PRINT "Current Radial: " + ROUND(radial, 3).
        PRINT "Current Normal: " + ROUND(normal, 3).
        PRINT "Current Error: " + ROUND(currentError, 3).

        DECLARE LOCAL statusLexicon TO LEXICON(
                        "Status", "Refining Transfer",
                        "Current Error", ROUND(currentError, 3),
                        "Target Error", targetError).
        updateGui:CALL(statusLexicon).
    }

    CLEARSCREEN.
    PRINT "Time to Periapsis: " + formatTime(timeToPeriapsis).
    PRINT "Final Inclination: " + ROUND(currentInclination, 3).
    PRINT "Final Periapsis: " + ROUND(currentPeriapsis, 3).
    IF (NOT optimizingPeriapsis) {
        PRINT "Final Distance to target: " + ROUND(closestApproachList[0], 3).
        PRINT "Final Time to target: " + formatTime(closestApproachList[1] - TIME:SECONDS).
        PRINT "Final Relative Speed: " + ROUND(relativeSpeed, 3).
    }
    PRINT "Final Error: " + ROUND(currentError, 3).

    //If there is no deltaV required to hit the target, remove the node
    IF (optimalNode:DELTAV:MAG < .05) {
        REMOVE optimalNode.
    }
}

DECLARE LOCAL FUNCTION calculatePeriapsisError {
    DECLARE PARAMETER currentInclination, currentPeriapsis.
    DECLARE PARAMETER targetInclination, targetPeriapsis.
    DECLARE PARAMETER destination.

    //Offsets for how big the error should be
    DECLARE LOCAL inclinationErrorScale TO 4.
    DECLARE LOCAL periapsisErrorScale TO 8.

    //Scale the error from 0 to 1, then by the adjustment
    DECLARE LOCAL adjustedInclincationError TO currentInclination / 180 * inclinationErrorScale.
    DECLARE LOCAL adjustedPeriapsisError TO ABS(currentPeriapsis - targetPeriapsis) / destination:SOIRADIUS * periapsisErrorScale.

    RETURN adjustedPeriapsisError + adjustedInclincationError.
}

//Calculates the error when trying to find an intercept between the ship and a target vessel
//Currently measured in distance to target and a penalty for a periapsis that is too low
DECLARE LOCAL FUNCTION calculateInterceptError {
    DECLARE PARAMETER targetVessel, closestApproachList, currentInclination, currentPeriapsis.

    DECLARE LOCAL closestApproachDistance TO closestApproachList[0].

    //Offsets for how big the error should be
    DECLARE LOCAL inclinationErrorScale TO 0.
    DECLARE LOCAL distanceErrorScale TO 10000.
    DECLARE LOCAL periapsisErrorScale TO 1500.

    DECLARE LOCAL adjustedDistanceError TO closestApproachDistance / distanceErrorScale.
    DECLARE LOCAL adjustedInclincationError TO ABS(currentInclination - targetVessel:ORBIT:INCLINATION) / 180 * inclinationErrorScale.
    DECLARE LOCAL lowPeriapsisError TO 0.
    IF (currentPeriapsis < 7000) {
        SET lowPeriapsisError TO (7000 - currentPeriapsis) / periapsisErrorScale.
    }

    RETURN adjustedDistanceError + adjustedInclincationError + lowPeriapsisError.
}
