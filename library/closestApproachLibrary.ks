@LAZYGLOBAL OFF.

DECLARE FUNCTION getClosestApproach {
    DECLARE PARAMETER targetVessel, currentOrbit TO ORBIT, targetOrbit TO targetVessel:ORBIT.
    DECLARE LOCAL closestApproachList TO getClosestApproachRecursive(currentOrbit, targetOrbit).
    DECLARE LOCAL bestApproach TO closestApproachList[0].
    FOR closestApproach IN closestApproachList {
        IF (closestApproach[0] < bestApproach[0]) {
            SET bestApproach TO closestApproach.
        }
    }
    DECLARE LOCAL currentMeanAnomaly TO currentOrbit:MEANANOMALYATEPOCH.
    DECLARE LOCAL interceptMeanAnomaly TO bestApproach[1].

    //Mean anomaly references where on the orbit we are in degrees
    //If currentMeanAnomaly > interceptMeanAnomaly, we assume a future intercept and adjust
    DECLARE LOCAL meanAnomalyDifference TO 0.
    IF (currentMeanAnomaly > interceptMeanAnomaly) {
        SET meanAnomalyDifference TO 360 - currentMeanAnomaly + interceptMeanAnomaly.
    } ELSE {
        SET meanAnomalyDifference TO interceptMeanAnomaly - currentMeanAnomaly.
    }
    DECLARE LOCAL timeToIntercept TO calculateTimeFromMeanAnomaly(meanAnomalyDifference).

    DECLARE LOCAL finalList TO list().
    finalList:ADD(bestApproach[0]).
    finalList:ADD(TIME:SECONDS + timeToIntercept).

    RETURN finalList.
}

//Calculates the time it takes to pass a given amount of mean anomaly given the orbital period
DECLARE FUNCTION calculateTimeFromMeanAnomaly {
//meanAnom: The amount of mean anomaly
//per: The orbital period, defaults to current period
    DECLARE PARAMETER meanAnom, per TO OBT:PERIOD.
    RETURN (per * meanAnom / 360).
}

//Calculates the mean anomaly of an orbit a given time from now
DECLARE FUNCTION calculateMeanAnomalyFromTime {
//Orb: the orbit
//dTime: time from now
    DECLARE PARAMETER Orb TO OBT, dTime TO 0.

    IF (OBT:ECCENTRICITY >= 1) {
        LOCAL Nu TO Orb:TRUEANOMALY.
        RETURN calculateMeanFromTrueAnomaly(Nu, Orb) + SQRT((OBT:BODY:MU) / (-OBT:SEMIMAJORAXIS ^ 3)) * (dTime).
    } ELSE {
        RETURN moduloBound(Orb:MEANANOMALYATEPOCH + 360 * (TIME:SECONDS + dTime - Orb:EPOCH) / Orb:PERIOD, 360).
    }
}

//Calculates the closest approaches between orbit 1 and orbit 2
//As this can't be analytically solved it is only an approximation and can take a lot of processing power (Uses "c_Tru_An_from_Mean()" a lot).
//I suggest raising the allowed operations per tick for kOS.
//It returns a LIST of [n]: Encounter-lists with each having:
//[0]: The distance at Encounter
//[1]: Mean Anomaly of orbit 1 at Encounter
//[2]: True Anomaly of orbit 1 at Encounter
//[3]: Mean Anomaly of orbit 2 at Encounter
//[4]: True Anomaly of orbit 2 at Encounter
//[5]: Radius in orbit 1 at Encounter from center of BODY
DECLARE LOCAL FUNCTION getClosestApproachRecursive {
    DECLARE PARAMETER orb1, orb2, anomalyOffset TO 0, Nu_min TO 0, delt_Nu TO 360, Nu_start TO orb1:TRUEANOMALY, epoch_start TO TIME:SECONDS.

    DECLARE LOCAL div TO 8.

    DECLARE LOCAL M1_0 TO moduloBound(orb1:MEANANOMALYATEPOCH + (epoch_start - orb1:EPOCH) * 360 / orb1:PERIOD, 360).
    DECLARE LOCAL M2_0 TO moduloBound(orb2:MEANANOMALYATEPOCH + anomalyOffset + (epoch_start - orb2:EPOCH) * 360 / orb2:PERIOD, 360).

    LOCAL distList TO LIST().
    FOR i IN RANGE(0, div) {
        LOCAL Nu TO Nu_min + i * delt_Nu / div.
        LOCAL Nu_1 TO moduloBound(Nu_start + Nu, 360).
        LOCAL pos_1 TO calculateOrbitalPosition(orb1, Nu_1).
        LOCAL M_1 TO calculateMeanFromTrueAnomaly(Nu_1, orb1).
        LOCAL T TO moduloBound(M_1 - M1_0, 360) * orb1:PERIOD / 360.
        LOCAL M_2 TO moduloBound(M2_0 + T * 360 / orb2:PERIOD, 360).
        LOCAL Nu_2 TO calculateTrueFromMeanAnomaly(M_2, orb2).
        LOCAL pos_2 TO calculateOrbitalPosition(orb2, Nu_2).
        distList:ADD(LIST((pos_1 - pos_2):MAG, M_2, Nu_2)).
    }
    LOCAL retList TO LIST().
    LOCAL lastDist TO distList[div - 1][0].
    LOCAL nextDist TO distList[1][0].
    FOR i IN RANGE(0, div) {
        IF (i = div - 1) {
            SET nextDist TO distList[0][0].
        } ELSE {
            SET nextDist TO distList[i + 1][0].
        }
        IF (distList[i][0] < lastDist AND distList[i][0] < nextDist) {
            LOCAL NuClose TO Nu_min + i * delt_Nu / div.
            LOCAL rClose TO calculateRadiusFromTrueAnomaly(NuClose + Nu_start, orb1).
            IF (rClose * delt_Nu * CONSTANT:PI / (180 * div) < 20) {
                LOCAL M_1 TO calculateMeanFromTrueAnomaly(NuClose + Nu_start, orb1).
                retList:ADD(LIST(distList[i][0], M_1, NuClose + Nu_start, distList[i][1], distList[i][2], rClose)).
            } ELSE {
                LOCAL subRetList TO getClosestApproachRecursive(orb1, orb2, anomalyOffset, NuClose - delt_Nu / div, 2 * delt_Nu / div, Nu_start, epoch_start).
                FOR Ret in subRetList {
                    retList:ADD(Ret).
                }
            }
        }
        SET lastDist TO distList[i][0].
    }
    RETURN retList.
}

//Clamps a value via modulo between two values
//E.g.: moduloBound(-45,360, 0) = 315 or moduloBound(270,180,-180) = -90
//Great for working with angles
DECLARE LOCAL FUNCTION moduloBound {
    DECLARE PARAMETER dividend, b_max TO 360, b_min TO 0.
    DECLARE LOCAL m TO b_max - b_min.
    RETURN MOD(MOD(dividend, m) - b_min + m, m) + b_min.
}

//Calculates the position in orbit in relation to the parent body and the solarprimevector
//It uses the solarprimevector as x-axis and north as z-axis.
//!!!Warning this is using a right-handed coordinate system!!!
DECLARE LOCAL FUNCTION calculateOrbitalPosition {
    DECLARE PARAMETER Orb TO OBT, Nu TO 0, argPe TO 0, lan TO 0, inc TO 0, ec TO False, sma TO FALSE.

    IF (Orb:TYPENAME = "Orbit") {
        IF (FALSE = Nu){
            SET Nu TO Orb:TRUEANOMALY.
        }
        IF (FALSE = argPe) {
            SET argPe TO Orb:ARGUMENTOFPERIAPSIS.
        }
        IF (FALSE = lan) {
            SET lan TO Orb:LAN.
        }
        IF (FALSE = inc) {
            SET inc TO Orb:INCLINATION.
        }
    }

    DECLARE LOCAL r TO calculateRadiusFromTrueAnomaly(Nu, Orb, ec, sma).
    RETURN V((-sin(lan) * cos(inc) * sin(Nu + argPe) + cos(lan) * cos(Nu + argPe)) * r,
            (sin(lan) * cos(Nu + argPe) + cos(lan) * cos(inc) * sin(Nu + argPe)) * r,
            sin(inc) * sin(Nu + argPe) * r).
}

//Calculates the Mean Anomaly from the True one
DECLARE FUNCTION calculateMeanFromTrueAnomaly {
    DECLARE PARAMETER Nu TO OBT:TRUEANOMALY, Orb TO OBT.

    DECLARE LOCAL ec TO Orb:ECCENTRICITY.

    IF (ec < 1) {
        LOCAL E TO 2 * ARCTAN(TAN(Nu / 2) * SQRT((1 - ec)/(1 + ec))).
        RETURN moduloBound((convertRadiansToDegrees(convertDegreesToRadians(E) - ec * SIN(E))), 360).
    } ELSE {
        LOCAL E TO convertRadiansToDegrees(ARCCOSH((ec + COS(Nu))/(1 + ec * COS(Nu)))).
        IF moduloBound(Nu, 180, -180) < 0 {
            SET E TO -E.
        }
        RETURN convertRadiansToDegrees(ec * SINH(convertDegreesToRadians(E)) - convertDegreesToRadians(E)).
    }
}

//Calculates the height over the BODY center from the true anomaly
DECLARE LOCAL FUNCTION calculateRadiusFromTrueAnomaly {
    DECLARE PARAMETER Nu, Orb TO OBT, ec TO FALSE, sma TO FALSE.

    IF (Orb:TYPENAME = "Orbit") {
        IF (FALSE = ec) {
            SET ec TO Orb:ECCENTRICITY.
        }
        IF (False = sma) {
            SET sma TO Orb:SEMIMAJORAXIS.
        }
    }

    RETURN sma * (1 - ec ^ 2) / (1 + ec * COS(Nu)).
}

//The inverse of the tanh(x)
DECLARE LOCAL FUNCTION ARCCOSH {
    DECLARE PARAMETER x.
    RETURN LN(x + SQRT(x ^ 2 - 1)).
}

//The sinh(x)
DECLARE LOCAL FUNCTION SINH {
    DECLARE PARAMETER x.
    RETURN (CONSTANT:E ^ x - CONSTANT:E ^ (-x)) / 2.
}

// Converts degrees to radians
DECLARE LOCAL FUNCTION convertRadiansToDegrees {
    DECLARE PARAMETER rad.
    Return rad * 180 / CONSTANT:PI.
}

// Converts radians to degrees
DECLARE LOCAL FUNCTION convertDegreesToRadians {
    DECLARE PARAMETER deg.
    Return deg * CONSTANT:PI / 180.
}

//Calculates the True Anomaly from the Mean one
//As this can't be analytically solved, it is only an approximation and takes a good bit of processing power to make exact.
//I suggest raising the allowed operations per tick for kOS.
DECLARE LOCAL FUNCTION calculateTrueFromMeanAnomaly {
    DECLARE PARAMETER M, Orb TO OBT.

    SET M TO moduloBound(M, 180, -180).
    LOCAL ec TO Orb:ECCENTRICITY.

    LOCAL err TO 180 * ec ^ 3.

    // Initialized with a good approximation
    LOCAL Nu_m TO M + 180 / CONSTANT:PI * (2 * ec - 0.25 * ec ^ 3) * sin(M) + 180 / CONSTANT:PI * (1.25 * ec ^ 2) * sin(2 * M) + 180 / CONSTANT:PI * (13 / 12 * ec ^ 3) * sin(3 * M).
    LOCAL Nu_0 TO 0.
    LOCAL Nu_1 TO 0.

    IF (M > 0) {
        SET Nu_0 TO MAX(0, Nu_m - err).
        SET Nu_1 TO MIN(180, Nu_m + err).
    } ELSE {
        SET Nu_0 TO MAX(-180, Nu_m - err).
        SET Nu_1 TO MIN(0, Nu_m + err).
    }

    LOCAL M_m TO moduloBound(calculateMeanFromTrueAnomaly(Nu_m, Orb), 180, -180).
    UNTIL (ABS(M_m - M) < 0.001) {
        IF (M_m > M) {
            SET Nu_1 TO Nu_m.
            SET Nu_m TO (Nu_0 + Nu_1) / 2.
        } ELSE {
            SET Nu_0 TO Nu_m.
            SET Nu_m TO (Nu_0 + Nu_1) / 2.
        }
        SET M_m TO moduloBound(calculateMeanFromTrueAnomaly(Nu_m, Orb), -180, 180).
    }
    RETURN moduloBound(Nu_m,360).
}
