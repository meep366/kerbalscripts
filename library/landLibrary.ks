@LAZYGLOBAL OFF.

DECLARE FUNCTION deorbitCraft {
    DECLARE PARAMETER targetPeriapsis, updateGui.

    IF (PERIAPSIS >= BODY:ATM:HEIGHT) {
        DECLARE LOCAL deorbitDeltaV TO calculateSemiMajorAxisDeltaV(targetPeriapsis, PERIAPSIS).
        DECLARE LOCAL timeAtPeriapsis TO TIME:SECONDS + ETA:PERIAPSIS.
        DECLARE LOCAL periapsisNode TO NODE(timeAtPeriapsis, 0, 0, deorbitDeltaV).
        ADD periapsisNode.

        executeNode(periapsisNode, updateGui).
    }

    LOCK STEERING TO SRFRETROGRADE.

    DECLARE LOCAL isLanded TO FALSE.
    DECLARE LOCAL startedChutes TO FALSE.

    CLEARSCREEN.
    UNTIL (isLanded) {
        PRINT "Radar Altitude: " + ROUND(ALT:RADAR) + "m    " AT (0, 0).
        PRINT "Vertical Speed: " + ROUND(ABS(VERTICALSPEED)) + "m/s    " AT (0, 1).
        PRINT "Time to Land:   " + ROUND(ABS(ALT:RADAR / VERTICALSPEED)) + "s    " AT (0, 2).

        DECLARE LOCAL statusLexicon TO LEXICON(
                        "Status", "Deorbiting",
                        "Radar Altitude", ROUND(ALT:RADAR) + "m",
                        "Vertical Speed", ROUND(ABS(VERTICALSPEED)) + "m/s",
                        "Time to Land", ROUND(ABS(ALT:RADAR / VERTICALSPEED)) + "s").
        updateGui:CALL(statusLexicon).

        IF (SHIP:STATUS = "LANDED" OR SHIP:STATUS = "SPLASHED") {
            SET isLanded TO TRUE.
        }
        DECLARE LOCAL chutesInStage TO getChutesInStage().
        IF (chutesInStage:LENGTH = 0 AND STAGE:NUMBER > 0) {
            STAGE.
        }
        IF (ALTITUDE < targetPeriapsis AND NOT startedChutes) {
            WHEN (NOT CHUTESSAFE) THEN {
                CHUTESSAFE ON.
                RETURN (NOT CHUTES).
            }
            SET startedChutes TO TRUE.
        }
        IF (ALT:RADAR < 1000) {
            UNLOCK STEERING.
        }
        WAIT .1.
    }
}

DECLARE FUNCTION landCraft {
    DECLARE PARAMETER landingPosition, updateGui.

    landAtCoordinates(landingPosition, updateGui).
    executeNode(NEXTNODE, updateGui).
    landingBurn(updateGui).
}

DECLARE LOCAL FUNCTION getChutesInStage {
    DECLARE LOCAL currentStage TO STAGE:NUMBER - 1.
    DECLARE LOCAL chuteList TO LIST().
    FOR part IN SHIP:PARTS {
        IF (part:HASMODULE("moduleParachute") AND part:STAGE = currentStage) {
            chuteList:ADD(part).
        }
    }
    RETURN chuteList.
}
