DECLARE FUNCTION transferFuel {
    DECLARE PARAMETER requestedDeltaV.
    DECLARE PARAMETER requestedMonopropellantUnits.
    DECLARE PARAMETER shipDryMass.
    DECLARE PARAMETER vacuumIsp.
    DECLARE PARAMETER updateGui.

    DECLARE LOCAL ratioFuelToOxidizer TO .45.
    DECLARE LOCAL ratioOxidizerToFuel TO .55.

    DECLARE LOCAL stationFuelTanks TO SHIP:PARTSTAGGED("stationFuelTank").
    DECLARE LOCAL stationMonoTanks TO SHIP:PARTSTAGGED("stationMonoTank").
    DECLARE LOCAL shipFuelTanks TO SHIP:PARTSTAGGED("shipFuelTank").
    DECLARE LOCAL shipMonoTanks TO SHIP:PARTSTAGGED("shipMonoTank").
    DECLARE LOCAL shipOreTanks TO SHIP:PARTSDUBBEDPATTERN("Holding Tank").

    CLEARSCREEN.

    IF (SHIP:ORE > 0) {
        DECLARE LOCAL stationIsru TO getShipIsru().
        changeIsruState(stationIsru, "start", "monoprop").

        DECLARE maxMono TO getResourceCapacity("Monopropellant").
        SET WARP TO 6.
        WAIT 2.
        SET WARP TO 7.

        UNTIL (SHIP:MONOPROPELLANT >= maxMono - .01 OR SHIP:ORE - .01 < 0) {
            CLEARSCREEN.
            PRINT "Converting Ore to Monopropellant".
            PRINT "Maximum Monopropellant: " + maxMono.
            PRINT "Current Monopropellant: " + SHIP:MONOPROPELLANT.

            DECLARE LOCAL statusLexicon TO LEXICON(
                            "Converting Ore to Monopropellant", "",
                            "Maximum Monopropellant: " + maxMono, "",
                            "Current Monopropellant: " + SHIP:MONOPROPELLANT, "").
            updateGui:CALL(statusLexicon).

            WAIT .1.
        }

        SET WARP TO 0.
        WAIT 2.
        changeIsruState(stationIsru, "stop", "monoprop").

        PRINT "Conversion complete".
        PRINT "Converting remaining ore to fuel".

        changeIsruState(stationIsru, "start", "lf+ox").

        DECLARE maxFuel TO getResourceCapacity("LiquidFuel").
        SET WARP TO 6.
        WAIT 2.
        SET WARP TO 7.

        UNTIL (SHIP:LIQUIDFUEL >= maxFuel - .01 OR SHIP:ORE - .01 < 0) {
            CLEARSCREEN.
            PRINT "Converting Ore to Fuel".
            PRINT "Maximum Fuel: " + maxFuel.
            PRINT "Current Fuel: " + SHIP:LIQUIDFUEL.

            DECLARE LOCAL statusLexicon TO LEXICON(
                            "Converting Ore to Fuel", "",
                            "Maximum Fuel: " + maxFuel, "",
                            "Current Fuel: " + SHIP:LIQUIDFUEL, "").
            updateGui:CALL(statusLexicon).
            WAIT .1.
        }
        SET WARP TO 0.
        WAIT 2.
        changeIsruState(stationIsru, "stop", "lf+ox").
        jettisonExtraOre(shipOreTanks).

        CLEARSCREEN.
        PRINT "All Ore Converted or Jettisoned".
    }
    PRINT "Transfering all fuel to station".
    DECLARE LOCAL statusLexicon TO LEXICON("Transfering all fuel to station", "").
    updateGui:CALL(statusLexicon).

    DECLARE LOCAL monoTransfer TO TRANSFERALL("Monopropellant", shipMonoTanks, stationMonoTanks).
    SET monoTransfer:ACTIVE TO TRUE.

    DECLARE LOCAL fuelTransfer TO TRANSFERALL("LiquidFuel", shipFuelTanks, stationFuelTanks).
    SET fuelTransfer:ACTIVE TO TRUE.

    DECLARE LOCAL oxidizerTransfer TO TRANSFERALL("Oxidizer", shipFuelTanks, stationFuelTanks).
    SET oxidizerTransfer:ACTIVE TO TRUE.

    UNTIL (NOT monoTransfer:ACTIVE AND NOT fuelTransfer:ACTIVE AND NOT oxidizerTransfer:ACTIVE) {
        WAIT .1.
    }

    PRINT "Fuel transfer complete".
    DECLARE LOCAL statusLexicon TO LEXICON("Fuel transfer complete", "").
    updateGui:CALL(statusLexicon).

    DECLARE LOCAL fuelDensity TO 0.
    DECLARE LOCAL monoDensity TO 0.

    FOR resource IN SHIP:RESOURCES {
        IF (resource:NAME = fuelTransfer:RESOURCE) {
            SET fuelDensity TO resource:DENSITY.
        }
        IF (resource:NAME = monoTransfer:RESOURCE) {
            SET monoDensity TO resource:DENSITY.
        }
    }

    DECLARE LOCAL remainingFuel TO 0.
    FOR fuelTank IN shipFuelTanks {
        SET remainingFuel TO remainingFuel + fuelTank:MASS - fuelTank:DRYMASS.
    }

    DECLARE LOCAL remainingMonoMass TO 0.
    FOR monoTank IN shipMonoTanks {
        SET remainingMonoMass TO remainingMonoMass + monoTank:MASS - monoTank:DRYMASS.
    }

    DECLARE LOCAL totalMonopropellentMass TO requestedMonopropellantUnits * monoDensity + remainingMonoMass.
    DECLARE LOCAL totalShipMass TO shipDryMass + totalMonopropellentMass.
    DECLARE LOCAL fuelCalculationExponent TO requestedDeltaV / CONSTANT():g0 / vacuumIsp.
    DECLARE LOCAL totalRequiredFuelMass TO totalShipMass * CONSTANT():E ^ fuelCalculationExponent - totalShipMass.

    DECLARE LOCAL fuelToTransfer TO (totalRequiredFuelMass - remainingFuel) / fuelDensity.
    DECLARE LOCAL liquidFuelToTransfer TO fuelToTransfer * ratioFuelToOxidizer.
    DECLARE LOCAL oxidizerToTransfer TO fuelToTransfer * ratioOxidizerToFuel.
    DECLARE LOCAL monoToTransfer TO requestedMonopropellantUnits - remainingMonoMass / monoDensity.

    SET liquidFuelToTransfer TO getMaxTransferAmount("LiquidFuel", liquidFuelToTransfer, shipFuelTanks, stationFuelTanks).
    SET oxidizerToTransfer TO getMaxTransferAmount("Oxidizer", oxidizerToTransfer, shipFuelTanks, stationFuelTanks).
    SET monoToTransfer TO getMaxTransferAmount("Monopropellant", monoToTransfer, shipMonoTanks, stationMonoTanks).

    IF (fuelToTransfer > 0) {
        PRINT "Transfering " + ROUND(liquidFuelToTransfer, 1) + " liquid fuel to ship".
        PRINT "Transfering " + ROUND(oxidizerToTransfer, 1) + " oxidizer to ship".
        DECLARE LOCAL refuellingFuelTransfer TO TRANSFER("LiquidFuel", stationFuelTanks, shipFuelTanks, liquidFuelToTransfer).
        SET refuellingFuelTransfer:ACTIVE TO TRUE.

        DECLARE LOCAL refuellingOxidizerTransfer TO TRANSFER("Oxidizer", stationFuelTanks, shipFuelTanks, oxidizerToTransfer).
        SET refuellingOxidizerTransfer:ACTIVE TO TRUE.

        untilTransferComplete(refuellingFuelTransfer, liquidFuelToTransfer, shipFuelTanks, updateGui).
        untilTransferComplete(refuellingOxidizerTransfer, oxidizerToTransfer, shipFuelTanks, updateGui).
    }

    IF (monoToTransfer > 0) {
        PRINT "Transfering " + ROUND(monoToTransfer, 1) + " monopropellant to ship".

        DECLARE LOCAL refuellingMonoTransfer TO TRANSFER("Monopropellant", stationMonoTanks, shipMonoTanks, requestedMonopropellantUnits).
        SET refuellingMonoTransfer:ACTIVE TO TRUE.
        untilTransferComplete(refuellingMonoTransfer, monoToTransfer, shipMonoTanks, updateGui).
    }

    IF (fuelToTransfer > 0 OR monoToTransfer > 0) {
        PRINT "Fuel transfer complete".
    }
}

DECLARE FUNCTION mineOre {
    DECLARE PARAMETER updateGui, fillMonoprop TO TRUE.

    RADIATORS ON.
    DEPLOYDRILLS ON.
    WAIT 5.
    DRILLS ON.

    DECLARE LOCAL shipIsru TO getShipIsru().
    IF (fillMonoprop) {
        changeIsruState(shipIsru, "start", "monoprop").
        DECLARE LOCAL maxMono TO getResourceCapacity("Monopropellant").
        SET WARP TO 5.
        WAIT 2.
        SET WARP TO 7.
        UNTIL (SHIP:MONOPROPELLANT >= maxMono) {
            CLEARSCREEN.
            PRINT "Converting Ore to Monopropellant".
            PRINT "Maximum Monopropellant: " + maxMono.
            PRINT "Current Monopropellant: " + SHIP:MONOPROPELLANT.

            DECLARE LOCAL statusLexicon TO LEXICON(
                                "Converting Ore to Monopropellant", "",
                                "Maximum Monopropellant: " + maxMono, "",
                                "Current Monopropellant: " + SHIP:MONOPROPELLANT, "").
            updateGui:CALL(statusLexicon).

            WAIT .1.
        }
        SET WARP TO 0.
        WAIT 2.
        changeIsruState(shipIsru, "stop", "monoprop").
    }

    changeIsruState(shipIsru, "start", "lf+ox").
    DECLARE LOCAL maxFuel TO getResourceCapacity("LiquidFuel").
    SET WARP TO 5.
    WAIT 2.
    SET WARP TO 7.
    UNTIL (SHIP:LIQUIDFUEL >= maxFuel - .01) {
        CLEARSCREEN.
        PRINT "Converting Ore to Fuel".
        PRINT "Maximum Fuel: " + maxFuel.
        PRINT "Current Fuel: " + SHIP:LIQUIDFUEL.
        DECLARE LOCAL statusLexicon TO LEXICON(
                        "Converting Ore to Fuel", "",
                        "Maximum Fuel: " + maxFuel, "",
                        "Current Fuel: " + SHIP:LIQUIDFUEL, "").
        updateGui:CALL(statusLexicon).
        WAIT .1.
    }
    SET WARP TO 0.
    WAIT 2.
    changeIsruState(shipIsru, "stop", "lf+ox").

    DECLARE LOCAL maxOre TO getResourceCapacity("Ore").
    SET WARP TO 5.
    WAIT 2.
    SET WARP TO 7.
    UNTIL (SHIP:ORE >= maxOre) {
        CLEARSCREEN.
        PRINT "Mining Ore".
        PRINT "Maximum Ore: " + maxOre.
        PRINT "Current Ore: " + SHIP:ORE.
        DECLARE LOCAL statusLexicon TO LEXICON(
                        "Mining Ore", "",
                        "Maximum Ore: " + maxOre, "",
                        "Current Ore: " + SHIP:ORE, "").
        updateGui:CALL(statusLexicon).
        WAIT .1.
    }
    SET WARP TO 0.
    WAIT 2.
    RADIATORS OFF.
    DEPLOYDRILLS OFF.
}

DECLARE LOCAL FUNCTION getMaxTransferAmount {
    PARAMETER fuelTypeToTransfer, quantityToTransfer, shipFuelTanks, stationFuelTanks.
    DECLARE LOCAL maxFuel TO quantityToTransfer.

    DECLARE LOCAL shipTotal TO 0.
    FOR resource IN SHIP:RESOURCES {
        IF (resource:NAME = fuelTypeToTransfer) {
            SET shipTotal TO shipTotal + resource:AMOUNT.
        }
    }

    DECLARE LOCAL shipFuelCapacity TO 0.
    FOR fuelTank IN shipFuelTanks {
        FOR resource IN fuelTank:RESOURCES {
            IF (resource:NAME = fuelTypeToTransfer) {
                SET shipFuelCapacity TO shipFuelCapacity + resource:CAPACITY - resource:AMOUNT.
            }
        }
    }

    DECLARE LOCAL stationFuelCapacity TO 0.
    FOR fuelTank IN stationFuelTanks {
        FOR resource IN fuelTank:RESOURCES {
            IF (resource:NAME = fuelTypeToTransfer) {
                SET stationFuelCapacity TO stationFuelCapacity + resource:CAPACITY.
            }
        }
    }

    SET maxFuel TO MIN(maxFuel, shipTotal).
    SET maxFuel TO MIN(maxFuel, shipFuelCapacity).
    SET maxFuel TO MIN(maxFuel, stationFuelCapacity).
    return maxFuel.
}

DECLARE LOCAL FUNCTION untilTransferComplete {
    DECLARE PARAMETER transfer, fuelToTransfer, shipFuelTanks, updateGui.

    DECLARE LOCAL fuelTransfered TO 0.
    DECLARE LOCAL fuelRoundingError TO 2.
    UNTIL (ROUND(fuelTransfered, fuelRoundingError) >= ROUND(fuelToTransfer, fuelRoundingError)
            OR transfer:STATUS = "Finished"
            OR transfer:STATUS = "Failed") {
        SET fuelTransfered TO 0.
        FOR fuelTank IN shipFuelTanks {
            FOR resource IN fuelTank:RESOURCES {
                IF (resource:NAME = transfer:RESOURCE) {
                    SET fuelTransfered TO fuelTransfered + resource:AMOUNT.
                }
            }
        }
        DECLARE LOCAL statusLexicon TO LEXICON("Transfering " + ROUND(fuelToTransfer, 1) + " " + transfer:RESOURCE + " to ship", "").
        updateGui:CALL(statusLexicon).
    }
}

DECLARE LOCAL FUNCTION getResourceCapacity {
    DECLARE PARAMETER fuelType.
    DECLARE maxResource TO 0.
    LOCAL resources is SHIP:RESOURCES.
    LOCAL resourcesIterator is SHIP:RESOURCES:ITERATOR.
    UNTIL (resourcesIterator:NEXT = FALSE) {
        IF (resourcesIterator:VALUE:NAME = fuelType) {
            SET maxResource TO maxResource + resourcesIterator:VALUE:CAPACITY.
        }
    }
    return maxResource.
}

DECLARE LOCAL FUNCTION changeIsruState {
    DECLARE PARAMETER isru, state, fuelType.

    LOCAL moduleIterator TO isru:MODULES:ITERATOR.
    UNTIL (moduleIterator:NEXT = FALSE) {
        IF (moduleIterator:VALUE = "ModuleResourceConverter") {
            DECLARE LOCAL resourceModule TO isru:GETMODULEBYINDEX(moduleIterator:INDEX).
            IF (resourceModule:HASEVENT(state + " isru [" + fuelType + "]")) {
                resourceModule:DOEVENT(state + " isru ["+ fuelType +"]").
            }
        }
    }
}

DECLARE LOCAL FUNCTION jettisonExtraOre {
    DECLARE PARAMETER shipOreTanks.
    FOR oreTank in shipOreTanks {
        LOCAL moduleIterator TO oreTank:MODULES:ITERATOR.
        UNTIL (moduleIterator:NEXT = FALSE) {
            DECLARE LOCAL resourceModule TO oreTank:GETMODULEBYINDEX(moduleIterator:INDEX).
            IF (resourceModule:HASEVENT("Jettison Tank Contents")) {
                resourceModule:DOEVENT("Jettison Tank Contents").
            }
        }
    }
}