@LAZYGLOBAL OFF.

//
//This script boosts a booster back and lands it Falcon 9 style
//If your booster has 9 engines like the Falcon 9, you can set ActionGroup1 to toggle 6 of the outer ones and ActionGroup2 to toggle the remaining 2 outer ones.
//The Gear-ActionGroup should toggle the landing legs.
//

parameter CdTimesA is 25.5, landedaltradar is 32.67, minthrottle is 0.6, arg_landtarget is latlng(-0.2187, -74.5576), droneshipaltradar is 1.95.

//Explanation of the parameters:
//CdTimesA		Aerodynamic value of the booster. Can be determined with the script measure.ks. Instructions are commented in the script.
//landedaltradar	The value alt:radar has when the booster is landed. Can be determined with the script measure.ks.
//minthrottle		The engines are not allowed to throttle below this value. For the Merlin 1D engines on the Falcon 9 rocket this value is 60% (0.6).
//arg_landtarget		The geocoordinates where the booster should land. Popular locations are:
//			- Spacecenter grass:	latlng(-0.2187, -74.5732)
//			- Launchpad:		latlng(-0.0972, -74.5576)
//			- Island Airfield:	latlng(-1.52, -71.9)
//			- Dessert Launch Site:	latlng(-6.5604, -143.95)
//droneshipaltradar	If you want to land on a droneship you need to set this value and have the droneship set as target, else this variable gets ignored. You can determine this value with the script measure.ks. Instructions are in the script itself.

local runmode is 39.
local throttpid is pidloop(0.05, 0.001, 0.7).
//kOS's steeringmanager has to be more aggressive a booster needs to move quickly even though it is very heavy
set steeringmanager:maxstoppingtime to 5.
set steeringmanager:pitchpid:kd to 1.
set steeringmanager:yawpid:kd to 1.
//this script requires a lot of processing
set config:ipu to 200. //TODO: set this higher and make the transition from runmode 42 to 44 work with it
//the movepid has to get more aggressive as the booster gets closer to the ground
local movekp_start is 15.
local moveki_start is 3.
local movekd_start is 0.
local movepid_startalt is 30000.
local movekp_final is 140.
local moveki_final is 0.
local movekd_final is 20.
local movepid_steps is 4. //in how many steps the movepid goes from the start to the final values
local movepid_curstep is 0.
local limit is 0.3.
local movelatpid is pidloop(movekp_start, moveki_start, movekd_start, -limit, limit).
local movelngpid is pidloop(movekp_start, moveki_start, movekd_start, -limit, limit).
local movepidhasreset is false.

local fulloffset is landedaltradar.
local lndtarget is arg_landtarget.
if hastarget {
	set arg_landtarget to target.
}
if arg_landtarget:istype("Vessel") {
	set lndtarget to arg_landtarget:geoposition.
	set fulloffset to landedaltradar + droneshipaltradar + arg_landtarget:altitude-max(lndtarget:terrainheight,0).
}

runpath("0:/flightstats.ks").
runpath("0:/predict.ks"). //also uses mathfunctions.ks
clearscreen.

setarrow("impact", { return curlandcoord:altitudeposition(max(0,curlandcoord:terrainheight)). }).
setarrow("target", targetvec@).

until runmode = 0 {
	set flightstats["runmode"] to runmode.
	set flightstats["decell time"] to round(decelltime()) + " s".
	set flightstats["throttle"] to round(throttle*100) + " %".
	set flightstats["minthrottle"] to round(minthrottle*100) + " %".
	set flightstats["landcoordspeed"] to round(landcoordspeed) + " m/s".
	set flightstats["distance"] to round((ship:geoposition:position - lndtarget:position):mag) + " m".
	printflightstats().
	refresharrows().

	//evaluate the landing spot to start steering
	if runmode = 39 {
		sas off.
		rcs on.

		//run twice so that landcoordspeed has a value
		predictlandcoord("precise", CdTimesA, fulloffset).
		predictlandcoord("precise", CdTimesA, fulloffset).
		local distance is (curlandcoord:position - lndtarget:position):mag.
		set flightstats["predicted distance"] to round(distance) + " m".
		if distance > 1000 {
			set runmode to 40.
			local tardirection is boostbackdirection(curlandcoord, lndtarget).
			lock steering to up.
			wait until vang(ship:facing:vector, up:vector) < 20.
			lock steering to tardirection.
			wait until vang(ship:facing:vector, tardirection:vector) < 60.
		} else {
			set runmode to 50.
		}
	//general boost back
	} else if runmode = 40 {
		sas off.
		rcs on.
		ag1 on.
		ag2 off.
		local tardirection is boostbackdirection(curlandcoord, lndtarget).
		local distance is (curlandcoord:position - lndtarget:position):mag.
		set flightstats["predicted distance"] to round(distance) + " m".

		if vang(ship:facing:vector, tardirection:vector) < 60 {
			lock throttle to 1.
			predictlandcoord("precise", CdTimesA, fulloffset).
			set tardirection to boostbackdirection(curlandcoord, lndtarget).
			lock steering to tardirection.
		} else {
			lock throttle to 0.
		}
		
		//throttle down when the target will be reached within the next 7 landcoord calculations
		if landcoordspeed * 7 * landcoordevalduration > distance {
			set runmode to 42.
		}
	//downthrottled boost back
	} else if runmode = 42 {
		sas off.
		rcs on.
		ag1 on.
		ag2 on.
		local tardirection is boostbackdirection(curlandcoord, lndtarget).
		local distance is (curlandcoord:position - lndtarget:position):mag.
		set flightstats["predicted distance"] to round(distance) + " m".

		if vang(ship:facing:vector, tardirection:vector) < 10 {
			lock throttle to max(minthrottle, 0.1).
			predictlandcoord("precise", CdTimesA, fulloffset).
			set tardirection to boostbackdirection(curlandcoord, lndtarget).
			lock steering to tardirection.
		} else {
			lock throttle to 0.
		}

		//start the final estimation when the target will be reached within the next 3 landcoord calculations
		if landcoordspeed * 3 * landcoordevalduration > distance {
			set runmode to 44.
		}
		if distance < 400 {
			set runmode to 50.
		}
	//estimate the last meters of the boostback
	} else if runmode = 44 {
		sas off.
		rcs on.
		ag1 on.
		ag2 on.
		lock throttle to max(minthrottle, 0.1).
	
		local distance is (curlandcoord:position - lndtarget:position):mag.
		
		//don't do any more time costly calculations of the landing coordinates
		//calculate when to stop based on the speed with which the landing coordinates moved before
		if landcoordspeed * (time:seconds - landcoordevaltime) > distance {
			lock throttle to 0.
			set runmode to 50.
			//do one final prediction to update the arrow and distance
			predictlandcoord("precise", CdTimesA, fulloffset).
			set distance to (curlandcoord:position - lndtarget:position):mag.
		}
		set flightstats["predicted distance"] to round(distance) + " m".
	//coast
	} else if runmode = 50 {
		rcs on.
		sas off.
		lock throttle to 0.
		//align to reentry angle while above the atmosphere
		if ship:verticalspeed > 0 or ship:altitude > 70000 {
			local posveltimeatreentry is posveltimeataltitude(min(70000,ship:orbit:apoapsis)).
			lock steering to -(posveltimeatreentry[1] - vcrs(ship:body:angularvel, posveltimeatreentry[0])).
		//fly retrograde through reentry
		} else {
			lock steering to -ship:velocity:surface.
		}

		//TODO: use better criterea for when reentry heat appears and not just "after a pressure of 0.002 atm"
		if ship:verticalspeed < 0 and ship:body:atm:altitudepressure(ship:altitude) > 0.002 {
			predictlandcoord("precise", CdTimesA, fulloffset).
			set runmode to 52.
		}
	//reentry burn
	} else if runmode = 52 {
		sas off.
		rcs on.
		//ag2 on.
	
		predictlandcoord("precise", CdTimesA, fulloffset).
		local distance is (curlandcoord:position - lndtarget:position):mag.
		set flightstats["predicted distance"] to round(distance) + " m".
		lock throttle to 1.

		//steering
		set movelatpid:setpoint to 0.
		local deltalat is curlandcoord:lat - lndtarget:lat.
		local curlatlean is movelatpid:update(time:seconds, deltalat).
		set movelngpid:setpoint to 0.
		local deltalng is curlandcoord:lng - lndtarget:lng.
		local curlnglean is movelngpid:update(time:seconds, deltalng).
		lock steering to curlatlean*heading(0, 0):vector + curlnglean*heading(90, 0):vector + (up:vector-ship:velocity:surface):normalized.
		set flightstats["deltalat"] to round(deltalat, 4) + "°".
		set flightstats["deltalng"] to round(deltalng, 4) + "°".
		set flightstats["latlean"] to round(curlatlean, 2).
		set flightstats["lnglean"] to round(curlnglean, 2).
		wait 0.001.

		//TODO: better criterea for when to stop reentry burn
		if ship:velocity:surface:mag < 850 {
			lock throttle to 0.
			set runmode to 56.
		}
	//coast aerodynamically
	} else if runmode = 56 {
		rcs on.
		sas off.
		lock throttle to 0.
		local hoverslamthr is hoverslamthrot().
		set flightstats["hoverslam thr"] to round(hoverslamthr * 100) + " %".
		//the term 0.2*(alt:radar-fulloffset) instead of just fulloffset is used to get a smoother transition to vertical flying
		predictlandcoord("kepler", 0, 0.2*(alt:radar-fulloffset)).
		local distance is (curlandcoord:position - lndtarget:position):mag.
		set flightstats["predicted distance"] to round(distance) + " m".

		//steering
		set movelatpid:setpoint to 0.
		set movelatpid:ki to 2.
		local deltalat is curlandcoord:lat - lndtarget:lat.
		local curlatlean is movelatpid:update(time:seconds, deltalat).
		set movelngpid:setpoint to 0.
		local deltalng is curlandcoord:lng - lndtarget:lng.
		local curlnglean is movelngpid:update(time:seconds, deltalng).
		lock steering to -curlatlean*heading(0, 0):vector -curlnglean*heading(90, 0):vector -ship:velocity:surface:normalized.
		set flightstats["deltalat"] to round(deltalat, 4) + "°".
		set flightstats["deltalng"] to round(deltalng, 4) + "°".
		set flightstats["latlean"] to round(curlatlean, 2).
		set flightstats["lnglean"] to round(curlnglean, 2).
		wait 0.001.

		//the pid controller has to get more aggressive as we get closer to the ground
		checkandsetmovepid().

		if hoverslamthr > (minthrottle+3)/4 {
                        lock throttle to hoverslamthr.
                        set runmode to 60.

			//reset the droneshipcoordinates to land on the CoM and not on the root part
			if hastarget {
				set lndtarget to target:geoposition.
			}
                }
        //hoverslam
	} else if runmode = 60 {
		rcs on.
		sas off.
		local hoverslamthr is hoverslamthrot().
		set flightstats["hoverslam thr"] to round(hoverslamthr * 100) + " %".

		local deltaT is decelltime().
		//steering
		if deltaT > 5 {
			predictlandcoord("kepler", 0, 0.2*(alt:radar-fulloffset)).
			local distance is (curlandcoord:position - lndtarget:position):mag.
			set flightstats["predicted distance"] to round(distance) + " m".

			set movelatpid:setpoint to 0.
			local deltalat is curlandcoord:lat - lndtarget:lat.
			local curlatlean is movelatpid:update(time:seconds, deltalat).
			set movelngpid:setpoint to 0.
			local deltalng is curlandcoord:lng - lndtarget:lng.
			local curlnglean is movelngpid:update(time:seconds, deltalng).
			//if aerodynamics are more important than the engine
			//TODO: better criterea for when aerodynamics have more effect than engine thrust
			if ship:velocity:surface:mag > 220 {
				lock steering to -curlatlean*heading(0, 0):vector -curlnglean*heading(90, 0):vector -ship:velocity:surface:normalized.
			} else {
				lock steering to curlatlean*heading(0, 0):vector + curlnglean*heading(90, 0):vector + (up:vector-ship:velocity:surface):normalized.
			}
			set flightstats["deltalat"] to round(deltalat, 4) + "°".
			set flightstats["deltalng"] to round(deltalng, 4) + "°".
			set flightstats["latlean"] to round(curlatlean, 2).
			set flightstats["lnglean"] to round(curlnglean, 2).
		} else if deltaT > 2 {
			lock steering to -ship:velocity:surface.
		} else { //right before touchdown, look straight up
			lock steering to lookdirup(up:vector, ship:facing:topvector).
		}

		//the pid controller has to get more aggressive as we get closer to the ground
		checkandsetmovepid().

		//gear
		if deltaT > 10 {
			gear off.
		} else {
			gear on.
		}

		//throttle
		local newthr is throttle.
		set throttpid:setpoint to 1-(minthrottle+3)/4.
		set newthr to newthr + throttpid:update(time:seconds, 1-hoverslamthr).
		lock throttle to valueInRange(newthr, minthrottle, 1).
		wait 0.001.

		if ship:verticalspeed > 0 {
			lock throttle to 0.
			//sas on.
			set runmode to 70.
		}	
	//don't fall over
	} else if runmode = 70 {
		lock steering to lookdirup(up:vector, ship:facing:topvector).
	}
}

declare function resetmovepid {
	parameter movekp, moveki, movekd, limit.
	set movelatpid to pidloop(movekp, moveki, movekd, -limit, limit).
	set movelngpid to pidloop(movekp, moveki, movekd, -limit, limit).
}

declare function checkandsetmovepid {
	local targetalt is (targetvec()-ship:body:position):mag - ship:body:radius.
	if (ship:altitude-targetalt)/(movepid_startalt-targetalt) * (movepid_steps+1) < movepid_steps-movepid_curstep {
		set movepid_curstep to movepid_curstep+1.
		resetmovepid(movekp_start+(movekp_final-movekp_start)/(movepid_steps)*movepid_curstep,
			moveki_start+(moveki_final-moveki_start)/(movepid_steps)*movepid_curstep,
			movekd_start+(movekd_final-movekd_start)/(movepid_steps)*movepid_curstep,
			limit).
	}
}

declare function targetvec {
	parameter geotarget is lndtarget, targetoffset is droneshipaltradar.
	local targetalt is 0.
	if hastarget {
		set targetalt to target:altitude + droneshipaltradar.
	} else {
		set targetalt to max(lndtarget:terrainheight, 0).
	}
	return geotarget:altitudeposition(targetalt).
}

declare function boostbackdirection {
	parameter curlandcoord, targetlandcoord.
	return heading(90 - arctan2(targetlandcoord:lat-curlandcoord:lat, targetlandcoord:lng-curlandcoord:lng), 0).
	//keep old code for now, just in case arctan2 doesn't work the way I hope it does
	//TODO: Add case for overflow:
	//the longitudes 179° and -179° are only 2° apart
	//May be not necessary because kOS may know this
	local dlat is targetlandcoord:lat - curlandcoord:lat.
	local dlng is targetlandcoord:lng - curlandcoord:lng.
	//positive angle between eastwards and target
	local alpha is arccos(dlng/sqrt(dlat*dlat+dlng*dlng)).
	if dlat < 0 { set alpha to 360-alpha. }
	return heading(90 - alpha, 0).
}

declare function hoverslamthrot {
	local targetaccres is (ship:velocity:surface * ship:up:vector) / (2*(alt:radar - fulloffset)) * ship:velocity:surface.
	local neededengineacc is targetaccres - gravitacc() - coriolisacc() - centrifugalacc() - dragforce(CdTimesA, 0.5*ship:altitude, 0.5*ship:velocity:surface)/ship:mass.
	return (neededengineacc:mag * ship:mass)/ship:availablethrust.
}

declare function decelltime {
	return 2*(fulloffset - alt:radar)/(ship:velocity:surface * ship:up:vector).
}
