@LAZYGLOBAL OFF.
global flightstats is lexicon().
global arrows is lexicon(). //the value is triple of the arrow, the getvec-function, and the getstartvec-function

declare function printflightstats {
        parameter flightstatstoprint is flightstats.
        local col0length is 0.
        local col1length is 0.
        for key in flightstats:keys {
                if (key+""):length > col0length {
                        set col0length to (key+""):length.
                }
        }
        for value in flightstats:values {
                if (value+""):length > col1length {
                        set col1length to (value+""):length.
                }
        }
        set col0length to col0length + 2. //because of ': '
        local row is 10.
        //print it
	for key in flightstats:keys {
		print ((key + ":"):padright(col0length) + 
(flightstats[key]+""):padleft(col1length)):padright(terminal:width) at (0, 
row).
		set row to row + 1.
	}
}

declare function setarrow {
	parameter name, getvec, factor is 1, getstartvec is { return v(0,0,0). }.
	set arrows[name] to list(vecdraw(getstartvec(), getvec(), white, name, factor, true, 0.2), getvec, getstartvec).
}

declare function removearrow {
	parameter name.
	set arrows[name][0]:show to false.
	arrows:remove(name).
}

declare function refresharrows {
	for arrow_getvec_getstartvec in arrows:values {
		set arrow_getvec_getstartvec[0]:start to arrow_getvec_getstartvec[2]().
		set arrow_getvec_getstartvec[0]:vec to arrow_getvec_getstartvec[1]().
		set arrow_getvec_getstartvec[0]:show to true.
	}
}
