@LAZYGLOBAL OFF.

declare function convertPosvecToGeocoord {
	parameter posvec.
	//sphere coordinates relative to xyz-coordinates
	local lat is 90 - vang(v(0,1,0), posvec).
	//circle coordinates relative to xz-coordinates
	local equatvec is v(posvec:x, 0, posvec:z).
	local phi is vang(v(1,0,0), equatvec).
	if equatvec:z < 0 {
		set phi to 360 - phi.
	}
	//angle between x-axis and geocoordinates
	local alpha is vang(v(1,0,0), latlng(0,0):position - ship:body:position).
	if (latlng(0,0):position - ship:body:position):z >= 0 {
		set alpha to 360 - alpha.
	}
	return latlng(lat, phi + alpha).
}

//if value is outside of the range it gets set to the boundary
declare function valueInRange {
	parameter value, lowerbound, upperbound.
	if value > upperbound {
		set value to upperbound.
	} else if value < lowerbound {
		set value to lowerbound.
	}
	return value.
}
